import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/pages.dart';
import 'package:eccount/services_binder.dart';
import 'package:eccount/shared/theme.dart';
import 'package:eccount/translations.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  if(kIsWeb){
    await FirebaseAuth.instance.setPersistence(Persistence.LOCAL);
  }
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final botToastBuilder=BotToastInit();
    return GetMaterialApp(
      builder: (context, child){
        child=ResponsiveWrapper.builder(
            ClampingScrollWrapper.builder(context, GestureDetector(child: child,
              onTap: (){
                if(!FocusScope.of(context).hasPrimaryFocus){
                  FocusScope.of(context).unfocus();
                }
              },)),
            minWidth: 480,
            defaultScale: true,
            defaultName: MOBILE,
            backgroundColor: Colors.grey,
            breakpoints: [
              ResponsiveBreakpoint.resize(480, name: MOBILE),
              ResponsiveBreakpoint.resize(600, name: MOBILE),
              ResponsiveBreakpoint.resize(850, name: TABLET),
              ResponsiveBreakpoint.resize(1080, name: DESKTOP),
            ],
            background: Container(color: Color(0xFFF5F5F5)));
        child=botToastBuilder(context,child);
        child=FlutterSmartDialog(child: child);
        return child;
      },
      initialBinding: ServicesBinder(),
      navigatorObservers: [BotToastNavigatorObserver()],
      locale: Locale('en','US'),
      getPages: pages,
      theme: AppThemes.lightTheme,
      fallbackLocale: Locale('en', 'US'),
      translations: AppTranslations(),
      initialRoute: '/splash',
      smartManagement: SmartManagement.keepFactory,
      debugShowCheckedModeBanner: false,
      defaultTransition: Transition.fadeIn,
    );
  }
}
