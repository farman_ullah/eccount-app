

import 'package:eccount/core/controllers/orders_controller.dart';
import 'package:eccount/core/controllers/payment_controller.dart';
import 'package:eccount/core/services/auth_service.dart';
import 'package:eccount/core/services/firestorage_service.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/core/services/multimedia_service.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/splash_screen/splashscreen_viewmodel.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/buisnessnameRegistration_controller.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/verificaiton_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/myfillings/myfillings_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/orders/orders_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/search/searchview_controller.dart';
import 'package:get/instance_manager.dart';

class ServicesBinder extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => FirestoreService());
    Get.lazyPut(() => MultiMediaService());
    Get.lazyPut(() => AuthService());
    Get.put(SplashScreenViewModel());
    Get.lazyPut(() => FireStorageService());
    Get.put(PaymentsController());
    Get.put(OrderController());
    Get.put(UserController());
    Get.put(WelcomeScreenController());
    Get.put(VerificationController());
    Get.put(CompanyFormationViewModel());
    Get.put(SearchViewController());
    Get.put(MyFillingsController());
    Get.put(OrdersController());
    Get.put(BuisnessNameRegController());



  }

}