import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../registrations/registration_viewmodel.dart';

class SecretaryAddViewOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AutoSizeText("Secretary Detail",style: TextStyles.h2,),
          SizedBox(height: 20,),
          StyledInfoWidget(child: AutoSizeText("Please give details below of the persons who have consented in writing to become directors.",),text: CompanyFormation.noteSeven,),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.surName,
            labelText: "Surname",
            hintText: "Enter surname",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.surName=val;
            },
            validate: (val){
              if(val.isEmpty){
                return "Surname is required";
              }
              return null;
            },
          ),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.foreName,
            labelText: "Forename",
            hintText: "Enter forename",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.foreName=val;
            },
            validate: (val){
              if(val.isEmpty){
                return "Forename is required";
              }
              return null;
            },
            suffix: GestureDetector(
                onTap: (){
                  Show.showBottomSheet(CompanyFormation.noteSeven);
                },
                child: Icon(Icons.info_outline,size: 24,color: primaryColor,)),

          ),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.formerSurname,
            labelText: "Former Surname",
            hintText: "Enter former surname",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.formerSurname=val;
            },
          ),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.formerForename,
            labelText: "Former Forename",
            hintText: "Enter former forename",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.formerForename=val;
            },
            suffix: GestureDetector(
                onTap: (){
                  Show.showBottomSheet(CompanyFormation.noteEight);
                },
                child: Icon(Icons.info_outline,size: 24,color: primaryColor,)),

          ),
          SizedBox(height: 20,),
          StyledTextFormField(
            controller: _.secretaryDOBController,
            labelText: "Date of birth",
            hintText: "Enter date of birth",
            readOnly: true,
            onTapped: _.selectDate,
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            validate: (val){
              if(val.isEmpty){
                return "Date of birth is required";
              }
              return null;
            },
          ),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                if(registrationViewModel.registrationFormKey.currentState.validate()){
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                }
              },
            ),
          )
        ],
      );
    });
  }
}
