import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_signature_view.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SecretaryAddViewThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AutoSizeText("Secretary Detail",style: TextStyles.h2,),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.postCode,
            labelText: "Postcode",
            hintText: "Enter postcode",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.postCode=val;
            },
          ),
          SizedBox(height: 20,),
          AutoSizeText("I/we hereby consent to act as secretary of the aforementioned company and I/we acknowledge that as secretary I/we have legal duties and obligations imposed by the Companies Act, other statutes and at common law."),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AutoSizeText("Signature",style: TextStyles.h3,),
              IconButton(icon: Icon(Icons.add,color: primaryColor,), onPressed: ()async{
                var data=await Get.to(()=>SignatureView(false));
                if(data!=null){
                  _.companyFormation.secretaryDetails.signatureImage=data;
                  _.update();
                }
              })
            ],),
          SizedBox(height: 20,),
          _.companyFormation.secretaryDetails.signatureImage!=null?CachedNetworkImage(imageUrl:_.companyFormation.secretaryDetails.signatureImage,height: Get.height*0.15,width: Get.width,):SizedBox(),
          SizedBox(height:_.companyFormation.secretaryDetails.signatureImage!=null?20:0,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                if(_.companyFormation.secretaryDetails.signatureImage==null){
                  Show.showErrorSnackBar(title: "Error", error: "Please add your signature to continue");
                  return;
                }
                final registrationViewModel=Get.find<RegistrationViewModel>();
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
              },
            ),
          )
        ],
      );
    });
  }
}
