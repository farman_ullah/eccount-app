import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SecretaryAddViewTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AutoSizeText("Secretary Detail",style: TextStyles.h2,),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.numberOfBodyCorporate,
            labelText: "Number of Body Corporate",
            hintText: "Enter number of body corporate",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.numberOfBodyCorporate=val;
            },
          ),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.bodyCorporateName,
            labelText: "Body Corporate Name",
            hintText: "Enter body corporate name",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.bodyCorporateName=val;
            },
          ),
          SizedBox(height: 20,),
          AutoSizeText("Name of Register where Body Corporate registered (if applicable)",),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.nameOfRegister,
            labelText: "Name of register",
            hintText: "Enter register where body corporate registered",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.nameOfRegister=val;
            },
          ),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.secretaryDetails.residentialAddress,
            labelText: "Residential Address",
            hintText: "Enter residential address",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.secretaryDetails.residentialAddress=val;
            },
            validate: (val){
              if(val.isEmpty){
                return "Forename is required";
              }
              return null;
            },
            suffix: GestureDetector(
                onTap: (){
                  Show.showBottomSheet(CompanyFormation.noteSeven);
                },
                child: Icon(Icons.info_outline,size: 24,color: primaryColor,)),

          ),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                if(registrationViewModel.registrationFormKey.currentState.validate()){
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                }
              },
            ),
          )
        ],
      );
    });
  }
}
