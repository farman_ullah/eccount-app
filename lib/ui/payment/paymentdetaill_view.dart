import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/payment_controller.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/models/order_model.dart';
import 'package:eccount/models/payment_model.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class PaymentDetailView extends StatefulWidget {
  final OrderModel order;
  PaymentDetailView({this.order});
  @override
  _PaymentDetailViewState createState() => _PaymentDetailViewState();
}

class _PaymentDetailViewState extends State<PaymentDetailView> {

  String cardId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        title: AutoSizeText("Payment Detail",style: TextStyles.title1.copyWith(color: Colors.black),),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: Get.width*0.05),
        child: Column(
          children: [
            SizedBox(height: Get.height*0.02,),
            AutoSizeText("Order Info",textAlign: TextAlign.center,style: TextStyles.h2,),
            SizedBox(height: Get.height*0.02,),
            Card(
              elevation: 10,
              child: Padding(
                padding: EdgeInsets.all(Get.width*0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    AutoSizeText("Order Type",style: TextStyles.body1,),
                    AutoSizeText(getRegistrationTypeStringFromInt(widget.order.dataType),style: TextStyles.h2,),
                    SizedBox(height: Get.height*0.02,),
                    AutoSizeText("Total Amount",style: TextStyles.body1,),
                    AutoSizeText("100\$"??"",style: TextStyles.h2,),
                  ],
                ),
              ),
            ),
            SizedBox(height: Get.height*0.02,),
            Row(children: [
              Expanded(
                child:
                AutoSizeText("Cards",style: TextStyles.h2,),
              ),
              SizedBox(width: 5,),
              RippleButton(onPressed: ()async{
                await Get.find<PaymentsController>().createPaymentMethod();
              },
                title: "Add Card",
                titleColor: Colors.purple,
                borderRadius: BorderRadius.circular(20),
                borderColor: Colors.purple,
                border: true,
                height: Get.height*0.06,)
            ],),
            SizedBox(height: Get.height*0.02,),
            Obx((){
              return Get.find<PaymentsController>().cards.length==0?AutoSizeText("Add a card to select"):DropdownButtonFormField(
                  hint: AutoSizeText("Select a card"),
                  value: cardId,
                  onChanged: (String id){
                    setState(() {
                      cardId=id;
                    });
                  },
                  items: Get.find<PaymentsController>().cards.map((element) => DropdownMenuItem(child: AutoSizeText("${element.brand} **** ${element.last4} | ${element.expMonth}/${element.expYear}",style: TextStyles.h2,),value: element.id,)).toList());
            }),
            SizedBox(height: Get.height*0.02,),
            Align(
              alignment: Alignment.center,
              child: RippleButton(
                backGroundColor: primaryColor,
                title: "Pay",
                titleColor: Colors.white,
                fontSize: FontSizes.s20,
                borderRadius: BorderRadius.circular(10),
                shadowColor: Colors.purple,
                addShadow: true,
                elevation: 6,
                onPressed: ()async{
                    if(cardId==null){
                      Show.showErrorSnackBar(title: "Select Card", error: "Please select a card to continue");
                      return;
                    }
                    else{
                       try{
                         Show.showLoader();
                         String id = Get.find<PaymentsController>().cards.firstWhere((element) => element.id==cardId).paymentId;
                         if(id==null){
                           return ;
                         }
                         PaymentModel model = PaymentModel(paymentMethodId:id ,amount: 10000,currency: "usd",status: "Initiated by client",orderId: widget.order.id);
                         await Get.find<FirestoreService>().addUserPayment(Get.find<UserController>().currentUser.value.id, model);
                         Get.back();
                         Get.back();
                         Show.showSnackBar(title: "Message", message: "Please wait while your payment is processed");

                       }
                       catch(e){
                         Show.showErrorSnackBar(title: "Error", error: e);
                       }
                    }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

