import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_progress_indicatior.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/ui/agreement/agreement_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AgreementView extends StatelessWidget {
  final RegistrationType registrationType;
  AgreementView({this.registrationType});
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AgreementViewModel>(
        init: AgreementViewModel(registrationType: registrationType),
        builder: (model){
      return Scaffold(
        body: model.agreementList==null?Center(child: AppProgressIndication(),):Padding(
          padding: EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).scaledWidth*0.05,vertical: ResponsiveWrapper.of(context).scaledHeight*0.01),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: kToolbarHeight,),
                      AutoSizeText("Before you get started",style: TextStyles.h2,),
                      SizedBox(height: 15,),
                      AutoSizeText("Please follow these guidelines to avoid any rejection.",style: TextStyles.body1.copyWith(fontSize: FontSizes.s16),),
                      SizedBox(height: 20,),
                      Card(
                        elevation: 12,
                        child: Padding(
                          padding: EdgeInsets.all(ResponsiveWrapper.of(context).scaledWidth*0.05),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AutoSizeText(model.agreementList[model.index].title,style: TextStyles.h2,),
                              SizedBox(height: 20,),
                              AutoSizeText(model.agreementList[model.index].subTitle,style: TextStyles.body1.copyWith(fontSize: FontSizes.s16),),],
                          ),
                        ),
                      )
                    ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                model.index==0?SizedBox():GestureDetector(
                  onTap: model.decrementIndex,
                  child: Card(
                    elevation: 12,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                    child: Container(
                        padding: EdgeInsets.all(20),
                        child: Icon(Icons.arrow_back_ios,color: Colors.black,)),),
                ),
                GestureDetector(
                  onTap: model.incrementIndex,
                  child: Card(
                    elevation: 12,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                    child: Container(
                        padding: EdgeInsets.all(20),
                        child: model.index==model.agreementList.length-1?AutoSizeText("Continue",style: TextStyles.h3,):Icon(Icons.arrow_forward_ios,color: Colors.black,)),),
                )
              ],)
            ],
          ),
        ),
      );
    });
  }
}
