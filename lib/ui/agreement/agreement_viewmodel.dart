
import 'package:eccount/models/agreement_single_view.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:get/get.dart';

class AgreementViewModel extends GetxController{
  AgreementViewModel({this.registrationType});
  final RegistrationType registrationType;
  List<AgreementSingleView> agreementList;

  int index=0;

  decrementIndex(){
    if(index!=0){
      index-=1;
      update();
    }
  }

  incrementIndex(){
    if(index==agreementList.length-1){
      print("coming here");
      Get.to(()=>RegistrationMain(registrationType: registrationType),preventDuplicates: false);
      return;
    }
    index+=1;
    update();
  }

  getViewsAccordingToRegistrationType(){
    switch(registrationType){
      case RegistrationType.CompanyFormation:
        agreementList=List.from(CompanyFormation.agreementsList);
        return;
      case RegistrationType.BusinessNameRegistration:
        agreementList=[AgreementSingleView(title: "Coming Soon",subTitle: "")];
        return;
      case RegistrationType.VatRegistration:
        agreementList=[AgreementSingleView(title: "Coming Soon",subTitle: "")];
        return;
      default:
        return;
    }
  }


  @override
  void onInit() {
    getViewsAccordingToRegistrationType();
    super.onInit();
  }
}