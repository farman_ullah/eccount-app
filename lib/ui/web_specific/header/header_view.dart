import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/styled_widgets/styled_button.dart';
import 'package:eccount/styled_widgets/styled_icon.dart';
import 'package:eccount/ui/web_specific/homepage/about_us/aboutus_view.dart';
import 'package:eccount/ui/web_specific/homepage/article/articles.dart';
import 'package:eccount/ui/web_specific/homepage/prices/prices_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as getx;
import 'package:get/get_core/src/get_main.dart';
import 'package:responsive_framework/responsive_framework.dart';

class HeaderView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: 70,
      elevation: 10,
      pinned: true,
      collapsedHeight: kToolbarHeight+10,
      flexibleSpace: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: ResponsiveValue<double>(
            context,defaultValue: 100,
            valueWhen: [
              Condition.smallerThan(name: DESKTOP,value: 20)
            ]
        ).value,vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            StyledIcon(),
            Spacer(),
            ResponsiveVisibility(
                visible: false,
                visibleWhen: [Condition.largerThan(name: MOBILE)],
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextButton(onPressed: (){



                  },child: AutoSizeText("SERVICES",)),
                )),
            ResponsiveVisibility(
                visible: false,
                visibleWhen: [Condition.largerThan(name: MOBILE)],
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextButton(onPressed: (){
                    Get.to(()=>Prices());

                  },child: AutoSizeText("PRICING",)),
                )),
            ResponsiveVisibility(
                visible: false,
                visibleWhen: [Condition.largerThan(name: MOBILE)],
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextButton(onPressed: (){},child: AutoSizeText("SUPPORT",)),
                )),
            ResponsiveVisibility(
                visible: false,
                visibleWhen: [Condition.largerThan(name: MOBILE)],
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextButton(onPressed: (){
                    Get.to(()=>Articles(),transition:getx.Transition.rightToLeft,duration: Duration(seconds: 1) );


                  },child: AutoSizeText("ARTICLES",)),
                )),
            ResponsiveVisibility(
                visible: false,
                visibleWhen: [Condition.largerThan(name: MOBILE)],
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextButton(onPressed: (){

                  Get.to(()=>AboutView(),transition: getx.Transition.leftToRight,duration: Duration(seconds: 1));

                  },child: AutoSizeText("ABOUT US",)),
                )),
            ResponsiveVisibility(
                visible: false,
                visibleWhen: [Condition.largerThan(name: MOBILE)],
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child:TextButton.icon(onPressed: (){
                    getx.Get.toNamed("/login");
                  },icon: Icon(Icons.login),label: AutoSizeText("LOG IN"),),
                )),
            ResponsiveVisibility(
              visible: false,
              visibleWhen: [Condition.largerThan(name: MOBILE)],
              child: Padding(
                padding: EdgeInsets.only(left: 8, right: 0),
                child: StyledButton("START A COMPANY",(){},EdgeInsets.symmetric(vertical: 20,horizontal: 25)),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
