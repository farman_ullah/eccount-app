import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_icon.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/auth/auth_viewmodel.dart';
import 'package:eccount/ui/web_specific/auth/signup_view.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as GetX;
import 'package:responsive_framework/responsive_framework.dart';


class WebLoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Positioned.fill(
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [Colors.blue[200],Colors.blue[900],Colors.purpleAccent],begin: Alignment.bottomLeft,end: Alignment.topRight,stops: [0.3,0.5,1])
                ),
              )),
          Center(
            child: GetX.GetBuilder<AuthViewModel>(builder: (controller){
              return ResponsiveConstraints(
                constraintsWhen: [
                  Condition.equals(name: MOBILE, value: BoxConstraints(maxWidth: 500)),
                  Condition.equals(name: TABLET, value: BoxConstraints(maxWidth: 600)),
                  Condition.largerThan(name: TABLET, value: BoxConstraints(maxWidth: 600)),
                ],
                child: SingleChildScrollView(
                  child: Form(
                    key: controller.loginFormKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            height: GetX.Get.height*0.08,
                            child: StyledIcon()),
                        SizedBox(height: GetX.Get.height*0.03,),
                        ResponsiveConstraints(
                          constraintsWhen: [
                            Condition.equals(name: MOBILE, value: BoxConstraints(maxWidth: 500,minWidth: 500)),
                            Condition.equals(name: TABLET, value: BoxConstraints(maxWidth: 600,minWidth: 600)),
                            Condition.largerThan(name: TABLET, value: BoxConstraints(maxWidth: 600,minWidth: 600)),
                          ],
                          child: Card(
                            elevation: 10,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 60,vertical: 50),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 30),
                                    child: AutoSizeText("Sign in to your account",style: TextStyles.h1.copyWith(fontSize: FontSizes.s24),),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Text("Email",
                                      style: TextStyles.h3,),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 35),
                                    child: Row(children: [Flexible(child: StyledTextFormField(
                                      validate: (val){
                                        if(val.isEmpty){
                                          return "Email is required";
                                        }
                                        else if(!GetX.GetUtils.isEmail(val)){
                                          return "Email is invalid";
                                        }
                                        return null;
                                      },
                                      onSaved: (val){
                                        controller.newUser.email=val;
                                      },
                                      borderColor: primaryColor,
                                      borderRadius: BorderRadius.circular(5),
                                      hintStyle: TextStyle(color: Colors.grey[500]),
                                      contentPadding: EdgeInsets.all(20),
                                    ))],),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Password",
                                          style: TextStyles.h3,),
                                        TextButton(onPressed: (){}, child: AutoSizeText("Forgot your password?",style: TextStyle(color: primaryColor,fontWeight: FontWeight.w400),))
                                      ],),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 30),
                                    child: Row(children: [Flexible(child: StyledTextFormField(
                                      obscure: !controller.showLoginPassword,
                                      suffix: GestureDetector(
                                        onTap: controller.toggleLoginShowPassword,
                                        child: Icon(controller.showLoginPassword?Icons.visibility_off:Icons.visibility,color: Colors.black,),
                                      ),
                                      validate: (val){
                                        if(val.isEmpty){
                                          return "Password is required";
                                        }
                                        else if(val.length <8){
                                          return "Password can't be less than 8 characters";
                                        }
                                        return null;
                                      },
                                      controller: controller.passwordController,
                                      borderColor: primaryColor,
                                      borderRadius: BorderRadius.circular(5),
                                      hintStyle: TextStyle(color: Colors.grey[500]),
                                      contentPadding: EdgeInsets.all(20),
                                    ))],),
                                  ),
                                  RippleButton(
                                    onPressed: (){
                                      if(controller.loginFormKey.currentState.validate()){
                                        controller.loginFormKey.currentState.save();
                                        controller.login();
                                      }
                                    },
                                    title: "Continue",elevation: 10,
                                    backGroundColor: primaryColor,
                                    width: double.infinity,
                                    height: 60,
                                    borderRadius: BorderRadius.circular(10),
                                    addShadow: true,
                                    shadowColor: Colors.purple,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: GetX.Get.height*0.02,),
                        AutoSizeText.rich(TextSpan(
                          children: [
                            TextSpan(
                              text: "Don't have an account? "
                            ),
                            TextSpan(
                              text: "Sign up",
                              style: TextStyles.title1,
                              recognizer: TapGestureRecognizer()..onTap=(){
                                  GetX.Get.to(()=>WebSignUpView());
                              }
                            )
                          ]
                        ))
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),

        ],
      ),
    );
  }
}
