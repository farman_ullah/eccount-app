import 'package:eccount/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';


class BlockWrapper extends StatelessWidget {
  final Widget child;
  BlockWrapper({this.child});
  @override
  Widget build(BuildContext context) {
    return ResponsiveConstraints(
        constraintsWhen: blockWidthConstraints,
        child: child);
  }
}
