import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';

class FilingPopupView extends StatelessWidget {
  final double maxWidth;
  final double maxHeight;
  FilingPopupView({this.maxHeight=double.infinity,this.maxWidth=double.infinity});
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxHeight: maxHeight,maxWidth: maxWidth),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black54,width: 1)
      ),
      child: Padding(
        padding:  EdgeInsets.symmetric(horizontal: Get.width*0.02),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          SizedBox(height: kToolbarHeight,),
          AutoSizeText("Register an Entity",style: TextStyles.h2,),
            SizedBox(height: 10,),
          TextField(
            decoration: InputDecoration(hintText: "I want to file a..."),
          ),
            SizedBox(height: 10,),
          AutoSizeText("Select from the options below",style: TextStyles.h2,),
            SizedBox(height: 10,),
            MouseRegion(
              cursor: MouseCursor.defer,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 8,horizontal: 3),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(5)
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AutoSizeText("Filing",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),),
                          SizedBox(height: 2,),
                            AutoSizeText('Form A1: Application to incorporate a company',style: TextStyles.title2,),
                          SizedBox(height: 2,),
                          AutoSizeText('Register: Companies',style: TextStyles.title2,),
                        ],
                      ),
                    ),
                    _ElevatedButton(title: "Start",onPressed: (){
                      SmartDialog.dismiss();
                      Get.to(()=>RegistrationMain(registrationType: RegistrationType.CompanyFormation),preventDuplicates: false);
                    },)
                  ],
                ),
              ),
            )
        ],),
      ),
    );
  }
}


class _ElevatedButton extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;
  _ElevatedButton({this.title,this.onPressed});
  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(onPressed: onPressed, icon: Icon(Icons.not_started), label: AutoSizeText(title),
      style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
              if (states.contains(MaterialState.hovered )|| states.contains(MaterialState.focused)||states.contains(MaterialState.pressed))
                return Colors.white;
              return primaryColor;
            },
          ),
          shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
                (Set<MaterialState> states) {
              if (states.contains(MaterialState.hovered) ||states.contains(MaterialState.focused) ||
                  states.contains(MaterialState.pressed))
                return RoundedRectangleBorder(borderRadius:BorderRadius.circular(20),side: BorderSide(color: Colors.white));
              return RoundedRectangleBorder(borderRadius:BorderRadius.circular(20),side: BorderSide(color: primaryColor));
            },
          ),
          padding: MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.symmetric(vertical: 15,horizontal: 20)),
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
              if (states.contains(MaterialState.hovered))
                return primaryColorDark;
              if (states.contains(MaterialState.focused) ||
                  states.contains(MaterialState.pressed))
                return primaryColorDarkPressed;
              return Colors.white;
            },
          )
      ),);
  }
}
