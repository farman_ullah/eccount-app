import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';




class AllDoneView extends StatelessWidget {
  const AllDoneView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: constraints.maxWidth*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

            CircleAvatar(
              radius: constraints.maxWidth*0.1,
              backgroundColor: Colors.white,

              child: Icon(Icons.check,color: Colors.green,size: constraints.maxWidth*0.1,),

            ),
              SizedBox(height: 20,),
              AutoSizeText("Payment",style: TextStyles.body2,),
              SizedBox(height: 20,),
              AutoSizeText("Please enter your card details",style: TextStyles.h3,),
              SizedBox(height: 20,),

              Text("We’ll notify you immediately when your company is registered. It will takes about 2-4 working day for Business Register to process applications.",overflow: TextOverflow.ellipsis,style: TextStyles.body2.copyWith(color: Colors.grey),)
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
, SizedBox(height: constraints.maxHeight*0.1,),
              Flexible(
                child: Container(

                    width:constraints.maxWidth*0.2,

                    height: 30,


                    decoration: BoxDecoration(

                        borderRadius: BorderRadius.circular(10),

                        gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                    child: GradientButton(buttonText: "DONE",onPress: (){},)),
              ),



            ],
          ),
        ),
      ),
    )
      ,
    );
  }


  Widget addTextFields(String title, {String hintText="lorem lpsum"}){

    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(title,style: TextStyles.body1.copyWith(color: Colors.grey),),
          ),
          Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(15),
              ),
              child: TxtField(

                hintTextColor: Colors.black87,

                hintTxt:hintText,


                // validate: (val){
                //   if(val.isEmpty){
                //     return "Name is required";
                //   }
                //   return null;
                // },
                // onSaved: (val){
                //   _.companyFormation.presenterDetail.name=val;
                // },

                fillColor: Colors.white,
              )),
        ],
      ),
    );
  }
}
