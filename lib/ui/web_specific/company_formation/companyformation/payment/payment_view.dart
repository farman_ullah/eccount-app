import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/payment_controller.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/models/order_model.dart';
import 'package:eccount/models/payment_model.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_wrapper.dart';


/*
class WebPaymentDetailView extends StatefulWidget {
  @override
  _WebPaymentDetailViewState createState() => _WebPaymentDetailViewState();
}

class _WebPaymentDetailViewState extends State<WebPaymentDetailView> {
  String cardId;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (controller)=>Padding(
      padding: EdgeInsets.symmetric(horizontal: Get.width*0.05),
      child: Column(
        children: [
          SizedBox(height: Get.height*0.02,),
          AutoSizeText("Order Info",textAlign: TextAlign.center,style: TextStyles.h2,),
          SizedBox(height: Get.height*0.02,),
          Card(
            elevation: 10,
            child: Padding(
              padding: EdgeInsets.all(Get.width*0.05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  AutoSizeText("Order Type",style: TextStyles.body1,),
                  AutoSizeText(getRegistrationTypeStringFromInt(controller.order.dataType),style: TextStyles.h2,),
                  SizedBox(height: Get.height*0.02,),
                  AutoSizeText("Total Amount",style: TextStyles.body1,),
                  AutoSizeText("100\$"??"",style: TextStyles.h2,),
                ],
              ),
            ),
          ),
          SizedBox(height: Get.height*0.02,),
          Row(children: [
            Expanded(
              child:
              AutoSizeText("Cards",style: TextStyles.h2,),
            ),
            SizedBox(width: 5,),
            RippleButton(onPressed: ()async{
              await Get.find<PaymentsController>().createPaymentMethod();
            },
              title: "Add Card",
              titleColor: Colors.purple,
              borderRadius: BorderRadius.circular(20),
              borderColor: Colors.purple,
              border: true,
              height: Get.height*0.06,)
          ],),
          SizedBox(height: Get.height*0.02,),
          Obx((){
            return Get.find<PaymentsController>().cards.length==0?AutoSizeText("Add a card to select"):DropdownButtonFormField(
                hint: AutoSizeText("Select a card"),
                value: cardId,
                onChanged: (String id){
                  setState(() {
                    cardId=id;
                  });
                },
                items: Get.find<PaymentsController>().cards.map((element) => DropdownMenuItem(child: AutoSizeText("${element.brand} **** ${element.last4} | ${element.expMonth}/${element.expYear}",style: TextStyles.h2,),value: element.id,)).toList());
          }),
          SizedBox(height: Get.height*0.02,),
          Align(
            alignment: Alignment.center,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Pay",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: ()async{
                if(cardId==null){
                  Show.showErrorSnackBar(title: "Select Card", error: "Please select a card to continue");
                  return;
                }
                else{
                  try{
                    Show.showLoader();
                    String id = Get.find<PaymentsController>().cards.firstWhere((element) => element.id==cardId).paymentId;
                    if(id==null){
                      return ;
                    }
                    PaymentModel model = PaymentModel(paymentMethodId:id ,amount: 10000,currency: "usd",status: "Initiated by client",orderId: controller.order.id);
                    await Get.find<FirestoreService>().addUserPayment(Get.find<UserController>().currentUser.value.id, model);
                    Get.back();
                    Get.back();
                    Show.showSnackBar(title: "Message", message: "Please wait while your payment is processed");

                  }
                  catch(e){
                    Show.showErrorSnackBar(title: "Error", error: e);
                  }
                }
              },
            ),
          )
        ],
      ),
    ));
  }
}


*/

class WebPaymentDetailView extends StatelessWidget {
String stepVal;
List<Widget> services;
WebPaymentDetailView(this.services,{Key key,this.stepVal}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GetBuilder<CompanyFormationViewModel>(        builder:(controller)=>LayoutBuilder(
      builder:(context,constraints)=> Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            StepCircle(value:this.stepVal?? "3",),
            SizedBox(height: 20,),
            AutoSizeText("Payment",style: TextStyles.subTitle.copyWith(color: Colors.black),),
            SizedBox(height: 20,),
            AutoSizeText("Please confirm your order",style: TextStyles.h2,),
            SizedBox(height: 20,),
            // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
            SizedBox(height: 20,),





Column(
  crossAxisAlignment: CrossAxisAlignment.center,
  children: [


  Container(
    width:  ResponsiveWrapper.of(context).isSmallerThan(TABLET)?constraints.maxWidth:constraints.maxWidth*0.6,

    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: ResponsiveWrapper.of(context).isSmallerThan(TABLET)?constraints.maxWidth:constraints.maxWidth*0.6,
          height: constraints.maxWidth*0.3,

          child: PaymentOrderCardsView(this.services)),
        SizedBox(height: constraints.maxHeight/4*0.1,),
        Row(

        children: [
          Checkbox(value: controller.isChecked.value, onChanged: (val)=>controller.acceptTos(val)),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(children: <InlineSpan>[
              TextSpan(
                  text: 'I agree to Eccount ',
                  style:TextStyles.body1),
              TextSpan(
                  recognizer: TapGestureRecognizer()..onTap = () {



                  },

                  text: "Terms of Service",
                  style: TextStyle(color: Colors.blue)),


            ]),
          ),
        ],
        ),
        Row(
        children: [
          Checkbox(value: controller.isCheckedPolicy.value, onChanged: (val)=>controller.checkPolicy(val)),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(children: <InlineSpan>[
              TextSpan(
                  text: 'I agree to Eccount ',
                  style:TextStyles.body1),
              TextSpan(
                  recognizer: TapGestureRecognizer()..onTap = () {



                  },

                  text: "Privacy Policy",
                  style: TextStyle(color: Colors.blue)),


            ]),
          ),
        ],
        ),
        Row(
        children: [
          Checkbox(value: controller.isCheckedNewsLetter.value, onChanged: (val)=>controller.checkNewsLetter(val)),

          Text("subscribe to Eccount newsletter")
        ],
        ),
        SizedBox(height: constraints.maxHeight/4*0.1,),
      ],
    ),
  ),




],),
            Divider(color: Colors.grey,),
            SizedBox(height: constraints.maxHeight/4*0.1,),
            Text("Payment Method",style: TextStyle(color: Colors.grey),),
            SizedBox(height: constraints.maxHeight/4*0.1,),


            Container(

              width: constraints.maxWidth*0.6,

              child: Row(

                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Container(
                      height: constraints.maxHeight/2*0.1,


                      decoration: BoxDecoration(

                        borderRadius: BorderRadius.circular(5),

                        color: Colors.indigo.shade900),

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                          Text("PAY BY BANK",style: TextStyles.body2.copyWith(color: Colors.white),),
                          Text("ULster Bank",style: TextStyles.body1.copyWith(color: Colors.white ,fontWeight: FontWeight.bold),)
                        ],),

                    ),
                  ),

                  SizedBox(width: constraints.maxWidth/2*0.2,),
                  Expanded(
                    child: GestureDetector(

                      onTap: (){

                      },
                      child: Container(
                        height: constraints.maxHeight/2*0.1,

                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          shape: BoxShape.rectangle,

                          color: Colors.lightBlue),

                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Text("PAY BY CARD",style: TextStyles.body2.copyWith(color: Colors.white)),
                            Text("stripe",style:TextStyles.subTitle.copyWith(color: Colors.white ,fontWeight: FontWeight.bold))
                          ],),

                      ),
                    ),
                  )
                ],),

            ),
            SizedBox(height: constraints.maxHeight/4*0.1,),
            TextButton(
                onPressed: (){

                  final registrationViewModel=Get.find<RegistrationViewModel>();
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);


                },
                child: Text("CANCEL ORDER",style: TextStyles.body1.copyWith(color: Colors.deepPurple),))





          ],
        ),
      ),
    )
        ,

    );
  }


}
class PaymentOrderCardsView extends StatelessWidget {
  
  List<Widget> services;
   PaymentOrderCardsView(this.services,{Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

decoration: BoxDecoration(

    color: Colors.white,
    shape: BoxShape.rectangle,borderRadius: BorderRadius.circular(15)),
      child: Row(

        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [

        Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [

            for(var widget in services.asMap().entries.toList())
           widget.value


          // Text("SERVICE"),
          // Text("Company formation (incl. fees)"),
          // Text("Registered Office Address (annual recurring fee)",overflow: TextOverflow.ellipsis,),
          // Text("Contact person (annual recurring fee)"),
          // Text("otal with no VAT")
        ],),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
          Text("Amount"),
          Text("\$230"),
          Text("\$89"),
          Text("\$80"),
          Text("\$399")


        ],)
      ],),

    );
  }
}


