import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
/*

class WebCompanyProfileView extends StatelessWidget {
 final aa=Get.put(()=>CompanyFormationViewModel());
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_) {
      return SingleChildScrollView(
        child:Padding(
          padding:  EdgeInsets.symmetric(horizontal: Get.width*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              StyledInfoWidget(child: AutoSizeText("Company Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),
              SizedBox(height: 15,),
              AutoSizeText("Company Contacts",style: TextStyles.h2,),
              SizedBox(height: 15,),
              Row(children: [
                Flexible(child:  StyledTextFormField(
                  initialValue: _.companyFormation.companyEmailAddress??_.companyFormation.presenterDetail.email,
                  hintText: "Enter company email address",
                  contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                  hintStyle: TextStyle(fontSize: FontSizes.s18),
                  labelStyle: defaultLabelStyle,
                  onSaved: (val){
                    _.companyFormation.companyEmailAddress=val;
                  },
                  validate: (val){
                    if(val.isEmpty){
                      return "Company email address is required";
                    }
                    else if(!GetUtils.isEmail(val)){
                      return "Enter a valid email address";
                    }
                    return null;
                  },
                ),),
                SizedBox(width: Get.width*0.02,),
                Flexible(child: StyledTextFormField(
                  initialValue: _.companyFormation.companyPhoneNumber??_.companyFormation.presenterDetail.telephone,
                  inputType: TextInputType.phone,
                  labelText: "Telephone",
                  labelStyle: defaultLabelStyle,
                  hintText: "Enter company phonenumber",
                  hintStyle: defaultHintStyle,
                  validate: (val){
                    if(val.isEmpty){
                      return "Phonenumber is required";
                    }
                    return null;
                  },
                  onSaved: (val){
                    _.companyFormation.presenterDetail.telephone=val;
                  },
                ))
              ],),
              SizedBox(height: 15,),
              AutoSizeText("Registered Office Address",style: TextStyles.h2,),
              SizedBox(height: 15,),
              Row(children: [
                Flexible(child:  StyledTextFormField(
                  initialValue: _.companyFormation.registeredOfficeAddress,
                  hintText: "Enter company address",
                  contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                  hintStyle: TextStyle(fontSize: FontSizes.s18),
                  labelStyle: defaultLabelStyle,
                  onSaved: (val){
                    _.companyFormation.registeredOfficeAddress=val;
                  },
                  validate: (val){
                    if(val.isEmpty){
                      return "Company address is required";
                    }
                    return null;
                  },
                ),),
              ],),
              SizedBox(height: 20,),
              Align(
                alignment: Alignment.centerRight,
                child: RippleButton(
                  backGroundColor: primaryColor,
                  title: "Next",
                  titleColor: Colors.white,
                  fontSize: FontSizes.s20,
                  borderRadius: BorderRadius.circular(10),
                  shadowColor: Colors.purple,
                  addShadow: true,
                  elevation: 6,
                  onPressed: (){
                    final registrationViewModel=Get.find<RegistrationViewModel>();
                    if(registrationViewModel.registrationFormKey.currentState.validate()){
                      registrationViewModel.registrationFormKey.currentState.save();
                      registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }
    );}
}
*/

class WebCompanyProfileView extends StatelessWidget {
  const WebCompanyProfileView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "2",),
              SizedBox(height: 20,),
              AutoSizeText("Company profile",style: TextStyles.subTitle.copyWith(color: Colors.black),),
              SizedBox(height: 20,),
              AutoSizeText("Please fill in your company details",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),



              Padding(
                padding: const EdgeInsets.only(top:40.0,bottom: 40),
                child: Divider(color: Colors.grey,),
              ),

              Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text("Company contacts",style: TextStyles.h3,),
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("MOBILE NUMBER",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(



                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: Colors.grey.shade200,
                                          blurRadius: 1.0,
                                          offset: Offset(0.0,3 ),
                                          spreadRadius: 0.7
                                      )
                                    ],

                                  ),
                                  child:   PhoneNumberField()),

                              Padding(
                                padding: const EdgeInsets.only(top:20.0,bottom: 20),
                                child: Row(children: [
                                  Icon(Icons.check_box_outline_blank,color: Colors.white,),
                                  Flexible(
                                    child: Text("Clearly distinctive from other business names registered in Ireland.",style: TextStyles.body2.copyWith(color: Colors.grey),
                                    overflow:TextOverflow.ellipsis,
                                    textAlign: TextAlign.start,
                                    ),
                                  )
                                ],),
                              )
                            ],
                          ),
                        ),
                        SizedBox(width: constraints.maxWidth/2*0.1,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("Email",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,


                                    // validate: (val){
                                    //   if(val.isEmpty){
                                    //     return "Email is required";
                                    //   }
                                    //   return null;
                                    // },
                                    // onSaved: (val){
                                    //   _.companyFormation.presenterDetail.name=val;
                                    // },

                                    fillColor: Colors.white,
                                    lblTxt: "abc@gmail.com",)),
                            ],
                          ),
                        ),
                        SizedBox(width: constraints.maxWidth/2*0.1,),









                      ],),
                  ),
                  Row(

                    children: [

                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            Padding(
                              padding: const EdgeInsets.only(bottom:20.0),
                              child: Text("Registered office address in Ireland",style: TextStyles.h3,),
                            ),


                            Container(


                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(15),


                                ),

                                child: TxtField(
                                  hintTxt: "Lorem Ipsum",
                                  hintTextColor: Colors.black87,


                                  validate: (val){
                                    if(val.isEmpty){
                                      return "Name is required";
                                    }
                                    return null;
                                  },
                                  onSaved: (val){
                                  },

                                  fillColor: Colors.white,
                                  lblTxt: "Lerem Lspusm",)),
                          ],
                        ),
                      ),
                      SizedBox(width: constraints.maxWidth/2*0.1,),







                    ],),
                ],
              ),

              Container(


                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Padding(
                      padding: const EdgeInsets.only(top:20.0,bottom: 10),
                      child: Text("NACE",style: TextStyles.h3,),
                    ),
                    Row(

                      children: [

                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("NATURE OF BUISNESS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    isdropDown: true,
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,


                                    validate: (val){
                                      if(val.isEmpty){
                                        return "Name is required";
                                      }
                                      return null;
                                    },
                                    onSaved: (val){
                                    },

                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",)),
                            ],
                          ),
                        ),
                        SizedBox(width: constraints.maxWidth/2*0.1,),






                      ],),
                    Row(

                      children: [

                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("NACE CODE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,


                                    validate: (val){
                                      if(val.isEmpty){
                                        return "Name is required";
                                      }
                                      return null;
                                    },
                                    onSaved: (val){
                                    },

                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",)),
                            ],
                          ),
                        ),
                        SizedBox(width: constraints.maxWidth/2*0.1,),







                      ],),
                  ],
                ),
              ),






              SizedBox(height: 50,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  TextButton(onPressed: (){}, child: Row(children: [
                    Icon(Icons.arrow_back_ios_outlined,),
                    SizedBox(width: 10,),
                    Text("BACK"),
                    SizedBox(width: 10,),
                  ],)),

                  SizedBox(width: 20,),
                  Flexible(
                    child: Container(

                        width:constraints.maxWidth*0.2,

                        height: 40,


                        decoration: BoxDecoration(

                            borderRadius: BorderRadius.circular(10),

                            gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                        child: GradientButton(buttonText: "NEXT",onPress: (){

                          final registrationViewModel=Get.find<RegistrationViewModel>();

                          registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);


                        },)),
                  ),
                ],
              ),


            ],
          ),
        ),
      ),
    )
      ,
    );
  }
}
