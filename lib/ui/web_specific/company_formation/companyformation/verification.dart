import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
/*
class VerificationView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<VerificationController>(
      builder:(controller)=> LayoutBuilder(
        builder: (context, constraints) => Container(
          width: constraints.maxWidth,
          height: constraints.maxHeight,
          color: Colors.grey[200],
          child: SingleChildScrollView(
            child: Padding(
              padding:  EdgeInsets.symmetric(horizontal: constraints.maxWidth*0.1),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  StepCircle(
                    value: "2",
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  AutoSizeText(
                    "Verification",
                    style: TextStyles.body2.copyWith(color: Colors.black87),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  AutoSizeText(
                    "Verification details",
                    style: TextStyles.h2,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                  SizedBox(
                    height: 20,
                  ),

                  Container(
                    width: constraints.maxWidth,
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("All Directors are required to sign this form.",style: TextStyles.title1,),
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                             Padding(
                               padding: const EdgeInsets.all(0.0),
                               child: Checkbox(


focusColor: Colors.white,


                                   value: controller.isChecked.value, onChanged: (val)=>controller.onChanged(val),



                               ),
                             ),
                                  Flexible(
                                    child: Column(
                                      children: [
                                        Text(
                                            "I/We hereby certify that the particulars contained in this form are correct and have been given in accordance with the Notes on Completion of the statutory A1 form.",
                                            textAlign: TextAlign.center,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyles.body2.copyWith(
                                              color: Colors.grey,
                                            )),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("Signature method",style: TextStyles.title1,),
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(0.0),
                                    child: Checkbox(


                                      focusColor: Colors.white,


                                      value: controller.isChecked.value, onChanged: (val)=>controller.onChanged(val),



                                    ),
                                  ),
                                  Flexible(
                                    child: Column(
                                      children: [
                                        Text(
                                            "I/We hereby certify that the particulars contained in this form are correct and have been given in accordance with the Notes on Completion of the statutory A1 form.",
                                            textAlign: TextAlign.center,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyles.body2.copyWith(
                                              color: Colors.grey,
                                            )),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("Signature Details",style: TextStyles.title1)
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(0.0),
                                    child: Checkbox(


                                      focusColor: Colors.white,


                                      value: controller.isChecked.value, onChanged: (val)=>controller.onChanged(val),



                                    ),
                                  ),
                                  Flexible(
                                    child: Column(
                                      children: [
                                        Text(
                                            "I/We hereby certify that the particulars contained in this form are correct and have been given in accordance with the Notes on Completion of the statutory A1 form.",
                                            textAlign: TextAlign.center,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyles.body2.copyWith(
                                              color: Colors.grey,
                                            )),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  "DIRECTORS",
                                  style: TextStyles.body2
                                      .copyWith(color: Colors.grey),
                                ),
                              ),
                              Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: TxtField(
                                    isdropDown: true,
                                    hintTextColor: Colors.black87,

                                    hintTxt: "Lorem Ipsum",

                                    // validate: (val){
                                    //   if(val.isEmpty){
                                    //     return "Name is required";
                                    //   }
                                    //   return null;
                                    // },
                                    // onSaved: (val){
                                    //   _.companyFormation.presenterDetail.name=val;
                                    // },

                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",
                                  )),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  "DIRECTOR NAME",
                                  style: TextStyles.body2
                                      .copyWith(color: Colors.grey),
                                ),
                              ),
                              Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Row(
                                    children: [
                                      Flexible(
                                        child: TxtField(
                                          hintTextColor: Colors.black87,

                                          hintTxt: "Lorem Ipsum",

                                          // validate: (val){
                                          //   if(val.isEmpty){
                                          //     return "Name is required";
                                          //   }
                                          //   return null;
                                          // },
                                          // onSaved: (val){
                                          //   _.companyFormation.presenterDetail.name=val;
                                          // },

                                          fillColor: Colors.white,
                                          lblTxt: "Lerem Lspusm",
                                        ),
                                      ),
                                      SizedBox(
                                        width: constraints.maxWidth / 4 * 0.1,
                                      ),
                                      Flexible(
                                        child: TxtField(
                                          isdropDown: true,
                                          hintTextColor: Colors.black87,

                                          hintTxt: "Lorem Ipsum",

                                          // validate: (val){
                                          //   if(val.isEmpty){
                                          //     return "Name is required";
                                          //   }
                                          //   return null;
                                          // },
                                          // onSaved: (val){
                                          //   _.companyFormation.presenterDetail.name=val;
                                          // },

                                          fillColor: Colors.white,
                                          lblTxt: "Lerem Lspusm",
                                        ),
                                      ),
                                    ],
                                  )),
                            ],
                          ),
                        ),


                      ],
                    ),
                  ),

                  Padding(
                    padding:  EdgeInsets.only(top:constraints.maxHeight/2*0.2,bottom:constraints.maxHeight/4*0.2,),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        TextButton(onPressed: (){}, child: Row(children: [
                          Icon(Icons.arrow_back_ios_outlined,),


                          Text("BACK"),
                          SizedBox(width: 20,),
                        ],)),

                        Flexible(
                          child: Container(

                              width:constraints.maxWidth*0.1,

                              height: 30,


                              decoration: BoxDecoration(

                                  borderRadius: BorderRadius.circular(10),

                                  gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                              child: GradientButton(buttonText: "NEXT",onPress: (){

                              },)),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      TextButton(onPressed: (){}, child: Row(children: [
                        Icon(Icons.arrow_back_ios_outlined,),


                        Text("BACK"),
                        SizedBox(width: 20,),
                      ],)),

                      Flexible(
                        child: Container(

                            width:constraints.maxWidth*0.1,

                            height: 30,


                            decoration: BoxDecoration(

                                borderRadius: BorderRadius.circular(10),

                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                            child: GradientButton(buttonText: "NEXT",onPress: (){

                            },)),
                      ),
                    ],
                  ),







                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
*/




class VerificationView extends StatelessWidget {
  const VerificationView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "2",),
              SizedBox(height: 20,),
              AutoSizeText("Verification",style: TextStyles.subTitle.copyWith(color: Colors.black),),
              SizedBox(height: 20,),
              AutoSizeText("Final verification details",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),



              Padding(
                padding: const EdgeInsets.only(top:40.0,bottom: 40),
                child: Divider(color: Colors.grey,),
              ),

              Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Padding(
                    padding: const EdgeInsets.only(top:20.0,bottom: 20),
                    child: Text("Review filing details",style: TextStyles.h3,),
                  ),


                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      children: [
                        Expanded(
                          child: Column(

                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: Text("Review the filing and associated attachments before completing the signature process.",style: TextStyles.body1.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    suffixIcon: Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: FloatingActionButton(

                                        elevation: 0.0,

                                        backgroundColor: Colors.white,
                                        isExtended: true,


                                        child: Container(
                                            width: 40,
                                            height: 40,

                                            decoration: BoxDecoration(

                                                shape: BoxShape.circle,

                                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                            child: Icon(Icons.download_rounded,color: Colors.white,)),),
                                    ),
                                    hintTxt: "View filing form",
                                    hintTextColor: Colors.black87,


                                    // validate: (val){
                                    //   if(val.isEmpty){
                                    //     return "Email is required";
                                    //   }
                                    //   return null;
                                    // },
                                    // onSaved: (val){
                                    //   _.companyFormation.presenterDetail.name=val;
                                    // },

                                    fillColor: Colors.white,
                                    lblTxt: "abc@gmail.com",)),
                            ],
                          ),
                        ),









                      ],),
                  ),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top:20.0,bottom: 20.0),
                                child: Text("Review filing details",style: TextStyles.h3,),
                              ),

                              Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: Text("Please download the signature file for signing.",style: TextStyles.body1.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    suffixIcon: Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: FloatingActionButton(

                                        elevation: 0.0,

backgroundColor: Colors.white,
                                        isExtended: true,


                                        child: Container(
                                          width: 40,
                                            height: 40,

                                            decoration: BoxDecoration(

                                                shape: BoxShape.circle,

                                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                            child: Icon(Icons.download_rounded,color: Colors.white,)),),
                                    ),
                                    hintTxt: "download",
                                    hintTextColor: Colors.black87,


                                    // validate: (val){
                                    //   if(val.isEmpty){
                                    //     return "Email is required";
                                    //   }
                                    //   return null;
                                    // },
                                    // onSaved: (val){
                                    //   _.companyFormation.presenterDetail.name=val;
                                    // },

                                    fillColor: Colors.white,
                                    lblTxt: "abc@gmail.com",)),
                            ],
                          ),
                        ),










                      ],),
                  ),

                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,

                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top:20.0),
                                child: Text("Upload signature file",style: TextStyles.h3,),
                              ),

                              Padding(
                                padding: const EdgeInsets.only(bottom: 20.0,top: 20),
                                child: Text("Once you have have the signed signature document, please upload it here.",style: TextStyles.body1.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTxt: "Upload",
                                    suffixIcon: Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: FloatingActionButton(

                                        elevation: 0.0,

                                        backgroundColor: Colors.white,
                                        isExtended: true,


                                        child: Container(
                                            width: 40,
                                            height: 40,

                                            decoration: BoxDecoration(

                                                shape: BoxShape.circle,

                                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                            child: Icon(Icons.download_rounded,color: Colors.white,)),),
                                    ),
                                    hintTextColor: Colors.black87,


                                    // validate: (val){
                                    //   if(val.isEmpty){
                                    //     return "Email is required";
                                    //   }
                                    //   return null;
                                    // },
                                    // onSaved: (val){
                                    //   _.companyFormation.presenterDetail.name=val;
                                    // },

                                    fillColor: Colors.white,
                                    lblTxt: "abc@gmail.com",)),
                            ],
                          ),
                        ),










                      ],),
                  ),
                ],
              ),








              SizedBox(height: 50,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  TextButton(onPressed: (){}, child: Row(children: [
                    Icon(Icons.arrow_back_ios_outlined,),
                    SizedBox(width: 10,),
                    Text("BACK"),
                    SizedBox(width: 10,),
                  ],)),

                  SizedBox(width: 20,),
                  Flexible(
                    child: Container(

                        width:150,

                        height: 30,


                        decoration: BoxDecoration(

                            borderRadius: BorderRadius.circular(10),

                            gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                        child: GradientButton(buttonText: "NEXT",onPress: (){

                          final registrationViewModel=Get.find<RegistrationViewModel>();
                          registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                        },)),





                  ),
                ],
              ),


            ],
          ),
        ),
      ),
    )
      ,
    );
  }
}
