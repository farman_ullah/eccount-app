import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_row_column.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

class WebCompanyNameView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FadeIn(
      child: GetBuilder<CompanyFormationViewModel>(
          init: CompanyFormationViewModel(),
          autoRemove: false,
          builder: (_){
            return LayoutBuilder(
              builder:(context,constraints)=> Container(
                width: constraints.maxWidth,
                height: constraints.maxHeight,
                color: Colors.grey[200],

                child: Column(
                //  mainAxisAlignment: MainAxisAlignment.start,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [


                    Container(

                      width: constraints.maxWidth,
                      height: constraints.maxHeight*0.4,
                      child: SingleChildScrollView(
                        child: Column(children: [

                          StepCircle(value: "1",),
                          SizedBox(height: 20,),
                          AutoSizeText("COMPANY NAME",style: TextStyles.body2.copyWith(color: Colors.black87),),
                          SizedBox(height: 20,),
                          Align(
                              alignment: Alignment.center,
                              child: AutoSizeText("Check name availability",style: TextStyles.h2,)),
                          SizedBox(height: 20,),
                          // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                          SizedBox(height: 20,),
                          Padding(
                            padding:  EdgeInsets.symmetric(horizontal: constraints.maxWidth*0.3),
                            child: TxtField(

                              //   initialValue: _.companyFormation.companyName,
                              initialValue: "dummy value",


                              validate: (val){
                                if(val.isEmpty){
                                  return "Company name is required";
                                }
                                return null;
                              },
                              onSaved: (val){
                                _.companyFormation.companyName=val;
                              },

                              fillColor: Colors.white,
                              hintTxt: "Type your company name here",),
                          ),
                          SizedBox(height: 20,),
                          Container(

                              width:200,

                              height: 50,


                              decoration: BoxDecoration(

                                  borderRadius: BorderRadius.circular(10),

                                  gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                              child: GradientButton(buttonText: "CHECK NOW",onPress: (){
                                final registrationViewModel=Get.find<RegistrationViewModel>();

                                registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                              },)),

                  Divider(color: Colors.grey,),


                    ],),
                      ),),







                    Expanded(
                      child: ListView(
                        children: [



                          Align(

                              alignment: Alignment.center,
                              child: Text("Eccount Pro Tip",style: TextStyles.body2.copyWith(fontWeight: FontWeight.bold),)),
                          SizedBox(height: 20,),
                          Align(
                            alignment: Alignment.center,
                              child: Text("A good company name has all U's",style: TextStyles.h2,)),
                          SizedBox(height: 20,),
                          ResponsiveRowColumn(layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                              ? ResponsiveRowColumnType.COLUMN
                              : ResponsiveRowColumnType.ROW,
                            rowCrossAxisAlignment: CrossAxisAlignment.start,
                            columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            rowMainAxisAlignment: MainAxisAlignment.center,
                            columnCrossAxisAlignment: CrossAxisAlignment.center,
                          //    columnMainAxisSize: MainAxisSize.max,
                            // rowPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth/2*0.2 , vertical: 80),
                            // columnPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth*0.1,vertical: 20),
                            columnSpacing: 30,
                            rowSpacing: 50,

                          children: [
                            ResponsiveRowColumnItem(

                              child: Column(

                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [

                                  Container(


                                      child: addCards(constraints.maxWidth, constraints.maxHeight,"Unique"),

                                  height:ResponsiveWrapper.of(context).isSmallerThan(TABLET)?200:ResponsiveWrapper.of(context).screenWidth*0.1 ,
                                  ),
                                  Container(


                                      height:ResponsiveWrapper.of(context).isSmallerThan(TABLET)?200:ResponsiveWrapper.of(context).screenWidth*0.1,
                                      child: addCards(constraints.maxWidth, constraints.maxHeight,"Universal")),
                                ],),
                            ),

                            ResponsiveRowColumnItem(
                              child: Column(

                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [

                                  Container(

                              height:ResponsiveWrapper.of(context).isSmallerThan(TABLET)?200:ResponsiveWrapper.of(context).screenWidth*0.1,

                                      child: addCards(constraints.maxWidth, constraints.maxHeight,"Unique")),
                                  Container(

                                      height:ResponsiveWrapper.of(context).isSmallerThan(TABLET)?200:ResponsiveWrapper.of(context).screenWidth*0.1,

                                      child: addCards(constraints.maxWidth, constraints.maxHeight,"Universal")),
                                ],),
                            ),

                          ],

                          ),
                        ],
                      ),
                    )


                    // Column(
                    //
                    //   children: [
                    //     Row(
                    //
                    //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    //       children: [
                    //
                    //       addCards(constraints.maxWidth, constraints.maxHeight,"Unique"),
                    //       addCards(constraints.maxWidth, constraints.maxHeight,"Universal"),
                    //     ],),
                    //     SizedBox(height: 20,),
                    //     Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    //       children: [
                    //
                    //       addCards(constraints.maxWidth, constraints.maxHeight,"Usable"),
                    //       addCards(constraints.maxWidth, constraints.maxHeight,"Unpatented"),
                    //     ],),
                    //   ],
                    // ):
                    //     Expanded(
                    //       child: ListView(children: [
                    //         addCardMobile(constraints.maxWidth, constraints.maxHeight,"Unique"),
                    //         addCardMobile(constraints.maxWidth, constraints.maxHeight,"Universal"),
                    //         addCardMobile(constraints.maxWidth, constraints.maxHeight,"Usable"),
                    //         addCardMobile(constraints.maxWidth, constraints.maxHeight,"Unpatented"),
                    //
                    //       ],),
                    //     ),

                  ],
                ),
              ),
            );
          }),
    );
  }

 Widget addCards(double width, double height, String text){

return Card(

shape: RoundedRectangleBorder(
  borderRadius: BorderRadius.circular(15.0),
),
  child: Column(

  mainAxisAlignment: MainAxisAlignment.spaceEvenly,


  children: [

  Icon(Icons.check,color: Colors.green,),
  Text(text,style: TextStyles.h3,),
  Text("Cearly distinctive from other business names registered in Ireland.",
    overflow: TextOverflow.ellipsis,

    textAlign: TextAlign.center,
    style: TextStyles.body2.copyWith(color: Colors.grey),),


],),
);

  }
  Widget addCardMobile(double width, double height,String text){

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width:width*0.3,height: width*0.4,
        child: Card(




          child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

          Icon(Icons.check,color: Colors.green,),
          Text(text,style: TextStyles.defaultTextStyle.copyWith(color: Colors.black),),
          Text("Cearly distinctive from other business names registered in Ireland.",
            overflow: TextOverflow.ellipsis,

            textAlign: TextAlign.center,
            style: TextStyles.body2.copyWith(color: Colors.grey),)

        ],),),
      ),
    );

  }


}


