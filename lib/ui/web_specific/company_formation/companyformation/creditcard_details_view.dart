import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';




class CreditCardDetails extends StatelessWidget {
  const CreditCardDetails({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: constraints.maxWidth*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "3",),
              SizedBox(height: 20,),
              AutoSizeText("Payment",style: TextStyles.subTitle.copyWith(color: Colors.black),),
              SizedBox(height: 20,),
              AutoSizeText("Please enter your card details",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),

          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Row(
                      children: [

                        Image.asset("assets/logo.png",width: constraints.maxWidth*0.1,),
                        Text(
                          'Eccount (TEST)',
                          style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(height: constraints.maxHeight/6*0.1,),
                    Text("Eccount company formation app",style: TextStyles.body3),
                    SizedBox(height: constraints.maxHeight/6*0.1,),
                    Text("\$399"),

                    SizedBox(height: constraints.maxHeight/6*0.1,),
                    Text("Payment for company formation",style: TextStyles.body3)
                  ],
                ),
                SizedBox(width: constraints.maxWidth*0.1,),
                Container(width: 1, color: Colors.grey), //
                SizedBox(width: constraints.maxWidth/2*0.1,),
                Expanded(
                  child: Column(
                    children: [

                      Text("Pay with card",style: TextStyle(fontWeight: FontWeight.bold),),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [



                            addTextFields("EMAIL",),

                            addTextFields("CARD NUMBER",),
                            addTextFields("EXPIRY",),
                            addTextFields("SECURITY",),
                            addTextFields("COUNTRY OR REGION",),
                            Padding(
                              padding:  EdgeInsets.only(top:constraints.maxHeight/4*0.2),
                              child: Container(



                                  height: 50,


                                  decoration: BoxDecoration(

                                      borderRadius: BorderRadius.circular(10),

                                      gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                  child: GradientButton(buttonText: "Pay \$399.00",onPress: (){

                                    final registrationViewModel=Get.find<RegistrationViewModel>();
                                    registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                                  },)),
                            ),


                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),


            ],
          ),
        ),
      ),
    )
      ,
    );
  }


 Widget addTextFields(String title, {String hintText="lorem lpsum"}){

   return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(title,style: TextStyles.body1.copyWith(color: Colors.grey),),
          ),
          Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(15),
              ),
              child: TxtField(

                hintTextColor: Colors.black87,

                hintTxt:hintText,


                // validate: (val){
                //   if(val.isEmpty){
                //     return "Name is required";
                //   }
                //   return null;
                // },
                // onSaved: (val){
                //   _.companyFormation.presenterDetail.name=val;
                // },

                fillColor: Colors.white,
              )),
        ],
      ),
    );
  }
}
