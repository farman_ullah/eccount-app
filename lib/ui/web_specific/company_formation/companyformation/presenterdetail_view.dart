import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WebPresenterDetailsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
        child: Container(
          width: constraints.maxWidth,
          height: constraints.maxHeight,

          color: Colors.grey[200],
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                StepCircle(value: "2",),
                SizedBox(height: 20,),
                AutoSizeText("Your Profile",style: TextStyles.subTitle.copyWith(color: Colors.black),),
                SizedBox(height: 20,),
                AutoSizeText("Please fill in your personal details",style: TextStyles.h2,),
                SizedBox(height: 20,),
                // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                SizedBox(height: 20,),
                Container(
                  
                  width: constraints.maxWidth,
                  child: Row(

                    children: [

                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text("FIRST NAME",style: TextStyles.body2.copyWith(color: Colors.grey),),
                            ),
                            Container(


                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(15),


                                ),

                                child: TxtField(
                                  hintTextColor: Colors.black87,

                                  hintTxt: "Lorem Ipsum",
                                  initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,

                                  // validate: (val){
                                  //   if(val.isEmpty){
                                  //     return "Name is required";
                                  //   }
                                  //   return null;
                                  // },
                                  // onSaved: (val){
                                  //   _.companyFormation.presenterDetail.name=val;
                                  // },

                                  fillColor: Colors.white,
                                  lblTxt: "Lerem Lspusm",)),
                          ],
                        ),
                      ),
                      SizedBox(width: constraints.maxWidth/2*0.1,),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text("LAST NAME",style: TextStyles.body2.copyWith(color: Colors.grey)),
                            ),
                            Container(

                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(15),


                                ),

                                child: TxtField(
                                  hintTxt: "Lorem Ipsum",
                                  hintTextColor: Colors.black87,
                                  initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,


                                  // validate: (val){
                                  //   if(val.isEmpty){
                                  //     return "Company name is required";
                                  //   }
                                  //   return null;
                                  // },
                                  // onSaved: (val){
                                  //   _.companyFormation.companyName=val;
                                  // },

                                  fillColor: Colors.white,
                                  lblTxt: "Lerem Lpsum",)),
                          ],
                        ),
                      ),
                      SizedBox(width: constraints.maxWidth/2*0.1,),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text("PERSONAL NOTIFICATION CODE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                            ),
                            Container(

                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(15),


                                ),

                                child: TxtField(
                                  hintTxt: "Lorem Ipsum",
                                  hintTextColor: Colors.black87,
                                  initialValue: _.companyFormation.companyName,


                                  fillColor: Colors.white,
                                 )),
                          ],
                        ),
                      ),





                    ],),
                ),


                Padding(
                  padding: const EdgeInsets.only(top:40.0,bottom: 40),
                  child: Divider(color: Colors.grey,),
                ),

                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("Contact",style: TextStyles.h3,),
                    ),
                    Container(
                      child: Row(

                        children: [

                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("Email",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(


                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,


                                      // validate: (val){
                                      //   if(val.isEmpty){
                                      //     return "Email is required";
                                      //   }
                                      //   return null;
                                      // },
                                      // onSaved: (val){
                                      //   _.companyFormation.presenterDetail.name=val;
                                      // },

                                      fillColor: Colors.white,
                                      lblTxt: "abc@gmail.com",)),
                              ],
                            ),
                          ),
                          SizedBox(width: constraints.maxWidth/2*0.1,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("Mobile",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(



                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                                    color: Colors.grey.shade200,
                                            blurRadius: 1.0,
                                            offset: Offset(0.0,3 ),
                                            spreadRadius: 0.7
                                        )
                                      ],

                                    ),
                                    child:   PhoneNumberField()),
                              ],
                            ),
                          ),






                        ],),
                    ),
                  ],
                ),

                Container(

                  width: constraints.maxWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Padding(
                        padding: const EdgeInsets.only(top:20.0,bottom: 10),
                        child: Text("Residence and citizenship",style: TextStyles.h3,),
                      ),
                      Row(

                        children: [

                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("RESIDENCE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(


                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,


                                      validate: (val){
                                        if(val.isEmpty){
                                          return "Name is required";
                                        }
                                        return null;
                                      },
                                      onSaved: (val){
                                        _.companyFormation.presenterDetail.name=val;
                                      },

                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lspusm",)),
                              ],
                            ),
                          ),
                          SizedBox(width: constraints.maxWidth/2*0.1,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("CITIZENSHIP",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(

                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,


                                      validate: (val){
                                        if(val.isEmpty){
                                          return "Company name is required";
                                        }
                                        return null;
                                      },
                                      onSaved: (val){
                                        _.companyFormation.companyName=val;
                                      },

                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lpsum",)),
                              ],
                            ),
                          ),






                        ],),
                      Row(

                        children: [

                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("STATE/PROVINCE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(


                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,


                                      validate: (val){
                                        if(val.isEmpty){
                                          return "Name is required";
                                        }
                                        return null;
                                      },
                                      onSaved: (val){
                                        _.companyFormation.presenterDetail.name=val;
                                      },

                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lspusm",)),
                              ],
                            ),
                          ),
                          SizedBox(width: constraints.maxWidth/2*0.1,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("POSTCODE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(

                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,


                                      validate: (val){
                                        if(val.isEmpty){
                                          return "Company name is required";
                                        }
                                        return null;
                                      },
                                      onSaved: (val){
                                        _.companyFormation.companyName=val;
                                      },

                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lpsum",)),
                              ],
                            ),
                          ),






                        ],),
                    ],
                  ),
                ),



                  SizedBox(height: 15,),




          SizedBox(height: 50,),
                Flexible(
                  child: Container(

                      width:constraints.maxWidth*0.2,

                      height: 40,


                      decoration: BoxDecoration(

                          borderRadius: BorderRadius.circular(10),

                          gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                      child: GradientButton(buttonText: "NEXT",onPress: (){


                        final registrationViewModel=Get.find<RegistrationViewModel>();

                        registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                      },)),
                ),


              ],
            ),
          ),
        ),
      )
,
      );
    });
  }
}
