import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeclarationView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,
        color: Colors.grey[200],
        child: SingleChildScrollView(
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: constraints.maxWidth*0.1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                StepCircle(
                  value: "2",
                ),
                SizedBox(
                  height: 20,
                ),
                AutoSizeText(
                  "Declaration",
                  style: TextStyles.body2.copyWith(color: Colors.grey),
                ),
                SizedBox(
                  height: 20,
                ),
                AutoSizeText(
                  "Please add declaration details",
                  style: TextStyles.h2,
                ),
                SizedBox(
                  height: 20,
                ),
                // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                SizedBox(
                  height: 20,
                ),

                Container(
                  width: constraints.maxWidth,
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.check_box_outline_blank,
                                  color: Colors.white,
                                ),
                                Flexible(
                                  child: Text(
                                      "Tick here if the person whom is signing is a Solicitor engaged in the formation of the company.",
                                      textAlign: TextAlign.center,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyles.body2.copyWith(
                                        color: Colors.grey,
                                      )),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "CHOOSE THE DIRECTOR",
                                style: TextStyles.body2
                                    .copyWith(color: Colors.grey),
                              ),
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: TxtField(
                                  isdropDown: true,
                                  hintTextColor: Colors.black87,

                                  hintTxt: "Lorem Ipsum",

                                  // validate: (val){
                                  //   if(val.isEmpty){
                                  //     return "Name is required";
                                  //   }
                                  //   return null;
                                  // },
                                  // onSaved: (val){
                                  //   _.companyFormation.presenterDetail.name=val;
                                  // },

                                  fillColor: Colors.white,
                                  lblTxt: "Lerem Lspusm",
                                )),
                          ],
                        ),
                      ),

                      // Flexible(child:StyledTextFormField(
                      //   initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,
                      //   labelText: "Name",
                      //   labelStyle: defaultLabelStyle,
                      //   hintText: "Enter your name",
                      //   hintStyle: defaultHintStyle,
                      //   validate: (val){
                      //     if(val.isEmpty){
                      //       return "Name is required";
                      //     }
                      //     return null;
                      //   },
                      //   onSaved: (val){
                      //     _.companyFormation.presenterDetail.name=val;
                      //   },
                      // ),),
                      // SizedBox(width: Get.width*0.02,),
                      // Flexible(child: StyledTextFormField(
                      //   initialValue: _.companyFormation.presenterDetail.dxNumber,
                      //   labelText: "DX number",
                      //   labelStyle: defaultLabelStyle,
                      //   hintText: "Enter dx number",
                      //   hintStyle: defaultHintStyle,
                      //   validate: (val){
                      //     if(val.isEmpty){
                      //       return "DX number is required";
                      //     }
                      //     return null;
                      //   },
                      //   onSaved: (val){
                      //     _.companyFormation.presenterDetail.dxNumber=val;
                      //   },
                      // ),)
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top: 40.0, bottom: 40),
                  child: Divider(
                    color: Colors.grey,
                  ),
                ),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("Declaration",
                          style: TextStyles.h3.copyWith(color: Colors.black)),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: TxtField(
                                    hintTextColor: Colors.black87,
                                    hintTxt: "Lorem Ipsum",
                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: constraints.maxWidth / 2 * 0.1,
                        ),

                        Text("of (address)"),
                        SizedBox(
                          width: constraints.maxWidth / 2 * 0.1,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: TxtField(
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,
                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lpsum",
                                  )),
                            ],
                          ),
                        ),

                        // Flexible(child:StyledTextFormField(
                        //   initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,
                        //   labelText: "Name",
                        //   labelStyle: defaultLabelStyle,
                        //   hintText: "Enter your name",
                        //   hintStyle: defaultHintStyle,
                        //   validate: (val){
                        //     if(val.isEmpty){
                        //       return "Name is required";
                        //     }
                        //     return null;
                        //   },
                        //   onSaved: (val){
                        //     _.companyFormation.presenterDetail.name=val;
                        //   },
                        // ),),
                        // SizedBox(width: Get.width*0.02,),
                        // Flexible(child: StyledTextFormField(
                        //   initialValue: _.companyFormation.presenterDetail.dxNumber,
                        //   labelText: "DX number",
                        //   labelStyle: defaultLabelStyle,
                        //   hintText: "Enter dx number",
                        //   hintStyle: defaultHintStyle,
                        //   validate: (val){
                        //     if(val.isEmpty){
                        //       return "DX number is required";
                        //     }
                        //     return null;
                        //   },
                        //   onSaved: (val){
                        //     _.companyFormation.presenterDetail.dxNumber=val;
                        //   },
                        // ),)
                      ],
                    ),
                    SizedBox(
                      height: constraints.maxHeight / 2 * 0.1,
                    ),
                    Row(
                      children: [
                        Text("do solemnly declare that I am a",style: TextStyles.body2.copyWith(
                          color: Colors.grey,
                        )),
                        SizedBox(
                          width: constraints.maxWidth / 4 * 0.1,
                        ),
                        Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: TxtField(
                                hintTextColor: Colors.black87,
                                hintTxt: "Lorem Ipsum",
                                fillColor: Colors.white,
                                lblTxt: "Lerem Lspusm",
                              )),
                        ),
                        SizedBox(
                          width: constraints.maxWidth / 4 * 0.1,
                        ),
                        Text("Email Address"),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: TxtField(
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,
                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lpsum",
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: constraints.maxHeight / 2 * 0.1,
                    ),
                    Text(
                        "and that all the requirements of the Companies Act 2014 in respect of the registration of the said company,",style: TextStyles.body2.copyWith(
                      color: Colors.grey,
                    ))
                  ],
                ),

                Container(
                  width: constraints.maxWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("NACE CODE",
                                      style: TextStyles.body2
                                          .copyWith(color: Colors.grey)),
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      validate: (val) {
                                        if (val.isEmpty) {
                                          return "Name is required";
                                        }
                                        return null;
                                      },
                                      onSaved: (val) {},
                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lspusm",
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                Container(
                  width: constraints.maxWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: constraints.maxHeight / 4 * 0.1,
                      ),
                      Text("I further declare that the place or places in the State where it is proposed to carry out the activity is/are:",style: TextStyles.body2.copyWith(
                        color: Colors.grey,
                      )),


                      SizedBox(
                        height: constraints.maxHeight / 4 * 0.1,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10.0),
                                  child: Text("ADDRESS",
                                      style: TextStyles.body2
                                          .copyWith(color: Colors.grey)),
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      validate: (val) {
                                        if (val.isEmpty) {
                                          return "Name is required";
                                        }
                                        return null;
                                      },
                                      onSaved: (val) {},
                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lspusm",
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),


                Container(
                  width: constraints.maxWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: constraints.maxHeight / 4 * 0.1,
                      ),
                      Text("Central Administration Office",  style: TextStyles.h3.copyWith(color: Colors.black)),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("ADDRESS",
                                      style: TextStyles.body2
                                          .copyWith(color: Colors.grey)),
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,
                                      validate: (val) {
                                        if (val.isEmpty) {
                                          return "Name is required";
                                        }
                                        return null;
                                      },
                                      onSaved: (val) {},
                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lspusm",
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding:  EdgeInsets.only(top:constraints.maxHeight*0.2,bottom:constraints.maxHeight*0.1 ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [

                            TextButton(onPressed: (){}, child: Row(children: [
                              Icon(Icons.arrow_back_ios_outlined,),


                              Text("BACK"),
                              SizedBox(width: 20,),
                            ],)),

                            Flexible(
                              child: Container(

                                  width:150,

                                  height: 30,


                                  decoration: BoxDecoration(

                                      borderRadius: BorderRadius.circular(10),

                                      gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                  child: GradientButton(buttonText: "NEXT",onPress: (){
                                    final registrationViewModel=Get.find<RegistrationViewModel>();
                                    registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                                  },)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),



              ],
            ),
          ),
        ),
      ),
    );
  }
}
