import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class CompanySharedDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return LayoutBuilder(builder: (context,constraints){
      double width=constraints.maxWidth;
      double height=constraints.maxHeight;

      return  SingleChildScrollView(


        child: Container(
          width: width,
          height:height,

          color: Colors.grey[200],
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                StepCircle(value: "2",),
                SizedBox(height: 20,),
                AutoSizeText("Company share capital",style: TextStyles.body2.copyWith(color: Colors.black),),
                SizedBox(height: 20,),
                AutoSizeText("Please add company share details",style: TextStyles.h3,),
                SizedBox(height: 20,),
                // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                SizedBox(height: 20,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [


                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Text("Authorised share capital",style: TextStyles.h3),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("RESIDENTIAL ADDRESS",style: TextStyles.body2.copyWith(color: Colors.grey),),
                        ),
                        Container(


                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              isdropDown: true,
                              hintTextColor: Colors.black87,

                              hintTxt: "Lorem Ipsum",


                              fillColor: Colors.white,
                              lblTxt: "Lerem Lspusm",)),
                        Align(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.only(top:20.0),
                            child: Container(

                                width:150,

                                height: 30,


                                decoration: BoxDecoration(

                                    borderRadius: BorderRadius.circular(10),

                                    gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                child: GradientButton(buttonText: "ADD",onPress: ()async{



                                  await    openDialogue(width, height);

                                },)),
                          ),
                        ),

                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Text("Issued share capital",style: TextStyles.h3),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("ISSUED SHARE CAPITAL",style: TextStyles.body2.copyWith(color: Colors.grey),),
                        ),
                        Container(


                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              isdropDown: true,
                              hintTextColor: Colors.black87,

                              hintTxt: "Lorem Ipsum",


                              fillColor: Colors.white,
                              lblTxt: "Lerem Lspusm",)),

                        SizedBox(height: 20,),
                        Align(
                          alignment: Alignment.center,
                          child: Container(width: 200,

                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [


                                Container(

                                    width:150,

                                    height: 30,


                                    decoration: BoxDecoration(

                                        borderRadius: BorderRadius.circular(10),

                                        gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                    child: GradientButton(buttonText: "ADD SIGNATURE",onPress: (){},)),
                                Padding(
                                  padding: const EdgeInsets.only(top:100),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [

                                      TextButton(onPressed: (){}, child: Row(children: [
                                        Icon(Icons.arrow_back_ios_outlined,),


                                        Text("BACK"),
                                        SizedBox(width: 20,),
                                      ],)),

                                      Flexible(
                                        child: Container(



                                            height: 30,


                                            decoration: BoxDecoration(

                                                borderRadius: BorderRadius.circular(10),

                                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                            child: GradientButton(



                                              buttonText: "DONE",onPress: (){

                                              final registrationViewModel=Get.find<RegistrationViewModel>();
                                              registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                                            },)),
                                      ),

                                      SizedBox(width: 25,)
                                    ],
                                  ),
                                ),


                              ],),
                          ),
                        )

                      ],
                    ),

                  ],
                ),










              ],
            ),
          ),
        ),


      );

    });
  }
}
Future openDialogue(double width,double height)async{

 await showGeneralDialog(
    barrierLabel: "Label",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 500),
    context: Get.context,
    pageBuilder: (context, anim1, anim2) {
      return Align(
        alignment: Alignment.center,
        child: Container(
          width: width*0.5,
          height: width*0.5,
          decoration:BoxDecoration(color: Colors.grey[100], borderRadius: BorderRadius.circular(20)) ,


          child: Padding(
            padding: const EdgeInsets.only(left: 30,right: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,

              children: [


                Padding(
                  padding:  EdgeInsets.all(10.0),
                  child: Text("Select From",style: TextStyles.h2,),
                ),

                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Padding(
                        padding:  EdgeInsets.all(10.0),
                        child: Text("CURRENCY",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),

                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),
                        child: Text("CLASS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),

                        child: Text("MEMBOER OF SHARES",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),

                        child: Text("NOMINAL VALUE (PER SHARE)",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),

                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),
                        child: Text("TOTAL VALUE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top:20.0,bottom: 10),
                  child: Container(


                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(

                            width:width*0.1,

                            height: 30,


                            decoration: BoxDecoration(

                                borderRadius: BorderRadius.circular(10),

                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                            child: GradientButton(buttonText: "ADD",onPress: ()async{



                              await    openDialogue(width, height);

                            },)),
                        SizedBox(width: 10,),
                        Text("CANCEL",style: TextStyles.body3,)
                      ],
                    ),
                  ),
                ),

              ],),
          ),
        ),
      );
    },
    transitionBuilder: (context, anim1, anim2, child) {
      return SlideTransition(
        position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
        child: child,
      );
    },
  );
}