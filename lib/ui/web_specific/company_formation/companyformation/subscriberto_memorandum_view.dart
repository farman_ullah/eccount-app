import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddSubscriberView extends StatelessWidget {
  const AddSubscriberView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: constraints.maxWidth/2*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "2",),
              SizedBox(height: 20,),
              AutoSizeText("Subscribers to Memorandum",style: TextStyles.subTitle.copyWith(color: Colors.black87),),
              SizedBox(height: 20,),
              AutoSizeText("Please add subscribers to memorandum details",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),



              Padding(
                padding: const EdgeInsets.only(top:40.0,bottom: 40),
                child: Divider(color: Colors.grey,),
              ),

              Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text("Subscribers to memorandum",style: TextStyles.h3,),
                  ),
                  Container(


                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [


                        Row(

                          children: [

                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text("SLECT SUBSCRIBERS TO MEMORANDUM",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                  ),
                                  Container(


                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        borderRadius: BorderRadius.circular(15),


                                      ),

                                      child: TxtField(
                                        isdropDown: true,
                                        hintTxt: "Lorem Ipsum",
                                        hintTextColor: Colors.black87,


                                        validate: (val){
                                          if(val.isEmpty){
                                            return "Name is required";
                                          }
                                          return null;
                                        },
                                        onSaved: (val){
                                        },

                                        fillColor: Colors.white,
                                        lblTxt: "Lerem Lspusm",)),
                                ],
                              ),
                            ),
                            SizedBox(width: constraints.maxWidth/2*0.1,),








                          ],),

                        SizedBox(height: 40,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(onPressed: (){}, child: Row(children: [
                              Icon(Icons.arrow_back_ios_outlined,color: Colors.grey[200],),

                              Visibility(

                                  maintainSize: true,
                                  maintainState: true,
                                  maintainAnimation: true,
                                  visible: false,
                                  child: Text("BACK",)),
                              SizedBox(width: 20,),


                            ],)),
                            Container(

                                width:150,

                                height: 30,


                                decoration: BoxDecoration(

                                    borderRadius: BorderRadius.circular(10),

                                    gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                child: GradientButton(buttonText: "ADD",onPress: ()async{


                               await   openDialogue(constraints.maxWidth,constraints.maxHeight);
                                },)),
                          ],
                        ),


                      ],
                    ),
                  ),
                ],
              ),








              Padding(
                padding:  EdgeInsets.only(top:constraints.maxHeight*0.2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    TextButton(onPressed: (){}, child: Row(children: [
                      Icon(Icons.arrow_back_ios_outlined,),


                      Text("BACK"),
                      SizedBox(width: 20,),
                    ],)),

                    Flexible(
                      child: Container(

                          width:150,

                          height: 30,


                          decoration: BoxDecoration(

                              borderRadius: BorderRadius.circular(10),

                              gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                          child: GradientButton(buttonText: "NEXT",onPress: (){
                            final registrationViewModel=Get.find<RegistrationViewModel>();
                            registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                          },)),
                    ),
                  ],
                ),
              ),


            ],
          ),
        ),
      ),
    ));
  }
}
Future openDialogue(double width,double height)async{

  await showGeneralDialog(
    barrierLabel: "Add Subscribers to memorandum",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 500),
    context: Get.context,
    pageBuilder: (context, anim1, anim2) {
      return Align(
        alignment: Alignment.center,
        child: Container(
          width: width*0.5,
          height: width*0.5,
          decoration:BoxDecoration(color: Colors.grey[100], borderRadius: BorderRadius.circular(20)) ,


          child: Padding(
            padding: const EdgeInsets.only(left: 30,right: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,

              children: [


                Padding(
                  padding:  EdgeInsets.all(10.0),
                  child: Text("Add Subscribers to memorandum",style: TextStyles.h2,),
                ),

                Flexible(child: Row(children: [
                  Icon(Icons.check_box_outline_blank,color: Colors.white,),
                  SizedBox(width: 10,),
                  Text("Subscriber")
                ],)),
                Flexible(child: Row(children: [
                  Icon(Icons.check_box_outline_blank,color: Colors.white,),
                  SizedBox(width: 10,),
                  Text("Agent")
                ],)),

                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Padding(
                        padding:  EdgeInsets.all(10.0),
                        child: Text("CURRENCY",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),

                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),
                        child: Text("CLASS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),

                        child: Text("MEMBOER OF SHARES",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),

                        child: Text("NOMINAL VALUE (PER SHARE)",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),

                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(10.0),
                        child: Text("TOTAL VALUE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                      ),
                      Flexible(child: TxtField(fillColor: Colors.white,),),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top:20.0,bottom: 10),
                  child: Container(


                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(

                            width:150,

                            height: 30,


                            decoration: BoxDecoration(

                                borderRadius: BorderRadius.circular(10),

                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                            child: GradientButton(buttonText: "SAVE",onPress: ()async{



                            
                            },)),
                        SizedBox(width: 10,),
                        Text("CANCEL",style: TextStyles.body3,)
                      ],
                    ),
                  ),
                ),

              ],),
          ),
        ),
      );
    },
    transitionBuilder: (context, anim1, anim2, child) {
      return SlideTransition(
        position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
        child: child,
      );
    },
  );
}