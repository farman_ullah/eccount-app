import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VerificationController extends GetxController {
      RxBool isChecked = false.obs;


      onChanged(bool value) {
            isChecked = value.obs;
            update();
      }
}