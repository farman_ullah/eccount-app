
import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/spacing.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/styled_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/web_specific/block_wrapper/blockwrapper_view.dart';
import 'package:eccount/ui/web_specific/header/header_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_framework/responsive_framework.dart';



class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          HeaderView(),
          _GetStarted(),
          _HowTo(),
         _WhyRegister(),
          SliverToBoxAdapter(
            child: Center(
              child: BlockWrapper(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: Color(0x20000000))),
                  margin: blockMargin,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: Align(
                          alignment: Alignment.center,
                          child: AutoSizeText("Popular articles",style: TextStyles.h1,),),
                      ),
                      ResponsiveRowColumn(
                        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                            ? ResponsiveRowColumnType.COLUMN
                            : ResponsiveRowColumnType.ROW,
                        rowCrossAxisAlignment: CrossAxisAlignment.start,
                        columnCrossAxisAlignment: CrossAxisAlignment.center,
                        columnMainAxisSize: MainAxisSize.min,
                        rowPadding: EdgeInsets.symmetric(horizontal: 80, vertical: 80),
                        columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                        columnSpacing: 0,
                        rowSpacing: 50,
                        children: [
                          ResponsiveRowColumnItem(
                            rowFlex: 1,
                            rowFit: FlexFit.tight,
                            child: ResponsiveRowColumn(
                              layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                                  ? ResponsiveRowColumnType.COLUMN
                                  : ResponsiveRowColumnType.ROW,
                              rowCrossAxisAlignment: CrossAxisAlignment.start,
                              columnCrossAxisAlignment: CrossAxisAlignment.center,
                              columnMainAxisSize: MainAxisSize.min,
                              rowPadding: EdgeInsets.symmetric(horizontal: 80, vertical: 80),
                              columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                              columnSpacing: 0,
                              rowSpacing: 50,
                              children: [
                                ResponsiveRowColumnItem(
                                    rowFlex: 1,
                                    rowFit: FlexFit.tight,
                                    child: Card(
                                  child: Container(
                                    height: 700,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Image.asset("assets/0.png",height: 200,),
                                        AutoSizeText("Does E-residency works?")
                                      ],
                                    ),
                                  ),
                                ))
                              ],
                            ),
                          ),
                          ResponsiveRowColumnItem(
                            rowFlex: 1,
                            rowFit: FlexFit.tight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(bottom: 16),
                                  child: Row(children: [
                                    Icon(Icons.done,color: Colors.green,size: 30,),
                                    Expanded(
                                      child: Padding(padding: EdgeInsets.only(left: 30),child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(bottom: 10),
                                            child: AutoSizeText("Strong currency",style: TextStyles.title1,),
                                          ),
                                          AutoSizeText("Estonia uses the Euro and is in the Single European Payment Area",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                        ],
                                      ),),
                                    )
                                  ],),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 16),
                                  child: Row(children: [
                                    Icon(Icons.done,color: Colors.green,size: 30,),
                                    Expanded(
                                      child: Padding(padding: EdgeInsets.only(left: 30),child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(bottom: 10),
                                            child: AutoSizeText("Digital signature",style: TextStyles.title1,),
                                          ),
                                          AutoSizeText("Directors of companies never need to sign a piece of paper again",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                        ],
                                      ),),
                                    )
                                  ],),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 16),
                                  child: Row(children: [
                                    Icon(Icons.done,color: Colors.green,size: 30,),
                                    Expanded(
                                      child: Padding(padding: EdgeInsets.only(left: 20),child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(bottom: 10),
                                            child: AutoSizeText("European market",style: TextStyles.title1,),
                                          ),
                                          AutoSizeText("Serve clients in the European Union and European Economic Area",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                        ],
                                      ),),
                                    )
                                  ],),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

}


class _GetStarted extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: BlockWrapper(
        child: ResponsiveRowColumn(
          layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
              ? ResponsiveRowColumnType.COLUMN
              : ResponsiveRowColumnType.ROW,
          rowCrossAxisAlignment: CrossAxisAlignment.start,
          columnCrossAxisAlignment: CrossAxisAlignment.start,
          columnMainAxisSize: MainAxisSize.min,
          rowMainAxisSize: MainAxisSize.min,
          rowPadding: EdgeInsets.symmetric(horizontal: 80, vertical: 80),
          columnPadding: EdgeInsets.symmetric(horizontal: 80, vertical: 50),
          columnSpacing: 50,
          rowSpacing: 50,
          children: [
            ResponsiveRowColumnItem(
              rowFlex: 2,
              rowFit: FlexFit.tight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Text("The simplest way to start an Irish company",
                      style: TextStyles.h1.copyWith(fontSize: FontSizes.s40),),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 35),
                    child: Text(
                        "Five minute online company formation in Ireland for 215\$ including the 190\$ state fee. Start by checking your business name availability",
                        style: TextStyles.subTitle),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 35),
                    child: Row(children: [Flexible(child: StyledTextFormField(
                      borderColor: primaryColor,
                      borderRadius: BorderRadius.circular(5),
                      hintText: "Check if your company name is available",
                      hintStyle: TextStyle(color: Colors.grey[500]),
                      contentPadding: EdgeInsets.symmetric(horizontal: 30,vertical: 25),
                      suffix: Padding(
                        padding: const EdgeInsets.only(right: 5),
                        child: StyledButton("CHECK NOW",(){},EdgeInsets.symmetric(vertical: 25,horizontal: 30)),
                      ),
                    ))],),
                  ),
                  Row(children: [
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 25),
                          child: SvgPicture.asset("assets/icon1.svg",width: 68,height: 68,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: AutoSizeText("Single founder",style: TextStyles.h2.copyWith(fontSize: 20),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 25),
                          child: AutoSizeText("Ideal for solo\nentrepreneurs",style: TextStyles.subTitle.copyWith(fontSize: 16),),
                        ),
                      ],)),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 25),
                          child: SvgPicture.asset("assets/icon2.svg",width: 68,height: 68,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: AutoSizeText("100% online",style: TextStyles.h2.copyWith(fontSize: 20),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 25),
                          child: AutoSizeText("Self setup &\nvirtual office",style: TextStyles.subTitle.copyWith(fontSize: 16),),
                        ),
                      ],)),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 25),
                          child: SvgPicture.asset("assets/icon3.svg",width: 68,height: 68,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: AutoSizeText("Expert guidance",style: TextStyles.h2.copyWith(fontSize: 20),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 25),
                          child: AutoSizeText("Live chat support\nevery weekday",style: TextStyles.subTitle.copyWith(fontSize: 16),),
                        ),
                      ],))
                  ],)
                ],
              ),
            ),
            ResponsiveRowColumnItem(
              rowFlex: 2,
              rowFit: FlexFit.tight,
              child: Center(child: Image.asset("assets/Illustration-Blue.png",fit: BoxFit.fill,)),
            ),
          ],
        ),
      ),
    );
  }
}


class _HowTo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Center(
        child: BlockWrapper(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
                border: Border.all(color: Color(0x20000000))),
            margin: blockMargin,
            child: ResponsiveRowColumn(
              layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                  ? ResponsiveRowColumnType.COLUMN
                  : ResponsiveRowColumnType.ROW,
              rowCrossAxisAlignment: CrossAxisAlignment.start,
              columnCrossAxisAlignment: CrossAxisAlignment.center,
              columnMainAxisSize: MainAxisSize.min,
              rowPadding: EdgeInsets.symmetric(horizontal: 80, vertical: 80),
              columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
              columnSpacing: 50,
              rowSpacing: 50,
              children: [
                ResponsiveRowColumnItem(
                  rowFlex: 1,
                  rowFit: FlexFit.tight,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 32),
                        child: AutoSizeText("How to set up a company in Ireland",style: TextStyles.h2,),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Row(children: [
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                color: primaryColor,
                                shape: BoxShape.circle
                            ),
                            child: Center(
                              child: AutoSizeText("1",style: TextStyles.title1.copyWith(color: Colors.white),),
                            ),
                          ),
                          Expanded(
                            child: Padding(padding: EdgeInsets.only(left: 20),child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: AutoSizeText("Apply for Estonian e-Residency online",style: TextStyles.title1,),
                                ),
                                AutoSizeText("Apply for Estonian digital identity for non-residents",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                              ],
                            ),),
                          )
                        ],),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Row(children: [
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                color: primaryLightest,
                                shape: BoxShape.circle
                            ),
                            child: Center(
                              child: AutoSizeText("2",style: TextStyles.title1.copyWith(color: primaryColor),),
                            ),
                          ),
                          Expanded(
                            child: Padding(padding: EdgeInsets.only(left: 20),child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: AutoSizeText("Log in to register a company online",style: TextStyles.title1,),
                                ),
                                AutoSizeText("Use your e-resident card or Smart-ID and log in to Unicount",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                              ],
                            ),),
                          )
                        ],),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Row(children: [
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                color: primaryLightest,
                                shape: BoxShape.circle
                            ),
                            child: Center(
                              child: AutoSizeText("3",style: TextStyles.title1.copyWith(color: primaryColor),),
                            ),
                          ),
                          Expanded(
                            child: Padding(padding: EdgeInsets.only(left: 20),child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: AutoSizeText("Add virtual office and contact person service",style: TextStyles.title1,),
                                ),
                                AutoSizeText("Subscribe to Unicount virtual office service when registering your company",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                              ],
                            ),),
                          )
                        ],),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Row(children: [
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                color: primaryLightest,
                                shape: BoxShape.circle
                            ),
                            child: Center(
                              child: AutoSizeText("4",style: TextStyles.title1.copyWith(color: primaryColor),),
                            ),
                          ),
                          Expanded(
                            child: Padding(padding: EdgeInsets.only(left: 10),child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: AutoSizeText("Set up a business bank account online",style: TextStyles.title1,),
                                ),
                                AutoSizeText("Apply for Wise business bank account and start invoicing the same day",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                              ],
                            ),),
                          )
                        ],),
                      ),
                    ],
                  ),
                ),
                ResponsiveRowColumnItem(
                  rowFlex: 1,
                  rowFit: FlexFit.tight,
                  child: Center(child: Image.asset("assets/Illustration-Yellow.png",fit: BoxFit.fill,)),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _WhyRegister extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Center(
        child: BlockWrapper(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
                border: Border.all(color: Color(0x20000000))),
            margin: blockMargin,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: Align(
                    alignment: Alignment.center,
                    child: AutoSizeText("Why register a company in Ireland?",style: TextStyles.h1,),),
                ),
                ResponsiveRowColumn(
                  layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                      ? ResponsiveRowColumnType.COLUMN
                      : ResponsiveRowColumnType.ROW,
                  rowCrossAxisAlignment: CrossAxisAlignment.start,
                  columnCrossAxisAlignment: CrossAxisAlignment.center,
                  columnMainAxisSize: MainAxisSize.min,
                  rowPadding: EdgeInsets.symmetric(horizontal: 80, vertical: 80),
                  columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                  columnSpacing: 0,
                  rowSpacing: 50,
                  children: [
                    ResponsiveRowColumnItem(
                      rowFlex: 1,
                      rowFit: FlexFit.tight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Row(children: [
                              Icon(Icons.done,color: Colors.green,size: 30,),
                              Expanded(
                                child: Padding(padding: EdgeInsets.only(left: 30),child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 10),
                                      child: AutoSizeText("Rule of law",style: TextStyles.title1,),
                                    ),
                                    AutoSizeText("Estonia has a democratic government and operates in the EU legal space",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                  ],
                                ),),
                              )
                            ],),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Row(children: [
                              Icon(Icons.done,color: Colors.green,size: 30,),
                              Expanded(
                                child: Padding(padding: EdgeInsets.only(left: 30),child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 10),
                                      child: AutoSizeText("Merchant accounts",style: TextStyles.title1,),
                                    ),
                                    AutoSizeText("Estonian companies can apply for Stripe, Paysera or Decta account",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                  ],
                                ),),
                              )
                            ],),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Row(children: [
                              Icon(Icons.done,color: Colors.green,size: 30,),
                              Expanded(
                                child: Padding(padding: EdgeInsets.only(left: 20),child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 10),
                                      child: AutoSizeText("Banking and fintech",style: TextStyles.title1,),
                                    ),
                                    AutoSizeText("Excellent online banking and IBAN account options such as Wise and Payoneer",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                  ],
                                ),),
                              )
                            ],),
                          ),
                        ],
                      ),
                    ),
                    ResponsiveRowColumnItem(
                      rowFlex: 1,
                      columnOrder: 0,
                      rowFit: FlexFit.tight,
                      child: Center(child: Image.asset("assets/1.png",fit: BoxFit.fill,)),
                    ),
                    ResponsiveRowColumnItem(
                      rowFlex: 1,
                      rowFit: FlexFit.tight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Row(children: [
                              Icon(Icons.done,color: Colors.green,size: 30,),
                              Expanded(
                                child: Padding(padding: EdgeInsets.only(left: 30),child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 10),
                                      child: AutoSizeText("Strong currency",style: TextStyles.title1,),
                                    ),
                                    AutoSizeText("Estonia uses the Euro and is in the Single European Payment Area",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                  ],
                                ),),
                              )
                            ],),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Row(children: [
                              Icon(Icons.done,color: Colors.green,size: 30,),
                              Expanded(
                                child: Padding(padding: EdgeInsets.only(left: 30),child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 10),
                                      child: AutoSizeText("Digital signature",style: TextStyles.title1,),
                                    ),
                                    AutoSizeText("Directors of companies never need to sign a piece of paper again",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                  ],
                                ),),
                              )
                            ],),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Row(children: [
                              Icon(Icons.done,color: Colors.green,size: 30,),
                              Expanded(
                                child: Padding(padding: EdgeInsets.only(left: 20),child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 10),
                                      child: AutoSizeText("European market",style: TextStyles.title1,),
                                    ),
                                    AutoSizeText("Serve clients in the European Union and European Economic Area",style: TextStyles.subTitle.copyWith(fontSize: FontSizes.s16),)
                                  ],
                                ),),
                              )
                            ],),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}



