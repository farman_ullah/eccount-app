import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/footer.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/web_specific/block_wrapper/blockwrapper_view.dart';
import 'package:eccount/ui/web_specific/dashboard/messages/message.dart';
import 'package:eccount/ui/web_specific/header/header_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AboutView extends StatelessWidget {
  const AboutView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      // body: BlockWrapper(
      //   child: Container(
      //     width: double.infinity,
      //
      //     child: Image.asset("home_background_image.jpeg",fit: BoxFit.cover,),),
      // ),

      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Stack(
            children: <Widget>[
              Container(
                width: ResponsiveWrapper.of(context).screenWidth,
                height: ResponsiveWrapper.of(context).scaledHeight,

                // color: Colors.red,
                child: new DecoratedBox(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: AssetImage(
                        "assets/home_background_image.jpg",
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal:
                              ResponsiveWrapper.of(context).screenWidth /
                                  2 *
                                  0.2),
                      child: Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(
                                        ResponsiveWrapper.of(context)
                                                .scaledHeight /
                                            8 *
                                            0.1),
                                    child: Text(
                                      "Eccount",
                                      style: TextStyles.h3.copyWith(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            ResponsiveWrapper.of(context)
                                                    .scaledHeight /
                                                8 *
                                                0.1),
                                    child: Container(
                                      child: Text(
                                        "We offer an ultra-easy Irish company formation for through an online self-service portal.With video tutorials and online support, you can set up a single shareholder and board member company with our virtual office address in five minutes or less. The service is based on the API of the Estonian Business Register.Multiple shareholder companies and subsdiaries can be registered with our virtual office and contact person service via Business Register.",
                                        style: TextStyles.body1.copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.start,
                                        maxLines: 6,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.only(
                                    top: ResponsiveWrapper.of(context)
                                            .scaledHeight /
                                        8 *
                                        0.2),
                                child: Card(
                                    child: Column(
                                  children: [
                                    Image.asset(
                                      "assets/world-map.png",
                                      fit: BoxFit.contain,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical:
                                              ResponsiveWrapper.of(context)
                                                      .scaledHeight /
                                                  8 *
                                                  0.2,
                                          horizontal:
                                              ResponsiveWrapper.of(context)
                                                      .scaledHeight /
                                                  8 *
                                                  0.2),
                                      child: Text(
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed purus id magna laoreet fringilla. Vestibulum ante ipsum primis in faucibus orci luctus",
                                        textAlign: TextAlign.start,
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyles.body1.copyWith(
                                            color: Colors.grey,
                                            letterSpacing: 2,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                )),
                              ),
                            ),
                          ],
                        ),
                        height: ResponsiveWrapper.of(context).screenWidth * 0.5,
                      ),
                    ),
                    ResponsiveRowColumn(
                      layout:
                          ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                              ? ResponsiveRowColumnType.COLUMN
                              : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(
                          horizontal:
                              ResponsiveWrapper.of(context).screenWidth /
                                  2 *
                                  0.2,
                          vertical: 30),
                      columnPadding: EdgeInsets.symmetric(
                          horizontal:
                              ResponsiveWrapper.of(context).screenWidth * 0.1,
                          vertical: 50),
                      columnSpacing: 50,
                      rowSpacing: 50,
                      children: [
                        ResponsiveRowColumnItem(
                          child: Ccard(
                            title: "Lorem Ipsum",
                            body:
                                "Lorem ipsum dolor sit amet, consectetur adip iscing elit. Donec sed purus id magna laoreet fringilla. Vestibulum ante ipsum primis in fauci bus orci luctus et ultrices posuere cubilia curae Ut sit amet sapien leo.",
                          ),
                          rowFit: FlexFit.tight,
                        ),
                        ResponsiveRowColumnItem(
                          child: Ccard(
                            title: "Lorem Ipsum",
                            body:
                                "Lorem ipsum dolor sit amet, consectetur adip iscing elit. Donec sed purus id magna laoreet fringilla. Vestibulum ante ipsum primis in fauci bus orci luctus et ultrices posuere cubilia curae Ut sit amet sapien leo.",
                          ),
                          rowFit: FlexFit.tight,
                        ),
                        ResponsiveRowColumnItem(
                          child: Ccard(
                            title: "Lorem Ipsum",
                            body:
                                "Lorem ipsum dolor sit amet, consectetur adip iscing elit. Donec sed purus id magna laoreet fringilla. Vestibulum ante ipsum primis in fauci bus orci luctus et ultrices posuere cubilia curae Ut sit amet sapien leo.",
                          ),
                          rowFit: FlexFit.tight,
                        ),
                      ],
                    ),
                    ResponsiveRowColumn(
                      layout:
                          ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                              ? ResponsiveRowColumnType.COLUMN
                              : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(
                          horizontal:
                              ResponsiveWrapper.of(context).screenWidth /
                                  2 *
                                  0.2,
                          vertical: 80),
                      columnPadding: EdgeInsets.symmetric(
                          horizontal:
                              ResponsiveWrapper.of(context).screenWidth * 0.1,
                          vertical: 20),
                      columnSpacing: 50,
                      rowSpacing: 50,
                      children: [],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          bottom:
                              ResponsiveWrapper.of(context).screenWidth * 0.1),
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.cyan.shade300,
                              borderRadius: BorderRadius.circular(10)),
                          child: TextButton(
                              child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "START A COMPANY",
                              style: TextStyles.body1
                                  .copyWith(color: Colors.white),
                            ),
                          ))),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Footer(
            textStyle: TextStyles.body1.copyWith(color: Colors.white),
          )
        ],
      ),
    );
  }
}

class Ccard extends StatelessWidget {
  String imageUrl;
  String title;
  String subTitle;
  String body;

  Ccard({
    Key key,
    this.title,
    this.body,
    this.imageUrl = "assets/logo.png",
    this.subTitle = "CEO",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: Container(
            height: 300,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[100]),
                color: Colors.white,
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding:  EdgeInsets.only(left: ResponsiveWrapper.of(context).screenWidth/6*0.1),
                  child: Text(
                    title,
                    style: TextStyles.body1.copyWith(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding:  EdgeInsets.only(left: ResponsiveWrapper.of(context).screenWidth/6*0.1),
                  child: Text(
                    subTitle,
                    style: TextStyles.body1.copyWith(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding:  EdgeInsets.only(left: ResponsiveWrapper.of(context).screenWidth/6*0.1),
                  child: Text(body,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      textAlign: TextAlign.start,
                      style: TextStyles.body2.copyWith(color: Colors.grey)),
                ),



              ],
            ),
          ),
        ),
        Stack(
          alignment: Alignment.center,
          children: [
            Container(
    decoration: BoxDecoration(
    color: Colors.white,
    shape: BoxShape.circle,
    boxShadow: [BoxShadow( color: Colors.grey[100], spreadRadius: 2)],
    ),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 60,
                child: CircleAvatar(
                  backgroundColor: Colors.white,

                  child: Image.asset(
                    imageUrl,
                    fit: BoxFit.contain,
                  ),
                  radius: 40,
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
