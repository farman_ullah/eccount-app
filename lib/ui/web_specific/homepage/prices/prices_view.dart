
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/footer.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/ui/web_specific/block_wrapper/blockwrapper_view.dart';
import 'package:eccount/ui/web_specific/header/header_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

class Prices extends StatelessWidget {
  const Prices({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(



      backgroundColor: Colors.transparent,
      // body: BlockWrapper(
      //   child: Container(
      //     width: double.infinity,
      //
      //     child: Image.asset("home_background_image.jpeg",fit: BoxFit.cover,),),
      // ),

      body: Stack(
        alignment: Alignment.bottomCenter,

        children: [
          Stack(
            children: <Widget>[


              Container(
                width:  ResponsiveWrapper.of(context).screenWidth,
                height: ResponsiveWrapper.of(context).scaledHeight,

              // color: Colors.red,
                child: new DecoratedBox(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: AssetImage("assets/home_background_image.jpg",),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),

              Text( ResponsiveWrapper.of(context).screenWidth.toString()),
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    Text("Pricing",style: TextStyles.h2.copyWith(color: Colors.white),),
                    Text("We like to keep it simple and short.",style: TextStyles.body1.copyWith(color: Colors.white),),
                    Text("No extras, no hidden costs",style: TextStyles.body1.copyWith(color: Colors.white)),
                    ResponsiveRowColumn(


                      layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                          ? ResponsiveRowColumnType.COLUMN
                          : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth*0.2 , vertical: 80),
                      columnPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth*0.1,vertical: 20),
                      columnSpacing: 50,
                      rowSpacing: 50,
                      children: [
                        ResponsiveRowColumnItem(




                            child: AddReadMoreCard(


                              imageUrl: "assets/2.png",
                              title: "Company formation",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",),


                        rowFit: FlexFit.tight,

                        ),
                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,



                              child: AddReadMoreCard(

                                imageUrl: "assets/2.png",
                                title: "Company formation",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",)),

                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,


                            child: AddReadMoreCard(

                              imageUrl: "assets/2.png",
                              title: "Company formation",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",))

                      ],
                    ),

                    Container(
                        decoration: BoxDecoration(color: Colors.cyan,borderRadius: BorderRadius.circular(10)),

                        child: TextButton(child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("START A COMPANY",style: TextStyles.body1.copyWith(color: Colors.white),),
                        ))),
                    Padding(
                      padding:  EdgeInsets.symmetric(vertical:ResponsiveWrapper.of(context).screenWidth/4*0.2),
                      child: Text("Company formation",style: TextStyles.body1.copyWith(color: Colors.black,fontWeight: FontWeight.bold)),
                    ),
                    Text("Register a company online for 215 euros incl. VAT address in Ireland. The VAT free state fee of 190 euros is included in our price. Terms of service apply.",style: TextStyles.body1.copyWith(color: Colors.grey),)
,
                   ResponsiveRowColumn(



                     layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                         ? ResponsiveRowColumnType.COLUMN
                         : ResponsiveRowColumnType.ROW,
                     rowCrossAxisAlignment: CrossAxisAlignment.start,
                     columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     rowMainAxisAlignment: MainAxisAlignment.center,
                     columnCrossAxisAlignment: CrossAxisAlignment.center,
                     columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).screenWidth*0.2, vertical: 10),
                     columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                     columnSpacing: 0,
                     rowSpacing: 50,


                     children: [




                       ResponsiveRowColumnItem(
     rowFit: FlexFit.tight,
                           child: AddCompanyFormationsCards(icon: Icon(Icons.card_membership_sharp),title: "Set up your electronic signature ",body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader orbrowser plugin issues.",))



                       ,   ResponsiveRowColumnItem(
                           rowFit: FlexFit.tight,
                           child: AddCompanyFormationsCards(icon: Icon(Icons.comment_outlined),title: "File an electronic application ",body: "Unicount has built an awesome company formation app that is connected to the Business Register API. After you have electronically signed your a Virtual office addressThe recurring 12 month service fee for registered office address. in Tallinn is 99 euros plus VAT. Terms of service apply.pplication, it is sent to the government for approval",))
                       ],




                   ),
                    ResponsiveRowColumn(



                      layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                          ? ResponsiveRowColumnType.COLUMN
                          : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).screenWidth*0.2, vertical: 10),
                      columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                      columnSpacing: 0,
                      rowSpacing: 50,


                      children: [




                        ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.person),title: "Subscribe to virtual office and contact person service ",
                              body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader or",))



                        ,   ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.query_builder),title: "Subscribe to virtual office and contact person service",
                              body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader or browser plugin issues.",))
                      ],




                    ),

                    Padding(
                      padding:  EdgeInsets.symmetric(vertical:ResponsiveWrapper.of(context).screenWidth/4*0.1),
                      child: Text("Virtual office address"),
                    ),
                    Text("The recurring 12 month service fee for registered office address. in Tallinn is 99 euros plus VAT. Terms of service apply.",style: TextStyles.body1.copyWith(color: Colors.grey),)
                    ,
                    ResponsiveRowColumn(



                      layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                          ? ResponsiveRowColumnType.COLUMN
                          : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).screenWidth*0.2, vertical: 10),
                      columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                      columnSpacing: 0,
                      rowSpacing: 50,


                      children: [




                        ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.card_membership_sharp),title: "Set up your electronic signature ",body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader orbrowser plugin issues.",))



                        ,   ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.card_membership_sharp),title: "Set up your electronic signature ",body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader orbrowser plugin issues.",))
                        ,  ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.card_membership_sharp),title: "Set up your electronic signature ",body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader orbrowser plugin issues.",))],




                    ),
                    Padding(
                      padding:  EdgeInsets.symmetric(vertical:ResponsiveWrapper.of(context).screenWidth/4*0.1),
                      child: Text("Virtual office address"),
                    ),
                    Text("The recurring 12 month service fee for registered office address. in Tallinn is 99 euros plus VAT. Terms of service apply.",style: TextStyles.body1.copyWith(color: Colors.grey),)
                    ,
                    ResponsiveRowColumn(



                      layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                          ? ResponsiveRowColumnType.COLUMN
                          : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).screenWidth*0.2, vertical: 10),
                      columnPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 50),
                      columnSpacing: 0,
                      rowSpacing: 50,


                      children: [




                        ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.card_membership_sharp),title: "File an electronic application",body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader orbrowser plugin issues.",))



                        ,   ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.comment_bank_sharp),title: "Set up your electronic signature ",body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader orbrowser plugin issues.",))
                        ,  ResponsiveRowColumnItem(
                            rowFit: FlexFit.tight,
                            child: AddCompanyFormationsCards(icon: Icon(Icons.comment_outlined),title: "Set up your electronic signature ",body: "All you need to start a company online is digital identity – your e-resident card or Smart-ID. You can create a Smart-ID to avoid ID-card reader orbrowser plugin issues.",))],




                    ),
                    Padding(
                      padding:  EdgeInsets.symmetric(  vertical:  ResponsiveWrapper.of(context).screenWidth/4*0.1),

                      child: Container(
                          decoration: BoxDecoration(color: Colors.cyan,borderRadius: BorderRadius.circular(10)),

                          child: TextButton(child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("START A COMPANY",style: TextStyles.body1.copyWith(color: Colors.white),),
                          ))),
                    ),
                  ],
                ),
              ),

            ],
          ),

         Footer(textStyle: TextStyles.body1.copyWith(color: Colors.white60),),

        ],
      ),
      
      
    );
  }

  addFooter(BuildContext context,TextStyle textStyle) {


  return  Container(
      decoration: BoxDecoration(

          gradient: LinearGradient(colors: [
            Colors.deepPurpleAccent,
            Colors.purple
          ])),

    width:   ResponsiveWrapper.of(context).screenWidth,height: 50,

  child: Padding(
    padding:  EdgeInsets.only(left:ResponsiveWrapper.of(context).screenWidth/4*0.1),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [


      Text("Eccount",style: textStyle,),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [

        Text("SUPPORT@ECCOUNT.IE",style: textStyle),
        SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),
        Text("TERMS OF SERVICES",style: textStyle),
          SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),
        Text("PRIVACY",style: textStyle),
          SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),
        Text("COOKIES",style: textStyle),
          SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),

      ],)
    ],),
  ),

  );

  }



}
class AddReadMoreCard extends StatelessWidget {
  String title;
  String body;
  String btnText;
  String imageUrl;
   AddReadMoreCard({Key key,this.title,this.body,this.btnText,this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(


      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,

        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Image.asset(imageUrl,fit: BoxFit.cover),
          ),
        Text(title,style: TextStyles.body1.copyWith(color: Colors.black,fontWeight: FontWeight.bold),),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("We pay the state fee of \$190 to get your company registered for the next working day.",style: TextStyles.body2.copyWith(color: Colors.grey,fontWeight: FontWeight.normal)

          ,textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,maxLines: 3,
          ),
        ),

           Padding(
             padding: const EdgeInsets.symmetric(vertical:10.0),
             child: Container(
               width: 100,
                    height:30,

                    decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.circular(10),
                        gradient: LinearGradient(colors: [
                          Colors.deepPurpleAccent,
                          Colors.purple
                        ])),

              child: GradientButton(child: Center(child: Text(btnText,style: TextStyles.body1.copyWith(color: Colors.white),),),)
                ),
           ),



      ],),
      decoration: BoxDecoration(
        color: Colors.white60,
      borderRadius: BorderRadius.circular(10,),



    ),);
  }
}
class AddCompanyFormationsCards extends StatelessWidget {
  Icon icon;
  String title;
  String body;

  AddCompanyFormationsCards({Key key,this.title,this.body,this.icon,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [

        Padding(
          padding: const EdgeInsets.only(top:40.0),
          child: Container(
            height: 200,

            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[100]),

                color: Colors.white,borderRadius: BorderRadius.circular(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,

              children: [



                Padding(
                  padding: const EdgeInsets.only(top:10.0),
                  child: Text(title,style: TextStyles.body1.copyWith(color: Colors.black,fontWeight: FontWeight.bold),),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(



                      body,overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      textAlign: TextAlign.center,style: TextStyles.body2.copyWith(color: Colors.grey)),
                ),


TextButton(onPressed: (){}, child: Text("READ MORE",style: TextStyles.body1.copyWith(color: Colors.deepPurple),))


              ],),
          ),
        ),
        Container(
            height: 80,


            child: StepCircle(child:this.icon ,)),
      ],
    );
  }
}