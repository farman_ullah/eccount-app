
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/footer.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/web_specific/block_wrapper/blockwrapper_view.dart';
import 'package:eccount/ui/web_specific/header/header_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:responsive_framework/responsive_framework.dart';

class Articles extends StatelessWidget {
  const Articles({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(



      backgroundColor: Colors.transparent,
      // body: BlockWrapper(
      //   child: Container(
      //     width: double.infinity,
      //
      //     child: Image.asset("home_background_image.jpeg",fit: BoxFit.cover,),),
      // ),

      body: Stack(
        alignment: Alignment.bottomCenter,

        children: [
          Stack(
            children: <Widget>[


              Container(
                width:  ResponsiveWrapper.of(context).screenWidth,
                height: ResponsiveWrapper.of(context).scaledHeight,

                // color: Colors.red,
                child: new DecoratedBox(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: AssetImage("assets/home_background_image.jpg",),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),

              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    Text("Articles",style: TextStyles.h2.copyWith(color: Colors.white),),
                 Padding(
                   padding:  EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).screenWidth*0.2,vertical: ResponsiveWrapper.of(context).screenWidth/4*0.1),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: [
                       Flexible(


                           child: TxtField(
                             fillColor: Colors.white,

                             hintTxt:"Search by article name or content" ,)),

                       SizedBox(width: ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),

                       Container(
                           height: 50,
                           width: 100,
                           decoration: BoxDecoration(
                               
                               
                               borderRadius: BorderRadius.circular(15),
                               color: Colors.cyanAccent),

                           child: GradientButton(
                             backgroundColor: Colors.cyanAccent,




                             child: Text("SEARCH",style: TextStyles.body1.copyWith(color: Colors.white),),



                           ))
                     ],
                   ),
                 ),

                    Padding(
                      padding:  EdgeInsets.symmetric( horizontal:ResponsiveWrapper.of(context).screenWidth/2*0.2),
                      child: Container(

                        child: Card(


                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          child: Row(children: [
Flexible(
  child:   Container(
      // width: ResponsiveWrapper.of(context).scaledWidth*0.3,

      child: Image.asset("assets/0.png",fit: BoxFit.contain,)),
),
                            VerticalDivider(color: Colors.grey[200],),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,


                                children: [

                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    width:  ResponsiveWrapper.of(context).scaledHeight*0.1,

                                      height:30,

                                      decoration: BoxDecoration(
                                        color: Colors.cyanAccent,
                                          borderRadius:
                                          BorderRadius.circular(20),
                                         ),

                                      child: GradientButton(

                                        borderRadius: 20,
                                        buttonText: "Popular article",
                                        textColor: Colors.white,

                                        )
                                  ),
                                ),

                                Padding(
                                  padding:  EdgeInsets.all( ResponsiveWrapper.of(context).scaledHeight/8*0.1),
                                  child: Text("How Eccounts works",style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),),
                                ),
                                Padding(
                                  padding:  EdgeInsets.only(bottom:  ResponsiveWrapper.of(context).scaledHeight/8*0.1,left:ResponsiveWrapper.of(context).scaledHeight/8*0.1),
                                  child: Text("${DateFormat.yMd().format(DateTime.now())}"),
                                ),
                                Padding(
                                  padding:  EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).scaledHeight/8*0.1),
                                  child: Container(



                                    child: Text(" Building a business can be tough, but starting a company should be simple. If you can register a domain name for your company in a few minutes then why not your company too?That’s why Eccount was created. Unicount is the simplest way to register an Irish company. It’s also the simplest way to start a paperless EU company from anywhere in the world. It takes just five minutes. Eccount is used by citizens and residents of Ireland, but also a growing number of people around the world because all you need is an digital ID, which can be obtained by citizens of othercountries living outside through e-Residency."

                                    ,style: TextStyles.body2.copyWith(color: Colors.grey),
                                   textAlign: TextAlign.start,
                                   maxLines: 6,
                                      overflow: TextOverflow.ellipsis,


                                    ),
                                  ),
                                ),
                                  Expanded(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                          width: 100,
                                          height:30,

                                          decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(10),
                                              gradient: LinearGradient(colors: [
                                                Colors.deepPurpleAccent,
                                                Colors.purple
                                              ])),

                                          child: GradientButton(buttonText: "READ MORE",)
                                      ),
                                    ),
                                  ),
                              ],),
                            )
                          ],),
                        ),



                      height: ResponsiveWrapper.of(context).scaledHeight*0.3,


                      ),
                    ),







                    ResponsiveRowColumn(


                      layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                          ? ResponsiveRowColumnType.COLUMN
                          : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth/2*0.2 , vertical: 30),
                      columnPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth*0.1,vertical: 50),
                      columnSpacing: 50,
                      rowSpacing: 50,
                      children: [
                        ResponsiveRowColumnItem(




                          child: AddReadMoreCard(


                            imageUrl: "assets/0.png",
                            title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",),


                          rowFit: FlexFit.tight,

                        ),
                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,



                            child: AddReadMoreCard(

                              imageUrl: "assets/mqdefault.jpeg",
                              title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",)),

                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,


                            child: AddReadMoreCard(

                              imageUrl: "assets/2.png",
                              title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",))

                      ],
                    ),
                    ResponsiveRowColumn(


                      layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                          ? ResponsiveRowColumnType.COLUMN
                          : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth/2*0.2 , vertical: 80),
                      columnPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth*0.1,vertical: 20),
                      columnSpacing: 50,
                      rowSpacing: 50,
                      children: [
                        ResponsiveRowColumnItem(




                          child: AddReadMoreCard(


                            imageUrl: "assets/0.png",
                            title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",),


                          rowFit: FlexFit.tight,

                        ),
                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,



                            child: AddReadMoreCard(

                              imageUrl: "assets/mqdefault.jpeg",
                              title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",)),

                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,


                            child: AddReadMoreCard(

                              imageUrl: "assets/2.png",
                              title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",))

                      ],
                    ),
                    ResponsiveRowColumn(


                      layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                          ? ResponsiveRowColumnType.COLUMN
                          : ResponsiveRowColumnType.ROW,
                      rowCrossAxisAlignment: CrossAxisAlignment.start,
                      columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      rowMainAxisAlignment: MainAxisAlignment.center,
                      columnCrossAxisAlignment: CrossAxisAlignment.center,
                      columnMainAxisSize: MainAxisSize.max,
                      rowPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth/2*0.2 , vertical: 80),
                      columnPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth*0.1,vertical: 20),
                      columnSpacing: 50,
                      rowSpacing: 50,
                      children: [
                        ResponsiveRowColumnItem(




                          child: AddReadMoreCard(


                            imageUrl: "assets/0.png",
                            title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",),


                          rowFit: FlexFit.tight,

                        ),
                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,



                            child: AddReadMoreCard(

                              imageUrl: "assets/mqdefault.jpeg",
                              title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",)),

                        ResponsiveRowColumnItem(

                            rowFit: FlexFit.tight,


                            child: AddReadMoreCard(

                              imageUrl: "assets/2.png",
                              title: "How Eccounts works",body:"We pay the state fee of \$190 to get your company registered for the next working day." ,btnText: "READ MORE",))

                      ],
                    ),
                    Padding(
                      padding:  EdgeInsets.only(bottom: ResponsiveWrapper.of(context).screenWidth*0.1),
                      child: Container(



                          decoration: BoxDecoration(color: Colors.cyan.shade300,borderRadius: BorderRadius.circular(10)),

                          child: TextButton(child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("START A COMPANY",style: TextStyles.body1.copyWith(color: Colors.white),),
                          ))),
                    ),





                  ],
                ),
              ),

            ],
          ),

          Footer(textStyle: TextStyles.body1.copyWith(color: Colors.white),)

        ],
      ),


    );
  }




}
class AddReadMoreCard extends StatelessWidget {
  String title;
  String body;
  String btnText;
  String imageUrl;
  AddReadMoreCard({Key key,this.title,this.body,this.btnText,this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,

      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        height: 400,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,

          children: [
            Expanded(child: Container(


                decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(imageUrl)),

        borderRadius: BorderRadius.only(
            topRight:Radius.circular(10),
            topLeft: Radius.circular(10))),
                
                )),
            Padding(
              padding:  EdgeInsets.only(left: ResponsiveWrapper.of(context).screenWidth/8*0.1,top: ResponsiveWrapper.of(context).screenWidth/8*0.1),
              child: Text(title,style: TextStyles.body1.copyWith(color: Colors.black,fontWeight: FontWeight.bold),),
            ),

            Padding(
              padding:  EdgeInsets.all( ResponsiveWrapper.of(context).screenWidth/8*0.1),
              child: Text("${DateFormat.yMd().format(DateTime.now())}"),
            ),
            Padding(
              padding:  EdgeInsets.symmetric(horizontal: ResponsiveWrapper.of(context).screenWidth/8*0.1),
              child: Text("Building a business can be tough, but starting a company should be simple. If you can register a domain name for your company in a few minutes then why not your company too?.",style: TextStyles.body2.copyWith(color: Colors.grey,fontWeight: FontWeight.normal)

                ,textAlign: TextAlign.start,overflow: TextOverflow.ellipsis,maxLines: 3,
              ),
            ),

            Align(
              alignment: Alignment.center,
              child: Padding(
                padding:  EdgeInsets.all( ResponsiveWrapper.of(context).screenWidth/8*0.1),
                child: Container(
                    width: 100,
                    height:30,
                    decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.circular(10),
                        gradient: LinearGradient(colors: [
                          Colors.deepPurpleAccent,
                          Colors.purple
                        ])),

                    child: GradientButton(buttonText: btnText,),),
              ),
            )




          ],),
      ),
  );
  }
}
