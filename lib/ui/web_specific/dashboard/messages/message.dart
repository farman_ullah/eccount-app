import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/chart.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/company_formation.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/myfillings/myfillings_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/search/searchview_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:timelines/timelines.dart';


class Messages extends StatelessWidget {
  const Messages({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    TabController _tabController;
    return Scaffold(

      floatingActionButton:     FloatingActionButton(

        elevation: 0.0,

        backgroundColor: Colors.grey[200],
        isExtended: true,


        child: Container(
            width: 50,
            height: 50,

            decoration: BoxDecoration(

                shape: BoxShape.circle,

                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

            child: Icon(Icons.print,color: Colors.white,)),),
      body: GetBuilder<MyFillingsController>(
        builder: (controller) => LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Container(
              color: Colors.grey[200],
              width: constraints.maxWidth,
              height: constraints.maxHeight,
              child: Stack(
                alignment: Alignment.topCenter,
                children: [

                  Container(
                    color: Colors.grey[200],
                    child: Container(
                      width: Get.width,

                      child: ListTile(
                        trailing: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle),
                          child: Image.asset(
                            "assets/profile.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      color: Colors.white,
                      height: 50,
                    ),
                  ),

                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,

                      children: [




                        Padding(
                          padding:  EdgeInsets.symmetric(vertical: constraints.maxHeight/2*0.1),
                          child: Container(
                              child: Text("Messages",style: TextStyles.subTitle.copyWith(color: Colors.black),),
                             ),
                        ),



                        ResponsiveRowColumn(

                          layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                              ? ResponsiveRowColumnType.COLUMN
                              : ResponsiveRowColumnType.ROW,
                          rowCrossAxisAlignment: CrossAxisAlignment.start,
                          columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          rowMainAxisAlignment: MainAxisAlignment.center,
                          columnCrossAxisAlignment: CrossAxisAlignment.center,
                          columnMainAxisSize: MainAxisSize.max,
                     //     rowPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth/2*0.2 , vertical: 30),
                          columnPadding: EdgeInsets.symmetric(horizontal:ResponsiveWrapper.of(context).screenWidth*0.1,vertical: 50),
                          columnSpacing: 50,
                          rowSpacing: 50,
                     
                          children: [
                            ResponsiveRowColumnItem(
                              child: Container(



                                child:
                              Column(

                                children: [
TxtField(hintTxt: "Search here...",fillColor: Colors.white,),
                                AddCard(),
                                AddCard(),
                                AddCard(),
                                AddCard(),
                                AddCard(),
                                AddCard(),

                              ],),

                              width: ResponsiveWrapper.of(context).isSmallerThan(TABLET)?constraints.maxWidth:constraints.maxWidth*0.3,


                              ),
                            ),
                            ResponsiveRowColumnItem(



                              child: Container(
                                width:ResponsiveWrapper.of(context).isSmallerThan(TABLET)?constraints.maxWidth: constraints.maxWidth * 0.5,
                                height: ResponsiveWrapper.of(context).isSmallerThan(TABLET)?constraints.maxWidth:constraints.maxWidth * 0.5,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                 Container(


                                   width: constraints.maxWidth*0.5,
                                 decoration: BoxDecoration(
                                     border: Border.all(color: Colors.grey[200]),

                                     borderRadius: BorderRadius.circular(10,)),
                                   child: Padding(
                                     padding: const EdgeInsets.only(left:10),
                                     child: Column(
                                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [


                                         Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut ante sollicitudin.",style: TextStyles.body1.copyWith(color: Colors.black),),


                                         Text("Reference",style: TextStyles.body2.copyWith(color: Colors.grey),),
                                         Text("Lorem Lpsum",style: TextStyles.body2.copyWith(color: Colors.black),)


                                       ],
                                     ),
                                   ),

                                 ),
                                    Padding(
                                      padding: const EdgeInsets.only(left:10),
                                      child: Container(
                                        width: constraints.maxWidth*0.5 ,height: constraints.maxHeight*0.1,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Lorem ipsum dolor sit amet,",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                            Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                            Text("Regards",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                            Text("Team",style: TextStyles.body2.copyWith(color: Colors.grey))
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left:10),
                                      child: Container(
                                        width: constraints.maxWidth*0.5 ,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Lorem ipsum dolor sit amet,",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                            Text("Lorem ipsum dolor sit amet,",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                            Text("Lorem ipsum dolor sit amet,",style: TextStyles.body2.copyWith(color: Colors.grey)),


                                          ],
                                        ),
                                      ),
                                    ),






                                  ],
                                ),
                              ),
                            ),

                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }




}

class AddCard extends StatelessWidget {
  const AddCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
                    Center(
            child:   ListTile(

                onTap: (){},
                      subtitle: Text("21/09/2021 21:23"),

                      leading: Icon(Icons.email),title:Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")),
          ),

          Container(

            decoration: BoxDecoration(

              borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])
            ),

            width: 5,height: 50,)
        ],
      ),
    );
  }
}

