import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/chart.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/company_formation.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/search/searchview_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:timelines/timelines.dart';

class NewFilingView extends StatelessWidget {
  const NewFilingView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    TabController _tabController;
    return GetBuilder<SearchViewController>(
      builder: (controller) => LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Container(
color: Colors.grey[200],
            width: constraints.maxWidth,
            height: constraints.maxHeight,
            child: Stack(
              alignment: Alignment.topCenter,
              children: [

                Container(
                  color: Colors.grey[200],
                  child: Container(
                    width: Get.width,

                    child: ListTile(
                      trailing: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200], shape: BoxShape.circle),
                        child: Image.asset(
                          "assets/profile.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    color: Colors.white,
                    height: 50,
                  ),
                ),

                Column(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: [




                    Padding(
                      padding:  EdgeInsets.symmetric(vertical: 30),
                      child: Text(
                        "New filings by selecting the form",
                        style: TextStyles.h2,
                      ),
                    ),



                    Container(
                      width: constraints.maxWidth*0.9,
                      height: constraints.maxWidth * 0.5,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30, right: 30),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              "Select From",
                              style: TextStyles.h2,
                            ),


                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                    child: Image.asset(
                                      "assets/0.png",
                                    ),
                                    height: constraints.maxWidth * 0.1 - 10,
                                  ),
                                  Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        "Company Formation",
                                        style: TextStyles.h3,
                                      ),
                                      Text("Start your business now.",
                                          style: TextStyles.body1
                                              .copyWith(color: Colors.grey)),
                                    ],
                                  ),
                                  Flexible(
                                    child: Container(
                                        width: constraints.maxWidth * 0.1,
                                        height: 30,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(10),
                                            gradient: LinearGradient(colors: [
                                              Colors.deepPurpleAccent,
                                              Colors.purple
                                            ])),
                                        child: GradientButton(
                                          buttonText: "Start",
                                          onPress: () {
                                            Get.back();
                                          },
                                        )),
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              height: constraints.maxWidth * 0.1,
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                    child: Image.asset(
                                      "assets/1.png",
                                    ),
                                    height: constraints.maxWidth * 0.1 - 10,
                                  ),
                                  Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        "Business name registration",
                                        style: TextStyles.h3,
                                      ),
                                      Text("Get your business registered today.",
                                          style: TextStyles.body1
                                              .copyWith(color: Colors.grey)),
                                    ],
                                  ),
                                  Flexible(
                                    child: Container(
                                        width: constraints.maxWidth * 0.1,
                                        height: 30,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(10),
                                            gradient: LinearGradient(colors: [
                                              Colors.deepPurpleAccent,
                                              Colors.purple
                                            ])),
                                        child: GradientButton(
                                          buttonText: "Start",
                                          onPress: () {},
                                        )),
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              height: constraints.maxWidth * 0.1,
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  SizedBox(
                                    child: Image.asset(
                                      "assets/2.png",
                                    ),
                                    height: constraints.maxWidth * 0.1 - 10,
                                  ),
                                  Column(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        "VAT registration",
                                        style: TextStyles.h3,
                                      ),
                                      Text(
                                          "List your business with the government.",
                                          style: TextStyles.body1
                                              .copyWith(color: Colors.grey)),
                                    ],
                                  ),
                                  Flexible(
                                    child: Container(
                                        width: constraints.maxWidth * 0.1,
                                        height: 30,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(10),
                                            gradient: LinearGradient(colors: [
                                              Colors.deepPurpleAccent,
                                              Colors.purple
                                            ])),
                                        child: GradientButton(
                                          buttonText: "Start",
                                          onPress: () {},
                                        )),
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              height: constraints.maxWidth * 0.1,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
