import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/chart.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/company_formation.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/myfillings/myfillings_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/search/searchview_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:timelines/timelines.dart';


class TransactionView extends StatelessWidget {
  const TransactionView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    TabController _tabController;
    return Scaffold(


      body: GetBuilder<MyFillingsController>(
        builder: (controller) => LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Container(
              color: Colors.grey[200],
              width: constraints.maxWidth,
              height: constraints.maxHeight,
              child: Stack(
                alignment: Alignment.topCenter,
                children: [

                  Container(
                    color: Colors.grey[200],
                    child: Container(
                      width: Get.width,

                      child: ListTile(
                        trailing: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle),
                          child: Image.asset(
                            "assets/profile.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      color: Colors.white,
                      height: 50,
                    ),
                  ),

                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: [




                      Padding(
                        padding:  EdgeInsets.symmetric(vertical: constraints.maxHeight/2*0.1),
                        child: Container(
                          child: Text("Transactions",style: TextStyles.subTitle.copyWith(color: Colors.black),),
                        ),
                      ),



                      Container(

                          width:ResponsiveWrapper.of(context).isSmallerThan(TABLET)? constraints.maxWidth: constraints.maxWidth*0.8,
                          height: ResponsiveWrapper.of(context).isSmallerThan(TABLET)?constraints.maxWidth*0.7: constraints.maxWidth*0.4,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [


                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text("Search for filings",style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),),
                                  Container(

                                    child: Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,

                                        children: [



                          Padding(
                              padding:  EdgeInsets.all(constraints.maxWidth/6*0.1),
                              child: Text("FROM",style: TextStyles.body1.copyWith(color: Colors.grey),),
                          ),

                          Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: Container(

                                        decoration: BoxDecoration(
                                            boxShadow: [
                                              BoxShadow(
                                                spreadRadius: 2,
                                                color: Colors.grey[200],
                                                offset: Offset(0.8, 1),
                                                blurRadius: 1,
                                              )
                                            ],
                                            borderRadius: BorderRadius.circular(15),

                                         ),

                                        child: TxtField(
                                          fillColor: Colors.white,

                                          suffixIcon: Icon(Icons.calendar_today),)),
                          ),
                                        Padding(
                                          padding:  EdgeInsets.all(constraints.maxWidth/6*0.1),

                                          child: Text("TO",style: TextStyles.body1.copyWith(color: Colors.grey),),
                                        ),

                                          Container(

                                              decoration: BoxDecoration(
                                                boxShadow: [
                                                  BoxShadow(
                                                    spreadRadius: 2,
                                                    color: Colors.grey[200],
                                                    offset: Offset(0.8, 1),
                                                    blurRadius: 1,
                                                  )
                                                ],
                                                borderRadius: BorderRadius.circular(15),

                                              ),

                                              child: TxtField(
                                                fillColor: Colors.white,

                                                suffixIcon: Icon(Icons.calendar_today),)),


                                          Padding(
                                            padding:  EdgeInsets.all(constraints.maxWidth/6*0.1),

                                            child: Text("TYPE",style: TextStyles.body1.copyWith(color: Colors.grey),),
                                          ),

                                          Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.symmetric(horizontal: 10),
                                              child: Container(

                                                  decoration: BoxDecoration(
                                                    boxShadow: [
                                                      BoxShadow(
                                                        spreadRadius: 2,
                                                        color: Colors.grey[200],
                                                        offset: Offset(0.8, 1),
                                                        blurRadius: 1,
                                                      )
                                                    ],
                                                    borderRadius: BorderRadius.circular(15),

                                                  ),

                                                  child: TxtField(
                                                    fillColor: Colors.white,

                                                    suffixIcon: Icon(Icons.calendar_today),)),
                                            ),
                                          ),






                          ],
                        ),
                                    ),
                      ),

                                  Container(
                                    width: constraints.maxWidth*0.1,


                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(15),

                                          border: Border.all(color: Colors.deepPurple)),

                                      child: TextButton(onPressed: (){}, child: Text("Clear")))
                                ],
                              ),
                            ),

                            VerticalDivider(color: Colors.grey,),
                            Flexible(

                                flex: 2,
                                child: Text("No Transaction Found",style: TextStyles.title1.copyWith(color: Colors.grey),))
                      ]
                      ))
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }




}

class AddCard extends StatelessWidget {
  const AddCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          Center(
            child:   ListTile(

                onTap: (){},
                subtitle: Text("21/09/2021 21:23"),

                leading: Icon(Icons.email),title:Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")),
          ),

          Container(

            decoration: BoxDecoration(

                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])
            ),

            width: 5,height: 50,)
        ],
      ),
    );
  }
}

