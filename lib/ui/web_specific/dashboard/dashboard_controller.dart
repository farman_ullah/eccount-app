
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/ui/order/orderlist/orderlist_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyname_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/directorslist_view.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:eccount/ui/web_specific/dashboard/help/help_view.dart';
import 'package:eccount/ui/web_specific/dashboard/messages/message.dart';
import 'package:eccount/ui/web_specific/dashboard/orders/orders_view.dart';
import 'package:eccount/ui/web_specific/dashboard/search/search_view.dart';
import 'package:eccount/ui/web_specific/dashboard/transactions/transaction_view.dart';
import 'package:eccount/ui/web_specific/homepage/homepage.dart';

import 'package:eccount/ui/web_specific/widgets/filingpopup_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:get/get.dart';

import 'dashFirst/welcome.dart';
import 'dashFirst/welcome_controller.dart';
import 'myfillings/myfiling_view.dart';
import 'newfiling/newfiling_view.dart';

class WebDashboardController extends GetxController{
  int selectedIndex=0;
  List<Widget> pages=[
    WelcomeScreen(),
SearchView(),
  NewFilingView(),
    MyFiling(),
    Messages(),
    OrdersView(),
    TransactionView(),
    HelpView(),






  ];

  changeIndex(int index){


    // if(index==2){
    //   showFilingPopUp();
    //   return;
    // }



    selectedIndex=index;

    update();



  }


  showFilingPopUp(){
    SmartDialog.show(
      isPenetrateTemp: true,
        widget:FilingPopupView(maxWidth:500,),
    alignmentTemp: Alignment.centerRight,
    clickBgDismissTemp: true);
  }
}