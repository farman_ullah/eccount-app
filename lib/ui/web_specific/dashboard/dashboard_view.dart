import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

class WebDashboardView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<WebDashboardController>(builder: (controller){
        return Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
         ResponsiveWrapper.of(context).screenWidth>850?
            NavigationRail(

              leading: SizedBox(
                  height: 60,
                  child: Image.asset("assets/logo.png")),
              extended: true,
              elevation: 10,
              selectedLabelTextStyle: TextStyles.h3,
              destinations: [
              NavigationRailDestination(icon:Image.asset("assets/Dashboard-Icon.png"), label: AutoSizeText("Dashboard")),
              NavigationRailDestination(icon: Icon(Icons.search), label: AutoSizeText("Search")),
              NavigationRailDestination(icon: Icon(Icons.description), label: AutoSizeText("New Filing")),
                NavigationRailDestination(icon: Icon(Icons.folder), label: AutoSizeText("My Filings")),
                NavigationRailDestination(icon: Icon(Icons.message), label: AutoSizeText("Messages")),

                NavigationRailDestination(icon: Icon(Icons.message), label: AutoSizeText("Order")),
                NavigationRailDestination(icon: Icon(Icons.message), label: AutoSizeText("Transactions")),

                NavigationRailDestination(icon: Icon(Icons.help), label: AutoSizeText("Help")),
                // ConsentSignature(),
                // CompanySharedDetails(),
                // AddSubscriberView(),
                //
                // DeclarationView(),
                // VerificationView(),
                // ReviewConfirm(),
                // VerificationView(),
                // WebPaymentDetailView(),
                // CreditCardDetails(),
                // AllDoneView(),
                // SearchView(),

              ], selectedIndex: controller.selectedIndex,
            onDestinationSelected: controller.changeIndex,
            ):Container(),
            Expanded(child: IndexedStack(
              children: controller.pages,
              index: controller.selectedIndex,
            )),

          ],
        );
      },),
    );
  }
}
