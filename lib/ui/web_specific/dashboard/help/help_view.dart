import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/chart.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/company_formation.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/search/searchview_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:timelines/timelines.dart';

class HelpView extends StatelessWidget {

  const HelpView({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context,BoxConstraints constraints){



          return
            Container(

              color: Colors.grey[200],
              child: Column(

                mainAxisAlignment: MainAxisAlignment.center,
                children: [



                  Container(




                    width: Get.width,
                    child:ListTile(trailing:   Container(


                      decoration: BoxDecoration(color: Colors.red,shape: BoxShape.circle),

                      child:Image.asset("assets/person.png",fit: BoxFit.contain,) ,),),



                    color: Colors.white,height: 50,),
                  Padding(
                    padding:  EdgeInsets.only(top: constraints.maxHeight*0.1),
                    child: Text("Help",style: TextStyles.h2.copyWith(color: Colors.black)),
                  ),


                  Expanded(
                    child: Padding(
                      padding:  EdgeInsets.all(constraints.maxWidth*0.1),
                      child: Container(

                        decoration: BoxDecoration(

                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        width: constraints.maxWidth,




                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                          children: [
                            Text("Contact Us",style: TextStyles.h2.copyWith(color: Colors.black),),
                         addContactSource(title:"Email",subtitle: "Please E-mail: loremipsum@gmail.com If you are unable to find an answer to your question by searching our website, you can e-mail our Information Office. Please enter nature of your query in the subject box.",count: "1"),
                            addContactSource(title:"Email",subtitle: "Please Telephone: 4578413224 From 10am to 12.30pm and from 2.30pm to 4pm, these numbers will connect you to our Information Office. Many questions can be quickly answered here. However, where necessary, you will be put through to a member of staff who deals with a particular aspect of the work of the Office. ",count: "2"),
                            addContactSource(title:"Email",subtitle: "All the information you need to help you meet your filing obligations with the CRO is available here. However, if you are having difficulties searching for information on this website or are encountering technical difficulties, please e-mail us at loremipsum For all other non-techncial queries, you should e-mail loremipsum.",count: "3"),
                            addContactSource(title:"Email",subtitle: "(For receipt of Postal Submissions only) Eccount is not a public office. Companies Registration Office",count: "4"),


                          ],
                        ),


                      ),
                    ),
                  ),



                ],),


            );

        },

    );
  }

  addContactSource({String count,String title, String subtitle}){

   return ListTile(
      title: Text(title,style: TextStyles.title1.copyWith(color: Colors.black),),
      subtitle: Text(subtitle,style: TextStyles.body1.copyWith(color: Colors.grey),),

      leading: StepCircle(value: count,),);


  }
}
