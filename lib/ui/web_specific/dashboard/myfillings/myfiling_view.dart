
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

import 'myfillings_controller.dart';

class MyFiling extends StatelessWidget {
  const MyFiling({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    TabController _tabController;
    return GetBuilder<MyFillingsController>(
      builder: (controller) => LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Container(
            color: Colors.grey[200],
            width: constraints.maxWidth,
            height: constraints.maxHeight,
            child: Stack(
              alignment: Alignment.topCenter,
              children: [

                Container(
                  color: Colors.grey[200],
                  child: Container(
                    width: Get.width,

                    child: ListTile(
                      trailing: Container(
                        decoration: BoxDecoration(
                          color: Colors.grey[200],
                             shape: BoxShape.circle),
                        child: Image.asset(
                          "assets/profile.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    color: Colors.white,
                    height: 50,
                  ),
                ),

                Column(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: [




                    Padding(
                      padding:  EdgeInsets.all(constraints.maxHeight/2*0.1),
                      child: Container(
                        width: constraints.maxWidth,

                    
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Spacer(flex: 1,),
                            Padding(
                              padding:  EdgeInsets.only(left:constraints.maxWidth/2*0.2),
                              child: Text(
                                "MY Fillings",
                                style: TextStyles.h2,
                              ),
                            ),
                            Spacer(flex: 1,),

                            Container(

                                height: 30,
                                decoration: BoxDecoration(
                                    borderRadius:
                                    BorderRadius.circular(10),
                                    gradient: LinearGradient(colors: [
                                      Colors.deepPurpleAccent,
                                      Colors.purple
                                    ])),
                                child: GradientButton(
                                  buttonText: "Start new filing",
                                  onPress: () {

                                  },
                                ))


                          ],
                        ),
                      ),
                    ),



                    Container(
                      width: constraints.maxWidth*0.95,
                      height: constraints.maxWidth * 0.4,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30, right: 30),
                        child: DefaultTabController(

                          length: 7,
                          initialIndex: 0,
                          child: Column(
                            children: [


                              TabBar(

onTap: (index){
  print(index.toString());
},

          labelColor: const Color(0xff525c6e),
          unselectedLabelColor: const Color(0xffacb3bf),
          indicatorPadding: EdgeInsets.all(0.0),
          indicatorWeight: 4.0,
          labelPadding: EdgeInsets.only(left: 0.0, right: 0.0),
          indicator: ShapeDecoration(
          shape: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.transparent, width: 0, style: BorderStyle.solid)),
          gradient:  LinearGradient(colors: [
            Colors.deepPurpleAccent,
            Colors.purple
          ])),



                                tabs: [
                                  Container(


                                    height: 40,
                                    alignment: Alignment.center,
                                    color: Colors.white,




                                    child:  Text("Drafts",style: TextStyles.body1.copyWith(color: Colors.black)),
                                  ),


                                  Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    color: Colors.white,
                                    child:  Text("Signatures",style: TextStyles.body1.copyWith(color: Colors.black)),
                                  ),
                                  Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    color: Colors.white,
                                    child:       Text("Payment",style: TextStyles.body1.copyWith(color: Colors.black)),
                                  ),
                                  Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    color: Colors.white,
                                    child:
                                    Text("Submitted",style: TextStyles.body1.copyWith(color: Colors.black)),
                                  ),
                                  Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    color: Colors.white,
                                    child:    Text("Returned",style: TextStyles.body1.copyWith(color: Colors.black)),
                                  ),
                                  Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    color: Colors.white,
                                    child:  Text("Registered",style: TextStyles.body1.copyWith(color: Colors.black)),
                                  ),

                                  Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    color: Colors.white,
                                    child:  Text("Registered",style: TextStyles.body1.copyWith(color: Colors.black)),
                                  ),









                                ],
                              ),
                            Expanded(
                              child: TabBarView(
                                children:controller.tabsView
                              ),
                            )  ]
                          )

                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
  
  
  
  
}
class DraftsTabView extends StatelessWidget {
  const DraftsTabView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

      child: Row(

mainAxisAlignment: MainAxisAlignment.start,

        children: [


          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: [
              Align(


                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0.0,vertical: 10),
                    child: Text("Search for filings",style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),),
                  ),alignment: Alignment.center,),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("FROM",style: TextStyles.body1.copyWith(color: Colors.grey),),
        ),
            TxtField(hintTxt: "ddd/mmm/yyyy",suffixIcon: Icon(Icons.calendar_today),),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("To",style: TextStyles.body1.copyWith(color: Colors.grey)),
            ),
            TxtField(hintTxt: "ddd/mmm/yyyy",suffixIcon: Icon(Icons.calendar_today)),
Padding(
  padding: const EdgeInsets.all(8.0),
  child:   Text("FILTER",style: TextStyles.body1.copyWith(color: Colors.grey)),
),
            TxtField(hintTxt: "ddd/mmm/yyyy",suffixIcon: Icon(Icons.calendar_today)),
          ],),),



          VerticalDivider(color: Colors.grey,),
          Expanded(
            flex: 2,


            child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,

            children: [

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 10),
                child: Text("Found 2 results",style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),),
              ),
           Container(
             height: 100,

             child: SingleChildScrollView(
               scrollDirection: Axis.horizontal,
               child: Card(
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: [Column(

                   crossAxisAlignment: CrossAxisAlignment.start,
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: [

                   Text("Reference",style: TextStyles.body1.copyWith(color: Colors.grey),),
                   Text("Lorem Lpsum")
                 ],),

                   Column(
                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: [

                     Text("Filing",style: TextStyles.body1.copyWith(color: Colors.grey)),
                     Text("Lorem Lpsum")
                   ],),
                   Column(
                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                     crossAxisAlignment: CrossAxisAlignment.start,

                     children: [

                     Text("Name",style: TextStyles.body1.copyWith(color: Colors.grey)),
                     Text("Lorem Lpsum")
                   ],),
                     Column(
                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                       crossAxisAlignment: CrossAxisAlignment.start,

                       children: [

                         Text("Processed on",style: TextStyles.body1.copyWith(color: Colors.grey)),
                         Text("Lorem Lpsum")
                       ],),
                   Container(
                       height: 30,

                       decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),

                           gradient: LinearGradient(colors: [
                             Colors.deepPurpleAccent,
                             Colors.purple
                           ])),
                       child: GradientButton(buttonText: "Download",))
                 ],),
               ),
             ),
           ),


              SizedBox(height:ResponsiveWrapper.of(context).screenWidth/4*0.1 ,),
              Container(
                height: 100,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Card(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [Column(

                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [

                          Text("Reference",style: TextStyles.body1.copyWith(color: Colors.grey),),
                          Text("Lorem Lpsum")
                        ],),

                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            Text("Filing",style: TextStyles.body1.copyWith(color: Colors.grey)),
                            Text("Lorem Lpsum")
                          ],),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                          crossAxisAlignment: CrossAxisAlignment.start,

                          children: [

                            Text("Name",style: TextStyles.body1.copyWith(color: Colors.grey)),
                            Text("Lorem Lpsum")
                          ],),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                          crossAxisAlignment: CrossAxisAlignment.start,

                          children: [

                            Text("Processed on",style: TextStyles.body1.copyWith(color: Colors.grey)),
                            Text("Lorem Lpsum")
                          ],),
                        Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),

                                gradient: LinearGradient(colors: [
                                  Colors.deepPurpleAccent,
                                  Colors.purple
                                ])),
                            height: 30,
                            child: GradientButton(buttonText: "Download",))
                      ],),
                  ),
                ),
              ),

            ],),),
        ],
      ),
    );
  }
}
