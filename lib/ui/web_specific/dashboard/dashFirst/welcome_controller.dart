import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class WelcomeScreenController extends GetxController{




final List<Widget> percentIdicators=<Widget>[
      CircularPercentIndicator(
    radius: 60.0,
    lineWidth: 5.0,
    percent:0.4,
    // center:  Text("50%"),
    progressColor: Colors.green,
  ),
  CircularPercentIndicator(
    radius: 60.0,
    lineWidth: 5.0,
    percent:0.7,
    // center:  Text("50%"),
    progressColor: Colors.green,
  ),
  CircularPercentIndicator(
    radius: 60.0,
    lineWidth: 5.0,
    percent:0.9,
    // center:  Text("50%"),
    progressColor: Colors.green,
  ),
  CircularPercentIndicator(
    radius: 60.0,
    lineWidth: 5.0,
    percent:0.3,
    // center:  Text("50%"),
    progressColor: Colors.green,
  )
];


Widget addCard(double width, double height){

  return  Container(


    decoration: BoxDecoration(

        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.circular(10)),

    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [

        SizedBox(height: 50,child: Image.asset("assets/Illustration-Blue.png"),),

        Flexible(
          child: Column(

            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("Search for an Entity",style: TextStyles.h2,),
              Text("Here you have access to data on companies, business names and societies."),





            ],
          ),
        ),

        SizedBox(width: 20,),
        Container(

            width:width*0.1,
            decoration: BoxDecoration(

                borderRadius: BorderRadius.circular(10),

                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

            child: GradientButton(buttonText: "Search",onPress: (){},))
      ],),

    width: width*0.8,height:height*0.5,);
}

@override
  void onInit() {

    super.onInit();
  }

}