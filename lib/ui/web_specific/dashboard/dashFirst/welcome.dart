/*import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/chart.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/company_formation.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:timelines/timelines.dart';

class WelcomeScreen extends StatelessWidget {

  const WelcomeScreen({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GetBuilder<WelcomeScreenController>(
      builder:(controller)=> LayoutBuilder(
        builder: (BuildContext context,BoxConstraints constraints){



          return
            Container(

            color: Colors.grey[200],
            child: Column(children: [

              Container(




                width: Get.width,
                child:ListTile(trailing:   Container(


                  decoration: BoxDecoration(color: Colors.red,shape: BoxShape.circle),

                  child:Image.asset("assets/person.png",fit: BoxFit.contain,) ,),),



                color: Colors.white,height: 50,),



              Expanded(child: Container(
                width: Get.width,

                child:SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,

                    children: [



                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Text("Welcome Back, George",style: TextStyles.h2.copyWith(fontSize: constraints.maxWidth*0.02)),
                      ),

                      SizedBox(height: 10,),
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Text("What would you like to do next?",

                            style: TextStyles.h2.copyWith(fontSize: constraints.maxWidth*0.02,color: Colors.grey,letterSpacing: 0)
                        ),
                      ),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Column(


                            children: [


                              Container(


                                decoration: BoxDecoration(

                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.3),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: Offset(0, 3), // changes position of shadow
                                      ),
                                    ],
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10)),

                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [

                                    SizedBox(height: constraints.maxHeight*0.1,child: Image.asset("assets/Illustration-Blue.png"),),

                                    Flexible(
                                      child: Column(

                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Text("Search for an Entity",style: TextStyles.h2.copyWith(fontSize: constraints.maxWidth*0.02),),
                                          Text("Here you have access to data on companies, business names and societies."),

                                          Container(

                                              decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                borderRadius: BorderRadius.circular(15),
                                                boxShadow: <BoxShadow>[
                                                  BoxShadow(
                                                      color: Colors.grey.shade200,
                                                      blurRadius: 1.0,
                                                      offset: Offset(0.0, 3),
                                                      spreadRadius: 0.7
                                                  )
                                                ],

                                              ),

                                              child: TxtField(

                                                fillColor: Colors.white,
                                                hintTxt: "Start typing to nd an entity, either by name or number.",)),

                                          Container(

                                              width:constraints.maxWidth*0.2,
                                              decoration: BoxDecoration(

                                                  borderRadius: BorderRadius.circular(10),

                                                  gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                              child: GradientButton(buttonText: "Search",onPress: (){},))

                                        ],
                                      ),
                                    ),

                                    SizedBox(width: 20,)
                                  ],),

                                width: constraints.maxWidth*0.6-15,height: constraints.maxHeight*0.2,)

                              ,
                              SizedBox(height: 10,),

                              Container(
                                width:
                                constraints.maxWidth*0.6,

                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,


                                  children: [

                                    Container(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [

                                          Text("File a Form",style: TextStyles.h3,),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                            child: Text(" Here you can le a form to register a new entity.You can also le forms for an existing entity.Please click start to proceed. ",textAlign: TextAlign.center,),
                                          ),
                                          Container(


                                              width:constraints.maxWidth*0.1,
                                              decoration: BoxDecoration(

                                                  borderRadius: BorderRadius.circular(10),

                                                  gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                              child: GradientButton(buttonText: "Start",onPress: (){

//popup a attachments toast
                                                showGeneralDialog(
                                                  barrierLabel: "Label",
                                                  barrierDismissible: true,
                                                  barrierColor: Colors.black.withOpacity(0.5),
                                                  transitionDuration: Duration(milliseconds: 500),
                                                  context: context,
                                                  pageBuilder: (context, anim1, anim2) {
                                                    return Align(
                                                      alignment: Alignment.center,
                                                      child: Container(
                                                        width: constraints.maxWidth*0.8,
                                                        height: constraints.maxHeight*0.5,
                                                    decoration:BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(20)) ,


                                                        child: Padding(
                                                          padding: const EdgeInsets.only(left: 30,right: 30),
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                                                            children: [


Text("Select From",style: TextStyles.h2,),
                                                              Container(


                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                  children: [


                                                                    SizedBox(


                                                                      child: Image.asset("assets/0.png",),

                                                                      height: constraints.maxHeight*0.1-10,
                                                                    ),

                                                                    Column(

                                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                      children: [
                                                                        Text("Company Formation",style: TextStyles.h3,),
                                                                        Text("Start your business now.",style: TextStyles.body1.copyWith(color: Colors.grey)),





                                                                      ],
                                                                    ),


                                                                    Flexible(
                                                                      child: Container(

                                                                          width:constraints.maxWidth*0.1,

                                                                          height: 30,


                                                                          decoration: BoxDecoration(

                                                                              borderRadius: BorderRadius.circular(10),

                                                                              gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                                                          child: GradientButton(buttonText: "Start",onPress: (){




Get.back();
                                                                              },)),
                                                                    )
                                                                  ],),
                                                                decoration: BoxDecoration(
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                        color: Colors.grey.withOpacity(0.3),
                                                                        spreadRadius: 5,
                                                                        blurRadius: 7,
                                                                        offset: Offset(0, 3), // changes position of shadow
                                                                      ),
                                                                    ],
                                                                    color: Colors.white,
                                                                    borderRadius: BorderRadius.circular(10)),
                                                                height: constraints.maxHeight*0.1,


                                                              ),

                                                              Container(


                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                  children: [


                                                                    SizedBox(


                                                                      child: Image.asset("assets/1.png",),

                                                                      height: constraints.maxHeight*0.1-10,
                                                                    ),

                                                                    Column(

                                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                      children: [
                                                                        Text("Business name registration",style: TextStyles.h3,),
                                                                        Text("Get your business registered today.",style: TextStyles.body1.copyWith(color: Colors.grey)),





                                                                      ],
                                                                    ),


                                                                    Flexible(
                                                                      child: Container(

                                                                          width:constraints.maxWidth*0.1,

                                                                          height: 30,


                                                                          decoration: BoxDecoration(

                                                                              borderRadius: BorderRadius.circular(10),

                                                                              gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                                                          child: GradientButton(buttonText: "Search",onPress: (){},)),
                                                                    )
                                                                  ],),
                                                                decoration: BoxDecoration(
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                        color: Colors.grey.withOpacity(0.3),
                                                                        spreadRadius: 5,
                                                                        blurRadius: 7,
                                                                        offset: Offset(0, 3), // changes position of shadow
                                                                      ),
                                                                    ],
                                                                    color: Colors.white,
                                                                    borderRadius: BorderRadius.circular(10)),
                                                                height: constraints.maxHeight*0.1,


                                                              ),

                                                              Container(


                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                  children: [


                                                                    SizedBox(


                                                                      child: Image.asset("assets/2.png",),

                                                                      height: constraints.maxHeight*0.1-10,
                                                                    ),

                                                                    Column(

                                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                      children: [
                                                                        Text("VAT registration",style: TextStyles.h3,),
                                                                        Text("List your business with the government.",style: TextStyles.body1.copyWith(color: Colors.grey)),





                                                                      ],
                                                                    ),


                                                                    Flexible(
                                                                      child: Container(

                                                                          width:constraints.maxWidth*0.1,

                                                                          height: 30,


                                                                          decoration: BoxDecoration(

                                                                              borderRadius: BorderRadius.circular(10),

                                                                              gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                                                          child: GradientButton(buttonText: "Search",onPress: (){},)),
                                                                    )
                                                                  ],),
                                                                decoration: BoxDecoration(
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                        color: Colors.grey.withOpacity(0.3),
                                                                        spreadRadius: 5,
                                                                        blurRadius: 7,
                                                                        offset: Offset(0, 3), // changes position of shadow
                                                                      ),
                                                                    ],
                                                                    color: Colors.white,
                                                                    borderRadius: BorderRadius.circular(10)),
                                                                height: constraints.maxHeight*0.1,


                                                              ),





                                                          ],),
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                  transitionBuilder: (context, anim1, anim2, child) {
                                                    return SlideTransition(
                                                      position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
                                                      child: child,
                                                    );
                                                  },
                                                );


                                              },))



                                        ],),

                                      decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.grey.withOpacity(0.3),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0, 3), // changes position of shadow
                                            ),
                                          ],
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(10)),

                                      width: constraints.maxWidth*0.2-10,height: constraints.maxHeight*0.2,),
                                    Container(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [

                                          Text("Account Statement",style: TextStyles.h3,),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                            child: Text("Click here to order a statement from the previousmonth, listing all of the transactions against your drawdown account, i.e. invoicesand deposits. ",textAlign: TextAlign.center,),
                                          ),
                                          Container(

                                              width:constraints.maxWidth*0.1,
                                              decoration: BoxDecoration(

                                                  borderRadius: BorderRadius.circular(10),

                                                  gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                              child: GradientButton(buttonText: "Order",onPress: (){},))



                                        ],),

                                      decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.grey.withOpacity(0.3),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0, 3), // changes position of shadow
                                            ),
                                          ],
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(10)),

                                      width: constraints.maxWidth*0.2-10,height: constraints.maxHeight*0.2,),
                                    Container(



                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [

                                          Text("Notifications",style: TextStyles.h3,),

                                          Image.asset("assets/logo.png",width: constraints.maxWidth*0.088,),

                                          Text("Notications from the CRO will appear here.",textAlign: TextAlign.center,style: TextStyles.body2.copyWith(color: Colors.grey),)



                                        ],),

                                      decoration: BoxDecoration(
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.grey.withOpacity(0.3),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: Offset(0, 3), // changes position of shadow
                                            ),
                                          ],
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(10)),

                                      width: constraints.maxWidth*0.2-10,height: constraints.maxHeight*0.2,),

                                  ],),
                              )

                            ],
                            crossAxisAlignment: CrossAxisAlignment.center,
                          ),
                          SizedBox(width: 10,),
                          Container(
                            child: Column(children: [

                              Text("Statistics",style: TextStyles.h3),

                              PieChartSample2(),



                              Flexible(child: ListView(children:[
                                ListTile(

                                  trailing: Text("1"),

                                  title: Text("Draft"),
                                  leading:   CircularPercentIndicator(
                                    radius: 40.0,
                                    lineWidth: 8.0,
                                    percent:0.3,
                                    // center:  Text("50%"),
                                    progressColor: Colors.deepPurple,
                                  ),),
                                ListTile(
                                  title: Text("Pending payment"),
                                  trailing: Text("1"),
                                  leading:   CircularPercentIndicator(
                                    radius: 40.0,
                                    lineWidth: 8.0,
                                    percent:0.6,
                                    // center:  Text("50%"),
                                    progressColor: Colors.purple,
                                  ),),
                                ListTile(
                                  title: Text("Pending Signature"),
                                  trailing: Text("1"),
                                  leading:   CircularPercentIndicator(
                                    radius: 40.0,
                                    lineWidth: 8.0,
                                    percent:0.8,
                                    // center:  Text("50%"),
                                    progressColor: Colors.green,
                                  ),),
                                ListTile(
                                  title: Text("Submitted"),
                                  trailing: Text("1"),
                                  leading:   CircularPercentIndicator(
                                    radius: 40.0,
                                    lineWidth: 8.0,
                                    percent:0.7,
                                    // center:  Text("50%"),
                                    progressColor: Colors.grey,
                                  ),),
                                ListTile(leading: Text("Total",style: TextStyles.h3),

                                  trailing: Text("12",style: TextStyles.h3),
                                )

                              ],)),




                            ],),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10)),

                            width: constraints.maxWidth*0.3,height: constraints.maxHeight*0.6,)



                        ],)
                    ],),
                ),



              ))
            ],),


          );

        },
      ),
    );
  }
}
// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key}) : super(key: key);
//
//
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage> {
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("kkkk"),
//       ),
//       body: FixedTimeline.tileBuilder(
//
//
//  theme: TimelineThemeData(
//
//
//    indicatorTheme: IndicatorThemeData(
//
//      color: Colors.red,
//      size: 30
//    )
//
//  ),
//         builder: TimelineTileBuilder.connectedFromStyle(
//
//
//
//           oppositeContentsBuilder: (context,index)=>Container(width: 10,height: 10,color: Colors.red,),
// contentsBuilder: (context,index)=>Container(width: 10,height: 10,color: Colors.red,),
//
//           connectionDirection: ConnectionDirection.after,
//           connectorStyleBuilder: (context, index) {
//             return (index!=4) ? ConnectorStyle.dashedLine : ConnectorStyle.solidLine;
//           },
//
//
//           indicatorStyleBuilder: (context, index) => IndicatorStyle.dot,
//
//
//           itemExtent: 40.0,
//           itemCount: 10,
//         ),
//       )
//     );
//   }
// }
*/
import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/chart.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/company_formation.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:timelines/timelines.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<WelcomeScreenController>(
        builder: (controller) => Stack(
          alignment: Alignment.topCenter,

          children: [




            Container(

              height: ResponsiveWrapper.of(context).screenHeight,
                  color: Colors.grey[200],
                  child: ResponsiveRowColumn(
                    columnCrossAxisAlignment: CrossAxisAlignment.start,

                    
                    layout:ResponsiveRowColumnType.COLUMN ,
                   columnMainAxisAlignment: MainAxisAlignment.start,


                    children:[
                      ResponsiveRowColumnItem(
                        child: Padding(
                          padding:      EdgeInsets.only(left: ResponsiveWrapper.of(context).screenWidth/2 * 0.1,top: ResponsiveWrapper.of(context).screenWidth/2*0.1 ),

    child: Container(
                            width: 300,
                            height: 100,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical:10),
                                  child: Text("Welcome Back, George",style: TextStyles.h3),
                                ),

                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Text("What would you like to do next?",

                                      style: TextStyles.h3.copyWith(color: Colors.grey,letterSpacing: 0)
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      ResponsiveRowColumnItem(
                      child: Expanded(
                        child: ResponsiveRowColumn(
                          layout: ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                              ? ResponsiveRowColumnType.COLUMN
                              : ResponsiveRowColumnType.ROW,
                          rowCrossAxisAlignment: CrossAxisAlignment.start,
                          columnMainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          rowMainAxisAlignment: MainAxisAlignment.start,

                          columnCrossAxisAlignment: CrossAxisAlignment.center,
                          columnMainAxisSize: MainAxisSize.max,


                          rowPadding: EdgeInsets.symmetric(
                              horizontal:
                                  ResponsiveWrapper.of(context).screenWidth / 2 * 0.1,
                              vertical: 10),
                          columnPadding: EdgeInsets.symmetric(
                              horizontal: ResponsiveWrapper.of(context).screenWidth * 0.1,
                              vertical: 2),
                          rowSpacing:ResponsiveWrapper.of(context).screenWidth/2*0.1 ,
                          columnSpacing: 10,
                          children: [


                            ResponsiveRowColumnItem(
                                child: Flexible(
                              child: SingleChildScrollView(
                                child: ResponsiveRowColumn(
                                  layout:
                                      ResponsiveWrapper.of(context).isSmallerThan(TABLET)
                                          ? ResponsiveRowColumnType.COLUMN
                                          : ResponsiveRowColumnType.COLUMN,
                                  children: [


                                    ResponsiveRowColumnItem(
                                      child: Container(

                                        decoration: BoxDecoration(
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.blue.withOpacity(0.3),
                                                spreadRadius: 5,
                                                blurRadius: 7,
                                                offset: Offset(
                                                    0, 3), // changes position of shadow
                                              ),
                                            ],
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(10)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            SizedBox(
                                              height: ResponsiveWrapper.of(context)
                                                      .screenWidth *
                                                  0.1,
                                              child: Image.asset(
                                                  "assets/Illustration-Blue.png"),
                                            ),
                                            Flexible(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Text(
                                                    "Search for an Entity",
                                                    style: TextStyles.h4,
                                                  ),
                                                  Text(
                                                      "Here you have access to data on companies, business names and societies.",style: TextStyles.body1.copyWith(color: Colors.grey),),
                                                  Padding(
                                                    padding: const EdgeInsets.symmetric(
                                                        horizontal: 10),
                                                    child: Container(
                                                        decoration: BoxDecoration(
                                                          shape: BoxShape.rectangle,
                                                          borderRadius:
                                                              BorderRadius.circular(15),
                                                          boxShadow: <BoxShadow>[
                                                            BoxShadow(
                                                                color:
                                                                    Colors.grey.shade200,
                                                                blurRadius: 1.0,
                                                                offset: Offset(0.0, 3),
                                                                spreadRadius: 0.7)
                                                          ],
                                                        ),
                                                        child: TxtField(
                                                          fillColor: Colors.white,
                                                          hintTxt:
                                                              "Start typing to nd an entity, either by name or number.",
                                                        )),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Container(
                                                        width: 100,
                                                        decoration: BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius.circular(10),
                                                            gradient: LinearGradient(
                                                                colors: [
                                                                  Colors.deepPurpleAccent,
                                                                  Colors.purple
                                                                ])),
                                                        child: GradientButton(
                                                          buttonText: "Search",
                                                          onPress: () {},
                                                        )),
                                                  ),
                                                ],
                                              ),
                                            ),

                                          ],
                                        ),
                                      ),
                                    ),
                                    ResponsiveRowColumnItem(child: SizedBox(height: 10,),),
                                    ResponsiveRowColumnItem(
                                      child: Row(
crossAxisAlignment: CrossAxisAlignment.start,

                                        children: [
                                          Expanded(
                                            child: Container(

                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Text(
                                                    "File a Form",
                                                       style:  ResponsiveWrapper.of(context).isSmallerThan(TABLET)?TextStyles.body1.copyWith(fontWeight: FontWeight.bold):TextStyles.h3),

                                                  Text(
                                                    " Here you can le a form to register a new entity.You can also le forms for an existing entity.Please click start to proceed. ",
                                                    textAlign: TextAlign.center,
                                                    overflow: TextOverflow.ellipsis,
                                                    maxLines: 4,


                                                  ),
                                                  Container(
                                                      width: 100,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  10),
                                                          gradient: LinearGradient(
                                                              colors: [
                                                                Colors
                                                                    .deepPurpleAccent,
                                                                Colors.purple
                                                              ])),
                                                      child: GradientButton(
                                                        buttonText: "Start",
                                                        onPress: () {
//popup a attachments toast
                                                          showGeneralDialog(
                                                            barrierLabel: "Label",
                                                            barrierDismissible:
                                                                true,
                                                            barrierColor: Colors
                                                                .black
                                                                .withOpacity(0.5),
                                                            transitionDuration:
                                                                Duration(
                                                                    milliseconds:
                                                                        500),
                                                            context: context,
                                                            pageBuilder: (context,
                                                                anim1, anim2) {
                                                              return Align(
                                                                alignment: Alignment
                                                                    .center,
                                                                child: Container(
                                                                  width: ResponsiveWrapper.of(
                                                                              context)
                                                                          .screenWidth *
                                                                      0.8,
                                                                  height: ResponsiveWrapper.of(
                                                                              context)
                                                                          .screenWidth *
                                                                      0.4,
                                                                  decoration: BoxDecoration(
                                                                      color: Colors
                                                                          .white,
                                                                      borderRadius:
                                                                          BorderRadius
                                                                              .circular(
                                                                                  20)),
                                                                  child: Padding(
                                                                    padding:
                                                                        const EdgeInsets
                                                                                .only(
                                                                            left:
                                                                                30,
                                                                            right:
                                                                                30),
                                                                    child: Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceEvenly,
                                                                      children: [
                                                                        Text(
                                                                          "Select Form",
                                                                          style:
                                                                              TextStyles
                                                                                  .h2,
                                                                        ),
                                                                        Container(
                                                                          child:
                                                                              Row(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.spaceEvenly,
                                                                            children: [
                                                                              SizedBox(
                                                                                child:
                                                                                    Image.asset(
                                                                                  "assets/0.png",
                                                                                ),
                                                                                height:
                                                                                    ResponsiveWrapper.of(context).screenWidth * 0.1 - 10,
                                                                              ),
                                                                              Column(
                                                                                mainAxisAlignment:
                                                                                    MainAxisAlignment.spaceEvenly,
                                                                                children: [
                                                                                  Text(
                                                                                    "Company Formation",
                                                                                    style:

                                                                                     ResponsiveWrapper.of(context).isSmallerThan(TABLET)?
                                                                              TextStyles.body1.copyWith(fontWeight: FontWeight.bold):TextStyles.h4,
                                                                                  ),
                                                                                  Text("Start your business now.", style: TextStyles.body1.copyWith(color: Colors.grey)),
                                                                                ],
                                                                              ),
                                                                              Flexible(
                                                                                child: Container(
                                                                                    width: 100,
                                                                                    height: 30,
                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),
                                                                                    child: GradientButton(
                                                                                      buttonText: "Start",
                                                                                      onPress: () {
                                                                                        Get.back();
                                                                                        Get.to(()=>RegistrationMain(registrationType: RegistrationType.CompanyFormation),preventDuplicates: false,transition: Transition.leftToRight,duration: Duration(seconds: 1));
                                                                                      },
                                                                                    )),
                                                                              )
                                                                            ],
                                                                          ),
                                                                          decoration: BoxDecoration(
                                                                              boxShadow: [
                                                                                BoxShadow(
                                                                                  color: Colors.grey.withOpacity(0.3),
                                                                                  spreadRadius: 5,
                                                                                  blurRadius: 7,
                                                                                  offset: Offset(0, 3), // changes position of shadow
                                                                                ),
                                                                              ],
                                                                              color: Colors
                                                                                  .white,
                                                                              borderRadius:
                                                                                  BorderRadius.circular(10)),
                                                                          height:
                                                                              ResponsiveWrapper.of(context).screenWidth *
                                                                                  0.1,
                                                                        ),
                                                                        Container(
                                                                          child:
                                                                              Row(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.spaceEvenly,
                                                                            children: [
                                                                              SizedBox(
                                                                                child:
                                                                                    Image.asset(
                                                                                  "assets/1.png",
                                                                                ),
                                                                                height:
                                                                                    ResponsiveWrapper.of(context).screenWidth * 0.1 - 10,
                                                                              ),
                                                                              Column(
                                                                                mainAxisAlignment:
                                                                                    MainAxisAlignment.spaceEvenly,
                                                                                children: [
                                                                                  Text(
                                                                                    "Business name registration",
                                                                                      style:   ResponsiveWrapper.of(context).isSmallerThan(TABLET)?
                                                                                      TextStyles.body1.copyWith(fontWeight: FontWeight.bold):TextStyles.h4,
                                                                                  ),
                                                                                  Text("Get your business registered today.", style: TextStyles.body1.copyWith(color: Colors.grey)),
                                                                                ],
                                                                              ),
                                                                              Flexible(
                                                                                child: Container(
                                                                                    width: 100,
                                                                                    height: 30,
                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),
                                                                                    child: GradientButton(
                                                                                      buttonText: "Start",
                                                                                      onPress: () {


                                                                                        Get.to(()=>RegistrationMain(registrationType: RegistrationType.BusinessNameRegistration),preventDuplicates: false,transition: Transition.leftToRight,duration: Duration(seconds: 1));


                                                                                      },
                                                                                    )),
                                                                              )
                                                                            ],
                                                                          ),
                                                                          decoration: BoxDecoration(
                                                                              boxShadow: [
                                                                                BoxShadow(
                                                                                  color: Colors.grey.withOpacity(0.3),
                                                                                  spreadRadius: 5,
                                                                                  blurRadius: 7,
                                                                                  offset: Offset(0, 3), // changes position of shadow
                                                                                ),
                                                                              ],
                                                                              color: Colors
                                                                                  .white,
                                                                              borderRadius:
                                                                                  BorderRadius.circular(10)),
                                                                          height:
                                                                              ResponsiveWrapper.of(context).screenWidth *
                                                                                  0.1,
                                                                        ),
                                                                        Container(
                                                                          child:
                                                                              Row(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.spaceEvenly,
                                                                            children: [
                                                                              SizedBox(
                                                                                child:
                                                                                    Image.asset(
                                                                                  "assets/2.png",
                                                                                ),
                                                                                height:
                                                                                    ResponsiveWrapper.of(context).screenWidth * 0.1 - 10,
                                                                              ),
                                                                              Column(
                                                                                mainAxisAlignment:
                                                                                    MainAxisAlignment.spaceEvenly,
                                                                                children: [
                                                                                  Text(
                                                                                    "VAT registration",
                                                                                    style:  ResponsiveWrapper.of(context).isSmallerThan(TABLET)?
                                                                                    TextStyles.h4.copyWith(fontSize: 13):TextStyles.h4,
                                                                                  ),
                                                                                  Text("List your business with the government.", style: TextStyles.body1.copyWith(color: Colors.grey)),
                                                                                ],
                                                                              ),
                                                                              Flexible(
                                                                                child: Container(
                                                                                    width: 100,
                                                                                    height: 30,
                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),
                                                                                    child: GradientButton(
                                                                                      buttonText: "Start",
                                                                                      onPress: () {



                                                                                        Get.to(()=>RegistrationMain(registrationType: RegistrationType.VatRegistration),preventDuplicates: false,transition: Transition.downToUp,duration: Duration(seconds: 1));



                                                                                      },
                                                                                    )),
                                                                              )
                                                                            ],
                                                                          ),
                                                                          decoration: BoxDecoration(
                                                                              boxShadow: [
                                                                                BoxShadow(
                                                                                  color: Colors.grey.withOpacity(0.3),
                                                                                  spreadRadius: 5,
                                                                                  blurRadius: 7,
                                                                                  offset: Offset(0, 3), // changes position of shadow
                                                                                ),
                                                                              ],
                                                                              color: Colors
                                                                                  .white,
                                                                              borderRadius:
                                                                                  BorderRadius.circular(10)),
                                                                          height:
                                                                              ResponsiveWrapper.of(context).screenWidth *
                                                                                  0.1,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                            transitionBuilder:
                                                                (context, anim1,
                                                                    anim2, child) {
                                                              return SlideTransition(
                                                                position: Tween(
                                                                        begin:
                                                                            Offset(
                                                                                0,
                                                                                1),
                                                                        end: Offset(
                                                                            0, 0))
                                                                    .animate(anim1),
                                                                child: child,
                                                              );
                                                            },
                                                          );
                                                        },
                                                      ))
                                                ],
                                              ),
                                              decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(0.3),
                                                      spreadRadius: 5,
                                                      blurRadius: 7,
                                                      offset: Offset(0,
                                                          3), // changes position of shadow
                                                    ),
                                                  ],
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(10)),
                                              height: ResponsiveWrapper.of(context).isSmallerThan(TABLET)?200:300
                                            ),
                                          ),
                                          ResponsiveRowColumnItem(child: SizedBox(width: 10,)),
                                          Expanded(
                                            child: Container(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Text(
                                                    "Account Statement",
                                                                                     style:  ResponsiveWrapper.of(context).isSmallerThan(TABLET)?TextStyles.body1.copyWith(fontWeight: FontWeight.bold):TextStyles.h3,

    ),
                                                  Text(
                                                    "Click here to order a statement from the previousmonth, listing all of the transactions against your drawdown account, i.e. invoicesand deposits. ",
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 4,

                                                    textAlign: TextAlign.center,
                                                  ),
                                                  Container(
                                                      width: 100,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius.circular(
                                                                  10),
                                                          gradient: LinearGradient(
                                                              colors: [
                                                                Colors
                                                                    .deepPurpleAccent,
                                                                Colors.purple
                                                              ])),
                                                      child: GradientButton(
                                                        buttonText: "Order",
                                                        onPress: () {},
                                                      ))
                                                ],
                                              ),
                                              height:ResponsiveWrapper.of(context).isSmallerThan(TABLET)?200:300,
                                              decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey
                                                          .withOpacity(0.3),
                                                      spreadRadius: 5,
                                                      blurRadius: 7,
                                                      offset: Offset(0,
                                                          3), // changes position of shadow
                                                    ),
                                                  ],
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(10)),
                                            ),
                                          ),
                                          ResponsiveRowColumnItem(child: SizedBox(width: 10,)),


                                          Expanded(
                                            child: Container(

    height:ResponsiveWrapper.of(context).isSmallerThan(TABLET)?200:300,

                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [

                                                  Text("Notifications",  style:  ResponsiveWrapper.of(context).isSmallerThan(TABLET)?TextStyles.body1.copyWith(fontWeight: FontWeight.bold):TextStyles.h3),

                                                  Image.asset("assets/notification.png"),

                                                  Text("Notications from the CRO will appear here.",textAlign: TextAlign.center,


                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyles.body2.copyWith(color: Colors.grey),)



                                                ],),

                                              decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors.grey.withOpacity(0.3),
                                                      spreadRadius: 5,
                                                      blurRadius: 7,
                                                      offset: Offset(0, 3), // changes position of shadow
                                                    ),
                                                  ],
                                                  color: Colors.white,
                                                  borderRadius: BorderRadius.circular(10)),

                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )),

                            ResponsiveRowColumnItem(
                              rowFlex: 1,
                                child: Container(


                                  child: SingleChildScrollView(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text("Statistics", style: TextStyles.h3),
                                        PieChartSample2(),
                                        ListTile(
                                          trailing: Text("1"),
                                          title: Text("Draft"),
                                          leading: CircularPercentIndicator(
                                            radius: 40.0,
                                            lineWidth: 8.0,
                                            percent: 0.3,
                                            // center:  Text("50%"),
                                            progressColor: Colors.deepPurple,
                                          ),
                                        ),
                                        ListTile(
                                          title: Text("Pending payment"),
                                          trailing: Text("1"),
                                          leading: CircularPercentIndicator(
                                            radius: 40.0,
                                            lineWidth: 8.0,
                                            percent: 0.6,
                                            // center:  Text("50%"),
                                            progressColor: Colors.purple,
                                          ),
                                        ),
                                        ListTile(
                                          title: Text("Pending Signature"),
                                          trailing: Text("1"),
                                          leading: CircularPercentIndicator(
                                            radius: 40.0,
                                            lineWidth: 8.0,
                                            percent: 0.8,
                                            // center:  Text("50%"),
                                            progressColor: Colors.green,
                                          ),
                                        ),
                                        ListTile(
                                          title: Text("Submitted"),
                                          trailing: Text("1"),
                                          leading: CircularPercentIndicator(
                                            radius: 40.0,
                                            lineWidth: 8.0,
                                            percent: 0.7,
                                            // center:  Text("50%"),
                                            progressColor: Colors.grey,
                                          ),
                                        ),
                                        ListTile(
                                          leading: Text("Total", style: TextStyles.h3),
                                          trailing: Text("12", style: TextStyles.h3),
                                        ),
                                      ],
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10)),
                                ))
                          ],
                        ),
                      ),
                    ),
                    ]
                  ),
                ),
            ResponsiveRowColumnItem(
              child: Container(




                child:ListTile(trailing:   CircleAvatar(
                  backgroundColor: Colors.grey[200],


                  // decoration: BoxDecoration(color: Colors.white,shape: BoxShape.circle),

                  child:Image.asset("assets/profile.png",fit: BoxFit.contain,) ,),),



                color: Colors.white,height: 50,),
            ),




          ],
        ));
  }
}
