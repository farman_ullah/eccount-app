import 'package:get/get.dart';

class SearchViewController extends GetxController{

  RxBool entity=false.obs;
  RxBool audit=false.obs;

  RxBool rest=false.obs;


  checkEntity(bool val)
  {
    this.entity=val.obs;
    update();
  }
  auditor(bool val)
  {
    this.audit=val.obs;
    update();
  }
  restricted(bool val)
  {
    this.rest=val.obs;
    update();
  }
}