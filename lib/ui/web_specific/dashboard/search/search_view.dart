import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/chart.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_main.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/company_formation.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/search/searchview_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:timelines/timelines.dart';

class SearchView extends StatelessWidget {

  const SearchView({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchViewController>(
      builder:(controller)=> LayoutBuilder(
        builder: (BuildContext context,BoxConstraints constraints){



          return
            Container(

              color: Colors.grey[200],
              child: Column(

                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                Container(




                  width:constraints.maxWidth,
                  child:ListTile(trailing:   Container(


                    decoration: BoxDecoration(color: Colors.grey[200],shape: BoxShape.circle),

                    child:Image.asset("assets/profile.png",fit: BoxFit.contain,) ,),),



                  color: Colors.white,height: 50,),



                Expanded(
                  child: Padding(
                    padding:  EdgeInsets.all(constraints.maxWidth*0.1),
                    child: Container(

                      decoration: BoxDecoration(

                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      width: constraints.maxWidth,



child: Column(
  mainAxisAlignment: MainAxisAlignment.start,

  children: [
        Image.asset("assets/0.png",width: constraints.maxWidth*0.1,),
        Text("Search here",style: TextStyles.subTitle.copyWith(color: Colors.black),),
   Column(
     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
     crossAxisAlignment: CrossAxisAlignment.start,
     children: [

       Padding(
         padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
         child: Text("Register",style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold),),
       ),
       Row(
         mainAxisAlignment: MainAxisAlignment.start,

         children: [




           Padding(
             padding: const EdgeInsets.only(left:20.0),
             child: Checkbox(value: controller.entity.value, onChanged: (val)=>controller.checkEntity(val)),
           ),
           Text("Entity",style: TextStyles.body3.copyWith(fontWeight: FontWeight.bold),),
           SizedBox(width: constraints.maxWidth/2*0.1,),
           Checkbox(value: controller.audit.value, onChanged: (val)=>controller.auditor(val)),

           Text("Auditor",style:TextStyles.body3.copyWith(fontWeight: FontWeight.bold)),
           SizedBox(width: constraints.maxWidth/2*0.1,),
           Checkbox(value: controller.rest.value, onChanged: (val)=>controller.restricted(val)),
           Text("Disqualified/Restricted",style:TextStyles.body3.copyWith(fontWeight: FontWeight.bold))
         ],
       ),
SizedBox(height: 20,),

       Divider(color: Colors.grey,),
       SizedBox(height: 20,),

     ],
   ),
    Container(

      width: constraints.maxWidth,

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
            child: Text("Name search type",style: TextStyles.body1.copyWith(fontWeight: FontWeight.bold)),
          ),
          Row(

            children: [




              Padding(
                padding: const EdgeInsets.only(left:20.0),
                child: Checkbox(value: controller.entity.value, onChanged: (val)=>controller.checkEntity(val)),
              ),
              Text("Contain word",style: TextStyles.body3.copyWith(fontWeight: FontWeight.bold)),   SizedBox(width: constraints.maxWidth/2*0.1,),
              Checkbox(value: controller.audit.value, onChanged: (val)=>controller.auditor(val)),

              Text("Stars with",style: TextStyles.body3.copyWith(fontWeight: FontWeight.bold)),   SizedBox(width: constraints.maxWidth/2*0.1,),
              Checkbox(value: controller.rest.value, onChanged: (val)=>controller.restricted(val)),
              Text("Exach match",style: TextStyles.body3.copyWith(fontWeight: FontWeight.bold))
            ],
          ),

Padding(
  padding:  EdgeInsets.symmetric(vertical: constraints.maxWidth/6*0.1,horizontal: 20),
  child:   Text("ENTITY NAME",style: TextStyles.body1.copyWith(color: Colors.grey),),
),


          Padding(
            padding:  EdgeInsets.only(bottom: constraints.maxHeight/6*0.1,left: 20),
            child: Container(

              width: constraints.maxWidth*0.5,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey.shade200),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TxtField(

                  hintTextColor: Colors.black87,

                  hintTxt:"Lorem Lpsum",


                  // validate: (val){
                  //   if(val.isEmpty){
                  //     return "Name is required";
                  //   }
                  //   return null;
                  // },
                  // onSaved: (val){
                  //   _.companyFormation.presenterDetail.name=val;
                  // },

                  fillColor: Colors.white60,
                )),
          ),

          Padding(
            padding:  EdgeInsets.only(bottom: constraints.maxWidth/6*0.1,left: 20),
            child: Text("ENTITY NUMBER",style: TextStyles.body1.copyWith(color: Colors.grey)),
          ),
          Padding(
            padding:  EdgeInsets.only(bottom: constraints.maxHeight/6*0.1,left: 20),
            child: Container(
                width: constraints.maxWidth*0.5,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey.shade200),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: TxtField(

                  hintTextColor: Colors.black87,

                  hintTxt:"Lorem Lpsum",


                  // validate: (val){
                  //   if(val.isEmpty){
                  //     return "Name is required";
                  //   }
                  //   return null;
                  // },
                  // onSaved: (val){
                  //   _.companyFormation.presenterDetail.name=val;
                  // },

                  fillColor: Colors.white60,
                )),
          ),
        ],
      )


      ,),
    Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [

        TextButton(onPressed: (){}, child: Text("CLEAR",style: TextStyles.subTitle.copyWith(color: Colors.deepPurple),)),

        SizedBox(width: 10,),
        Container(

            width:constraints.maxWidth*0.2,

            height: 50,


            decoration: BoxDecoration(

                borderRadius: BorderRadius.circular(10),

                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

            child: GradientButton(buttonText: "DONE",onPress: (){},)),
      ],
    ),
  ],
),


                    ),
                  ),
                ),


              ],),


            );

        },
      ),
    );
  }
}
