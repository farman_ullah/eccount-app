import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VatDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "2",),
              SizedBox(height: 20,),
              AutoSizeText("VAT DETAILS",style: TextStyles.body2.copyWith(color: Colors.black87),),
              SizedBox(height: 20,),
              AutoSizeText("Please fill in your VAT details",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),
              Row(

                children: [

                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("VAT REQUIRES DATE",style: TextStyles.body2.copyWith(color: Colors.grey),),
                        ),
                        Container(


                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              hintTextColor: Colors.black87,

                              hintTxt: "Lorem Ipsum",


                              fillColor: Colors.white,
                              lblTxt: "Lerem Lspusm",)),
                      ],
                    ),
                  ),
                  SizedBox(width: constraints.maxWidth/2*0.1,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("VATABLE ACTIVITY DESCRIPTION",style: TextStyles.body2.copyWith(color: Colors.grey)),
                        ),
                        Container(

                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              hintTxt: "Lorem Ipsum",
                              hintTextColor: Colors.black87,



                              fillColor: Colors.white,
                              lblTxt: "Lerem Lpsum",)),
                      ],
                    ),
                  ),





                ],),

              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Divider(color: Colors.grey,),
              ),


              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Align(alignment: Alignment.center,

                child: Text("Contract copy details",style: TextStyles.body2.copyWith(fontWeight: FontWeight.bold),),

                ),
              ),

              Flexible(child: Row(

                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: [
                  Text("Are you applying for the cash receipts basis of accounting for goods and services?",style: TextStyles.body1.copyWith(color: Colors.grey)

                    ,overflow: TextOverflow.ellipsis,), Row(
                    children: [
                      Checkbox(value: true, onChanged: (val){}),
                      Text("YES")
                    ],
                  ),
                  Row(
                    children: [
                      Checkbox(value: true, onChanged: (val){}),
                      Text("NO")
                    ],
                  )
                ],
              )),
              Container(

                width: constraints.maxWidth,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Row(

                      children: [

                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("CONTRACT NAME",style: TextStyles.body2.copyWith(color: Colors.grey),),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTextColor: Colors.black87,

                                    hintTxt: "Lorem Ipsum",


                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",)),
                            ],
                          ),
                        ),
                        SizedBox(width: constraints.maxWidth/2*0.1,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("CONTRACTOR NAME",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(

                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,



                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lpsum",)),
                            ],
                          ),
                        ),





                      ],),

                    Row(

                      children: [

                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("CONTRACTOR REGISTRATION NUMBER",style: TextStyles.body2.copyWith(color: Colors.grey),),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTextColor: Colors.black87,

                                    hintTxt: "Lorem Ipsum",


                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",)),
                            ],
                          ),
                        ),
                        SizedBox(width: constraints.maxWidth/2*0.1,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("LOCATION OF SUPPLIES",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(

                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,



                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lpsum",)),
                            ],
                          ),
                        ),





                      ],),
                    Row(

                      children: [

                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("CONTRACT DURATION",style: TextStyles.body2.copyWith(color: Colors.grey),),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTextColor: Colors.black87,

                                    hintTxt: "Lorem Ipsum",


                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",)),
                            ],
                          ),
                        ),
                        SizedBox(width: constraints.maxWidth/2*0.1,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("CONTRACT VALUE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(

                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTxt: "Lorem Ipsum",
                                    hintTextColor: Colors.black87,



                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lpsum",)),
                            ],
                          ),
                        ),





                      ],),

                    Padding(
                      padding:  EdgeInsets.only(top:constraints.maxHeight*0.2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          TextButton(onPressed: (){}, child: Row(children: [
                            Icon(Icons.arrow_back_ios_outlined,),


                            Text("BACK"),
                            SizedBox(width: 20,),
                          ],)),

                          Flexible(
                            child: Container(

                                width:100,

                                height: 30,


                                decoration: BoxDecoration(

                                    borderRadius: BorderRadius.circular(10),

                                    gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                child: GradientButton(buttonText: "NEXT",onPress: (){

                                  final registrationViewModel=Get.find<RegistrationViewModel>();
                                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                                },)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),











            ],
          ),
        ),
      ),

    )
    );
  }
}