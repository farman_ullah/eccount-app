import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/buisnessnameRegistration_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

class VatTypesView extends StatelessWidget {
  const VatTypesView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BuisnessNameRegController>(
      builder:(controller)=> LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
        child: Container(
          width: constraints.maxWidth,
          height: constraints.maxHeight,

          color: Colors.grey[200],
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              StepCircle(value: "1",),
              SizedBox(height: 20,),
              AutoSizeText("VAT TYPE",style: TextStyles.body2.copyWith(color: Colors.black87),),
              SizedBox(height: 20,),
              AutoSizeText("Please select the VAT type",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),




              Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Container(


                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [


                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                          children: [


                            Column(children: [


                              Checkbox(value: controller.vatIndividual.value, onChanged: (val)=>controller.changeVatIndividual(val)),
                              Text("Individual"),

                            ],),

                            Column(

                              children: [


                                Checkbox(value: controller.vatPartnership.value, onChanged: (val)=>controller.changeVatPar(val)),
                                Text("Partnership"),


                              ],),







                          ],),

                        Padding(
                          padding: const EdgeInsets.only(top:40),
                          child: Container(
                              width: 120,
                              height: 30,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),
                              child: GradientButton(
                                buttonText: "CONTINUE",
                                onPress: () {

                                  final registrationViewModel=Get.find<RegistrationViewModel>();
                                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);


                                },
                              )),
                        )

                      ],
                    ),
                  ),
                ],
              ),









            ],
          ),
        ),
      )),
    );
  }
}
