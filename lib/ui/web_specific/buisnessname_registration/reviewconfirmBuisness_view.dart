import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/verificaiton_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ReviewConfirmBuisnessName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<VerificationController>(
      builder:(controller)=> LayoutBuilder(
        builder: (context, constraints) => Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                StepCircle(
                  value: "2",
                ),
                SizedBox(
                  height: 20,
                ),
                AutoSizeText(
                  "REVIEW & CONFIRM",
                  style: TextStyles.body2.copyWith(color: Colors.black87),
                ),
                SizedBox(
                  height: 20,
                ),
                AutoSizeText(
                  "Please review and confirm your application",
                  style: TextStyles.h2,
                ),
                SizedBox(
                  height: 20,
                ),
                // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                SizedBox(
                  height: 20,
                ),







                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(width: constraints.maxWidth*0.98,height: constraints.maxWidth*0.5,

                      child: Column(

                        mainAxisAlignment: MainAxisAlignment.start,

                        children: [

                          Text("Buisness name", style: TextStyles.body1.copyWith(color: Colors.grey),),
                          Text("Lorem Ipsum", style: TextStyles.body1.copyWith(color: Colors.black,fontWeight: FontWeight.bold),),
                          Column(

                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(


                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [

                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("CODE",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                      Text("Lorem Ipsum"),

                                      SizedBox(height: 10,),
                                      Text("PRINCIPAL PLACE OF BUSINESS",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                      Text("Lorem Ipsum"),

                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("NATURE OF BUISNESS",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                      Text("Lorem Ipsum"),
                                      SizedBox(height: 10,),
                                      Text("ADDRESS",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                      Text("Lorem Ipsum"),

                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("EIR CODE",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                      Text("Lorem Ipsum"),
                                      SizedBox(height: 10,),
                                      Text("CITY",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                      Text("Lorem Ipsum"),

                                    ],
                                  )
                                ],),


                            ],
                          ),
                          Divider(color: Colors.grey,),

                          SizedBox(height: 20,),
                          Text("Company founder & board member", style: TextStyles.body1.copyWith(color: Colors.grey),),
                          SizedBox(height: 10,),
                          Text("Lorem Ipsum", style: TextStyles.body1.copyWith(color: Colors.black,fontWeight: FontWeight.bold),),
                          SizedBox(height: 20,),
                          Row(


                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("PHONE NUMBER",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                  Text("+345 987789421314"),

                                ],
                              ),

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("DX NUMBER",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                  Text("Lorem Ipsum"),

                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("COUNTRY",style: TextStyles.body1.copyWith(color: Colors.grey)),
                                  Text("Lorem ipsum dolor sit amet."),

                                ],
                              )
                            ],),
                        ],),


                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(20),




                      ),

                    ),

                  ],
                ),
                Padding(
                  padding:  EdgeInsets.only(top:constraints.maxHeight/2*0.2,bottom:constraints.maxHeight/4*0.2,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      TextButton(onPressed: (){}, child: Row(children: [
                        Icon(Icons.arrow_back_ios_outlined,),


                        Text("BACK"),
                        SizedBox(width: 20,),
                      ],)),

                      Flexible(
                        child: Container(

                            width:100,

                            height: 30,


                            decoration: BoxDecoration(

                                borderRadius: BorderRadius.circular(10),

                                gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                            child: GradientButton(buttonText: "CONFIRM",onPress: (){
                              final registrationViewModel=Get.find<RegistrationViewModel>();
                              registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                            },)),
                      ),
                    ],
                  ),
                ),

              ],
            ),
          ),
          width: constraints.maxWidth,
          height: constraints.maxHeight,
          color: Colors.grey[200],

        ),
      ),
    );
  }
}
