import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/buisnessnameRegistration_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

class BuisnessTypeView extends StatelessWidget {
  const BuisnessTypeView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BuisnessNameRegController>(
      builder:(controller)=> LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
        child: Container(
          width: constraints.maxWidth,
          height: constraints.maxHeight,

          color: Colors.grey[200],
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "1",),
              SizedBox(height: 20,),
              AutoSizeText("BUSINESS TYPE",style: TextStyles.subTitle.copyWith(color: Colors.black87),),
              SizedBox(height: 20,),
              AutoSizeText("What type of Business Name would you like to register?",style: ResponsiveWrapper.of(context).isSmallerThan(TABLET)?
              TextStyles.body1.copyWith(fontWeight: FontWeight.bold):TextStyles.h3,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),




              Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Container(


                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [


                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                          children: [


Column(children: [


   Checkbox(value: controller.individual.value, onChanged: (val)=>controller.changeIndividual(val)),
  Text("Individual"),

],),

                            Column(

                              children: [


                              Checkbox(value: controller.partnership.value, onChanged: (val)=>controller.changePar(val)),
                              Text("Partnership"),


                            ],),

                            Column(children: [


                              Checkbox(value: controller.bodyCorporate.value, onChanged: (val)=>controller.changeBody(val)),
                              Text("Body Corporate"),

                            ],)





                          ],),

                        Padding(
                          padding: const EdgeInsets.only(top:40),
                          child: Container(
                              width: 120,
                              height: 30,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),
                              child: GradientButton(
                                buttonText: "CONTINUE",
                                onPress: () {

                                  final registrationViewModel=Get.find<RegistrationViewModel>();
                                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);


                                },
                              )),
                        )

                   ],
                    ),
                  ),
                ],
              ),









            ],
          ),
        ),
      )),
    );
  }
}
