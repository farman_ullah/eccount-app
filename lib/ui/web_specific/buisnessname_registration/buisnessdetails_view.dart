import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BuisnessDetailsViewWeb extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "2",),
              SizedBox(height: 20,),
              AutoSizeText("BUSINESS TYPE",style: TextStyles.subTitle.copyWith(color: Colors.black),),
              SizedBox(height: 20,),
              AutoSizeText("Please fill in your business details",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),
              Row(

                children: [

                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("BUISNESS NAME",style: TextStyles.body2.copyWith(color: Colors.grey),),
                        ),
                        Container(


                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              hintTextColor: Colors.black87,

                              hintTxt: "Lorem Ipsum",


                              fillColor: Colors.white,
                              lblTxt: "Lerem Lspusm",)),
                      ],
                    ),
                  ),
                  SizedBox(width: constraints.maxWidth/2*0.1,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("NATURE OF BUISNESS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                        ),
                        Container(

                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              hintTxt: "Lorem Ipsum",
                              hintTextColor: Colors.black87,



                              fillColor: Colors.white,
                              lblTxt: "Lerem Lpsum",)),
                      ],
                    ),
                  ),





                ],),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text("PRINCIPLE PLACE OF BUISNESS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                  ),
                  Container(

                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(15),


                      ),

                      child: TxtField(
                        hintTxt: "Lorem Ipsum",
                        hintTextColor: Colors.black87,



                        fillColor: Colors.white,
                        lblTxt: "Lerem Lpsum",)),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text("ADDRESS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                  ),
                  Container(

                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(15),


                      ),

                      child: TxtField(
                        hintTxt: "Lorem Ipsum",
                        hintTextColor: Colors.black87,



                        fillColor: Colors.white,
                        lblTxt: "Lerem Lpsum",)),
                ],
              ),
              Row(

                children: [

                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("CITY",style: TextStyles.body2.copyWith(color: Colors.grey),),
                        ),
                        Container(


                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              hintTextColor: Colors.black87,

                              hintTxt: "Lorem Ipsum",


                              fillColor: Colors.white,
                              lblTxt: "Lerem Lspusm",)),
                      ],
                    ),
                  ),
                  SizedBox(width: constraints.maxWidth/2*0.1,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text("STATE PROVINCE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                        ),
                        Container(

                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(15),


                            ),

                            child: TxtField(
                              hintTxt: "Lorem Ipsum",
                              hintTextColor: Colors.black87,



                              fillColor: Colors.white,
                              lblTxt: "Lerem Lpsum",)),
                      ],
                    ),
                  ),





                ],),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text("EIRCODE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                  ),
                  Container(

                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(15),


                      ),

                      child: TxtField(
                        hintTxt: "Lorem Ipsum",
                        hintTextColor: Colors.black87,



                        fillColor: Colors.white,
                        lblTxt: "Lerem Lpsum",)),
                ],
              ),
              Padding(
                padding:  EdgeInsets.only(top:constraints.maxHeight*0.2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    TextButton(onPressed: (){}, child: Row(children: [
                      Icon(Icons.arrow_back_ios_outlined,),


                      Text("BACK"),
                      SizedBox(width: 20,),
                    ],)),

                    Container(

                        width:100,

                        height: 30,


                        decoration: BoxDecoration(

                            borderRadius: BorderRadius.circular(10),

                            gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                        child: GradientButton(buttonText: "NEXT",onPress: (){

                          final registrationViewModel=Get.find<RegistrationViewModel>();
                          registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                        },)),
                  ],
                ),
              ),











            ],
          ),
        ),
      ),

    )
    );
  }
}