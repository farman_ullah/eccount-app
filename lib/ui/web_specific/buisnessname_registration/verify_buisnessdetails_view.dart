import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/verificaiton_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VerifiyBuisnessDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (context, constraints) => Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                StepCircle(
                  value: "3",
                ),
                SizedBox(
                  height: 20,
                ),
                AutoSizeText(
                  "Verification",
                  style: TextStyles.body2.copyWith(color: Colors.black87),
                ),
                SizedBox(
                  height: 20,
                ),
                AutoSizeText(
                  "Please verify the details",
                  style: TextStyles.h2,
                ),
                SizedBox(
                  height: 20,
                ),
                // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                SizedBox(
                  height: 20,
                ),







                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    width: constraints.maxWidth,
                    child: Card(child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("I/We hereby certify that the particulars contained in this form are correct and have been given in accordance with the Notes on Completion of the statutory RBN1 form",
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyles.body1.copyWith(color: Colors.grey),


                      ),
                    ),),
                  ),
                )
                  ,
                  Container(

                    height: 300,
                    child: Card(
                      margin: EdgeInsets.all(10),
                      

                      shape: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(15)


                      ),
                      child: Column(
                        children: [
                          Align(



                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Text("Signature Details",style: TextStyles.body2.copyWith(fontWeight: FontWeight.bold)),
                            ),
                            alignment: Alignment.center,
                          ),
                          Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                          Expanded(
                            flex: 4,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [


                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("SIGNATURE TYPE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,

                                    border: Border.all(color: Colors.grey[200]),
                                    borderRadius: BorderRadius.circular(15),


                                  ),

                                  child: TxtField(
                                    hintTextColor: Colors.black87,

                                    hintTxt: "Lorem Ipsum",


                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",)),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("EMAIL ADDRESS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                              ),
                              Container(


                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(color: Colors.grey[200])


                                  ),

                                  child: TxtField(
                                    hintTextColor: Colors.black87,

                                    hintTxt: "Lorem Ipsum",


                                    fillColor: Colors.white,
                                    lblTxt: "Lerem Lspusm",)),
                            ],),
                          ),

                            Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [


                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text("BUISNESS NAME OWNER NAME",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                  ),
                                  Container(


                                      decoration: BoxDecoration(
                                        shape: BoxShape.rectangle,

                                        border: Border.all(color: Colors.grey[200]),
                                        borderRadius: BorderRadius.circular(15),


                                      ),

                                      child: TxtField(
                                        hintTextColor: Colors.black87,

                                        hintTxt: "Lorem Ipsum",


                                        fillColor: Colors.white,
                                        lblTxt: "Lerem Lspusm",)),

                                ],),
                            ),
                    ],),
                        ],
                      ),),
                  ),
                Padding(
                  padding:  EdgeInsets.only(top:constraints.maxHeight/2*0.2,bottom:constraints.maxHeight/4*0.2,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      TextButton(onPressed: (){}, child: Row(children: [
                        Icon(Icons.arrow_back_ios_outlined,),


                        Text("BACK"),
                        SizedBox(width: 20,),
                      ],)),

                      Container(

                          width:100,

                          height: 30,


                          decoration: BoxDecoration(

                              borderRadius: BorderRadius.circular(10),

                              gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                          child: GradientButton(buttonText: "NEXT",onPress: (){
                            final registrationViewModel=Get.find<RegistrationViewModel>();
                            registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                          },)),
                    ],
                  ),
                ),

              ],
            ),
          ),
          width: constraints.maxWidth,
          height: constraints.maxHeight,
          color: Colors.grey[200],

        ),
      );
  }
}
