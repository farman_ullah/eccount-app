import 'package:get/get.dart';

class BuisnessNameRegController extends GetxController{


  RxBool individual=false.obs;
  RxBool partnership=false.obs;
  RxBool bodyCorporate=false.obs;
//VAT REGISTRAION
  RxBool vatIndividual=false.obs;
  RxBool vatPartnership=false.obs;

  changeIndividual(bool val)
  {
    this.individual=val.obs;
    update();
  }
  changePar(bool val)
  {
    this.partnership=val.obs;
    update();
  }
  changeBody(bool val)
  {
    this.bodyCorporate=val.obs;
    update();
  }


  changeVatIndividual(bool val)
  {
    this.vatIndividual=val.obs;
    update();
  }
  changeVatPar(bool val)
  {
    this.vatPartnership=val.obs;
    update();
  }

}