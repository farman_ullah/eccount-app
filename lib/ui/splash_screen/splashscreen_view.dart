import 'package:animate_do/animate_do.dart';
import 'package:eccount/ui/splash_screen/splashscreen_viewmodel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: GetBuilder<SplashScreenViewModel>(builder: (_){
        return Center(
            child: SizedBox(
              width: kIsWeb?Get.width*0.3:Get.width*0.5,
              child: BounceInUp(child: Image.asset("assets/logo.png")),
            )
        );
      },),
    );
  }
}
