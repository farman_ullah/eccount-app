

import 'package:eccount/core/services/auth_service.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

class SplashScreenViewModel extends GetxController{
  @override
  void onInit() {
    checkStartUpLogic();
    super.onInit();
  }

  checkStartUpLogic()async{
    await Future.delayed(Duration(seconds: 2));
    final _authService=Get.find<AuthService>();
//    await _authService.signOut();
    if(_authService.checkLoggedIn){
      Get.offNamed('/dashboard');
    }
    else{
      if(kIsWeb){
        Get.offAllNamed("/home");
      }
      else{
        Get.offNamed("/onboarding");
      }
    }
  }
}