
import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/models/registration_single_view.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegistrationViewModel extends GetxController{
  RegistrationViewModel({this.registrationType});
  final RegistrationType registrationType;
  GlobalKey<FormState> registrationFormKey;
  int selectedIndex=0;
  int subViewIndex=0;
  bool inSubView=false;
  List<RegistrationSingleView> registrationViews;


  Future<bool> popScope()async{
    var result = await Get.dialog(
     Material(
       type: MaterialType.transparency,
       child: Center(child: Container(
         width: Get.width*0.85,
         decoration: BoxDecoration(
           color: Colors.white,
           borderRadius: BorderRadius.circular(10)
         ),
         padding: EdgeInsets.all(Get.width*0.05),
         child: Column(
           mainAxisSize: MainAxisSize.min,
           children: [
             AutoSizeText("Are you sure you want to leave?",style: TextStyles.h2,),
             SizedBox(height: Get.height*0.01,),
             AutoSizeText("Save it as a draft or you can continue editing it.",style: TextStyles.body1,),
             SizedBox(height: Get.height*0.01,),
             Row(children: [
               Expanded(child: RippleButton(
                 height: Get.height*0.065,
                 title: "Save as draft",
                 shadowColor: Colors.purple,
                 border: true,
                 borderColor: Colors.purple,
                 borderRadius: BorderRadius.circular(15),
                  titleColor: Colors.purple,
                 onPressed: (){},
               ),),
               SizedBox(width: 3,),
               Expanded(child: RippleButton(
                 height: Get.height*0.065,
                 title: "Continue",
                 shadowColor: Colors.purple,
                 border: true,
                 borderColor: Colors.purple,
                 borderRadius: BorderRadius.circular(15),
                 titleColor: Colors.purple,
                 onPressed: (){
                   Get.back(result: false);
                 },
               ),),
               SizedBox(width: 3,),
               Expanded(child: RippleButton(
                 height: Get.height*0.065,
                 title: "Discard",
                 shadowColor: Colors.purple,
                 border: true,
                 borderColor: Colors.purple,
                 borderRadius: BorderRadius.circular(15),
                 titleColor: Colors.purple,
                 onPressed: (){
                   Get.back(result: true);
                 },
               ),)
             ],)
           ],
         ),
       ),),
     )
    );
    if(result is bool){
      return result;
    }
    return false;
  }


  changeIndex(int index){
    if(registrationViews[index-1].subViews.length>0 && subViewIndex<registrationViews[index-1].subViews.length-1){
      inSubView=true;
      subViewIndex+=1;
      update();
      return;
    }
    if(inSubView && subViewIndex==registrationViews[index-1].subViews.length-1){
      selectedIndex+=1;
      inSubView=false;
      subViewIndex=0;
      update();
      return;
    }
    selectedIndex+=1;
    if(registrationViews[selectedIndex].subViews.length>0){
      inSubView=true;
    }
    update();
  }
  @override
  void onInit() {
    registrationFormKey=GlobalKey<FormState>();
    getViewsAccordingToRegistrationType();
    super.onInit();
  }
  getViewsAccordingToRegistrationType(){
    switch(registrationType){
      case RegistrationType.CompanyFormation:
        registrationViews=kIsWeb?List.from(CompanyFormation.webRegistrationView):List.from(CompanyFormation.registrationViews);
        return;
      case RegistrationType.BusinessNameRegistration:
        registrationViews=kIsWeb?List.from(CompanyFormation.buisnessNameRegistraionView):List.from(CompanyFormation.buisnessNameRegistraionView);
        return;
      case RegistrationType.VatRegistration:
        registrationViews=kIsWeb?List.from(CompanyFormation.vatRegistrationView):List.from(CompanyFormation.vatRegistrationView);

        return;
      default:
        return;
    }
  }
}