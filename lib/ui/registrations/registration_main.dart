import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

class RegistrationMain extends StatelessWidget {
  final RegistrationType registrationType;
  RegistrationMain({this.registrationType});
  @override
  Widget build(BuildContext context) {
    return kIsWeb
        ? _WebRegistrationView(
            registrationType: registrationType,
          )
        : GetBuilder<RegistrationViewModel>(
            init: RegistrationViewModel(registrationType: registrationType),
            builder: (_) {
              return WillPopScope(
                onWillPop: _.popScope,
                child: Scaffold(
                  appBar: AppBar(
                    backgroundColor: Colors.white,
                    elevation: 0,
                    iconTheme: IconThemeData(color: Colors.black),
                  ),
                  body: GestureDetector(
                    onTap: () {
                      if (!FocusScope.of(context).hasPrimaryFocus) {
                        FocusScope.of(context).requestFocus(FocusNode());
                      }
                    },
                    child: Form(
                      key: _.registrationFormKey,
                      child: SingleChildScrollView(
                        child: Container(
                            height: ResponsiveWrapper.of(context).scaledHeight,
                            width: ResponsiveWrapper.of(context).scaledWidth,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  height: ResponsiveWrapper.of(context)
                                          .scaledHeight *
                                      0.13,
                                  width:
                                      ResponsiveWrapper.of(context).scaledWidth,
                                  child: ListView.builder(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 20),
                                      itemCount: _.registrationViews.length,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, index) {
                                        return GestureDetector(
                                          onTap: () {
                                            _.changeIndex(index);
                                          },
                                          child: Container(
//                    width: index==_.selectedIndex?200:100,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Row(
                                                  children: [
                                                    index != 0
                                                        ? Container(
                                                            height: 1,
                                                            width: ResponsiveWrapper.of(
                                                                        context)
                                                                    .scaledWidth /
                                                                8,
                                                            color: _
                                                                    .registrationViews[
                                                                        index -
                                                                            1]
                                                                    .completed
                                                                ? Colors
                                                                    .greenAccent
                                                                : Colors.grey,
                                                          )
                                                        : Container(
                                                            height: 1,
                                                            width: ResponsiveWrapper.of(
                                                                        context)
                                                                    .scaledWidth /
                                                                8,
                                                          ),
                                                    Container(
                                                        padding:
                                                            EdgeInsets.all(10),
                                                        decoration:
                                                            BoxDecoration(
                                                                color: _
                                                                        .registrationViews[
                                                                            index]
                                                                        .completed
                                                                    ? Colors
                                                                        .greenAccent
                                                                    : index ==
                                                                            _
                                                                                .selectedIndex
                                                                        ? primaryColor
                                                                        : Colors
                                                                            .grey,
                                                                shape: BoxShape
                                                                    .circle),
                                                        child: AutoSizeText(
                                                          "${index + 1}",
                                                          style: TextStyle(
                                                              color: index ==
                                                                          _
                                                                              .selectedIndex ||
                                                                      _
                                                                          .registrationViews[
                                                                              index]
                                                                          .completed
                                                                  ? Colors.white
                                                                  : Colors
                                                                      .black,
                                                              fontSize:
                                                                  FontSizes
                                                                      .s20),
                                                        )),
                                                    index !=
                                                            _.registrationViews
                                                                    .length -
                                                                1
                                                        ? Container(
                                                            height: 1,
                                                            width: ResponsiveWrapper.of(
                                                                        context)
                                                                    .scaledWidth /
                                                                8,
                                                            color: _
                                                                    .registrationViews[
                                                                        index]
                                                                    .completed
                                                                ? Colors
                                                                    .greenAccent
                                                                : Colors.grey,
                                                          )
                                                        : Container(
                                                            height: 1,
                                                            width: ResponsiveWrapper.of(
                                                                        context)
                                                                    .scaledWidth /
                                                                8,
                                                          ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: ResponsiveWrapper.of(
                                                              context)
                                                          .screenHeight *
                                                      0.01,
                                                ),
                                                AutoSizeText(
                                                  _.registrationViews[index]
                                                      .title,
                                                  textAlign: TextAlign.center,
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                ),
                                Expanded(
                                    child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 5),
                                  child: _.registrationViews.length > 0
                                      ? _.registrationViews[_.selectedIndex]
                                          .child
                                      : Center(
                                          child: AutoSizeText("Coming soon!"),
                                        ),
                                ))
                              ],
                            )),
                      ),
                    ),
                  ),
                ),
              );
            });
  }
}

class _WebRegistrationView extends StatelessWidget {
  final RegistrationType registrationType;
  _WebRegistrationView({this.registrationType});
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegistrationViewModel>(
        init: RegistrationViewModel(registrationType: registrationType),
        builder: (controller) {
          return WillPopScope(
              onWillPop: controller.popScope,
              child: Form(
                key: controller.registrationFormKey,
                child: Scaffold(
                  body: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      /*     NavigationRail(

                      leading: SizedBox(
                        width: 20,
                          height: 60,
                          child: Image.asset("assets/logo.png")),
                      extended: true,
                      elevation: 10,
                      selectedLabelTextStyle: TextStyles.h3,
                      destinations: [
                        NavigationRailDestination(icon: Icon(Icons.speed_outlined), label: AutoSizeText("Dashboard")),
                        NavigationRailDestination(icon: Icon(Icons.search), label: AutoSizeText("Search")),
                        NavigationRailDestination(icon: Icon(Icons.description), label: AutoSizeText("New Filing")),
                        NavigationRailDestination(icon: Icon(Icons.folder), label: AutoSizeText("My Filings")),
                        NavigationRailDestination(icon: Icon(Icons.message), label: AutoSizeText("Messages")),

                        NavigationRailDestination(icon: Icon(Icons.message), label: AutoSizeText("Help")),
                        NavigationRailDestination(icon: Icon(Icons.message), label: AutoSizeText("My fi")),

                      ], selectedIndex: controller.selectedIndex,
                      onDestinationSelected: controller.changeIndex,
                    ),
                  */

                      Container(
                        width:ResponsiveWrapper.of(context).screenWidth*0.2,
                        color: Colors.deepPurple,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            for (var entry in controller.registrationViews
                                .asMap()
                                .entries
                                .toList())
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Flexible(
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: entry.key ==
                                                      controller.selectedIndex
                                                  ? Colors.white70
                                                  : Colors.white30),
                                          child: controller
                                                  .registrationViews[entry.key]
                                                  .completed
                                              ? Icon(
                                                  Icons.done,
                                                  color: Colors.deepPurple,
                                                  size: 20,
                                                )
                                              : AutoSizeText(
                                                  "${entry.key + 1}",
                                                  style: TextStyles.h2.copyWith(
                                                      color: Colors.deepPurple),
                                                ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          child: AutoSizeText(
                                        "${controller.registrationViews[entry.key].title}",
                                        style: TextStyles.h2.copyWith(
                                            color: entry.key ==
                                                    controller.selectedIndex
                                                ? Colors.white70
                                                : Colors.white30,
                                            fontWeight: FontWeight.w300),
                                      ))
                                    ],
                                  ),
                                  SizedBox(
                                    height: Get.height * 0.01,
                                  ),
                                  for (var entry1 in controller
                                      .registrationViews[entry.key].subViews
                                      .asMap()
                                      .entries
                                      .toList())
                                    Padding(
                                      padding: EdgeInsets.only(left: 50),
                                      child: AutoSizeText(
                                        controller.registrationViews[entry.key]
                                            .subViews[entry1.key].title,
                                        style: TextStyles.h2.copyWith(
                                            color: entry.key ==
                                                        controller
                                                            .selectedIndex &&
                                                    entry1.key ==
                                                        controller.subViewIndex
                                                ? Colors.white70
                                                : Colors.white30,
                                            fontWeight: FontWeight.w300),
                                      ),
                                    )
                                ],
                              )
                          ],
                        ),
                      ),
                      Flexible(
                          child: Container(
                              child: controller.inSubView
                                  ? controller
                                      .registrationViews[
                                          controller.selectedIndex]
                                      .subViews[controller.subViewIndex]
                                      .child
                                  : controller
                                      .registrationViews[
                                          controller.selectedIndex]
                                      .child))
                    ],
                  ),
                ),
              ));
        });
  }
}
