import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/models/order_model.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_signature_view.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeclarationViewTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AutoSizeText.rich(
              TextSpan(children: [
                TextSpan(text: 'I further declare that',style: TextStyles.h2),
                TextSpan(text: ' the purpose, or one of the purposes, for which the company is being formed is the carrying on by it of an activity in the State and that it appears to me that either')
              ])
            ),
            SizedBox(height: 20,),
            AutoSizeText('(a) the activity can be classified in accordance with the relevant classification system as follows:'),
            SizedBox(height: 20,),
            StyledInfoWidget(child: Row(children: [
              AutoSizeText("NACE Code"),
              SizedBox(width: 5,),
              Flexible(
                child: StyledTextFormField(
                  initialValue: _.companyFormation.naceCode,
                  labelText: "NACE Code",
                  contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                  borderRadius: BorderRadius.circular(10),
                  hintStyle: TextStyle(fontSize: FontSizes.s18),
                  labelStyle: defaultLabelStyle,
                  onSaved: (val){
                    _.companyFormation.naceCode=val;
                  },
                  validate: (val){
                    if(val.isEmpty){
                      return "NACE code is required";
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(width: 5,),
            ],),text: CompanyFormation.noteSeventeen,),
            SizedBox(height: 20,),
            StyledInfoWidget(child: AutoSizeText('and that the general nature of the activity is'),text: CompanyFormation.noteSeventeen,),
            SizedBox(height: 10,),
            StyledTextFormField(
              initialValue: _.companyFormation.natureOfActivity,
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                _.companyFormation.natureOfActivity=val;
              },
            ),
            SizedBox(height: 20,),
            StyledInfoWidget(child: AutoSizeText('or (b) that the activity cannot be so classified but is precisely described as follows:'),text: CompanyFormation.noteEighteen,),
            SizedBox(height: 10,),
            StyledTextFormField(
              initialValue: _.companyFormation.activityDescribedAs,
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                _.companyFormation.activityDescribedAs=val;
              },
            ),
            SizedBox(height: 20,),
            StyledInfoWidget(child: AutoSizeText('I further declare that the place or places in the State where it is proposed to carry on the activity is/are'),text: CompanyFormation.noteNineteen,),
            SizedBox(height: 10,),
            StyledTextFormField(
              initialValue: _.companyFormation.placesOfActivity,
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                _.companyFormation.placesOfActivity=val;
              },
            ),
            SizedBox(height: 20,),
            StyledInfoWidget(child: AutoSizeText('and that the place where the central administration of the company will normally be carried on will be'),text: CompanyFormation.noteNineteen,),
            SizedBox(height: 10,),
            StyledTextFormField(
              initialValue: _.companyFormation.centralAdministrationPlaces,
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                _.companyFormation.centralAdministrationPlaces=val;
              },
            ),
            SizedBox(height: 20,),
            AutoSizeText('I further declare that this form has been fully and accurately completed.'),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AutoSizeText("Signature",style: TextStyles.h3,),
                IconButton(icon: Icon(Icons.add,color: primaryColor,), onPressed: ()async{
                  var data=await Get.to(()=>SignatureView(false),preventDuplicates: false);
                  if(data!=null){
                    _.companyFormation.declarationSignatureImage=data;
                    _.update();
                  }
                })
              ],),
            SizedBox(height: 20,),
            _.companyFormation.declarationSignatureImage!=null?CachedNetworkImage(imageUrl:_.companyFormation.declarationSignatureImage,height: Get.height*0.15,width: Get.width,):SizedBox(),
            SizedBox(height: _.companyFormation.declarationSignatureImage!=null?20:0,),
            Align(
              alignment: Alignment.centerRight,
              child: RippleButton(
                backGroundColor: primaryColor,
                title: "Confirm",
                titleColor: Colors.white,
                fontSize: FontSizes.s20,
                borderRadius: BorderRadius.circular(10),
                shadowColor: Colors.purple,
                addShadow: true,
                elevation: 6,
                onPressed: ()async{
                  if(_.companyFormation.declarationSignatureImage==null){
                    Show.showErrorSnackBar(title: "Error", error: "Please add your signature to continue");
                    return;
                  }
                  _.companyFormation.declarationDate=DateTime.now();
                  final registrationViewModel=Get.find<RegistrationViewModel>();
                  if(registrationViewModel.registrationFormKey.currentState.validate()){
                    registrationViewModel.registrationFormKey.currentState.save();
                    OrderModel id=await _.handleCompletion();
                    if(id!=null){
                      Get.offAllNamed('/dashboard');
                    }
                  }
                },
              ),
            ),
            SizedBox(height: 20,)
          ],
        ),
      );
    });
  }
}
