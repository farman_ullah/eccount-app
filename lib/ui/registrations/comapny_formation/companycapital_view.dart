import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/authorisedshare_model.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/addshare_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CompanyCapitalView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            StyledInfoWidget(child: AutoSizeText("Company Capital",style: TextStyles.h2,),text: CompanyFormation.noteFourteen,),
            SizedBox(height: 20,),
            StyledTextFormField(
              initialValue: _.companyFormation.companyCapitalModel.totalValueOfAuthorisedShares,
              labelText: "Total authorised share value",
              hintText: "Enter total authorised share value",
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                _.companyFormation.companyCapitalModel.totalValueOfAuthorisedShares=val;
              },
            ),
            SizedBox(height: 20,),
            StyledTextFormField(
              initialValue: _.companyFormation.companyCapitalModel.totalNumberOfAuthorisedShares!=null?_.companyFormation.companyCapitalModel.totalNumberOfAuthorisedShares.toString():null,
              labelText: "Total authorised share number",
              hintText: "Enter total authorised share number",
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                if(int.tryParse(val)!=null){
                  _.companyFormation.companyCapitalModel.totalNumberOfAuthorisedShares=int.parse(val);
                }
              },
            ),
            SizedBox(height: 20,),
            ListTile(
              onTap: ()async{
                var data=await Get.to(()=>AddShareView(className: 'authorised',),preventDuplicates: false);
                if(data!=null){
                  _.addAuthorisedShareModel(data);
                }
              },
              title: AutoSizeText("Add authorised share class"),
              trailing: Icon(Icons.add,color: Colors.black,),
            ),
            SizedBox(height: 20,),
            for(ShareModel model in _.companyFormation.companyCapitalModel.authorisedShareList)
            ListTile(title: AutoSizeText(model.authorisedShareClass),
            tileColor: Colors.grey[200],
            onTap: ()async{
              var data=await Get.to(()=>AddShareView(shareModel: model,className: "authorised",),preventDuplicates: false);
              if(data!=null){
                _.updateAuthorisedShareModel(model, data);
              }
            },
            trailing: GestureDetector(
                onTap: (){
                  _.removeAuthorisedShareModel(model);
                },
                child: Icon(Icons.close,color: Colors.red,)),
            leading: SizedBox(
                width: Get.width*0.1,
                child: Center(child: AutoSizeText(model.numberOfShares.toString()),)),
            subtitle: AutoSizeText("value : ${model.valuePerShare}"),),
            SizedBox(height: 20,),
            StyledTextFormField(
              initialValue: _.companyFormation.companyCapitalModel.totalIssuedSharesValue,
              labelText: "Total issued share value",
              hintText: "Enter total issued share value",
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                _.companyFormation.companyCapitalModel.totalIssuedSharesValue=val;
              },
            ),
            SizedBox(height: 20,),
            StyledTextFormField(
              initialValue: _.companyFormation.companyCapitalModel.totalNumberOfIssuedShares!=null?_.companyFormation.companyCapitalModel.totalNumberOfIssuedShares.toString():null,
              labelText: "Total issued share number",
              hintText: "Enter total issued share number",
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
               if(int.tryParse(val)!=null){
                 _.companyFormation.companyCapitalModel.totalNumberOfIssuedShares=int.parse(val);
               }
              },
            ),
            SizedBox(height: 20,),
            ListTile(
              onTap: ()async{
                var data=await Get.to(()=>AddShareView(className: 'issued',),preventDuplicates: false);
                if(data!=null){
                  _.addIssuedShareModel(data);
                }
              },
              title: AutoSizeText("Add Issued share class"),
              trailing: Icon(Icons.add,color: Colors.black,),
            ),
            SizedBox(height: 20,),
            for(ShareModel model in _.companyFormation.companyCapitalModel.issuedShareList)
              ListTile(title: AutoSizeText(model.authorisedShareClass),
                tileColor: Colors.grey[200],
                onTap: ()async{
                  var data=await Get.to(()=>AddShareView(shareModel: model,),preventDuplicates: false);
                  if(data!=null){
                    _.updateIssuedShareModel(model, data);
                  }
                },
                trailing: GestureDetector(
                    onTap: (){
                      _.removeIssuedShareModel(model);
                    },
                    child: Icon(Icons.close,color: Colors.red,)),
                leading: SizedBox(
                    width: Get.width*0.1,
                    child: Center(child: AutoSizeText(model.numberOfShares.toString()),)),
                subtitle: AutoSizeText("value : ${model.valuePerShare}"),),
            SizedBox(height: 20,),
            Align(
              alignment: Alignment.centerRight,
              child: RippleButton(
                backGroundColor: primaryColor,
                title: "Next",
                titleColor: Colors.white,
                fontSize: FontSizes.s20,
                borderRadius: BorderRadius.circular(10),
                shadowColor: Colors.purple,
                addShadow: true,
                elevation: 6,
                onPressed: (){
                  final registrationViewModel=Get.find<RegistrationViewModel>();
                  if(registrationViewModel.registrationFormKey.currentState.validate()){
                    registrationViewModel.registrationFormKey.currentState.save();
                    registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                    registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                  }
                },
              ),
            )
          ],
        ),
      );
    });
  }
}
