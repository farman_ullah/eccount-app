import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/authorisedshare_model.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_wrapper.dart';


class AddShareView extends StatefulWidget {
  final ShareModel shareModel;
  final String className;
  AddShareView({this.shareModel,this.className});
  @override
  _AddShareViewState createState() => _AddShareViewState();
}

class _AddShareViewState extends State<AddShareView> {
  List<String> shareClass=['Ordinary Shares','Preference Shares','Deferred Shares','Redeemable Shares','Share Options','Golden Share'];
  ShareModel shareModel;
  GlobalKey<FormState> _formKey=GlobalKey<FormState>();
  String className;
  @override
  void initState() {
    className=widget.className;
    if(widget.shareModel!=null){
      shareModel=widget.shareModel;
    }
    else{
      shareModel=ShareModel();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        title: AutoSizeText("Shares Class",style: TextStyle(color: Colors.black),),
        actions: [
          TextButton(onPressed: (){
            if(_formKey.currentState.validate()){
              _formKey.currentState.save();
              Get.back(result: shareModel);
            }
          }, child: AutoSizeText("ADD",presetFontSizes: [16,14],style: TextStyle(color: primaryColor),))
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              DropdownButtonFormField(
                hint: AutoSizeText("Class of $className shares"),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
                value: shareModel.authorisedShareClass,
                validator: (val){
                  if(val==null){
                    return "Select share class";
                  }
                  return null;
                },
                onChanged: (val){
                  setState(() {
                    shareModel.authorisedShareClass=val;
                    print(val);
                  });
                },
                items: shareClass.map((e) => DropdownMenuItem(child: Container(
                    width: ResponsiveWrapper.of(context).screenWidth*0.7,
                    child: Row(children: [Flexible(child: AutoSizeText(e,overflow: TextOverflow.visible,))],)),value: e,)).toList(),
              ),
              SizedBox(height: 20,),
              StyledTextFormField(
                initialValue: shareModel.numberOfShares!=null?shareModel.numberOfShares.toString():null,
                inputType: TextInputType.number,
                labelText: "Share number",
                hintText: "Enter share number",
                contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                borderRadius: BorderRadius.circular(10),
                hintStyle: TextStyle(fontSize: FontSizes.s18),
                labelStyle: defaultLabelStyle,
                onSaved: (val){
                    if(int.tryParse(val)!=null){
                      shareModel.numberOfShares=int.parse(val);
                    }
                },
                validate: (val){
                  if(val.isEmpty){
                    return "Share number is required";
                  }
                  return null;
                },
              ),
              SizedBox(height: 20,),
              StyledTextFormField(
                initialValue: shareModel.valuePerShare!=null?shareModel.valuePerShare.toStringAsFixed(2):null,
                inputType: TextInputType.number,
                labelText: "Share value",
                hintText: "Enter share value",
                contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                borderRadius: BorderRadius.circular(10),
                hintStyle: TextStyle(fontSize: FontSizes.s18),
                labelStyle: defaultLabelStyle,
                onSaved: (val){
                 if(double.tryParse(val)!=null){
                   shareModel.valuePerShare=double.parse(val) ;
                 }
                },
                validate: (val){
                  if(val.isEmpty){
                    return "Share value is required";
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

