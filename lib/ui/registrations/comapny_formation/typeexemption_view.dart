import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_checkbox_message.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TypeExemption extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StyledInfoWidget(child: AutoSizeText("Type Exemption",style: TextStyles.h2,),text: CompanyFormation.noteFive,),
          SizedBox(height: 20,),
          AutoSizeText("Please tick the box if the company is applying for an exemption or has been granted exemption from the requirement to describe their company type as part of the company name."),
          SizedBox(height: 20,),
          AutoSizeText("Exemption 1: Available to Designated Activity Companies and Companies Limited by Guarantee only. No other company type is eligible for the exemption and must have their company type at the end of their company name. (S.971/1180 Companies Act 2014)"),
          SizedBox(height: 20,),
          StyledCheckBoxMessage(value: _.companyFormation.typeExemption??false,onPressed: _.toggleTypeExemption,message: 'I confirm that the company is applying for the exemption and Form G5 is attached to this application.',),
          SizedBox(height: 20,),
          AutoSizeText("Exemption 2: Available to Unlimited Companies. No other company type is eligible for this exemption. (Section 1237 Companies Act 2014)."),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
              },
            ),
          )
        ],
      );
    });
  }
}
