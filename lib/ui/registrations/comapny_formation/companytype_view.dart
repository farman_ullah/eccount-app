import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

class CompanyTypeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(
        autoRemove: false,
        builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AutoSizeText("Company Type",style: TextStyles.h2,),
          SizedBox(height: 20,),
          AutoSizeText("Please indicate which company type is proposed for registration."),
          SizedBox(height: 20,),
          DropdownButtonFormField(
            hint: AutoSizeText("Select Company Type"),
            decoration: InputDecoration(
              border: OutlineInputBorder(),
                    ),
            value: _.companyFormation.companyType,
            validator: (val){
              if(val==null){
                return "Select company type";
              }
              return null;
            },
            onSaved: _.selectCompanyType,
            onChanged: _.selectCompanyType,
            items: CompanyFormation.companyTypes.map((e) => DropdownMenuItem(child: Container(
                width: ResponsiveWrapper.of(context).screenWidth*0.7,
                child: Row(children: [Flexible(child: AutoSizeText(e,overflow: TextOverflow.visible,))],)),value: e,)).toList(),
          ),
          SizedBox(height: 20,),
          StyledInfoWidget(child: AutoSizeText('Each company type has requirements regarding the company name which must be applied. eg. A LTD company\'s name must end in either "Limited" or "Teoranta".'),text: CompanyFormation.noteTwo,),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                if(registrationViewModel.registrationFormKey.currentState.validate()){
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                }
              },
            ),
          )
        ],
      );
    });
  }
}
