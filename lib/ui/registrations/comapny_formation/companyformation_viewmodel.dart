import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/authorisedshare_model.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/models/companycapital_model.dart';
import 'package:eccount/models/director_details.dart';
import 'package:eccount/models/order_model.dart';
import 'package:eccount/models/presenter_detail.dart';
import 'package:eccount/models/secretary_detail.dart';
import 'package:eccount/models/subscribers.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CompanyFormationViewModel extends GetxController{

RxBool isChecked=false.obs;
RxBool isCheckedNewsLetter=false.obs;
RxBool isCheckedPolicy=false.obs;
  CompanyFormation companyFormation;
  OrderModel order;
  TextEditingController secretaryDOBController;
  @override
  void onInit() {
    secretaryDOBController=TextEditingController();
    companyFormation=CompanyFormation();
    companyFormation.presenterDetail=PresenterDetail();
    companyFormation.secretaryDetails=SecretaryDetail();
    companyFormation.companyCapitalModel=CompanyCapitalModel();
    companyFormation.companyCapitalModel.issuedShareList=[];
    companyFormation.companyCapitalModel.authorisedShareList=[];
    companyFormation.registeredOfficeAddressSameAsAgentOffice=false;
    companyFormation.typeExemption=false;
    companyFormation.nameRestriction=false;
    companyFormation.directorDetailsList=[];
    companyFormation.subscribers=[];
    companyFormation.declarationType=0;
    super.onInit();
  }

  selectCompanyType(dynamic val){
    print(val);
    companyFormation.companyType=val;
    update();
  }

  toggleRegisteredOfficeSameAsAgent(bool value){
    companyFormation.registeredOfficeAddressSameAsAgentOffice=value;
    update();
  }

  toggleTypeExemption(bool value){
    companyFormation.typeExemption=value;
    update();
  }

  toggleNameRestriction(bool value){
    companyFormation.nameRestriction=value;
    update();
  }


  addDirectorDetail(var data){
    if(data!=null){
      companyFormation.directorDetailsList.add(data);
      update();
    }
  }


  addSubscriber(var data){
    if(data!=null){
      companyFormation.subscribers.add(data);
      update();
    }
  }

  updateSubscriber(Subscribers oldSubscriber,Subscribers newSubscriber){
    oldSubscriber=newSubscriber;
    update();
  }

  removeSubscriber(Subscribers subscriber){
    companyFormation.subscribers.remove(subscriber);
    update();
  }
  updateDirectoryDetail(DirectorDetails oldDetail,DirectorDetails newDetail){
    oldDetail=newDetail;
    update();
  }

  removeDirectorDetail(DirectorDetails detail){
    companyFormation.directorDetailsList.remove(detail);
    update();
  }


  _buildIOsDatePicker(BuildContext context) async{
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (picked) {
                if (picked != null && picked != companyFormation.secretaryDetails.dateOfBirth && picked.isBefore(DateTime.now()))
                  companyFormation.secretaryDetails.dateOfBirth= picked;
                secretaryDOBController.text=DateFormat.yMMMd().format(companyFormation.secretaryDetails.dateOfBirth);
                update();
              },
              initialDateTime: DateTime.now().subtract(Duration(days: 4745)),
              minimumYear: 1950,
              maximumYear: DateTime.now().subtract(Duration(days: 4745)).year,
            ),
          );
        });
  }
  _buildAndroidDatePicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now().subtract(Duration(days: 4745)), // Refer step 1
      firstDate: DateTime(1950),
      lastDate: DateTime.now().subtract(Duration(days: 4745)),
    );
    if (picked != null && picked != companyFormation.secretaryDetails.dateOfBirth)
      companyFormation.secretaryDetails.dateOfBirth= picked;
    secretaryDOBController.text=DateFormat.yMMMd().format(companyFormation.secretaryDetails.dateOfBirth);
    update();
  }



  selectDate()async{
    final ThemeData theme = Theme.of(Get.context);
    assert(theme.platform != null);
    switch (theme.platform) {
      case TargetPlatform.android:
        return _buildAndroidDatePicker(Get.context);
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
      case TargetPlatform.iOS:
        return _buildIOsDatePicker(Get.context);
      case TargetPlatform.macOS:
        return _buildAndroidDatePicker(Get.context);
    }
  }

  addAuthorisedShareModel(data) {
      companyFormation.companyCapitalModel.authorisedShareList.add(data);
      update();
  }

  updateAuthorisedShareModel(ShareModel oldModel,ShareModel newModel){
    oldModel=newModel;
    update();
  }

   removeAuthorisedShareModel(ShareModel model) {
    companyFormation.companyCapitalModel.authorisedShareList.remove(model);
    update();
   }

  addIssuedShareModel(data) {
    companyFormation.companyCapitalModel.issuedShareList.add(data);
    update();
  }

  updateIssuedShareModel(ShareModel oldModel,ShareModel newModel){
    oldModel=newModel;
    update();
  }

  removeIssuedShareModel(ShareModel model) {
    companyFormation.companyCapitalModel.issuedShareList.remove(model);
    update();
  }


  changeDeclarationClientType(int val){
    companyFormation.declarationType=val;
    update();
  }

  Future<OrderModel> handleCompletion()async{
    Show.showLoader();
    try{
      OrderModel id=await Get.find<FirestoreService>().createNewOrder(companyFormation.toMap(), getRegistrationTypeToInt(Get.find<RegistrationViewModel>().registrationType));
     if(Get.isOverlaysOpen){
       Get.back();
     }
     return id;
    }
    catch(e){
      Get.back();
      Show.showErrorSnackBar(title: "Error", error: e?.message??e);
    }
  }

acceptTos(bool val)
{

  this.isChecked=val.obs;
  update();
}
checkNewsLetter(bool val)
{

  this.isCheckedNewsLetter=val.obs;
  update();
}checkPolicy(bool val)
{

  this.isCheckedPolicy=val.obs;
  update();
}


}