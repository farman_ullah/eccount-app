import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CompanyEmailView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StyledInfoWidget(child: AutoSizeText("Company Email Address",style: TextStyles.h2,),text: CompanyFormation.noteFour,),
          SizedBox(height: 20,),
          AutoSizeText("Please nominate an email address. The certificate of incorporation will issue to this email address in electronic format. This is required information."),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.companyFormation.companyEmailAddress,
            labelText: "Comapny Email Address",
            hintText: "Enter company email address",
            contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
            borderRadius: BorderRadius.circular(10),
            hintStyle: TextStyle(fontSize: FontSizes.s18),
            labelStyle: defaultLabelStyle,
            onSaved: (val){
              _.companyFormation.companyEmailAddress=val;
            },
            validate: (val){
              if(val.isEmpty){
                return "Company email address is required";
              }
              else if(!GetUtils.isEmail(val)){
                return "Enter a valid email address";
              }
              return null;
            },
          ),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                if(registrationViewModel.registrationFormKey.currentState.validate()){
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                }
              },
            ),
          )
        ],
      );
    });
  }
}
