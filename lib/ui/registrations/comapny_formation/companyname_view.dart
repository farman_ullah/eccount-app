import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CompanyNameView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FadeIn(
      child: GetBuilder<CompanyFormationViewModel>(
          init: CompanyFormationViewModel(),
          autoRemove: false,
          builder: (_){
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AutoSizeText("Company Name",style: TextStyles.h2,),
            SizedBox(height: 20,),
            StyledTextFormField(
              initialValue: _.companyFormation.companyName,
              labelText: "Company Name",
              hintText: "Enter company name",
              contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
              borderRadius: BorderRadius.circular(10),
              hintStyle: TextStyle(fontSize: FontSizes.s18),
              labelStyle: defaultLabelStyle,
              onSaved: (val){
                _.companyFormation.companyName=val;
              },
              validate: (val){
                if(val.isEmpty){
                  return "Company name is required";
                }
                return null;
              },
            ),
            SizedBox(height: 20,),
            AutoSizeText("A good company name has all four U's",style: TextStyles.h2,),
            SizedBox(height: 20,),
            ListTile(leading: Icon(Icons.check,color: Colors.green,),title: Text("Unique"),
            subtitle: Text("Clearly distinctive from other business names registered."),),
            ListTile(leading: Icon(Icons.check,color: Colors.green,),title: Text("Universal"),
              subtitle: Text("Universally available in singular and plural,also when written together or seperated"),),
            ListTile(leading: Icon(Icons.check,color: Colors.green,),title: Text("Usable"),
              subtitle: Text("Written in the latin alphabet,without special characters or symbols."),),
            ListTile(leading: Icon(Icons.check,color: Colors.green,),title: Text("Unpatented"),
              subtitle: Text("Not registered as a trademark."),),
            Align(
              alignment: Alignment.centerRight,
              child: RippleButton(
                backGroundColor: primaryColor,
                title: "Next",
                titleColor: Colors.white,
                fontSize: FontSizes.s20,
                borderRadius: BorderRadius.circular(10),
                shadowColor: Colors.purple,
                addShadow: true,
                elevation: 6,
                onPressed: (){
                  final registrationViewModel=Get.find<RegistrationViewModel>();
                  if(registrationViewModel.registrationFormKey.currentState.validate()){
                    registrationViewModel.registrationFormKey.currentState.save();
                    registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                    registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                  }
                },
              ),
            ),
          ],
        );
      }),
    );
  }
}
