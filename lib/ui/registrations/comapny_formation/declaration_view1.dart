import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeclarationViewOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        children: [
          AutoSizeText("Declaration of compliance and section 24 declaration",style: TextStyles.h2,),
          SizedBox(height: 20,),
          StyledInfoWidget(child: AutoSizeText('The declaration is an unsworn declaration of compliance with all the legal requirements relating to incorporation. It is a criminal offence pursuant to section 876 of the Companies Act 2014 for a person to knowingly or recklessly deliver a document to the CRO which is false in a material particular.',textAlign: TextAlign.justify,),text: CompanyFormation.noteSixteen,),
          SizedBox(height: 20,),
          Row(children: [
            AutoSizeText("I",style: TextStyles.h2,),
            SizedBox(width: 8,),
            Flexible(
              child: StyledTextFormField(
                initialValue: _.companyFormation.declarationName,
                labelText: "Name",
                hintText: "Enter name in bold capitals",
                contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                borderRadius: BorderRadius.circular(10),
                hintStyle: TextStyle(fontSize: FontSizes.s18),
                labelStyle: defaultLabelStyle,
                onSaved: (val){
                  _.companyFormation.declarationName=val;
                },
                validate: (val){
                  if(val.isEmpty){
                    return "Name is required";
                  }
                  return null;
                },
              ),
            ),
          ],),
          SizedBox(height: 20,),
          StyledInfoWidget(
            child: Row(children: [
              AutoSizeText("of",style: TextStyles.h2,),
              SizedBox(width: 8,),
              Flexible(
                child: StyledTextFormField(
                  initialValue: _.companyFormation.declarationResidentialAddress,
                  labelText: "Residential address",
                  hintText: "Enter residential address",
                  contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                  borderRadius: BorderRadius.circular(10),
                  hintStyle: TextStyle(fontSize: FontSizes.s18),
                  labelStyle: defaultLabelStyle,
                  onSaved: (val){
                    _.companyFormation.declarationResidentialAddress=val;
                  },
                  validate: (val){
                    if(val.isEmpty){
                      return "residential address is required";
                    }
                    return null;
                  },
                ),
              ),
            ],),
            text: CompanyFormation.noteSeven,
          ),
          SizedBox(height: 20,),
          StyledInfoWidget(child: AutoSizeText('do solemnly and sincerely declare that I am a'),text: CompanyFormation.noteTwo,),
          SizedBox(height: 20,),
          Row(children: [
            Radio(
                activeColor: primaryColor,
                value: 0, groupValue: _.companyFormation.declarationType, onChanged: _.changeDeclarationClientType),
            SizedBox(width: 5,),
            AutoSizeText("Director"),
            SizedBox(width: 5,),
            Radio(activeColor: primaryColor,value: 1, groupValue: _.companyFormation.declarationType, onChanged: _.changeDeclarationClientType),
            SizedBox(width: 5,),
            AutoSizeText("Secretary"),
            SizedBox(width: 5,),
            Radio(activeColor: primaryColor,value: 2, groupValue: _.companyFormation.declarationType, onChanged: _.changeDeclarationClientType),
            SizedBox(width: 5,),
            AutoSizeText("Solicitor"),
            SizedBox(width: 5,)
          ],),
          SizedBox(height: 20,),
          AutoSizeText('and that all the requirements of the Companies Act in respect of the registration of the said company, and of matters precedent and incidental thereto have been complied with and that Form A1 has been completed in accordance with the Notes on Completion of Form A1.'),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                if(registrationViewModel.registrationFormKey.currentState.validate()){
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                }
              },
            ),
          )
        ],
      );
    });
  }
}
