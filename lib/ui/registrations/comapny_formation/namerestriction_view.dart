import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_checkbox_message.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../registration_viewmodel.dart';

class NameRestrictionView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AutoSizeText("Name Restriction",style: TextStyles.h2,),
          SizedBox(height: 20,),
          AutoSizeText("Please tick the box if the company is applying for a company name which includes restricted words which require permission from a government department or other specified body."),
          SizedBox(height: 20,),
          StyledCheckBoxMessage(value: _.companyFormation.nameRestriction??false,onPressed: _.toggleNameRestriction,message: 'I confirm that the company\'s proposed name contains a restricted word or expression and that permission has been sought from the relevant government department or other specified body and that the notice of permission is attached to this application. (please see Information Leaflet 1 or visit www.cro.ie/registration/company regarding restricted words or expressions)',),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
              },
            ),
          )
        ],
      );
    });
  }
}
