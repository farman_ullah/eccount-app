import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/models/order_model.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ReviewAndConfirm  extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(
        init: CompanyFormationViewModel(),
        builder: (controller){
      return SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: Get.height*0.03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            AutoSizeText("Please review your application",style: TextStyles.h2,),
            SizedBox(height: 20,),
            AutoSizeText("Company Detail",style: TextStyles.h1,),
            SizedBox(height: 10,),
            _CompanyDetailReview(),
              SizedBox(height: 20,),
              AutoSizeText("Presenter Detail",style: TextStyles.h1,),
              SizedBox(height: 10,),
              _PresenterDetailReview(),
              SizedBox(height: 15,),
              Align(
                alignment: Alignment.centerRight,
                child: RippleButton(
                  backGroundColor: primaryColor,
                  title: "Next",
                  titleColor: Colors.white,
                  fontSize: FontSizes.s20,
                  borderRadius: BorderRadius.circular(10),
                  shadowColor: Colors.purple,
                  addShadow: true,
                  elevation: 6,
                  onPressed: ()async{
                    final registrationViewModel=Get.find<RegistrationViewModel>();
                    if(registrationViewModel.registrationFormKey.currentState.validate()){
                      registrationViewModel.registrationFormKey.currentState.save();
                      OrderModel order=await controller.handleCompletion();
                      Get.find<CompanyFormationViewModel>().order=order;
                      registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                    }
                  },
                ),
              )
          ],),
        ),
      );
    });
  }
}

class _CompanyDetailReview extends GetView<CompanyFormationViewModel> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      child: Padding(
        padding: EdgeInsets.all(Get.width*0.04),
        child: Column(

          children: [
            Row(children: [
              Flexible(child:StyledTextFormField(
                hintText: "Company Name",
                initialValue: controller.companyFormation?.companyName,
                hintStyle: defaultHintStyle,
                readOnly: true,
              ),),
              SizedBox(width: Get.width*0.02,),
              Flexible(child: DropdownButtonFormField(
                isDense: true,
                isExpanded: true,
                hint: AutoSizeText("Select Company Type"),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.zero),borderSide: BorderSide(color:Colors.grey)),
                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.zero,),borderSide: BorderSide(color: Colors.grey[500])),
                  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.zero),borderSide: BorderSide(color:Colors.grey)),
                  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.zero),borderSide: BorderSide(color:Colors.red)),
                ),
                value: controller.companyFormation.companyType,
                validator: (val){
                  if(val==null){
                    return "Select company type";
                  }
                  return null;
                },
                onSaved: controller.selectCompanyType,
                onChanged: controller.selectCompanyType,
                items: CompanyFormation.companyTypes.map((e) => DropdownMenuItem(child:AutoSizeText(e),value: e,)).toList(),
              ),)
            ],),
            SizedBox(height: 15,),
            Row(children: [
              Flexible(child:  StyledTextFormField(
                initialValue: controller.companyFormation.companyEmailAddress??controller.companyFormation.presenterDetail.email,
                hintText: "Enter company email address",
                readOnly: true,
                contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                hintStyle: TextStyle(fontSize: FontSizes.s18),
                labelStyle: defaultLabelStyle,
              ),),
              SizedBox(width: Get.width*0.02,),
              Flexible(child: StyledTextFormField(
                initialValue: controller.companyFormation.companyPhoneNumber??controller.companyFormation.presenterDetail.telephone,
                inputType: TextInputType.phone,
                labelText: "Telephone",
                labelStyle: defaultLabelStyle,
                hintText: "Enter company phonenumber",
                hintStyle: defaultHintStyle,
                readOnly: true,
              ))
            ],),
            SizedBox(height: 15,),
            Row(children: [
              Flexible(child:  StyledTextFormField(
                initialValue: controller.companyFormation.registeredOfficeAddress,
                hintText: "Enter company address",
                contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                hintStyle: TextStyle(fontSize: FontSizes.s18),
                labelStyle: defaultLabelStyle,
                readOnly: true,
              ),),
            ],),
            SizedBox(height: 20,),
          ],
        ),
      ),
    );
  }
}


class _PresenterDetailReview extends GetView<CompanyFormationViewModel> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      child: Padding(
        padding: EdgeInsets.all(Get.width*0.04),
        child: Column(
          children: [
            Row(children: [
              Flexible(child:StyledTextFormField(
                initialValue: controller.companyFormation.presenterDetail.name,
                labelText: "Name",
                labelStyle: defaultLabelStyle,
                hintText: "Enter your name",
                readOnly: true,
                hintStyle: defaultHintStyle,
              ),),
              SizedBox(width: Get.width*0.02,),
              Flexible(child: StyledTextFormField(
                initialValue: controller.companyFormation.presenterDetail.dxNumber,
                labelText: "DX number",
                labelStyle: defaultLabelStyle,
                hintText: "Enter dx number",
                hintStyle: defaultHintStyle,
                readOnly: true,
              ),)
            ],),
            SizedBox(height: 15,),
            Row(
              children: [
                Flexible(child: StyledTextFormField(
                  initialValue: controller.companyFormation.presenterDetail.email,
                  labelText: "Email",
                  labelStyle: defaultLabelStyle,
                  hintText: "Enter your email",
                  hintStyle: defaultHintStyle,
                  readOnly: true,
                ),),
                SizedBox(width: Get.width*0.02,),
                Flexible(child: StyledTextFormField(
                  initialValue: controller.companyFormation.presenterDetail.telephone,
                  inputType: TextInputType.phone,
                  labelText: "Telephone",
                  labelStyle: defaultLabelStyle,
                  hintText: "Enter your phonenumber",
                  hintStyle: defaultHintStyle,
                  readOnly: true,
                ),)
              ],
            ),
            SizedBox(height: 15,),
            Row(children: [
              Flexible(child: StyledTextFormField(
                initialValue: controller.companyFormation.presenterDetail.address,
                labelText: "Address",
                labelStyle: defaultLabelStyle,
                hintText: "Enter your address",
                hintStyle: defaultHintStyle,
                readOnly: true,
              ),),
            ],),
          ],
        ),
      ),
    );
  }
}


class _DirectorDetailReview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
