import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eccount/models/subscribers.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/styled_signature_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SubscriberAddView extends StatefulWidget {
  final Subscribers subscriber;
  SubscriberAddView({this.subscriber});
  @override
  _SubscriberAddViewState createState() => _SubscriberAddViewState();
}

class _SubscriberAddViewState extends State<SubscriberAddView> {
  Subscribers subscriber;
  int subscriberType=0;

  changeType(int value){
    setState(() {
      subscriberType=value;
    });
  }
  @override
  void initState() {
    if(widget.subscriber!=null){
      subscriber=widget.subscriber;
    }
    else{
      subscriber=Subscribers();
      subscriber.subscriberType=SubscriberType.Subscriber;
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black),
          title: AutoSizeText("Subscriber Detail",style: TextStyle(color: Colors.black),),
          actions: [
            TextButton(onPressed: ()async{
              if(subscriber.signatureImage==null){
                Show.showErrorSnackBar(title: "Error", error: "Please provide subscriber signature to add");
              }
              else{
                subscriber.date=DateTime.now();
                Get.back(result: subscriber);
              }
            }, child: AutoSizeText('ADD',style: TextStyle(color: primaryColor,fontSize: FontSizes.s16,fontWeight: FontWeight.w600),))
          ],
        ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AutoSizeText("Signature",style: TextStyles.h3,),
                IconButton(icon: Icon(Icons.add,color: primaryColor,), onPressed: ()async{
                  var data=await Get.to(()=>SignatureView(false));
                  if(data!=null){
                    setState(() {
                      subscriber.signatureImage=data;
                    });
                  }
                })
              ],),
            SizedBox(height: 20,),
            subscriber.signatureImage!=null?CachedNetworkImage(imageUrl:subscriber.signatureImage,height: Get.height*0.15,width: Get.width,):SizedBox(),
            SizedBox(height: subscriber.signatureImage!=null?20:0,),
            Row(children: [
              AutoSizeText("Subscriber"),
              SizedBox(width: 5,),
              Radio(value: 0,groupValue: subscriberType, onChanged: changeType,activeColor: primaryColor,),
              SizedBox(width: 5,),
              AutoSizeText("Agent"),
              SizedBox(width: 5,),
              Radio(value: 1,groupValue: subscriberType, onChanged: changeType,activeColor: primaryColor,),
            ],)
          ],
        ),
      ),
    );
  }
}
