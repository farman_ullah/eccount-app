import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/director_details.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/gradient_button.dart';
import 'package:eccount/styled_widgets/my_components/phone_textfield.dart';
import 'package:eccount/styled_widgets/my_components/step_circle.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/text_field.dart';
import 'package:eccount/ui/directoradd_views/directoradd_view1.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
/*
class DirectorsListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AutoSizeText("Directors List",style: TextStyles.h2,),
          SizedBox(height: 20,),
          ListTile(title: AutoSizeText("Add Director"),onTap: ()async{
           var data=await Get.to(()=>DirectorAddViewOne(),preventDuplicates: false);
           if(data!=null){
             _.addDirectorDetail(data);
           }
          },trailing: Icon(Icons.add_circle_outline),),
          SizedBox(height: 20,),
          for(DirectorDetails detail in _.companyFormation.directorDetailsList)
            ListTile(
              onTap: ()async{
                  var data=await Get.to(()=>DirectorAddViewOne(directorDetails: detail,),preventDuplicates: false);
                  print(data);
                if(data!=null){
                  _.updateDirectoryDetail(detail,data);
                }
              },
              tileColor: Colors.grey[100],
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              title: AutoSizeText(detail.surName+" "+detail.foreName),
            trailing: IconButton(icon: Icon(Icons.close,color: Colors.red,), onPressed: (){
              _.removeDirectorDetail(detail);
            }),),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                if(_.companyFormation.directorDetailsList.length==0){
                  Show.showErrorSnackBar(title: "Error", error: "Add atleast one director to continue");
                }else{
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                }
              },
            ),
          )
        ],
      );
    });
  }
}
*/
  class DirectorsListView extends StatelessWidget {
    const DirectorsListView({Key key}) : super(key: key);

    @override
    Widget build(BuildContext context) {
         return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
        child: Container(
          width: constraints.maxWidth,
          height: constraints.maxHeight,

          color: Colors.grey[200],
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              StepCircle(value: "2",),
              SizedBox(height: 20,),
              AutoSizeText("Appoint new director",style: TextStyles.body2.copyWith(color: Colors.black87),),
              SizedBox(height: 20,),
              AutoSizeText("Please select new director",style: TextStyles.h2,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),



              Padding(
                padding: const EdgeInsets.only(top:40.0,bottom: 40),
                child: Divider(color: Colors.grey,),
              ),

              Padding(
                padding:  EdgeInsets.symmetric(horizontal: constraints.maxWidth*0.2),
                child: Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("Directors list",style: TextStyles.h3,),
                    ),
                    Container(


                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [


                          Row(

                            children: [

                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text("DIRECTORS",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                    ),
                                    Container(


                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          borderRadius: BorderRadius.circular(15),


                                        ),

                                        child: TxtField(
                                          isdropDown: true,
                                          hintTxt: "Lorem Ipsum",
                                          hintTextColor: Colors.black87,


                                          validate: (val){
                                            if(val.isEmpty){
                                              return "Name is required";
                                            }
                                            return null;
                                          },
                                          onSaved: (val){
                                          },

                                          fillColor: Colors.white,
                                          lblTxt: "Lerem Lspusm",)),
                                  ],
                                ),
                              ),
                              // SizedBox(width: constraints.maxWidth/2*0.1,),








                            ],),

                          SizedBox(height: 40,),
                          Align(alignment: Alignment.center,child:   Container(

                              width:150,

                              height: 40,


                              decoration: BoxDecoration(

                                  borderRadius: BorderRadius.circular(10),

                                  gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                              child: GradientButton(buttonText: "CHECK NOW",onPress: (){},)),),



                        ],
                      ),
                    ),
                  ],
                ),
              ),








              Padding(
                padding:  EdgeInsets.only(top:constraints.maxHeight*0.2,right: 75),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    TextButton(onPressed: (){}, child: Row(children: [
                      Icon(Icons.arrow_back_ios_outlined,),


                      Text("BACK"),
                      SizedBox(width: 20,),
                    ],)),

                    Flexible(
                      child: Container(

                          width:150,

                          height: 40,


                          decoration: BoxDecoration(

                              borderRadius: BorderRadius.circular(10),

                              gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                          child: GradientButton(buttonText: "NEXT",onPress: (){
                            final registrationViewModel=Get.find<RegistrationViewModel>();

                            registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                          },)),
                    ),
                  ],
                ),
              ),


            ],
          ),
        ),
      ));
    }
  }
class DirectorDetailsForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


      return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
        child: Container(
          width: constraints.maxWidth,
          height: constraints.maxHeight,

          color: Colors.grey[200],
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                StepCircle(value: "2",),
                SizedBox(height: 20,),
                AutoSizeText("Appoint new director",style: TextStyles.subTitle.copyWith(color: Colors.black),),
                SizedBox(height: 20,),
                AutoSizeText("Please fill new director details",style: TextStyles.h2,),
                SizedBox(height: 20,),
                // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
                SizedBox(height: 20,),
                Container(

                  width: constraints.maxWidth,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Text("Director list",style: TextStyles.h3),
                      Row(

                        children: [

                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("FIRST NAME",style: TextStyles.body2.copyWith(color: Colors.grey),),
                                ),
                                Container(


                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTextColor: Colors.black87,

                                      hintTxt: "Lorem Ipsum",


                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lspusm",)),
                              ],
                            ),
                          ),
                          SizedBox(width: constraints.maxWidth/2*0.1,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("LAST NAME",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(

                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,



                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lpsum",)),
                              ],
                            ),
                          ),
                          SizedBox(width: constraints.maxWidth/2*0.1,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("DATE OF BIRTH",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(

                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,


                                      // validate: (val){
                                      //   if(val.isEmpty){
                                      //     return "Company name is required";
                                      //   }
                                      //   return null;
                                      // },
                                      // onSaved: (val){
                                      //   _.companyFormation.companyName=val;
                                      // },

                                      fillColor: Colors.white,
                                    )),
                              ],
                            ),
                          ),




                          // Flexible(child:StyledTextFormField(
                          //   initialValue: _.companyFormation.presenterDetail.name??Get.find<UserController>().currentUser.value.name,
                          //   labelText: "Name",
                          //   labelStyle: defaultLabelStyle,
                          //   hintText: "Enter your name",
                          //   hintStyle: defaultHintStyle,
                          //   validate: (val){
                          //     if(val.isEmpty){
                          //       return "Name is required";
                          //     }
                          //     return null;
                          //   },
                          //   onSaved: (val){
                          //     _.companyFormation.presenterDetail.name=val;
                          //   },
                          // ),),
                          // SizedBox(width: Get.width*0.02,),
                          // Flexible(child: StyledTextFormField(
                          //   initialValue: _.companyFormation.presenterDetail.dxNumber,
                          //   labelText: "DX number",
                          //   labelStyle: defaultLabelStyle,
                          //   hintText: "Enter dx number",
                          //   hintStyle: defaultHintStyle,
                          //   validate: (val){
                          //     if(val.isEmpty){
                          //       return "DX number is required";
                          //     }
                          //     return null;
                          //   },
                          //   onSaved: (val){
                          //     _.companyFormation.presenterDetail.dxNumber=val;
                          //   },
                          // ),)
                        ],),
                      Row(

                        children: [

                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("NATIONALITY",style: TextStyles.body2.copyWith(color: Colors.grey),),
                                ),
                                Container(


                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTextColor: Colors.black87,

                                      hintTxt: "Lorem Ipsum",


                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lspusm",)),
                              ],
                            ),
                          ),
                          SizedBox(width: constraints.maxWidth/2*0.1,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("POST CODE",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(

                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,



                                      fillColor: Colors.white,
                                      lblTxt: "Lerem Lpsum",)),
                              ],
                            ),
                          ),
                          SizedBox(width: constraints.maxWidth/2*0.1,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text("BUISNESS OCCUPATION",style: TextStyles.body2.copyWith(color: Colors.grey)),
                                ),
                                Container(

                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),


                                    ),

                                    child: TxtField(
                                      hintTxt: "Lorem Ipsum",
                                      hintTextColor: Colors.black87,


                                      // validate: (val){
                                      //   if(val.isEmpty){
                                      //     return "Company name is required";
                                      //   }
                                      //   return null;
                                      // },
                                      // onSaved: (val){
                                      //   _.companyFormation.companyName=val;
                                      // },

                                      fillColor: Colors.white,
                                    )),
                              ],
                            ),
                          ),





                        ],),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text("RESIDENTIAL ADDRESS",style: TextStyles.body2.copyWith(color: Colors.grey),),
                          ),
                          Container(


                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(15),


                              ),

                              child: TxtField(
                                hintTextColor: Colors.black87,

                                hintTxt: "Lorem Ipsum",


                                fillColor: Colors.white,
                                lblTxt: "Lerem Lspusm",)),

                          Padding(
                            padding: const EdgeInsets.only(top:20.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [

                              Icon(Icons.check_box_outline_blank,color: Colors.white,),
                              Text("EEA Resident",style: TextStyles.body2.copyWith(color: Colors.grey))
                            ],),
                          )
                        ],
                      ),
                      Padding(
                        padding:  EdgeInsets.only(top:constraints.maxHeight*0.2),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [

                            TextButton(onPressed: (){}, child: Row(children: [
                              Icon(Icons.arrow_back_ios_outlined,),


                              Text("BACK"),
                              SizedBox(width: 20,),
                            ],)),

                            Flexible(
                              child: Container(

                                  width:150,

                                  height: 30,


                                  decoration: BoxDecoration(

                                      borderRadius: BorderRadius.circular(10),

                                      gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                  child: GradientButton(buttonText: "NEXT",onPress: (){

                                    final registrationViewModel=Get.find<RegistrationViewModel>();
                                    registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                                  },)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),











              ],
            ),
          ),
        ),

      )
      );
  }
}
class ConsentSignature extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return LayoutBuilder(        builder:(context,constraints)=>SingleChildScrollView(
      child: Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,

        color: Colors.grey[200],
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: Get.width/2*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

              StepCircle(value: "2",),
              SizedBox(height: 20,),
              AutoSizeText("Appoint new director",style: TextStyles.body2.copyWith(color: Colors.grey),),
              SizedBox(height: 20,),
              AutoSizeText("Consent & signature",style: TextStyles.h3,),
              SizedBox(height: 20,),
              // StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
              SizedBox(height: 20,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Text("Consent",style: TextStyles.h3),
                  Padding(
                    padding: const EdgeInsets.only(top:20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        Icon(Icons.check_box_outline_blank,color: Colors.white,),
                        Flexible(
                          child: Text(" I hereby consent to act as director of the aforementioned company and I acknowledge that as director, I have legal duties and obligations composed by the Companies Act, other statutes and at common law.",
                             textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,style: TextStyles.body2.copyWith(color: Colors.grey,)),
                        )
                      ],),
                  ),
                  Text("Signature",style: TextStyles.h3),
                 Align(
                   alignment: Alignment.center,
                   child: Container(width: 200,
                   height: 400,
                   child: Column(
                     crossAxisAlignment: CrossAxisAlignment.end,
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [


                     Container(

                         width:150,

                         height: 30,


                         decoration: BoxDecoration(

                             borderRadius: BorderRadius.circular(10),

                             gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                         child: GradientButton(buttonText: "ADD SIGNATURE",onPress: (){},)),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: [

                           TextButton(onPressed: (){}, child: Row(children: [
                             Icon(Icons.arrow_back_ios_outlined,),


                             Text("BACK"),
                             SizedBox(width: 20,),
                           ],)),

                           Flexible(
                             child: Container(

                                 width:150,

                                 height: 30,


                                 decoration: BoxDecoration(

                                     borderRadius: BorderRadius.circular(10),

                                     gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                                 child: GradientButton(



                                   buttonText: "DONE",onPress: (){

                                   final registrationViewModel=Get.find<RegistrationViewModel>();
                                   registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);

                                 },)),
                           ),
                         ],
                       ),


                   ],),
                   ),
                 )
                ],
              ),










            ],
          ),
        ),
      ),

    )
    );
  }
}