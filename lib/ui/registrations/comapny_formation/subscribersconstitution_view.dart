import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/models/subscribers.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/comapny_formation/subscriberadd_view.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SubscriberConstitutionView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            StyledInfoWidget(child: AutoSizeText("Subscribers to constitution",style: TextStyles.h2,),text: CompanyFormation.noteThirteen,),
            SizedBox(height: 20,),
            ListTile(title: AutoSizeText("Add Subscriber to constitution"),onTap: ()async{
              var data=await Get.to(()=>SubscriberAddView(),preventDuplicates: false);
              if(data!=null){
                  _.addSubscriber(data);
              }
            },trailing: Icon(Icons.add_circle_outline),),
            SizedBox(height: 20,),
            for(Subscribers detail in _.companyFormation.subscribers)
              ListTile(
                onTap: ()async{
                  var data=await Get.to(()=>SubscriberAddView(subscriber: detail,),preventDuplicates: false);
                  print(data);
                  if(data!=null){
                    _.updateSubscriber(detail,data);
                  }
                },
                tileColor: Colors.grey[100],
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                title:CachedNetworkImage(imageUrl:detail.signatureImage,
                height: Get.height*0.15,
                width: Get.width*0.5,),
                trailing: IconButton(icon: Icon(Icons.close,color: Colors.red,), onPressed: (){
                  _.removeSubscriber(detail);
                }),),
            SizedBox(height: 20,),
            Align(
              alignment: Alignment.centerRight,
              child: RippleButton(
                backGroundColor: primaryColor,
                title: "Next",
                titleColor: Colors.white,
                fontSize: FontSizes.s20,
                borderRadius: BorderRadius.circular(10),
                shadowColor: Colors.purple,
                addShadow: true,
                elevation: 6,
                onPressed: (){
                  final registrationViewModel=Get.find<RegistrationViewModel>();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                },
              ),
            )
          ],
        ),
      );
    });
  }
}
