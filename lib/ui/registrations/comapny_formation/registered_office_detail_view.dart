import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_checkbox_message.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisteredOfficeDetailView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return Column(
        children: [
          StyledInfoWidget(child: AutoSizeText("Registered Office",style: TextStyles.h2,),text: CompanyFormation.noteFour,),
          SizedBox(height: 20,),
          StyledCheckBoxMessage(value: _.companyFormation.registeredOfficeAddressSameAsAgentOffice??false,onPressed: _.toggleRegisteredOfficeSameAsAgent,message: 'Please tick box if the registered office address is that of a Registered Office Agent (ROA).',),
          SizedBox(height: 20,),
          _.companyFormation.registeredOfficeAddressSameAsAgentOffice??false?SizedBox():StyledTextFormField(
            initialValue: _.companyFormation.registeredOfficeAddress,
            labelText: "Office address",
            labelStyle: defaultLabelStyle,
            hintText: "Enter office address",
            hintStyle: defaultHintStyle,
            onSaved:(val){
              _.companyFormation.registeredOfficeAddress=val;
            } ,
            validate: (val){
              if(val.isEmpty){
                return "Office address is required";
              }
              return null;
            },
          ),
          SizedBox(height: 20,),
          Align(
            alignment: Alignment.centerRight,
            child: RippleButton(
              backGroundColor: primaryColor,
              title: "Next",
              titleColor: Colors.white,
              fontSize: FontSizes.s20,
              borderRadius: BorderRadius.circular(10),
              shadowColor: Colors.purple,
              addShadow: true,
              elevation: 6,
              onPressed: (){
                final registrationViewModel=Get.find<RegistrationViewModel>();
                if(registrationViewModel.registrationFormKey.currentState.validate()){
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                }
              },
            ),
          )
        ],
      );
    });
  }
}
