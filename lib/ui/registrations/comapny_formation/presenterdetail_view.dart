import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PresenterDetailsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompanyFormationViewModel>(builder: (_){
      return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            StyledInfoWidget(child: AutoSizeText("Presenter Details",style: TextStyles.h1,),text: CompanyFormation.noteThree,),
            SizedBox(height: 20,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.name,
              labelText: "Name",
              labelStyle: defaultLabelStyle,
              hintText: "Enter your name",
              hintStyle: defaultHintStyle,
              validate: (val){
                if(val.isEmpty){
                  return "Name is required";
                }
                return null;
              },
              onSaved: (val){
                _.companyFormation.presenterDetail.name=val;
              },
            ),
            SizedBox(height: 15,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.address,
              labelText: "Address",
              labelStyle: defaultLabelStyle,
              hintText: "Enter your address",
              hintStyle: defaultHintStyle,
              validate: (val){
                if(val.isEmpty){
                  return "Address is required";
                }
                return null;
              },
              onSaved: (val){
                _.companyFormation.presenterDetail.address=val;
              },
            ),
            SizedBox(height: 15,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.telephone,
              inputType: TextInputType.phone,
              labelText: "Telephone",
              labelStyle: defaultLabelStyle,
              hintText: "Enter your phonenumber",
              hintStyle: defaultHintStyle,
              validate: (val){
                if(val.isEmpty){
                  return "Phonenumber is required";
                }
                return null;
              },
              onSaved: (val){
                _.companyFormation.presenterDetail.telephone=val;
              },
            ),
            SizedBox(height: 15,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.email,
              labelText: "Email",
              labelStyle: defaultLabelStyle,
              hintText: "Enter your email",
              hintStyle: defaultHintStyle,
              validate: (val){
                if(val.isEmpty){
                  return "Email is required";
                }
                else if(!GetUtils.isEmail(val)){
                  return "Enter a valid email";
                }
                return null;
              },
              onSaved: (val){
                _.companyFormation.presenterDetail.email=val;
              },
            ),
            SizedBox(height: 15,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.faxNumber,
              labelText: "Fax Number",
              labelStyle: defaultLabelStyle,
              hintText: "Enter your fax number",
              hintStyle: defaultHintStyle,
              onSaved: (val){
                _.companyFormation.presenterDetail.faxNumber=val;
              },
            ),
            SizedBox(height: 15,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.contactPerson,
              labelText: "Contact Person",
              labelStyle: defaultLabelStyle,
              hintStyle: defaultHintStyle,
              onSaved: (val){
                _.companyFormation.presenterDetail.contactPerson=val;
              },
            ),
            SizedBox(height: 15,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.dxNumber,
              labelText: "DX number",
              labelStyle: defaultLabelStyle,
              hintText: "Enter dx number",
              hintStyle: defaultHintStyle,
              validate: (val){
                if(val.isEmpty){
                  return "DX number is required";
                }
                return null;
              },
              onSaved: (val){
                _.companyFormation.presenterDetail.dxNumber=val;
              },
            ),
            SizedBox(height: 15,),
            StyledTextFormField(
              initialValue: _.companyFormation.presenterDetail.referenceNumber,
              labelText: "Reference Number",
              labelStyle: defaultLabelStyle,
              hintText: "Enter reference number",
              hintStyle: defaultHintStyle,
              validate: (val){
                if(val.isEmpty){
                  return "Reference number is required";
                }
                return null;
              },
              onSaved: (val){
                _.companyFormation.presenterDetail.referenceNumber=val;
              },
            ),
            SizedBox(height: 15,),
            Align(
              alignment: Alignment.centerRight,
              child: RippleButton(
                backGroundColor: primaryColor,
                title: "Next",
                titleColor: Colors.white,
                fontSize: FontSizes.s20,
                borderRadius: BorderRadius.circular(10),
                shadowColor: Colors.purple,
                addShadow: true,
                elevation: 6,
                onPressed: (){
                  final registrationViewModel=Get.find<RegistrationViewModel>();
                  if(registrationViewModel.registrationFormKey.currentState.validate()){
                  registrationViewModel.registrationFormKey.currentState.save();
                  registrationViewModel.registrationViews[registrationViewModel.selectedIndex].completed=true;
                  registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
                  }
                },
              ),
            )
          ],
        ),
      );
    });
  }
}
