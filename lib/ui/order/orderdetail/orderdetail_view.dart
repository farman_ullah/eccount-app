import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/order_model.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/ui/order/orderdetail/orderdetail_controller.dart';
import 'package:eccount/ui/payment/paymentdetaill_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class OrderDetailView extends StatelessWidget {
  final OrderModel order;
  OrderDetailView({this.order});
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrderDetailsController>(
        init: OrderDetailsController(order: order),
        builder: (controller){
      return  Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: AutoSizeText("Order Details",style: TextStyles.title1.copyWith(color: Colors.black),),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body:SafeArea(
          bottom: true,
          top: false,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Get.width*0.05),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(height: Get.height*0.02,),
                  AutoSizeText("Basic Info",textAlign: TextAlign.center,style: TextStyles.h2,),
                  SizedBox(height: Get.height*0.02,),
                  Card(
                    child: Padding(
                      padding: EdgeInsets.all(Get.width*0.05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                AutoSizeText("Order Type",style: TextStyles.body1,),
                                AutoSizeText(getRegistrationTypeStringFromInt(controller.order.dataType),style: TextStyles.h2,),
                              ],),
                            ),
                            SizedBox(width: 5,),
                            RippleButton(onPressed: (){

                            },
                              title: "Edit",
                              titleColor: Colors.purple,
                            borderRadius: BorderRadius.circular(20),
                            borderColor: Colors.purple,
                            border: true,
                            height: Get.height*0.06,)
                          ],),
                          SizedBox(height: Get.height*0.02,),
                          Row(children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText("Status",style: TextStyles.body1,),
                                  AutoSizeText(getOrderStatusStringFromInt(controller.order.status)??"",style: TextStyles.h2,),
                                ],
                              ),
                            ),
                            SizedBox(width: 5,),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: kIsWeb?CrossAxisAlignment.end:CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText("Date",style: TextStyles.body1,),
                                  AutoSizeText(DateFormat.yMMMMd().format(controller.order.date)??"",style: TextStyles.h2,),
                                ],
                              ),
                            )
                          ],)
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: Get.height*0.02,),
                  controller.order.agentId==null?
                  SizedBox():AutoSizeText("Agent Info",textAlign: TextAlign.center,style: TextStyles.h2,),
                  SizedBox(height:  controller.order.agentId==null?0:Get.height*0.02,),
                  controller.order.agentId==null?
                 SizedBox()
                      :Card(
                    child: Padding(
                      padding: EdgeInsets.all(Get.width*0.05),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          SizedBox(height: Get.height*0.02,),
                          Row(children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText("Status",style: TextStyles.body1,),
                                  AutoSizeText(getOrderStatusStringFromInt(controller.order.status)??"",style: TextStyles.h2,),
                                ],
                              ),
                            ),
                            SizedBox(width: 5,),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText("Date",style: TextStyles.body1,),
                                  AutoSizeText(DateFormat.yMMMMd().format(controller.order.date)??"",style: TextStyles.h2,),
                                ],
                              ),
                            )
                          ],),
                        ],
                      ),
                    ),
                  ),
                  Expanded(child: SizedBox()),
                  controller.order.paid?SizedBox():RippleButton(onPressed: (){
                      Get.to(PaymentDetailView(order: order,));
                  },
                    width: Get.width,
                    title: "Pay",
                    titleColor: Colors.purple,
                    borderRadius: BorderRadius.circular(10),
                    borderColor: Colors.purple,
                    border: true,
                    fontSize: FontSizes.s24,
                    height: Get.height*0.065,),
                    SizedBox(height: Get.height*0.03,)
                ],
              ),
          ),
        )
      );
    });
  }
}
