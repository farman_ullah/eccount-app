

import 'dart:async';

import 'package:eccount/core/controllers/orders_controller.dart';
import 'package:eccount/models/order_model.dart';
import 'package:get/get.dart';

class OrderDetailsController extends GetxController{
  OrderModel order;
  OrderDetailsController({this.order});
  StreamSubscription orderChangeSubscription;

  listenToOrderChanges(){
    orderChangeSubscription=Get.find<OrderController>().orders.listen((data) {
      data.forEach((element) {
        if(element.id==order.id){
          order=element;
          update();
        }
      });
    });
  }

  @override
  void onInit() {
    listenToOrderChanges();
    super.onInit();
  }

  @override
  void onClose() {
    orderChangeSubscription?.cancel();
    super.onClose();
  }
}