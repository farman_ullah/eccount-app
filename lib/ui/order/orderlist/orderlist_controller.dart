

import 'package:eccount/core/controllers/orders_controller.dart';
import 'package:eccount/models/order_model.dart';
import 'package:get/get.dart';

class OrderListController extends GetxController{
    List<OrderModel> pastOrders=[];
    List<OrderModel> currentOrders=[];
    @override
  void onInit() {
    listenToOrdersList();
    super.onInit();
  }

  listenToOrdersList(){
      updateOrdersList(Get.find<OrderController>().orders);
      Get.find<OrderController>().orders.listen((data) {
        updateOrdersList(data);
      });
  }

  updateOrdersList(List<OrderModel> orders){
      pastOrders.clear();
      currentOrders.clear();
      orders.forEach((element) {
        if(element.completed){
          pastOrders.add(element);
        }
        else{
          currentOrders.add(element);
        }
      });
      update();
  }
}