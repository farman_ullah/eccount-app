import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/orders_controller.dart';
import 'package:eccount/shared/app_progress_indicatior.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/ui/order/orderdetail/orderdetail_view.dart';
import 'package:eccount/ui/order/orderlist/orderlist_controller.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class OrderListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrderListController>(
        init: OrderListController(),
        builder: (controller){
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          title: AutoSizeText("Orders",style: TextStyles.title1.copyWith(color: Colors.black),),
        ),
        body: Padding(padding: EdgeInsets.symmetric(horizontal: Get.width*0.05),
          child: Get.find<OrderController>().orders.length==0?
        Center(child: AutoSizeText("No Orders to show",style: TextStyles.h2,),)
            :controller.pastOrders.length==0 && controller.currentOrders.length==0?
        Center(child: AppProgressIndication()):
        CustomScrollView(
          slivers: [
            SliverToBoxAdapter(child: SizedBox(height: kToolbarHeight,),),
            SliverToBoxAdapter(child: controller.currentOrders.length!=0?AutoSizeText("Current Orders",style: TextStyles.h2,):SizedBox()),
            controller.currentOrders.length!=0?SliverList(delegate: SliverChildBuilderDelegate((_,index){
              return GestureDetector(
                onTap: (){
                  Get.to(OrderDetailView(order: controller.currentOrders[index],));
                },
                child: Card(
                  elevation: 10,
                  child: Padding(
                    padding: EdgeInsets.all(Get.width*0.05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AutoSizeText("Order Type",style: TextStyles.body1,),
                        AutoSizeText(getRegistrationTypeStringFromInt(controller.currentOrders[index].dataType),style: TextStyles.h2,),
                        SizedBox(height: Get.height*0.02,),
                       Row(children: [
                         Expanded(
                           child: Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [
                               AutoSizeText("Status",style: TextStyles.body1,),
                               AutoSizeText(getOrderStatusStringFromInt(controller.currentOrders[index].status)??"",style: TextStyles.h2,),
                             ],
                           ),
                         ),
                         SizedBox(width: 5,),
                         Expanded(
                           child: Column(
                             crossAxisAlignment: kIsWeb?CrossAxisAlignment.end:CrossAxisAlignment.start,
                             children: [
                               AutoSizeText("Date",style: TextStyles.body1,),
                               AutoSizeText(DateFormat.yMMMMd().format(controller.currentOrders[index].date)??"",style: TextStyles.h2,),
                             ],
                           ),
                         )
                       ],)
                      ],
                    ),
                  ),
                ),
              );
            },childCount: controller.currentOrders.length)):
            SliverToBoxAdapter(child: SizedBox(),)
          ],
        ),),
      );
    });
  }
}
