import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_signature_view.dart';
import 'package:eccount/ui/directoradd_views/directoradd_viewmodel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DirectorAddViewThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        title: AutoSizeText("Director Details",style: TextStyle(color: Colors.black),),
      ),
      body: GetBuilder<DirectorAddViewModel>(builder: (_){
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
          child: Form(
            key: _.directorFormKey3,
            child: Center(
              child: Container(
                constraints: BoxConstraints(maxWidth: 700),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                      AutoSizeText("Other directorships",style: TextStyles.h3,),
                        IconButton(icon: Icon(Icons.add,color: primaryColor,), onPressed: (){

                        })
                    ],),
                    SizedBox(height: 20,),
                    AutoSizeText("Consent",style: TextStyles.h2,),
                    SizedBox(height: 20,),
                    AutoSizeText("I hereby consent to act as director of the aforementioned company and I acknowledge that as director, I have legal duties and obligations imposed by the Companies Act, other statutes and at common law."),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizeText("Signature",style: TextStyles.h3,),
                        IconButton(icon: Icon(Icons.add,color: primaryColor,), onPressed: ()async{
                          var data=await Get.to(()=>SignatureView(false),preventDuplicates: false);
                          if(data!=null){
                            _.directorDetails.signatureImage=data;
                            _.update();
                          }
                        })
                      ],),
                    SizedBox(height: 20,),
                    _.directorDetails.signatureImage!=null?kIsWeb?Container(
                        height: Get.height*0.15,width: Get.width,
                        child: Image.network(_.directorDetails.signatureImage)):CachedNetworkImage(imageUrl:_.directorDetails.signatureImage,height: Get.height*0.15,width: Get.width,):SizedBox(),
                    SizedBox(height: _.directorDetails.signatureImage!=null?20:0,),
                    Align(
                      alignment: Alignment.centerRight,
                      child: RippleButton(
                        backGroundColor: primaryColor,
                        title: "Done",
                        titleColor: Colors.white,
                        fontSize: FontSizes.s20,
                        borderRadius: BorderRadius.circular(10),
                        shadowColor: Colors.purple,
                        addShadow: true,
                        elevation: 6,
                        onPressed: (){
//                          if(_.directorDetails.signatureImage==null){
//                            Show.showErrorSnackBar(title: "Error", error: "Please add your signature to continue");
//                            return;
//                          }
                          _.directorDetails.dateRegistered=DateTime.now();
                          Get.back();
                          Get.back();
                          Get.back(result: _.directorDetails);
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },),
    );
  }
}
