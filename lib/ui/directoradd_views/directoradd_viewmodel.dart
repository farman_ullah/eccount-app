import 'package:eccount/models/director_details.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class DirectorAddViewModel extends GetxController{
  DirectorAddViewModel({this.directorDetails});
  DirectorDetails directorDetails;
  TextEditingController dateOfBirthController;
  GlobalKey<FormState> directorFormKey1;
  GlobalKey<FormState> directorFormKey2;
  GlobalKey<FormState> directorFormKey3;
  @override
  void onInit() {
    directorFormKey1=GlobalKey<FormState>();
    directorFormKey2=GlobalKey<FormState>();
    directorFormKey3=GlobalKey<FormState>();
    dateOfBirthController=TextEditingController();
    if(directorDetails==null){
      directorDetails=DirectorDetails();
      directorDetails.alternateDirector=false;
    }
    else{
      dateOfBirthController.text=DateFormat.yMMMd().format(directorDetails.dateOfBirth);
    }
    super.onInit();
  }

  toggleEeaResident(bool value){
    directorDetails.eeaResident=value;
    update();
  }
  _buildIOsDatePicker(BuildContext context) async{
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (picked) {
                if (picked != null && picked != directorDetails.dateOfBirth && picked.isBefore(DateTime.now()))
                  directorDetails.dateOfBirth= picked;
                    dateOfBirthController.text=DateFormat.yMMMd().format(directorDetails.dateOfBirth);
                    update();
              },
              initialDateTime: DateTime.now().subtract(Duration(days: 4745)),
              minimumYear: 1950,
              maximumYear: DateTime.now().subtract(Duration(days: 4745)).year,
            ),
          );
        });
  }
  _buildAndroidDatePicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now().subtract(Duration(days: 4745)), // Refer step 1
      firstDate: DateTime(1950),
      lastDate: DateTime.now().subtract(Duration(days: 4745)),
    );
    if (picked != null && picked != directorDetails.dateOfBirth)
      directorDetails.dateOfBirth= picked;
        dateOfBirthController.text=DateFormat.yMMMd().format(directorDetails.dateOfBirth);
        update();
  }



  selectDate()async{
    final ThemeData theme = Theme.of(Get.context);
    assert(theme.platform != null);
    switch (theme.platform) {
      case TargetPlatform.android:
        return _buildAndroidDatePicker(Get.context);
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
      case TargetPlatform.iOS:
        return _buildIOsDatePicker(Get.context);
      case TargetPlatform.macOS:
        return _buildAndroidDatePicker(Get.context);
    }
  }


  toggleAlternateDirector(bool value) {
    directorDetails.alternateDirector=value;
    update();
  }
}