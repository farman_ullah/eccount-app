import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/models/director_details.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_checkbox_message.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/directoradd_views/directoradd_view2.dart';
import 'package:eccount/ui/directoradd_views/directoradd_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DirectorAddViewOne extends StatelessWidget {
  final DirectorDetails directorDetails;
  DirectorAddViewOne({this.directorDetails});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        title: AutoSizeText("Director Details",style: TextStyle(color: Colors.black),),
      ),
     body: GetBuilder<DirectorAddViewModel>(
        init: DirectorAddViewModel(directorDetails: directorDetails),
        builder: (_){
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
          child: SingleChildScrollView(
            child: Form(
              key: _.directorFormKey1,
              child: Center(
                child: Container(
                  constraints: BoxConstraints(maxWidth: 700),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      StyledInfoWidget(child: AutoSizeText("Please give details below of the persons who have consented in writing to become directors.",),text: CompanyFormation.noteSix,),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        initialValue: _.directorDetails.surName,
                        labelText: "Surname",
                        hintText: "Enter surname",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        onSaved: (val){
                          _.directorDetails.surName=val;
                        },
                        validate: (val){
                          if(val.isEmpty){
                            return "Surname is required";
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        initialValue: _.directorDetails.foreName,
                        labelText: "Forename",
                        hintText: "Enter forename",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        onSaved: (val){
                          _.directorDetails.foreName=val;
                        },
                        validate: (val){
                          if(val.isEmpty){
                            return "Forename is required";
                          }
                          return null;
                        },
                        suffix: GestureDetector(
                            onTap: (){
                              Show.showBottomSheet(CompanyFormation.noteSeven);
                            },
                            child: Icon(Icons.info_outline,size: 24,color: primaryColor,)),

                      ),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        initialValue: _.directorDetails.formerSurname,
                        labelText: "Former Surname",
                        hintText: "Enter former surname",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        onSaved: (val){
                          _.directorDetails.formerSurname=val;
                        },
                      ),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        initialValue: _.directorDetails.formerForename,
                        labelText: "Former Forename",
                        hintText: "Enter former forename",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        onSaved: (val){
                          _.directorDetails.formerForename=val;
                        },
                        suffix: GestureDetector(
                            onTap: (){
                              Show.showBottomSheet(CompanyFormation.noteEight);
                            },
                            child: Icon(Icons.info_outline,size: 24,color: primaryColor,)),

                      ),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        controller: _.dateOfBirthController,
                        labelText: "Date of birth",
                        hintText: "Enter date of birth",
                        readOnly: true,
                        onTapped: _.selectDate,
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        validate: (val){
                          if(val.isEmpty){
                            return "Date of birth is required";
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 20,),
                      StyledInfoWidget(child:  StyledCheckBoxMessage(value: _.directorDetails.eeaResident??false,onPressed: _.toggleEeaResident,message: 'EEA Resident',),text: CompanyFormation.noteNine,),
                      SizedBox(height: 20,),
                      Align(
                        alignment: Alignment.centerRight,
                        child: RippleButton(
                          backGroundColor: primaryColor,
                          title: "Next",
                          titleColor: Colors.white,
                          fontSize: FontSizes.s20,
                          borderRadius: BorderRadius.circular(10),
                          shadowColor: Colors.purple,
                          addShadow: true,
                          elevation: 6,
                          onPressed: (){
                              if(_.directorFormKey1.currentState.validate()){
                                _.directorFormKey1.currentState.save();
                                Get.to(()=>DirectorAddViewTwo());
                              }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },),
    );
  }
}
