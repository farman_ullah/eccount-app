import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/models/company_formation.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_checkbox_message.dart';
import 'package:eccount/styled_widgets/styled_info_widget.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/directoradd_views/directoradd_view3.dart';
import 'package:eccount/ui/directoradd_views/directoradd_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DirectorAddViewTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        title: AutoSizeText("Director Details",style: TextStyle(color: Colors.black),),
      ),
      body: GetBuilder<DirectorAddViewModel>(builder: (_){
        return SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
            child: Form(
              key: _.directorFormKey2,
              child: Center(
                child: Container(
                  constraints: BoxConstraints(maxWidth: 700),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      StyledTextFormField(
                        initialValue: _.directorDetails.residentialAddress,
                        labelText: "Residential Address",
                        hintText: "Enter residential address",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        validate: (val){
                          if(val.isEmpty){
                            return "Residential address is required";
                          }
                          return null;
                        },
                        onSaved: (val){
                          _.directorDetails.residentialAddress=val;
                        },
                        suffix: GestureDetector(
                            onTap: (){
                              Show.showBottomSheet(CompanyFormation.noteSeven);
                            },
                            child: Icon(Icons.info_outline,size: 24,color: primaryColor,)),

                      ),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        initialValue: _.directorDetails.postCode,
                        labelText: "Postcode",
                        hintText: "Enter postcode",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        onSaved: (val){
                          _.directorDetails.postCode=val;
                        },
                      ),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        initialValue: _.directorDetails.businessOccupation,
                        labelText: "Business occupation",
                        hintText: "Enter business occupation",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        onSaved: (val){
                          _.directorDetails.businessOccupation=val;
                        },
                      ),
                      SizedBox(height: 20,),
                      StyledTextFormField(
                        initialValue: _.directorDetails.nationality,
                        labelText: "Nationality",
                        hintText: "What's your nationality?",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        onSaved: (val){
                          _.directorDetails.nationality=val;
                        },
                      ),
                      SizedBox(height: 20,),
                      StyledInfoWidget(
                        child: StyledCheckBoxMessage(
                          value: _.directorDetails.alternateDirector,
                          onPressed: _.toggleAlternateDirector,
                          message: "Alternate Director",
                        ),
                        text: CompanyFormation.noteTen,
                      ),
                      SizedBox(height: 20,),
                      _.directorDetails.alternateDirector?StyledTextFormField(
                        initialValue: _.directorDetails.fullDirectorAppointingAlternateDirector,
                        labelText: "Full Director",
                        hintText: "Enter full director",
                        contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                        borderRadius: BorderRadius.circular(10),
                        hintStyle: TextStyle(fontSize: FontSizes.s18),
                        labelStyle: defaultLabelStyle,
                        validate: (val){
                          if(val.isEmpty){
                            return "Full director is required";
                          }
                          return null;
                        },
                        onSaved: (val){
                          _.directorDetails.fullDirectorAppointingAlternateDirector=val;
                        },
                      ):SizedBox(),
                      _.directorDetails.alternateDirector?SizedBox(height: 20,):SizedBox(),
                      Align(
                        alignment: Alignment.centerRight,
                        child: RippleButton(
                          backGroundColor: primaryColor,
                          title: "Next",
                          titleColor: Colors.white,
                          fontSize: FontSizes.s20,
                          borderRadius: BorderRadius.circular(10),
                          shadowColor: Colors.purple,
                          addShadow: true,
                          elevation: 6,
                          onPressed: (){
                            if(_.directorFormKey2.currentState.validate()){
                              _.directorFormKey2.currentState.save();
                              Get.to(()=>DirectorAddViewThree());
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },),
    );
  }
}
