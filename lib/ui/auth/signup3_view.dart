import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_strings.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/auth/auth_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpThreeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: GetBuilder<AuthViewModel>(builder: (_){
        return Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            AutoSizeText("Create\nyour account",
                style: TextStyle(fontSize: FontSizes.s24,fontWeight: FontWeight.w800,height: 1.5)),
            SizedBox(height: 20,),
            StyledTextFormField(
              validate: (String val){
                if(val.isEmpty){
                  return email_empty_error.tr;
                }
                else if(!GetUtils.isEmail(val)){
                  return email_invalid_error.tr;
                }
                return null;
              },
              onSaved: (val){
                _.newUser.email=val.toString().trim();
              },
              borderRadius: BorderRadius.circular(5),
              hintText: enter_email.tr,
              hintStyle: TextStyle(color: Colors.grey),
              labelText: email.tr,
              labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
              contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
              onEditingComplete:(){
                FocusScope.of(context).nextFocus();
              },
            ),

            SizedBox(height: 20,),
            StyledTextFormField(
              validate: (String val){
                if(val.isEmpty){
                  return "Phone Number is required".tr;
                }
                return null;
              },
              onSaved: (val){
                _.newUser.phoneNumber=val;
              },
              inputType: TextInputType.phone,
              borderRadius: BorderRadius.circular(5),
              hintText: "Enter your phonenumber",
              hintStyle: TextStyle(color: Colors.grey),
              labelText: "PhoneNumber",
              labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
              contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
              onEditingComplete:  (){
                FocusScope.of(context).nextFocus();
              },
            ),
            SizedBox(height: 20,),
            StyledTextFormField(
              controller: _.passwordController,
              validate: (String val){
                if(val.isEmpty){
                  return pass_empty_error.tr;
                }
                else if(val.length<8){
                  return pass_length_error.tr;
                }
                return null;
              },
              obscure: true,
              borderRadius: BorderRadius.circular(5),
              hintText: create_password.tr,
              hintStyle: TextStyle(color: Colors.grey),
              labelText: password.tr,
              labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
              contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
              onEditingComplete:  (){
                FocusScope.of(context).nextFocus();
              },
            ),
            SizedBox(height: 20,),
            StyledTextFormField(
              obscure: true,
              validate: (String val){
                if(val.isEmpty){
                  return pass_confirm_error.tr;
                }
                else if(val!=_.passwordController.text){
                  return pass_nomatch_error.tr;
                }
                return null;
              },
              borderRadius: BorderRadius.circular(5),
              hintText: confirm_password_msg.tr,
              hintStyle: TextStyle(color: Colors.grey),
              labelText: confirm_password.tr,
              labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
              contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
              onEditingComplete:  (){
                FocusScope.of(context).unfocus();
              },
            ),
            SizedBox(height: 20,),
            RippleButton(
              onPressed: (){
                if(_.signUpFormKey.currentState.validate()){
                  _.signUpFormKey.currentState.save();
                  _.signUp();
                }
              },
              shadowColor: Colors.purple,
              addShadow: true,
              backGroundColor: primaryColor,
              width: Get.width,
              elevation: 12,
              borderRadius: BorderRadius.circular(5),
              title: signup.tr,
              titleColor: Colors.white,
              fontSize: 20,
            )
          ],);
      }),
    );
  }
}
