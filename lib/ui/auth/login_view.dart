import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_strings.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/auth/auth_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/get_utils/get_utils.dart';
import 'package:get/get.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: GetBuilder<AuthViewModel>(builder: (_){
        return Form(
          key: _.loginFormKey,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              AutoSizeText("Welcome\nback",
                  style: TextStyle(fontSize: FontSizes.s30,fontWeight: FontWeight.w800,height: 1.5)),
              SizedBox(height: 20,),
              StyledTextFormField(
                validate: (String val){
                  if(val.isEmpty){
                    return email_empty_error.tr;
                  }
                  else if(!GetUtils.isEmail(val)){
                    return email_invalid_error.tr;
                  }
                  return null;
                },
                onSaved: (val){
                  _.newUser.email=val;
                },
                borderRadius: BorderRadius.circular(5),
                hintText: enter_email.tr,
                hintStyle: TextStyle(color: Colors.grey),
                labelText: email.tr,
                labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
                contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
                onEditingComplete:  (){
                  FocusScope.of(context).nextFocus();
                },
              ),
              SizedBox(height: 20,),
              StyledTextFormField(
                controller: _.passwordController,
                validate: (String val){
                  if(val.isEmpty){
                    return pass_empty_error.tr;
                  }
                  else if(val.length<8){
                    return pass_length_error.tr;
                  }
                  return null;
                },
                obscure: true,
                borderRadius: BorderRadius.circular(5),
                hintText: enter_pass.tr,
                hintStyle: TextStyle(color: Colors.grey),
                labelText: password.tr,
                labelStyle:defaultLabelStyle,
                contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
                onEditingComplete:  (){
                  FocusScope.of(context).unfocus();
                },
              ),
              SizedBox(height: 20,),
              AutoSizeText(forget_pass.tr,style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500,fontSize: FontSizes.s16),),
              SizedBox(height: 40,),
              RippleButton(
                onPressed: (){
                  if(_.loginFormKey.currentState.validate()){
                    _.loginFormKey.currentState.save();
                    _.login();
                  }
                },
                shadowColor: Colors.purple,
                addShadow: true,
                backGroundColor: primaryColor,
                width: Get.width,
                elevation: 12,
                borderRadius: BorderRadius.circular(5),
                title: login.tr,
                titleColor: Colors.white,
                fontSize: 20,
              ),
              SizedBox(height: 40,),
              AutoSizeText("\u{2501} or \u{2501}",style: TextStyle(color: Colors.grey,fontSize: FontSizes.s20),textAlign: TextAlign.center,),
              SizedBox(height: 20,),
              AutoSizeText(login_with.tr,style: TextStyle(color: Colors.black,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),textAlign: TextAlign.center,),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                SizedBox(
                    width: 50,
                    height: 50,
                    child: Card(
                      margin: EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100),),
                        elevation: 12,
                        child: Image.asset("assets/fb.png"))),
                SizedBox(width: 20,),
                SizedBox(
                    width: 50,
                    height: 50,
                    child: Card(
                        margin: EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100),),
                        elevation: 12,
                        child: Image.asset("assets/google.png"))),
                  SizedBox(width: 20,),
                SizedBox(
                    width: 50,
                    height: 50,
                    child: Card(
                        margin: EdgeInsets.all(0),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100),),
                        elevation: 12,
                        child: Image.asset("assets/apple.png")))
              ],)
            ],
          ),
        );
      }),
    );
  }
}
