
import 'package:eccount/models/user_model.dart';
import 'package:eccount/core/services/auth_service.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/shared/app_strings.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/ui/auth/signup1_view.dart';
import 'package:eccount/ui/auth/signup2_view.dart';
import 'package:eccount/ui/auth/signup3_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AuthViewModel extends GetxController{
  UserModel newUser;
  bool signup=true;
  bool showLoginPassword=false;
  bool showSignUpPassword=false;
  TextEditingController passwordController;
  TextEditingController dobController;
  final signUpFormKey=GlobalKey<FormState>();
  final loginFormKey=GlobalKey<FormState>();
  List<Widget> signUpViews=[
      SignUpOneView(),
    SignUpTwoView(),
    SignUpThreeView()
  ];

  int signUpIndex=0;
  setAuthType(bool value){
    if(signup!=value){
      newUser=UserModel();
      dobController.clear();
      passwordController.clear();
      signUpIndex=0;
      signup=value;
      update();
    }
  }


  toggleLoginShowPassword(){
    showLoginPassword=!showLoginPassword;
    update();
  }
  toggleSignUpShowPassword(){
    showSignUpPassword=!showSignUpPassword;
    update();
  }
  @override
  void onInit() {
    newUser=UserModel();
    dobController=TextEditingController();
    passwordController=TextEditingController();
    super.onInit();
  }

  login()async{
    Show.showLoader();
    try{
      final _authservice=Get.find<AuthService>();
      await _authservice.loginWithEmailAndPassword(email: newUser.email, password: passwordController.text);
      Get.back();
      Get.offAllNamed('/dashboard');
    }
    catch(e){
      Get.back();
      Show.showErrorSnackBar(title: signup_error.tr, error: e.message??"Unknown Error");
    }
  }
  signUp()async{
    Show.showLoader();
    try{
      final _authservice=Get.find<AuthService>();
      await _authservice.signUpWithEmailAndPassword(email: newUser.email, password: passwordController.text);
      await Get.find<FirestoreService>().createUser(_authservice.firebaseUser.uid, newUser);
      Get.back();
      Get.offAllNamed('/dashboard');
    }
    catch(e){
      Get.back();
      Show.showErrorSnackBar(title: signup_error.tr, error: e.message??"Unknown Error");
    }
  }

  continueSignUp(){
      if(signUpIndex==signUpViews.length-1){
        signUp();
        return;
      }
      signUpIndex+=1;
      update();
  }


  _buildIOsDatePicker(BuildContext context) async{
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (picked) {
                if (picked != null && picked != newUser.dateOfBirth && picked.isBefore(DateTime.now())){
                 return picked;
                }
              },
              initialDateTime: DateTime.now().subtract(Duration(days: 4745)),
              minimumYear: 1950,
              maximumYear: DateTime.now().subtract(Duration(days: 4745)).year,
            ),
          );
        });
  }
  _buildAndroidDatePicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now().subtract(Duration(days: 4745)), // Refer step 1
      firstDate: DateTime(1950),
      lastDate: DateTime.now().subtract(Duration(days: 4745)),
    );
    if (picked != null && picked != newUser.dateOfBirth && picked.isBefore(DateTime.now())){
      return picked;
    }

  }

  backSignUpPage(){
    if(signUpIndex!=0){
      signUpIndex-=1;
      update();
    }
  }

  selectDate(BuildContext context)async{
    final ThemeData theme = Theme.of(context);
    assert(theme.platform != null);
    switch (theme.platform) {
      case TargetPlatform.android:
        return _buildAndroidDatePicker(context);
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
      case TargetPlatform.iOS:
        return _buildIOsDatePicker(context);
      case TargetPlatform.macOS:
        return _buildAndroidDatePicker(context);
    }
  }

  @override
  void onClose() {
    passwordController?.dispose();
    dobController?.dispose();
    super.onClose();
  }
}