import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_strings.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/auth/auth_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class SignUpOneView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: GetBuilder<AuthViewModel>(builder: (_){
        return Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
          AutoSizeText("Create\nyour account",
          style: TextStyle(fontSize: FontSizes.s24,fontWeight: FontWeight.w800,height: 1.5)),
          SizedBox(height: 20,),
          StyledTextFormField(
            initialValue: _.newUser.name,
            validate: (String val){
              if(val.isEmpty){
                return name_empty_error.tr;
              }
              return null;
            },
            onSaved: (val){
              _.newUser.name=val;
            },
            borderRadius: BorderRadius.circular(5),
            hintText: enter_name.tr,
            hintStyle: TextStyle(color: Colors.grey),
            labelText: name.tr,
            labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
            contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
            onEditingComplete:  (){
              FocusScope.of(context).nextFocus();
            },
          ),
          SizedBox(height: 20,),
          StyledTextFormField(
            controller: _.dobController,
            readOnly: true,
            onTapped: ()async{
              var date=await _.selectDate(context);
              if(date!=null){
               _.newUser.dateOfBirth=date;
                _.dobController.text=DateFormat.yMMMd().format(_.newUser.dateOfBirth);
                _.update();
              }
            },
            borderRadius: BorderRadius.circular(5),
            hintText: "Enter your age",
            hintStyle: TextStyle(color: Colors.grey),
            labelText: "Age",
            labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
            contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
          ),
          SizedBox(height: 20,),
          RippleButton(
            onPressed: (){
              if(_.signUpFormKey.currentState.validate()){
                _.signUpFormKey.currentState.save();
                _.continueSignUp();
              }
            },
            shadowColor: Colors.purple,
            addShadow: true,
            backGroundColor: primaryColor,
            width: Get.width,
            elevation: 12,
            borderRadius: BorderRadius.circular(5),
            title: "Continue",
            titleColor: Colors.white,
            fontSize: 20,
          )
        ],);
      }),
    );
  }
}
