import 'package:animations/animations.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_strings.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/ui/auth/auth_viewmodel.dart';
import 'package:eccount/ui/auth/login_view.dart';
import 'package:eccount/ui/auth/signup1_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AuthView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthViewModel>(
        init: AuthViewModel(),
        builder: (_){
      return WillPopScope(child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: ResponsiveWrapper.of(context).scaledWidth,
            height: ResponsiveWrapper.of(context).scaledHeight,
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                SizedBox(
                    width: 150,
                    height: 150,
                    child: Image.asset('assets/logo.png')),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding:EdgeInsets.symmetric(horizontal: 20),
                  child: Row(children: [
                    Expanded(child: GestureDetector(
                      onTap: (){
                        _.setAuthType(true);
                      },
                      child: AnimatedContainer(duration: Duration(milliseconds: 500),
                        padding: EdgeInsets.symmetric(vertical: 15),
                        decoration: BoxDecoration(
                            border:Border(bottom: BorderSide(color: _.signup?primaryColor:Colors.grey[400],width: 2))
                        ),
                        child: Center(child: AutoSizeText(signup.tr,style: TextStyle(color: _.signup?primaryColor:Colors.grey[400],fontWeight: _.signup?FontWeight.w700:FontWeight.w500,fontSize: 20),),),),
                    )),
                    Expanded(child: GestureDetector(
                      onTap: (){
                        _.setAuthType(false);
                      },
                      child: AnimatedContainer(duration: Duration(milliseconds: 500),
                        padding: EdgeInsets.symmetric(vertical: 15),
                        decoration: BoxDecoration(
                            border:Border(bottom: BorderSide(color: _.signup?Colors.grey[400]:primaryColor,width: 2))
                        ),
                        child: Center(child: AutoSizeText(login.tr,style: TextStyle(color: _.signup?Colors.grey[400]:primaryColor,fontWeight: _.signup?FontWeight.w500:FontWeight.w700,fontSize: 20),),),),
                    ))
                  ],),
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: PageTransitionSwitcher(
                    duration: Duration(milliseconds: 1000),
                    reverse: _.signup,
                    transitionBuilder: (child,_,__){
                      return SharedAxisTransition(animation: _, secondaryAnimation: __, transitionType: SharedAxisTransitionType.horizontal,child: child,);
                    },
                    child: _.signup?Form(key: _.signUpFormKey,child: _.signUpViews[_.signUpIndex],):LoginView(),),
                )
              ],
            ),
          ),
        ),
      ), onWillPop: ()async{
        if(_.signUpIndex!=0 && _.signup){
          _.backSignUpPage();
          return false;
        }
        return true;
      });
    });
  }
}
