import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_strings.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/auth/auth_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

class SignUpTwoView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: GetBuilder<AuthViewModel>(builder: (_){
        return Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            AutoSizeText("Enter\nyour address",
                style: TextStyle(fontSize: FontSizes.s24,fontWeight: FontWeight.w800,height: 1.5)),
            SizedBox(height: 20,),
            StyledTextFormField(
              initialValue: _.newUser.address,
              validate: (String val){
                if(val.isEmpty){
                  return "Address is required";
                }
                return null;
              },
              onSaved: (val){
                _.newUser.address=val;
              },
              borderRadius: BorderRadius.circular(5),
              hintText: "Enter your address",
              hintStyle: TextStyle(color: Colors.grey),
              labelText: "Address",
              labelStyle: TextStyle(color: primaryColor,fontSize: FontSizes.s20,fontWeight: FontWeight.w600),
              contentPadding: EdgeInsets.symmetric(horizontal: 12,vertical: 18),
              onEditingComplete:  (){
                FocusScope.of(context).unfocus();
              },
            ),
            SizedBox(height: 20,),
            RippleButton(
              onPressed: (){
                if(_.signUpFormKey.currentState.validate()){
                  _.signUpFormKey.currentState.save();
                  _.continueSignUp();
                }
              },
              shadowColor: Colors.purple,
              addShadow: true,
              backGroundColor: primaryColor,
              width: ResponsiveWrapper.of(context).scaledWidth,
              elevation: 12,
              borderRadius: BorderRadius.circular(5),
              title: "Continue",
              titleColor: Colors.white,
              fontSize: 20,
            )
          ],);
      }),
    );
  }
}
