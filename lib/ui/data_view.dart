import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DataView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child:ListView(children: [
        Obx(()=>Get.find<UserController>().data!=null?AutoSizeText("${Get.find<UserController>().data}"):Container(
          height: 300,
          width: 300,
          color: Colors.pinkAccent,
        ))
      ],),
    );
  }
}
