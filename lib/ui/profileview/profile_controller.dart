

import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/core/services/multimedia_service.dart';
import 'package:get/get.dart';

class ProfileController extends GetxController{
  changeUserImage()async{
    final multiMediaService=Get.find<MultiMediaService>();
    SelectImage type=await multiMediaService.selectImageFrom();
    if(type!=null){
      String image=await multiMediaService.pickImage(type);
      if(image!=null){
        Get.find<UserController>().uploadUserImage(image);
      }
    }
  }
}