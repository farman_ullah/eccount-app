import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/core/services/auth_service.dart';
import 'package:eccount/ui/profileview/profile_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GetBuilder<ProfileController>(builder: (controller){
          return SingleChildScrollView(
            child: Container(
              height: Get.height,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Get.width*0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: kToolbarHeight,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: (){
                          Get.back();
                        }),
                        GestureDetector(
                          onTap: (){
                            Get.find<AuthService>().signOut();
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: AutoSizeText("Log out".tr,style: TextStyle(color: Colors.black),presetFontSizes: [18,16],),
                          ),
                        ),
                      ],),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: Get.height*0.15,
                          width: Get.height*0.15,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: Stack(
                            clipBehavior: Clip.none,
                            fit: StackFit.expand,
                            children: [
                              Obx(()=>Hero(
                                tag: Get.find<UserController>().currentUser.value.id??"",
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Get.find<UserController>().currentUser.value.imageUrl!=null?CachedNetworkImage(
                                    imageUrl: Get.find<UserController>().currentUser.value.imageUrl,
                                    fit: BoxFit.cover,
                                  ):Image.asset("assets/person.png"),
                                ),
                              ),),
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: GestureDetector(
                                    onTap: (){
                                      controller.changeUserImage();
                                    },
                                    child: Container(
                                        padding: EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.purple,
                                            shape: BoxShape.circle
                                        ),
                                        child: Icon(Icons.camera_alt_outlined,color: Colors.white,)),
                                  ))
                            ],),
                        ),
                      ],),
                    SizedBox(height: Get.height*0.02,),
                    AutoSizeText(Get.find<UserController>().currentUser.value.email??"",textAlign: TextAlign.center,),
                    Expanded(child: SizedBox())
                  ],
                ),
              ),
            ),
          );
        },)
    );
  }
}
