import 'dart:math';

import 'package:animations/animations.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/ripple_button.dart';
import 'package:eccount/styled_widgets/styled_card.dart';
import 'package:eccount/ui/auth/auth_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:gooey_carousel/gooey_carrousel.dart';


class OnboardingView extends StatefulWidget {
  @override
  _OnboardingViewState createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GooeyCarousel(
            children: <Widget>[
              ContentCard(
                color: 'Red',
                altColor: Color(0xFF4259B2),
                title: "Apply for Irish e-Residency\nonline",
                subtitle:
                'Apply for Irish digital identity for non-residents',
              ),
              ContentCard(
                color: 'Yellow',
                altColor: Color(0xFF904E93),
                title: "Log in to register a company online",
                subtitle:
                'Self setup and virtual office.',
              ),
              ContentCard(
                color: 'Blue',
                altColor: Color(0xFFFFB138),
                title: "Add virtual office and contact\nperson service",
                subtitle:
                'Subscribe to Eccounts virtual office service when registering your company.',
              ),
            ],
          ),
        ],
      ),
    );
  }
}


class ContentCard extends StatefulWidget {
  final String color;
  final Color altColor;
  final String title;
  final String subtitle;

  ContentCard({this.color, this.title = "", this.subtitle, this.altColor}) : super();

  @override
  _ContentCardState createState() => _ContentCardState();
}

class _ContentCardState extends State<ContentCard> {
  Ticker _ticker;

  @override
  void initState() {
    _ticker = Ticker((d) {
      setState(() {});
    })
      ..start();
    super.initState();
  }

  @override
  void dispose() {
    _ticker.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var time = DateTime.now().millisecondsSinceEpoch / 2000;
    var scaleX = 1.2 + sin(time) * .05;
    var scaleY = 1.2 + cos(time) * .07;
    var offsetY = 20 + cos(time) * 20;
    return Stack(
      alignment: Alignment.center,
      fit: StackFit.expand,
      children: <Widget>[
        Transform(
          transform: Matrix4.diagonal3Values(scaleX, scaleY, 1),
          child: Transform.translate(
            offset: Offset(-(scaleX - 1) / 2 * size.width, -(scaleY - 1) / 2 * size.height + offsetY),
            child: Image.asset('assets/Bg-${widget.color}.png', fit: BoxFit.cover),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.only(top: 75.0, bottom: 25.0),
            child: Column(
              children: <Widget>[
                //Top Image
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Image.asset('assets/Illustration-${widget.color}.png', fit: BoxFit.contain),
                  ),
                ),

                //Slider circles
                Container(height: 14, child: Image.asset('assets/Slider-${widget.color}.png')),

                //Bottom content
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 18.0),
                    child: _buildBottomContent(),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildBottomContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(widget.title,
            textAlign: TextAlign.center,
            style: TextStyle(height: 1.2, fontSize: 30.0, fontFamily: 'DMSerifDisplay', color: Colors.white)),
        Text(widget.subtitle,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300, fontFamily: 'OpenSans', color: Colors.white)),
        Padding(
          padding:  EdgeInsets.symmetric(horizontal: 25.0),
          child:  Row(children: [
            Expanded(
              flex:2,
              child: RippleButton(
                elevation: 10,
                backGroundColor: widget.altColor,
                title: "Start Now",
                fontSize: 18,
                titleColor: Colors.white,
                borderRadius: BorderRadius.circular(8),
                onPressed: (){

                },
              ),
            ),
            SizedBox(width: 10,),
            Expanded(
              flex: 1,
              child: OpenContainer(
                  closedElevation: 10,
                  closedColor: Colors.black12.withAlpha(0),
                  transitionType: ContainerTransitionType.fadeThrough,
                  closedShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  transitionDuration: Duration(milliseconds: 500),
                  closedBuilder: (_,__){
                return StyledCard(title: "Login",backgroundColor:widget.altColor,
                  titleColor: Colors.white,
                  borderRadius: 10,
                  padding: EdgeInsets.symmetric(vertical: 20),
                  elevation: 0,shadowColor: widget.altColor,titleFontSize: 18,);
              }, openBuilder: (_,__){
                return AuthView();
              }),
            )
          ],)
        )
      ],
    );
  }
}