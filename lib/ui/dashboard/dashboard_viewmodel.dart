
import 'package:get/get.dart';

class DashboardViewModel extends GetxController{

  getCurrentGreeting(){
    final now=DateTime.now();
    if(now.hour>6 && now.hour<12){
      return "Good Morning";
    }
    else if(now.hour>=12 && now.hour<17){
      return "Good Afternoon";
    }
    else {
      return "Good Evening";
    }
  }
}