import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/shared/enums.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/styled_widgets/styled_dashboard_card.dart';
import 'package:eccount/styled_widgets/styled_text_form_field.dart';
import 'package:eccount/ui/agreement/agreement_view.dart';
import 'package:eccount/ui/dashboard/dashboard_viewmodel.dart';
import 'package:eccount/ui/drawer/drawer_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

class DashBoardView extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey=GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      endDrawer: DrawerView(),
      body: GetBuilder<DashboardViewModel>(builder: (_){
        return SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: ResponsiveWrapper.of(context).screenHeight*0.45,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [Colors.lightBlueAccent,Colors.deepPurple],begin: Alignment.topRight,end: Alignment.bottomLeft)
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Stack(children: [
                    Center(
                      child: Opacity(
                          opacity: 0.1,
                          child: Image.asset("assets/dashboard_illustration.png")),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: kToolbarHeight,),
                        Row(children: [
                          Flexible(
                            child: GestureDetector(
                              onTap:(){
                                Get.toNamed("/profile");
                              },
                              child: Row(
                                children: [Container(
                                  height: 60,
                                  width: 60,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.white,width: 2),
                                      borderRadius: BorderRadius.circular(12)
                                  ),
                                  child: Obx((){
                                    return ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Get.find<UserController>().currentUser.value.imageUrl!=null?CachedNetworkImage(imageUrl: Get.find<UserController>().currentUser.value.imageUrl,fit: BoxFit.cover,):Image.asset("assets/person.png"),
                                    );
                                  }),
                                ),
                                  SizedBox(width: 10,),
                                  Flexible(
                                    child: Obx((){
                                      return AutoSizeText.rich(TextSpan(
                                          children: [
                                            TextSpan(text:_.getCurrentGreeting()),
                                            TextSpan(text: Get.find<UserController>().currentUser.value.name!=null?"\n"+Get.find<UserController>().currentUser.value.name:"\n"+"User",style: TextStyle(fontWeight: FontWeight.w700,fontSize: FontSizes.s18))
                                          ]
                                      ),style: TextStyle(color: Colors.white,),);
                                    }),
                                  ),],
                              ),
                            ),
                          ),
                          GestureDetector(
                              onTap: (){
                                _scaffoldKey.currentState.openEndDrawer();
                              },
                              child: Icon(Icons.menu,color: Colors.white,size: 50,))
                        ],),
                        SizedBox(height: 10,),
                        AutoSizeText("Check \nname availability",style: TextStyle(color: Colors.white,fontSize: FontSizes.s24,letterSpacing: 1.3,fontWeight: FontWeight.w700),),
                        SizedBox(height: 10,),
                        AutoSizeText("The simplest way to start an Irish company.Start by checking your business name availability",style: TextStyle(color: Colors.white,fontSize: FontSizes.s16)),
                        Spacer(),
                        StyledTextFormField(
                          backGroundColor: Colors.white,
                          contentPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                          borderRadius: BorderRadius.circular(10),
                          hintText: "Type your company name",
                          hintStyle: TextStyle(fontSize: FontSizes.s18),
                          suffix: Container(
                            margin: EdgeInsets.all(8),
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                                gradient: LinearGradient(colors: [Colors.lightBlueAccent,Colors.deepPurple],begin: Alignment.topCenter,end: Alignment.bottomCenter)
                            ),
                            child: Center(child: Icon(Icons.search,size: 30,color: Colors.white,),),
                          ),
                        ),
                        Spacer()
                      ],
                    )
                  ],),
                ),
              ),
              ResponsiveRowColumn(
                layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)?ResponsiveRowColumnType.COLUMN:ResponsiveRowColumnType.ROW,
                columnPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                rowCrossAxisAlignment: CrossAxisAlignment.start,
                columnCrossAxisAlignment: CrossAxisAlignment.center,
                rowPadding: EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                rowSpacing: 20,
                columnSpacing: 10,
              children: [
               ResponsiveRowColumnItem(
                   rowFlex: 1,
                   rowFit: FlexFit.tight,
                   child: StyledDashboardCard(
                 index: 0,
                 title: "Company\nFormation",
                 subTitle: "Start your business now",
                 icon: "assets/company_formation.png",
                 openedWidget: AgreementView(registrationType: RegistrationType.CompanyFormation,),
               )),
                ResponsiveRowColumnItem(
                    rowFlex: 1,
                    rowFit: FlexFit.tight,
                    child: StyledDashboardCard(
                  index: 1,
                  title: "Business Name\nRegistration",
                  subTitle: "Get your business registered today",
                  icon: "assets/business_name.png",
                  openedWidget: AgreementView(registrationType: RegistrationType.BusinessNameRegistration,),
                )),
                ResponsiveRowColumnItem(
                    rowFlex: 1,
                    rowFit: FlexFit.tight,
                    child: StyledDashboardCard(
                  index: 2,
                  title: "VAT\nRegistration",
                  subTitle: "List your business with the goverment",
                  icon: "assets/tax_registration.png",
                  openedWidget: AgreementView(registrationType: RegistrationType.VatRegistration,),
                ))
              ],
              )
            ],
          ),
        );
      })
    );
  }
}
