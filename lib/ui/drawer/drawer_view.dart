import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class DrawerView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          DrawerHeader(child: Center(child: Image.asset("assets/logo.png"),)),
          ListTile(
            leading: Icon(Icons.shopping_cart_outlined,color: Colors.purple,),
            title: AutoSizeText("Orders"),
            onTap: (){
              Get.toNamed("/orders");
            },
          ),
          ListTile(
            leading: Icon(Icons.notifications_outlined,color: Colors.purple,),
            title: AutoSizeText("Notifications"),
          ),
          ListTile(
            leading: Icon(Icons.help_center_outlined,color: Colors.purple,),
            title: AutoSizeText("Help center"),
          ),
          ListTile(
            leading: Icon(Icons.settings_outlined,color: Colors.purple,),
            title: AutoSizeText("Settings"),
          ),
          ListTile(
            leading: Icon(Icons.book,color: Colors.purple,),
            title: AutoSizeText("Terms & Conditions / Privacy"),
          )
        ],
      ),
    );
  }
}
