import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/models/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AuthService extends GetxService{


  final auth=FirebaseAuth.instance;
  User get firebaseUser=>auth.currentUser;
  bool get checkLoggedIn=>auth.currentUser !=null;

  Stream<User> fireBaseAuthUserStream(){
    return auth.authStateChanges();
  }

  signUpWithEmailAndPassword({@required String email,@required String password})async{
    try{
      await auth.createUserWithEmailAndPassword(email: email, password: password);
    }
    catch(e){
      rethrow;
    }
  }

  loginWithEmailAndPassword({@required String email,@required String password})async{
    try{
      await auth.signInWithEmailAndPassword(email: email, password: password);
    }
    catch(e){
      rethrow;
    }
  }

  signOut()async{
    Get.find<UserController>().currentUser.value=UserModel();
    await auth.signOut();
    Get.offAllNamed('/auth');
  }

}