
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';

class FireStorageService extends GetxService{

  Reference storageReference=FirebaseStorage.instance.ref();

  Future<String> uploadData({@required Uint8List data})async{
    String imageUrl;
    try{
      var uuid = Uuid();

      final Reference fileReference = storageReference.child("${uuid.v4()}");
      final UploadTask uploadTask = fileReference.putData(data);

      final StreamSubscription<TaskSnapshot> streamSubscription =
      uploadTask.asStream().listen((event) {
        print('EVENT ${event.state}');
        print(event.bytesTransferred);
      });

      // Cancel your subscription when done.
      await uploadTask.whenComplete(() => streamSubscription.cancel());


      imageUrl = await fileReference.getDownloadURL();
      return imageUrl;
    }
    catch(e){
      rethrow;
    }
  }


  Future<String> uploadImage(String file,int height,int width,int quality)async{
    String imageUrl;
    try{
      final Reference fileReference = storageReference.child("${file.split("/").last}");
      final UploadTask uploadTask = fileReference.putFile(File(file));

      final StreamSubscription<TaskSnapshot> streamSubscription =
      uploadTask.asStream().listen((event) {
        print('EVENT ${event.state}');
        print(event.bytesTransferred);
      });

      // Cancel your subscription when done.
      await uploadTask.whenComplete(() => streamSubscription.cancel());


      imageUrl = await fileReference.getDownloadURL();
      return imageUrl;
    }
    catch(e){
      print(e);
      return imageUrl;
    }
  }
}