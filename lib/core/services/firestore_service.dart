import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eccount/core/controllers/payment_controller.dart';
import 'package:eccount/models/card_model.dart';
import 'package:eccount/models/order_model.dart';
import 'package:eccount/models/payment_model.dart';
import 'package:eccount/models/user_model.dart';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FirestoreService extends GetxService{
  final _firestore=FirebaseFirestore.instance;

  Stream<DocumentSnapshot> fireStoreUserStream(String userId){
    return _firestore.collection("users").doc(userId).snapshots();
  }

  Future createUser(String id,UserModel user)async{
    try{
      DocumentSnapshot snapshot=await _firestore.collection('users').doc(id).get();
      print(snapshot.exists);
      if(snapshot.exists){
        updateUser(id, user.toMap());
      }
      else{
        await _firestore.collection('users').doc(id).set(user.toMap(),SetOptions(merge: true));
      }
    }
    catch(e){
      rethrow;
    }
  }


  Stream<List<OrderModel>> getCurrentUserOrders({@required String userId}){
    return _firestore.collection("orders").where("user_id",isEqualTo: userId).snapshots().map((event) => event.docs.map((e) => OrderModel.fromJson(e.data(),e.id)).toList());
  }

  Stream<List<CardModel>>  getCurrentUserPaymentMethods(String userId){
    return _firestore.collection("users").doc(userId).collection("payment_methods").snapshots().map((event) => event.docs.map((e) => CardModel.fromJson(e.data(), e.id)).toList());
  }

  addPaymentMethod(String id,String paymentMethodId)async{
    try{
      await _firestore.collection("users").doc(id).collection("payment_methods").add({"id":paymentMethodId});
    }
    catch(e){
      rethrow;
    }
  }


  addUserPayment(String userId,PaymentModel model)async{
    try{
          await _firestore.collection("users").doc(userId).collection("payments").add(model.toMap());
    }
    catch(e){
      rethrow;
    }
  }

  updateUser(String id,Map data)async{
    try{
      await _firestore.collection('users').doc(id).update(Map.from(data));
    }
    catch(e){
      print(e);
      rethrow;
    }
  }

  Future<UserModel> getUser(String id)async{
    try{
      DocumentSnapshot snapshot=await _firestore.collection("users").doc(id).get();
      if(snapshot.exists){
        UserModel user=UserModel.fromJson(snapshot.data(), snapshot.id);
        return user;
      }else{
        return null;
      }
    }
    catch(e){
      rethrow;
    }
  }

  Future<OrderModel> createNewOrder(Map data,int dataType)async{
    try{
      OrderModel order=OrderModel();
      order.paid=false;
      order.completed=false;
      order.userId=Get.find<UserController>().currentUser.value.id;
      order.dataType=dataType;
      order.date=DateTime.now();
      order.orderData=data;
      order.status=0;
      DocumentReference snapshot=await _firestore.collection('orders').add(order.toMap());
      order.id=snapshot.id;
      return order;
    }
    catch(e){
      rethrow;
    }
  }

  Future getData()async{
    _firestore.collection('orders').get().then((value){
      value.docs.forEach((element) {
        print(element.data());
      });
    });
  }
}