import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

enum SelectImage{
  fromCamera,
  fromGallery
}

class MultiMediaService extends GetxService{
  final picker = ImagePicker();


  Future<String> pickImage(SelectImage type)async{
    PickedFile pickedFile ;
    if(type==SelectImage.fromGallery){
      pickedFile = await picker.getImage(source: ImageSource.gallery);
    }else{
      pickedFile = await picker.getImage(source: ImageSource.camera);
    }
    return pickedFile?.path;
  }

//  Future<String> cropImage(String image)async{
//    File croppedFile = await ImageCropper.cropImage(
//        sourcePath: image,
//        aspectRatioPresets: [
//          CropAspectRatioPreset.square,
//          CropAspectRatioPreset.ratio3x2,
//          CropAspectRatioPreset.original,
//          CropAspectRatioPreset.ratio4x3,
//          CropAspectRatioPreset.ratio16x9
//        ],
//        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
//        androidUiSettings: AndroidUiSettings(
//            toolbarTitle: 'Cropper',
//            toolbarColor: Colors.deepOrange,
//            toolbarWidgetColor: Colors.white,
//            initAspectRatio: CropAspectRatioPreset.original,
//            lockAspectRatio: true),
//        iosUiSettings: IOSUiSettings(
//          minimumAspectRatio: 1.0,
//        )
//    );
//    return croppedFile?.path;
//  }


  Future<SelectImage> selectImageFrom()async{
    return await Get.defaultDialog(title: "select_image".tr,middleText: "select_image_option".tr,
      cancel: ElevatedButton(onPressed: (){
        Get.back(result: SelectImage.fromGallery);
      },child: Text("From Gallery".tr,style: TextStyle(color: Colors.white),),
        style: ButtonStyle(backgroundColor:MaterialStateProperty.all<Color>(Colors.purple) ),
      ),confirm: ElevatedButton(
        style: ButtonStyle(backgroundColor:MaterialStateProperty.all<Color>(Colors.purple) ),
        onPressed: (){
        Get.back(result: SelectImage.fromCamera);
      },child: Text("From Camera".tr,style: TextStyle(color: Colors.white),),
      ),);
  }

  Future<String> compressImage(String image)async{

  }

}