

import 'dart:async';

import 'package:eccount/core/controllers/orders_controller.dart';
import 'package:eccount/core/controllers/payment_controller.dart';
import 'package:eccount/core/services/auth_service.dart';
import 'package:eccount/core/services/firestorage_service.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/models/user_model.dart';
import 'package:eccount/shared/show.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserController extends GetxService{
  Rx<UserModel> currentUser=UserModel().obs;
  Map data;
  StreamSubscription authUserSubscription;
  StreamSubscription firestoreUserStream;

  currentUserStream(){
    print("listening to stream");
    authUserSubscription=Get.find<AuthService>().fireBaseAuthUserStream().listen((User event1) {
      print(event1);
      if(event1!=null){
        firestoreUserStream = Get.find<FirestoreService>().fireStoreUserStream(event1.uid).listen((event2) {
          if(event2.exists){
            try{
              currentUser.value = UserModel.fromJson(Map.from(event2.data() as Map), event2.id);
              Get.find<PaymentsController>().listenToPaymentMethods();
              Get.find<OrderController>().listenToUserOrders();
            }
            on Exception catch(exception){
              Show.showErrorSnackBar(title:"User Error",error: exception.toString());
            }
            catch (e){
              Show.showErrorSnackBar(title:"User Error", error:e.toString());
            }
          }
        });
      }
      else{
        currentUser.value=UserModel();
      }
    });
  }

  updateCurrentUser({@required UserModel user}){
   currentUser.value=user;
  }

  uploadUserImage(String image)async{
    try{
      String imageUrl=await Get.find<FireStorageService>().uploadImage(image, 400, 400,80);
      if(imageUrl==null){
        throw Exception;
      }
      Get.find<FirestoreService>().updateUser(currentUser.value.id, {"image_url":imageUrl});
    }
    catch(e){
      Show.showErrorSnackBar(title: "Upload Error", error: "Cannot upload user image");
    }
  }

  @override
  void onInit() {
    currentUserStream();
    super.onInit();
  }

  @override
  void onClose() {
    authUserSubscription?.cancel();
    firestoreUserStream?.cancel();
    super.onClose();
  }
}