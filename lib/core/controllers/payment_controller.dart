import 'dart:async';
import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/models/card_model.dart';
import 'package:eccount/ui/payment/creditcard_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_stripe_payment/flutter_stripe_payment.dart';
import 'package:get/get.dart';


class PaymentsController extends GetxService{
  FlutterStripePayment _stripePayment;
  StreamSubscription paymentMethodsSubscription;
  RxList<CardModel> cards=RxList();
@override
  void onInit() {
    if(!kIsWeb)
    _stripePayment = FlutterStripePayment()..setStripeSettings("pk_test_51GreejLDS5lViaDa2Px07Cl9kQvmZIJNDWUcWsHtiKrTGwLirtJc3EWKM1ITU9A2SoC0MZZTuOSn9ExRATZ567JH00wb2sn9b8");
    super.onInit();
  }

  listenToPaymentMethods(){
    paymentMethodsSubscription = Get.find<FirestoreService>().getCurrentUserPaymentMethods(Get.find<UserController>().currentUser.value.id).listen((event) {
      cards.assignAll(event);
    });
  }

  createPaymentMethod()async{
    if(kIsWeb){
      String data=await Get.to(()=>CreditCardView());
      if(data!=null){
        Get.find<FirestoreService>().addPaymentMethod(Get.find<UserController>().currentUser.value.id,data);
      }
    }
    else{
      var paymentResponse = await _stripePayment.addPaymentMethod();
      if(paymentResponse.status == PaymentResponseStatus.succeeded){
        Get.find<FirestoreService>().addPaymentMethod(Get.find<UserController>().currentUser.value.id,paymentResponse.paymentMethodId);
      }
      else if(paymentResponse.status == PaymentResponseStatus.failed){
        print("failed");
      }
      else{
        print("canceled by user");
      }
    }
  }

  @override
  void onClose() {
    paymentMethodsSubscription?.cancel();
    super.onClose();
  }
}