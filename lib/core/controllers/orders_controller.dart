

import 'dart:async';

import 'package:eccount/core/controllers/user_controller.dart';
import 'package:eccount/core/services/firestore_service.dart';
import 'package:eccount/models/order_model.dart';
import 'package:get/get.dart';

class OrderController extends GetxService{
  RxList<OrderModel> orders = RxList();
  StreamSubscription userOrdersStream;


  listenToUserOrders(){
    userOrdersStream=Get.find<FirestoreService>().getCurrentUserOrders(userId: Get.find<UserController>().currentUser.value.id).listen((event) {
      orders.assignAll(event);
    });
  }

  @override
  void onClose() {
    userOrdersStream?.cancel();
    super.onClose();
  }
}