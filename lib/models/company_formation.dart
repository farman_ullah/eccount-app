/*import 'package:eccount/models/agreement_single_view.dart';
import 'package:eccount/models/companycapital_model.dart';
import 'package:eccount/models/director_details.dart';
import 'package:eccount/models/presenter_detail.dart';
import 'package:eccount/models/registration_single_view.dart';
import 'package:eccount/models/secretary_detail.dart';
import 'package:eccount/models/subscribers.dart';
import 'package:eccount/styled_widgets/styled_signature_view.dart';
import 'package:eccount/ui/payment/creditcard_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/company_email_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companycapital_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyname_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companytype_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/declaration_view1.dart';
import 'package:eccount/ui/registrations/comapny_formation/declaration_view2.dart';
import 'package:eccount/ui/registrations/comapny_formation/directorslist_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/namerestriction_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/presenterdetail_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/registered_office_detail_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/reviewinfo_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/subscribersconstitution_view.dart';
import 'package:eccount/ui/secretaryadd_views/secretaryadd_view1.dart';
import 'package:eccount/ui/registrations/comapny_formation/typeexemption_view.dart';
import 'package:eccount/ui/secretaryadd_views/secretaryadd_view2.dart';
import 'package:eccount/ui/secretaryadd_views/secretaryadd_view3.dart';
import 'package:eccount/ui/web_specific/registrations/companyformation/compantname_view.dart';
import 'package:eccount/ui/web_specific/registrations/companyformation/payment_view.dart';
import 'package:eccount/ui/web_specific/registrations/companyformation/presenterdetail_view.dart';
import 'package:eccount/ui/web_specific/registrations/companyformation/webcompanyprofile_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class CompanyFormation{
  String companyName;
  String companyType;
  int companyTypeCode;
  PresenterDetail presenterDetail;
  String registeredOfficeAddress;
  bool registeredOfficeAddressSameAsAgentOffice;
  String companyEmailAddress;
  String companyPhoneNumber;
  bool typeExemption;
  bool nameRestriction;
  List<DirectorDetails> directorDetailsList;
  SecretaryDetail secretaryDetails;
  List<Subscribers> subscribers;
  CompanyCapitalModel companyCapitalModel;
  String declarationName;
  String declarationResidentialAddress;
  int declarationType;
  String naceCode;
  String natureOfActivity;
  String activityDescribedAs;
  String placesOfActivity;
  String centralAdministrationPlaces;
  String declarationSignatureImage;
  DateTime declarationDate;

  CompanyFormation({this.companyPhoneNumber,this.subscribers,this.declarationDate,this.declarationSignatureImage,this.centralAdministrationPlaces,this.placesOfActivity,this.activityDescribedAs,this.natureOfActivity,this.naceCode,this.declarationType,this.declarationResidentialAddress,this.declarationName,this.companyCapitalModel,this.secretaryDetails,this.directorDetailsList,this.nameRestriction,this.typeExemption,this.companyEmailAddress,this.registeredOfficeAddressSameAsAgentOffice,this.registeredOfficeAddress,this.presenterDetail,this.companyTypeCode,this.companyType,this.companyName});

  CompanyFormation.fromJson(Map data):
      companyName=data['company_name'],
      companyType=data['company_type'],
      companyTypeCode=data['comapny_type_code'],
      presenterDetail=PresenterDetail.fromJson(data['presenter_details']),
      registeredOfficeAddress=data['registered_office_address'],
      registeredOfficeAddressSameAsAgentOffice=data['registered_office_same_as_agent'],
      companyEmailAddress=data['company_emailaddress'],
      typeExemption=data['type_excemption'],
      nameRestriction=data['name_restriction'],
      companyPhoneNumber=data['company_phonenumber'],
      directorDetailsList=data['director_details'].map((e)=>DirectorDetails.fromJson(e)).toList(),
      secretaryDetails=SecretaryDetail.fromJson(data['secretary_details']),
      subscribers=data['subscribers'].map((e)=>Subscribers.fromJosn(e)).toList(),
      companyCapitalModel=CompanyCapitalModel.fromJson(data['company_capital_model']),
      declarationName=data['declaration_name'],
      declarationResidentialAddress=data['declaration_residential_address'],
      declarationType=data['declaration_type'],
      naceCode=data['nace_code'],
      natureOfActivity=data['nature_of_activity'],
      activityDescribedAs=data['activity_described_as'],
      placesOfActivity=data['place_of_activity'],
      centralAdministrationPlaces=data['central_administration_places'],
      declarationSignatureImage=data['declaration_signature_image'],
      declarationDate=data['declaration_date']!=null?DateTime.parse(data['declaration_date']):null;



  toMap(){
    return {
      'company_name':companyName,
      'company_type':companyType,
      'comapny_type_code':companyTypeCode,
      'presenter_details':presenterDetail.toMap(),
      'registered_office_address':registeredOfficeAddress,
      'registered_office_same_as_agent':registeredOfficeAddressSameAsAgentOffice,
      'company_emailaddress':companyEmailAddress,
      'type_excemption':typeExemption,
      'name_restriction':nameRestriction,
      'director_details':directorDetailsList.map((e) => e.toMap()).toList(),
      'secretary_details':secretaryDetails.toMap(),
      'company_capital_model':companyCapitalModel.toMap(),
      'declaration_name':declarationName,
      'company_phonenumber':companyPhoneNumber,
      'declaration_residential_address':declarationResidentialAddress,
      'declaration_type':declarationType,
      'nace_code':naceCode,
      'subscribers':subscribers.map((e) => e.toMap()).toList(),
      'nature_of_activity':natureOfActivity,
      'activity_described_as':activityDescribedAs,
      'place_of_activity':placesOfActivity,
      'central_administration_places':centralAdministrationPlaces,
      'declaration_signature_image':declarationSignatureImage,
      'declaration_date':declarationDate?.toString()
    };
  }
  static const List<String> companyTypes=[
    'Private Limited Company',
    'Single Member Company',
    'Designated Activity Company',
    'Public Limited Company',
    'General Partnership',
    'Limited Partnership',
    'Freelance or soletrader',
    'Unlimited Company/Private Unlimited Company'
  ];
  static const String generalMessage="This form must be completed correctly, in full and in accordance with the following notes. Every section of the form must be completed. Where “not applicable”, “nil” or “none” is appropriate, please state. Where €/_ appears, please insert/delete as appropriate. Where /_ applies, give the relevant currency, if not euro. Where the space provided on Form A1 is considered inadequate, the information should be presented on a continuation sheet in the same format as the relevant section in the form. The use of a continuation sheet must be so indicated in the relevant section.";
  static const String noteOne='The proposed company name must be given in full and must correspond exactly with the company name given on the accompanying constitution. The correct company type must be included in the name unless exempted. Abbreviation of the company type description will not be accepted.\n\nCompany types:\n• A company being incorporated under Part 2 of the Companies Act as a Private company limited by shares must end its name with "Limited" or "Teoranta". No abbreviations accepted.\n• A company being incorporated under Part 16 of the Companies Act as a Designated Activity Company, either limited by shares or guarantee must end its name with either "Designated Activity Company" or "Cuideachta Ghníomhaíochta Ainmnithe" unless exempted.\n• A company being incorporated under Part 17/24 of the Companies Act as a Public Limited Company must end its name with either "Public Limited Company" or "Cuideachta Phoiblí Theoranta"\n• A company being incorporated under Part 18 of the Companies Act as a Company Limited by Guarantee must end its name with either "Company Limited by Guarantee" or "Cuideachta faoi Theorainn Ráthaíochta" unless exempted.\n• A company being incorporated under Part 19 of the Companies Act as an unlimited company, whether public/private, must end its name with either "Unlimited Company" or "Cuideachta Neamhtheoranta".';
  static const String noteTwo='Select the relevant type.';
  static const String noteThree='This section must be completed by the person who is presenting the application form to the Registrar. This may be either the applicant or a person on his/her behalf.';
  static const String noteFour='A full postal address in the State at which post is capable of being readily delivered by the postal service must be given. A P.O. Box will not suffice.\nCRO will issue the certificate of incorporation to the email address.\nIf the address of the registered office is placed in the care of a Registered Office Agent, who has been approved by the CRO, then form B2 would only be completed in the future to note the cessation of appointment of the Registered Office Agent.';
  static const String noteFive='The word “Designated Activity Company” or “Company Limited by Guarantee” may be dropped from the company’s name where the company is a Designated Activity Company or a Company Limited by Guarantee and the constitution of the company states that the objects will be the promotion of commerce, art, science, education, religion or charity. In addition, the company’s constitution must state that:\n(a) the profits of the company (if any) or other income are required to be applied to the promotion of the objects;\n(b) payment of dividends/distributions to its members is prohibited;\n(c) all assets which would otherwise be available to its members are required to be transferredon its winding up to another company whose objects are the promotion of commerce, art, science, religion or charity.\nIt should be noted, however, that a company which is exempted from the obligation to use the words as part of its name, is still obliged to show on its letters and order forms the fact that it is such a company. Form G5 must accompany the form A1/constitution application.';
  static const String noteSix='All company types must have at least two directors with the exception of Private Companies Limited by Shares (LTD companies) which may have a sole director. All directors must be over the age of 18 years. (s.131 CA 2014). Where a company has only one director, that person may not also hold the office of secretary of the company.\nWhere a person who has consented to be a director of this company is currently disqualified under the law of another state from being appointed or acting as a director or secretary of a body corporate or undertaking, he/she must complete Form B74 which must be submitted to CRO with Form A1. Otherwise he/she will be deemed to be disqualified from acting as a director of an Irish- registered company for the balance remaining of his/her foreign disqualification.\n‘Shadow director’ means a person in accordance with whose directions or instructions the directors of a company are accustomed to act.';
  static const String noteSeven='Insert full name (initials will not suffice) and the usual residential address. Where the secretary is a firm, the name of the firm, registered address and the register where it is registered ought to be stated. Where a person is signing on behalf of a company which is the secretary, he/she should state that he/she is signing for and on behalf of the company which is acting as secretary. His/ her name should be printed in bold capitals or typescript below the signature. All secretaries and directors must be over the age of 18 years. (s.131 CA 2014).';
  static const String noteEight='Any former forename and surname must also be stated. However, it does not include the following: (a) In the case of a person usually known by a title different from his/her surname, the name by which he/she is known previous to the adoption of a succession to the title; (b) in the case of any person, a former forename or surname where the forename or surname was changed or disused before the person bearing the name attained the age of 18 years or has been changed or disused for a period of not less than 20 years; (c) in the case of a married person or civil partner, the name or surname by which he/she was known previous to his/her marriage or civil partnership.';
  static const String noteNine='Every company must have at least one European Economic Area (EEA)-resident full director or a bond pursuant to s137 Companies Act 2014. Note that an EEA-resident alternate director is not sufficient for the purposes of s137. Place a tick in the “EEA resident” box if the director is resident in the State in accordance with s137 Companies Act 2014. If no full director is so resident, a valid bond must be furnished with the application.\n(Note that “EEA-resident” means resident in a member state of the EEA. The EEA is the EU plus Norway, Iceland and Liechtenstein). For information on the bond, see Leaflet No.17.';
  static const String noteTen='Tick the box if the director appointed is an alternate/substitute director. Where the box is ticked, the name of the full director appointing the alternate/substitute director must also be inserted in the space provided.\nIf the company’s articles so permit and subject to compliance with those articles, a director may appoint a person to be an alternate/substitute director on his/her behalf. The appointment of any person to act as director is notifiable by a company to the CRO, regardless of how that appointment is described. The company is statutorily obliged to notify the CRO of the addition to and removal of each person from its register of directors. In the event that a full director who has appointed an alternate director ceases to act as a director, the company is required to notify the CRO of the termination of appointment of the full director and his/her alternate. Note: CRO accepts no responsibility for maintaining the link between a full director and his/her alternate.';
  static const String noteEleven='State the company name and number of other bodies corporate, whether incorporated in the State or elsewhere, of which the person is or has been director. Exceptions to this rule are made for bodies (a) of which the person has not been a director at any time during the past 5 years; (b) which is held or was held by a director in bodies corporate of which the company is (or was) the wholly owned subsidiary or which are or were the wholly owned subsidiaries either of the company or of another body corporate of which the company is or was the wholly owned subsidiary.Pursuant to s142 Companies Act 2014, a person shall not at a particular time be a director of more than 25 companies. However, under s142(3) of the Act, certain directorships are not reckoned for the purposes of s142(1). For further information, see CRO Information Leaflet No.1.';
  static const String noteTwelve='Place of incorporation if outside the State.';
  static const String noteThirteen='The subscribers in this section must correspond with the subscribers to the accompanying constitution except where an agent signs this section on behalf of the subscriber(s). Where the space is inadequate, the signatures must be presented on a continuation sheet in the same format as this section.';
  static const String noteFourteen='Where applicable, the details must correspond exactly with the share details given in the accompanying constitution.';
  static const String noteFifteen='Indicate cash or stock.';
  static const String noteSixteen='The declaration is a declaration of compliance with all the legal requirements relating to the incorporation of a company. As the declaration confirms that all other registration requirements have been completed, it must be signed after the form has been completed in full, and so the date of declaration must not predate the dates of other signatures which appear on the form and accompanying constitution.';
  static const String noteSeventeen='The NACE code is the common basis for statistical classifications of economic activities within the E.U. The code is available on www.cro.ie. The four digit NACE code and general nature of the activity must correspond with the proposed company’s principal object in the accompanying memorandum of association in the constitution with the exception of Private Companies Limited by Shares (LTD companies) which do not have stated objects. (An LTD company must still submit a NACE code description).Where there are two or more activities, give details of the principal activity in the State.';
  static const String noteEighteen='As all activities can be classified under the NACE code it should rarely be necessary to complete (b)';
  static const String noteNineteen='Full postal address must be given. A P.O. Box will not suffice.';
  static const String changes='After registration, you must notify the CRO of any changes to the registered company details. The following forms are the principal ones completed and submitted to the CRO (for the full list go to www.cro.ie):\nB2 Notice of change in the situation of the registered office\nB10 Notice of change of directors or secretaries or in their particulars\nG1Q Change of company name';
  static const String croAddress='When you have completed and signed the form, please file with the CRO.\nThe Public Office is at Bloom House, Gloucester Place Lower, Dublin 1.\nIf submitting by post, please send with the prescribed fee to the Registrar of Companies at:\nNew Companies Section, Companies Registration Office, Bloom House, Gloucester Place Lower Dublin 1';
  static const String paymentNote='If paying by cheque, postal order or bank draft, please make the fee payable to the Companies Registration Office. Cheques or bank drafts must be drawn on a bank in the Republic of Ireland.';
  static const String importantNote='Please carefully study the explanatory notes overleaf. A Form A1 that is not completed correctly or is not accompanied by the correct documents or fee is liable to be rejected and returned to the presenter by the CRO.';
  static  List<RegistrationSingleView> registrationViews=[
    RegistrationSingleView(title: "Company Name",child: CompanyNameView(),subViews: []),
    RegistrationSingleView(title: "Company Type",child: CompanyTypeView(),subViews: []),
    RegistrationSingleView(title: "Presenter Details",child: PresenterDetailsView(),subViews: []),
    RegistrationSingleView(title: "Registered Office",child: RegisteredOfficeDetailView(),subViews: []),
    RegistrationSingleView(title: "Company\nemail address",child: CompanyEmailView(),subViews: []),
    RegistrationSingleView(title: "Type Exemption",child: TypeExemption(),subViews: []),
    RegistrationSingleView(title: "Name Restriction",child: NameRestrictionView(),subViews:[]),
    RegistrationSingleView(title: "Directors List",child: DirectorsListView(),subViews: []),
    RegistrationSingleView(title: "Secretary Detail",child: SecretaryAddViewOne(),subViews: []),
    RegistrationSingleView(title: "Secretary Detail",child: SecretaryAddViewTwo(),subViews: []),
    RegistrationSingleView(title: "Secretary Detail",child: SecretaryAddViewThree(),subViews: []),
    RegistrationSingleView(title: "Subscribers to\nConstitution",child: SubscriberConstitutionView(),subViews: []),
    RegistrationSingleView(title: "Company Capital",child: CompanyCapitalView(),subViews: []),
    RegistrationSingleView(title: "Declaration of\ncompliance",child: DeclarationViewOne(),subViews: []),
    RegistrationSingleView(title: "Declaration of\ncompliance",child: DeclarationViewTwo(),subViews: []),
  ];

  static List<RegistrationSingleView> webRegistrationView=[
    RegistrationSingleView(title: "Company Info",child: WebCompanyNameView(),subViews: []),
    RegistrationSingleView(title: "Application",
    subViews: [
      RegistrationSingleView(
        title: "Your profile",
        child: WebPresenterDetailsView()
      ),
      RegistrationSingleView(
          title: "Company Profile",
          child: WebCompanyProfileView()
      ),
      RegistrationSingleView(
          title: "Director Details",
          child: Padding(
            padding: EdgeInsets.all(Get.width*0.1),
            child: DirectorsListView(),
          )
      ),
      RegistrationSingleView(
          title: "Signature",
          child: SignatureView(true)
      )
    ]),
    RegistrationSingleView(
        title: "Review & Confirm",
        child:ReviewAndConfirm(),
      subViews:[]
    ),
    RegistrationSingleView(
      title: "Payment",subViews:[],
      child: WebPaymentDetailView()
    )
  ];

  static List<AgreementSingleView> agreementsList=[
    AgreementSingleView(title: "Company Name",subTitle: "• The company name must be stated in full.\n• The name must match the name as stated in the constitution.\n• Failure to state the name correctly will result in the application being rejected."),
    AgreementSingleView(title: "Company Type",subTitle: "• The company type must be included at the end of the company name as required under the Companies Act 2014 unless exempted. Abbreviation of the company type is not accepted."),
    AgreementSingleView(title: "Presenter Details",subTitle: "• Name\n• Address\n• Telephone number\n• Email\n• DX number/Exchange\n")
  ];
}

*/
import 'package:eccount/models/agreement_single_view.dart';
import 'package:eccount/models/companycapital_model.dart';
import 'package:eccount/models/director_details.dart';
import 'package:eccount/models/presenter_detail.dart';
import 'package:eccount/models/registration_single_view.dart';
import 'package:eccount/models/secretary_detail.dart';
import 'package:eccount/models/subscribers.dart';
import 'package:eccount/styled_widgets/styled_signature_view.dart';
import 'package:eccount/ui/payment/creditcard_view.dart';
import 'package:eccount/ui/payment/paymentdetaill_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/company_email_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companycapital_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyname_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/companytype_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/declaration_view1.dart';
import 'package:eccount/ui/registrations/comapny_formation/declaration_view2.dart';
import 'package:eccount/ui/registrations/comapny_formation/directorslist_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/namerestriction_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/presenterdetail_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/registered_office_detail_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/reviewinfo_view.dart';
import 'package:eccount/ui/registrations/comapny_formation/subscribersconstitution_view.dart';
import 'package:eccount/ui/secretaryadd_views/secretaryadd_view1.dart';
import 'package:eccount/ui/registrations/comapny_formation/typeexemption_view.dart';
import 'package:eccount/ui/secretaryadd_views/secretaryadd_view2.dart';
import 'package:eccount/ui/secretaryadd_views/secretaryadd_view3.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/buisnessdetails_view.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/buisnesspersonalProfile_view.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/busnesstype_view.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/reviewconfirmBuisness_view.dart';
import 'package:eccount/ui/web_specific/buisnessname_registration/verify_buisnessdetails_view.dart';

import 'package:eccount/ui/web_specific/company_formation/companyformation/compantname_view.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/companyshared_details.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/creditcard_details_view.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/declaration.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/payment/alldone_view.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/payment/payment_view.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/presenterdetail_view.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/review_confirm.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/subscriberto_memorandum_view.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/verification.dart';
import 'package:eccount/ui/web_specific/company_formation/companyformation/webcompanyprofile_view.dart';
import 'package:eccount/ui/web_specific/dashboard/dashFirst/welcome.dart';
import 'package:eccount/ui/web_specific/vatRegistration/review&confirm_view.dart';
import 'package:eccount/ui/web_specific/vatRegistration/vatIndividual_details_view.dart';
import 'package:eccount/ui/web_specific/vatRegistration/vatTypes_view.dart';
import 'package:eccount/ui/web_specific/vatRegistration/vatdetails_view.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CompanyFormation {
  String companyName;
  String companyType;
  int companyTypeCode;
  PresenterDetail presenterDetail;
  String registeredOfficeAddress;
  bool registeredOfficeAddressSameAsAgentOffice;
  String companyEmailAddress;
  String companyPhoneNumber;
  bool typeExemption;
  bool nameRestriction;
  List<DirectorDetails> directorDetailsList;
  SecretaryDetail secretaryDetails;
  List<Subscribers> subscribers;
  CompanyCapitalModel companyCapitalModel;
  String declarationName;
  String declarationResidentialAddress;
  int declarationType;
  String naceCode;
  String natureOfActivity;
  String activityDescribedAs;
  String placesOfActivity;
  String centralAdministrationPlaces;
  String declarationSignatureImage;
  DateTime declarationDate;

  CompanyFormation(
      {this.companyPhoneNumber,
      this.subscribers,
      this.declarationDate,
      this.declarationSignatureImage,
      this.centralAdministrationPlaces,
      this.placesOfActivity,
      this.activityDescribedAs,
      this.natureOfActivity,
      this.naceCode,
      this.declarationType,
      this.declarationResidentialAddress,
      this.declarationName,
      this.companyCapitalModel,
      this.secretaryDetails,
      this.directorDetailsList,
      this.nameRestriction,
      this.typeExemption,
      this.companyEmailAddress,
      this.registeredOfficeAddressSameAsAgentOffice,
      this.registeredOfficeAddress,
      this.presenterDetail,
      this.companyTypeCode,
      this.companyType,
      this.companyName});

  CompanyFormation.fromJson(Map data)
      : companyName = data['company_name'],
        companyType = data['company_type'],
        companyTypeCode = data['comapny_type_code'],
        presenterDetail = PresenterDetail.fromJson(data['presenter_details']),
        registeredOfficeAddress = data['registered_office_address'],
        registeredOfficeAddressSameAsAgentOffice =
            data['registered_office_same_as_agent'],
        companyEmailAddress = data['company_emailaddress'],
        typeExemption = data['type_excemption'],
        nameRestriction = data['name_restriction'],
        companyPhoneNumber = data['company_phonenumber'],
        directorDetailsList = data['director_details']
            .map((e) => DirectorDetails.fromJson(e))
            .toList(),
        secretaryDetails = SecretaryDetail.fromJson(data['secretary_details']),
        subscribers =
            data['subscribers'].map((e) => Subscribers.fromJosn(e)).toList(),
        companyCapitalModel =
            CompanyCapitalModel.fromJson(data['company_capital_model']),
        declarationName = data['declaration_name'],
        declarationResidentialAddress = data['declaration_residential_address'],
        declarationType = data['declaration_type'],
        naceCode = data['nace_code'],
        natureOfActivity = data['nature_of_activity'],
        activityDescribedAs = data['activity_described_as'],
        placesOfActivity = data['place_of_activity'],
        centralAdministrationPlaces = data['central_administration_places'],
        declarationSignatureImage = data['declaration_signature_image'],
        declarationDate = data['declaration_date'] != null
            ? DateTime.parse(data['declaration_date'])
            : null;

  toMap() {
    return {
      'company_name': companyName,
      'company_type': companyType,
      'comapny_type_code': companyTypeCode,
      'presenter_details': presenterDetail.toMap(),
      'registered_office_address': registeredOfficeAddress,
      'registered_office_same_as_agent':
          registeredOfficeAddressSameAsAgentOffice,
      'company_emailaddress': companyEmailAddress,
      'type_excemption': typeExemption,
      'name_restriction': nameRestriction,
      'director_details': directorDetailsList.map((e) => e.toMap()).toList(),
      'secretary_details': secretaryDetails.toMap(),
      'company_capital_model': companyCapitalModel.toMap(),
      'declaration_name': declarationName,
      'company_phonenumber': companyPhoneNumber,
      'declaration_residential_address': declarationResidentialAddress,
      'declaration_type': declarationType,
      'nace_code': naceCode,
      'subscribers': subscribers.map((e) => e.toMap()).toList(),
      'nature_of_activity': natureOfActivity,
      'activity_described_as': activityDescribedAs,
      'place_of_activity': placesOfActivity,
      'central_administration_places': centralAdministrationPlaces,
      'declaration_signature_image': declarationSignatureImage,
      'declaration_date': declarationDate?.toString()
    };
  }

  static const List<String> companyTypes = [
    'Private Limited Company',
    'Single Member Company',
    'Designated Activity Company',
    'Public Limited Company',
    'General Partnership',
    'Limited Partnership',
    'Freelance or soletrader',
    'Unlimited Company/Private Unlimited Company'
  ];
  static const String generalMessage =
      "This form must be completed correctly, in full and in accordance with the following notes. Every section of the form must be completed. Where “not applicable”, “nil” or “none” is appropriate, please state. Where €/_ appears, please insert/delete as appropriate. Where /_ applies, give the relevant currency, if not euro. Where the space provided on Form A1 is considered inadequate, the information should be presented on a continuation sheet in the same format as the relevant section in the form. The use of a continuation sheet must be so indicated in the relevant section.";
  static const String noteOne =
      'The proposed company name must be given in full and must correspond exactly with the company name given on the accompanying constitution. The correct company type must be included in the name unless exempted. Abbreviation of the company type description will not be accepted.\n\nCompany types:\n• A company being incorporated under Part 2 of the Companies Act as a Private company limited by shares must end its name with "Limited" or "Teoranta". No abbreviations accepted.\n• A company being incorporated under Part 16 of the Companies Act as a Designated Activity Company, either limited by shares or guarantee must end its name with either "Designated Activity Company" or "Cuideachta Ghníomhaíochta Ainmnithe" unless exempted.\n• A company being incorporated under Part 17/24 of the Companies Act as a Public Limited Company must end its name with either "Public Limited Company" or "Cuideachta Phoiblí Theoranta"\n• A company being incorporated under Part 18 of the Companies Act as a Company Limited by Guarantee must end its name with either "Company Limited by Guarantee" or "Cuideachta faoi Theorainn Ráthaíochta" unless exempted.\n• A company being incorporated under Part 19 of the Companies Act as an unlimited company, whether public/private, must end its name with either "Unlimited Company" or "Cuideachta Neamhtheoranta".';
  static const String noteTwo = 'Select the relevant type.';
  static const String noteThree =
      'This section must be completed by the person who is presenting the application form to the Registrar. This may be either the applicant or a person on his/her behalf.';
  static const String noteFour =
      'A full postal address in the State at which post is capable of being readily delivered by the postal service must be given. A P.O. Box will not suffice.\nCRO will issue the certificate of incorporation to the email address.\nIf the address of the registered office is placed in the care of a Registered Office Agent, who has been approved by the CRO, then form B2 would only be completed in the future to note the cessation of appointment of the Registered Office Agent.';
  static const String noteFive =
      'The word “Designated Activity Company” or “Company Limited by Guarantee” may be dropped from the company’s name where the company is a Designated Activity Company or a Company Limited by Guarantee and the constitution of the company states that the objects will be the promotion of commerce, art, science, education, religion or charity. In addition, the company’s constitution must state that:\n(a) the profits of the company (if any) or other income are required to be applied to the promotion of the objects;\n(b) payment of dividends/distributions to its members is prohibited;\n(c) all assets which would otherwise be available to its members are required to be transferredon its winding up to another company whose objects are the promotion of commerce, art, science, religion or charity.\nIt should be noted, however, that a company which is exempted from the obligation to use the words as part of its name, is still obliged to show on its letters and order forms the fact that it is such a company. Form G5 must accompany the form A1/constitution application.';
  static const String noteSix =
      'All company types must have at least two directors with the exception of Private Companies Limited by Shares (LTD companies) which may have a sole director. All directors must be over the age of 18 years. (s.131 CA 2014). Where a company has only one director, that person may not also hold the office of secretary of the company.\nWhere a person who has consented to be a director of this company is currently disqualified under the law of another state from being appointed or acting as a director or secretary of a body corporate or undertaking, he/she must complete Form B74 which must be submitted to CRO with Form A1. Otherwise he/she will be deemed to be disqualified from acting as a director of an Irish- registered company for the balance remaining of his/her foreign disqualification.\n‘Shadow director’ means a person in accordance with whose directions or instructions the directors of a company are accustomed to act.';
  static const String noteSeven =
      'Insert full name (initials will not suffice) and the usual residential address. Where the secretary is a firm, the name of the firm, registered address and the register where it is registered ought to be stated. Where a person is signing on behalf of a company which is the secretary, he/she should state that he/she is signing for and on behalf of the company which is acting as secretary. His/ her name should be printed in bold capitals or typescript below the signature. All secretaries and directors must be over the age of 18 years. (s.131 CA 2014).';
  static const String noteEight =
      'Any former forename and surname must also be stated. However, it does not include the following: (a) In the case of a person usually known by a title different from his/her surname, the name by which he/she is known previous to the adoption of a succession to the title; (b) in the case of any person, a former forename or surname where the forename or surname was changed or disused before the person bearing the name attained the age of 18 years or has been changed or disused for a period of not less than 20 years; (c) in the case of a married person or civil partner, the name or surname by which he/she was known previous to his/her marriage or civil partnership.';
  static const String noteNine =
      'Every company must have at least one European Economic Area (EEA)-resident full director or a bond pursuant to s137 Companies Act 2014. Note that an EEA-resident alternate director is not sufficient for the purposes of s137. Place a tick in the “EEA resident” box if the director is resident in the State in accordance with s137 Companies Act 2014. If no full director is so resident, a valid bond must be furnished with the application.\n(Note that “EEA-resident” means resident in a member state of the EEA. The EEA is the EU plus Norway, Iceland and Liechtenstein). For information on the bond, see Leaflet No.17.';
  static const String noteTen =
      'Tick the box if the director appointed is an alternate/substitute director. Where the box is ticked, the name of the full director appointing the alternate/substitute director must also be inserted in the space provided.\nIf the company’s articles so permit and subject to compliance with those articles, a director may appoint a person to be an alternate/substitute director on his/her behalf. The appointment of any person to act as director is notifiable by a company to the CRO, regardless of how that appointment is described. The company is statutorily obliged to notify the CRO of the addition to and removal of each person from its register of directors. In the event that a full director who has appointed an alternate director ceases to act as a director, the company is required to notify the CRO of the termination of appointment of the full director and his/her alternate. Note: CRO accepts no responsibility for maintaining the link between a full director and his/her alternate.';
  static const String noteEleven =
      'State the company name and number of other bodies corporate, whether incorporated in the State or elsewhere, of which the person is or has been director. Exceptions to this rule are made for bodies (a) of which the person has not been a director at any time during the past 5 years; (b) which is held or was held by a director in bodies corporate of which the company is (or was) the wholly owned subsidiary or which are or were the wholly owned subsidiaries either of the company or of another body corporate of which the company is or was the wholly owned subsidiary.Pursuant to s142 Companies Act 2014, a person shall not at a particular time be a director of more than 25 companies. However, under s142(3) of the Act, certain directorships are not reckoned for the purposes of s142(1). For further information, see CRO Information Leaflet No.1.';
  static const String noteTwelve =
      'Place of incorporation if outside the State.';
  static const String noteThirteen =
      'The subscribers in this section must correspond with the subscribers to the accompanying constitution except where an agent signs this section on behalf of the subscriber(s). Where the space is inadequate, the signatures must be presented on a continuation sheet in the same format as this section.';
  static const String noteFourteen =
      'Where applicable, the details must correspond exactly with the share details given in the accompanying constitution.';
  static const String noteFifteen = 'Indicate cash or stock.';
  static const String noteSixteen =
      'The declaration is a declaration of compliance with all the legal requirements relating to the incorporation of a company. As the declaration confirms that all other registration requirements have been completed, it must be signed after the form has been completed in full, and so the date of declaration must not predate the dates of other signatures which appear on the form and accompanying constitution.';
  static const String noteSeventeen =
      'The NACE code is the common basis for statistical classifications of economic activities within the E.U. The code is available on www.cro.ie. The four digit NACE code and general nature of the activity must correspond with the proposed company’s principal object in the accompanying memorandum of association in the constitution with the exception of Private Companies Limited by Shares (LTD companies) which do not have stated objects. (An LTD company must still submit a NACE code description).Where there are two or more activities, give details of the principal activity in the State.';
  static const String noteEighteen =
      'As all activities can be classified under the NACE code it should rarely be necessary to complete (b)';
  static const String noteNineteen =
      'Full postal address must be given. A P.O. Box will not suffice.';
  static const String changes =
      'After registration, you must notify the CRO of any changes to the registered company details. The following forms are the principal ones completed and submitted to the CRO (for the full list go to www.cro.ie):\nB2 Notice of change in the situation of the registered office\nB10 Notice of change of directors or secretaries or in their particulars\nG1Q Change of company name';
  static const String croAddress =
      'When you have completed and signed the form, please file with the CRO.\nThe Public Office is at Bloom House, Gloucester Place Lower, Dublin 1.\nIf submitting by post, please send with the prescribed fee to the Registrar of Companies at:\nNew Companies Section, Companies Registration Office, Bloom House, Gloucester Place Lower Dublin 1';
  static const String paymentNote =
      'If paying by cheque, postal order or bank draft, please make the fee payable to the Companies Registration Office. Cheques or bank drafts must be drawn on a bank in the Republic of Ireland.';
  static const String importantNote =
      'Please carefully study the explanatory notes overleaf. A Form A1 that is not completed correctly or is not accompanied by the correct documents or fee is liable to be rejected and returned to the presenter by the CRO.';
  static List<RegistrationSingleView> registrationViews = [
    RegistrationSingleView(
        title: "Company Name", child: CompanyNameView(), subViews: []),
    RegistrationSingleView(
        title: "Company Type", child: CompanyTypeView(), subViews: []),
    RegistrationSingleView(
        title: "Presenter Details",
        child: PresenterDetailsView(),
        subViews: []),
    RegistrationSingleView(
        title: "Registered Office",
        child: RegisteredOfficeDetailView(),
        subViews: []),
    RegistrationSingleView(
        title: "Company\nemail address",
        child: CompanyEmailView(),
        subViews: []),
    RegistrationSingleView(
        title: "Type Exemption", child: TypeExemption(), subViews: []),
    RegistrationSingleView(
        title: "Name Restriction", child: NameRestrictionView(), subViews: []),
    RegistrationSingleView(
        title: "Directors List", child: DirectorsListView(), subViews: []),
    RegistrationSingleView(
        title: "Secretary Detail", child: SecretaryAddViewOne(), subViews: []),
    RegistrationSingleView(
        title: "Secretary Detail", child: SecretaryAddViewTwo(), subViews: []),
    RegistrationSingleView(
        title: "Secretary Detail",
        child: SecretaryAddViewThree(),
        subViews: []),
    RegistrationSingleView(
        title: "Subscribers to\nConstitution",
        child: SubscriberConstitutionView(),
        subViews: []),
    RegistrationSingleView(
        title: "Company Capital", child: CompanyCapitalView(), subViews: []),
    RegistrationSingleView(
        title: "Declaration of\ncompliance",
        child: DeclarationViewOne(),
        subViews: []),
    RegistrationSingleView(
        title: "Declaration of\ncompliance",
        child: DeclarationViewTwo(),
        subViews: []),
  ];
  static List<RegistrationSingleView> vatRegistrationView = [
    RegistrationSingleView(
        title: "Vat Type", child: VatTypesView(), subViews: []),
    RegistrationSingleView(title: "DETAILS", subViews: [
      RegistrationSingleView(
          title: "Individual details",
          child: VatIndividualDetails(),
          subViews: []),
      RegistrationSingleView(
          title: "VAT Details", child: VatDetails(), subViews: []),
      RegistrationSingleView(
          title: "Review & confirm",
          child: ReviewConfirmVatRegistration(stepVal: 2.toString(),),
          subViews: [])
    ]),
    RegistrationSingleView(
        title: "Payment",
        child: WebPaymentDetailView([
          // Text("SERVICE"),
          Text("VAT registration (incl. fees)"),
          Text(
            "Registered Office Address (annual recurring fee)",
            overflow: TextOverflow.ellipsis,
          ),
          Text("Contact person (annual recurring fee)"),
          Text("Total with no VAT")
        ],stepVal: "3",),
        subViews: []),
  ];

  static List<RegistrationSingleView> webRegistrationView = [
    RegistrationSingleView(
        title: "Company name", child: WebCompanyNameView(), subViews: []),
    RegistrationSingleView(title: "Application", subViews: [
      RegistrationSingleView(
        title: "Your profile",
        child: WebPresenterDetailsView(),
      ),
      RegistrationSingleView(
          title: "Company Profile", child: WebCompanyProfileView()),
      RegistrationSingleView(
          title: "Appoint new director", child: DirectorsListView()),
      RegistrationSingleView(
          title: "Appoint new director",
          child: Padding(
            padding: EdgeInsets.all(Get.width * 0.1),
            child: DirectorsListView(),
          )),
      RegistrationSingleView(
          title: "Director details", child: DirectorDetailsForm()),
      RegistrationSingleView(title: "Add signature", child: ConsentSignature()),
      RegistrationSingleView(
          title: "Company share capital", child: CompanySharedDetails()),
      RegistrationSingleView(
          title: "Subscribers to Memorandum", child: AddSubscriberView()),
      RegistrationSingleView(
        title: "Declaration",
        child: DeclarationView(),
      ),
      RegistrationSingleView(
          title: "Verification", subViews: [], child: VerificationView()),
      RegistrationSingleView(
          title: "Review & Confirm", subViews: [], child: ReviewConfirm()),
    ]),
    RegistrationSingleView(
      title: "Payment",
      child: WebPaymentDetailView([
        Text("Company formation (incl. fees)"),
        Text(
          "Registered Office Address (annual recurring fee)",
          overflow: TextOverflow.ellipsis,
        ),
        Text("Contact person (annual recurring fee)"),
        Text("Total with no VAT")
      ]),
      subViews: [
        RegistrationSingleView(
          title: "Confirm order",
          child: WebPaymentDetailView([
            Text("Company formation (incl. fees)"),
            Text(
              "Registered Office Address (annual recurring fee)",
              overflow: TextOverflow.ellipsis,
            ),
            Text("Contact person (annual recurring fee)"),
            Text("otal with no VAT")
          ]),
        ),
        RegistrationSingleView(
          title: "Card details",
          child: CreditCardDetails(),
        ),
        RegistrationSingleView(
          title: "Done",
          child: AllDoneView(),
        ),
      ],
    )
  ];
  static List<RegistrationSingleView> buisnessNameRegistraionView = [
    RegistrationSingleView(
        title: "Buisness name details",
        child: BuisnessTypeView(),
        subViews: []),
    RegistrationSingleView(
        title: "buisness type", child: BuisnessDetailsViewWeb(), subViews: []),
    RegistrationSingleView(title: "Individual details", subViews: [
      RegistrationSingleView(
          title: "Your profile",
          child: BuisnessPersonalProfile(),
          subViews: []),
      RegistrationSingleView(
          title: "Review & confirm",
          child: ReviewConfirmBuisnessName(),
          subViews: []),
    ]),
    RegistrationSingleView(
        title: "Verification", child: VerifiyBuisnessDetails(), subViews: []),
    RegistrationSingleView(
        title: "Payment",
        child: WebPaymentDetailView([
          // Text("SERVICE"),
          Text("Business name registration (incl. fees)"),
          Text(
            "Registered Office Address (annual recurring fee)",
            overflow: TextOverflow.ellipsis,
          ),
          Text("Contact person (annual recurring fee)"),
          Text("Total with no VAT")
        ]),
        subViews: []),
  ];

  static List<AgreementSingleView> agreementsList = [
    AgreementSingleView(
        title: "Company Name",
        subTitle:
            "• The company name must be stated in full.\n• The name must match the name as stated in the constitution.\n• Failure to state the name correctly will result in the application being rejected."),
    AgreementSingleView(
        title: "Company Type",
        subTitle:
            "• The company type must be included at the end of the company name as required under the Companies Act 2014 unless exempted. Abbreviation of the company type is not accepted."),
    AgreementSingleView(
        title: "Presenter Details",
        subTitle:
            "• Name\n• Address\n• Telephone number\n• Email\n• DX number/Exchange\n")
  ];
}
