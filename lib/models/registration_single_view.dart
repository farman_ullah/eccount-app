import 'package:flutter/material.dart';

class RegistrationSingleView{
  String title;
  Widget child;
  List<RegistrationSingleView> subViews;
  bool completed;
  RegistrationSingleView({this.child,this.title,this.completed=false,this.subViews});
}