class ShareModel{
  String authorisedShareClass;
  int numberOfShares;
  double valuePerShare;

  ShareModel({this.numberOfShares,this.valuePerShare,this.authorisedShareClass});
  ShareModel.fromJson(Map data):
      authorisedShareClass=data['authorisedshare_class'],
      numberOfShares=data['no_of_shares'],
      valuePerShare=data['value_per_share'];

  toMap(){
    return {
      'authorisedshare_class':authorisedShareClass,
      'no_of_shares':numberOfShares,
      'value_per_share':valuePerShare
    };
  }
}