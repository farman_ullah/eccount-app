class CardModel{
  final String id;
  final String brand;
  final int expMonth;
  final int expYear;
  final String last4;
  final String paymentId;
  final String type;
  CardModel({this.id,this.paymentId,this.brand,this.expMonth,this.expYear,this.last4,this.type});
  CardModel.fromJson(Map data,String id):
      id=id,
      brand=data['card']!=null?data['card']['brand']:"",
      expMonth=data['card']!=null?data['card']['exp_month']:0,
        expYear=data['card']!=null?data['card']['exp_year']:0,
      last4=data['card']!=null?data['card']['last4']:"",
      paymentId=data['id'],
      type=data['type'];
}