class UserModel{
  String id;
  String name;
  String phoneNumber;
  String govtId;
  DateTime dateOfBirth;
  String address;
  String email;
  String imageUrl;
  UserModel({this.id,this.name,this.imageUrl,this.phoneNumber,this.email,this.govtId});
  UserModel.fromJson(Map data,String id):
      id=id,
      name=data['name'],
      phoneNumber=data['phone_number'],
      govtId=data['govt_id'],
      dateOfBirth=data['date_of_birth']!=null?DateTime.parse(data['date_of_birth']):null,
      address=data['address'],
      email=data['email'],
      imageUrl=data['image_url'];

  toMap(){
    return {
      'name':name,
      'phone_number':phoneNumber,
      'govt_id':govtId,
      'date_of_birth':dateOfBirth?.toIso8601String(),
      'address':address,
      'email':email,
      'image_url':imageUrl
    };
  }
}