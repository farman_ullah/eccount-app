import 'dart:typed_data';

enum SubscriberType{
  Subscriber,
  Agent
}

class Subscribers{
  String signatureImage;
  SubscriberType subscriberType;
  DateTime date;
  Subscribers({this.signatureImage,this.date,this.subscriberType});
  Subscribers.fromJosn(Map data):
      signatureImage=data['signature_image'],
      subscriberType=getSubscriberEnum(data['subscriber_type']),
      date=data['date']!=null?DateTime.parse(data['date']):null;

  static getSubscriberEnum(int value){
    if(value==0){
      return SubscriberType.Subscriber;
    }
    else{
      return SubscriberType.Agent;
    }
  }

  getSubscriberValue(SubscriberType type){
    if(type==SubscriberType.Subscriber){
      return 0;
    }
    else{
      return 1;
    }
  }
  toMap(){
    return {
      "signature_image":signatureImage,
      'subscriber_type':getSubscriberValue(subscriberType),
      'date':date.toString()
    };
  }
}