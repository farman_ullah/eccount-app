import 'dart:typed_data';

import 'package:eccount/models/directorships.dart';

class DirectorDetails{
  String surName;
  String foreName;
  String formerSurname;
  String formerForename;
  DateTime dateOfBirth;
  bool eeaResident;
  String residentialAddress;
  String postCode;
  String businessOccupation;
  String nationality;
  bool alternateDirector;
  String fullDirectorAppointingAlternateDirector;
  String signatureImage;
  DateTime dateRegistered;
  List<DirectorShips> directorShips;

  DirectorDetails({this.eeaResident,this.alternateDirector,this.businessOccupation,this.dateOfBirth,this.dateRegistered,this.directorShips,this.foreName,this.formerForename,this.formerSurname,this.fullDirectorAppointingAlternateDirector,this.nationality,this.postCode,this.residentialAddress,this.signatureImage,this.surName});

  DirectorDetails.fromJson(Map data):
      surName=data['sur_name'],
      foreName=data['fore_name'],
      formerSurname=data['former_surname'],
      formerForename=data['former_forename'],
      eeaResident=data['eea_resident'],
      dateOfBirth=data['date_of_birth']!=null?DateTime.parse(data['date_of_birth']):null,
      residentialAddress=data['residential_address'],
      postCode=data['post_code'],
      businessOccupation=data['business_occupation'],
      nationality=data['nationality'],
      alternateDirector=data['alternate_director'],
      fullDirectorAppointingAlternateDirector=data['full_director'],
      signatureImage=data['signature_image'],
      dateRegistered=data['date_registered']!=null?DateTime.parse(data['date_registered']):null,
      directorShips=getDirectorShips(data['other_directorships']);

   static getDirectorShips(List data){
    return data.map((e) => DirectorShips.fromJson(e)).toList();
  }

  toMap(){
     return {
       'sur_name':surName,
       'fore_name':foreName,
       'former_surname':formerSurname,
       'eea_resident':eeaResident,
       'former_forename':formerForename,
       'date_of_birth':dateOfBirth?.toString(),
       'residential_address':residentialAddress,
       'post_code':postCode,
       'business_occupation':businessOccupation,
       'nationality':nationality,
       'alternate_director':alternateDirector,
       'full_director':fullDirectorAppointingAlternateDirector,
       'signature_image':signatureImage,
       'date_registered':dateRegistered.toString(),
       'other_directorships':directorShips?.map((e) => e.toMap())?.toList()
     };
  }
}