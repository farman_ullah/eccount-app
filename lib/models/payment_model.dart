
class PaymentModel {
  final String id;
  final String paymentMethodId;
  final int amount;
  final String currency;
  final String status;
  final String orderId;

  PaymentModel({this.id,this.paymentMethodId,this.status,this.amount,this.currency,this.orderId});

  PaymentModel.fromJson(Map data,String id):
      id=id,
      paymentMethodId=data['payment_method'],
      orderId=data['order_id'],
      amount=data['amount'],
      currency=data['currency'],
      status=data['status'];

  toMap(){
    return {
      "payment_method":paymentMethodId,
      "amount":amount,
      "currency":currency,
      "status":status,
      "order_id":orderId
    };
  }
}