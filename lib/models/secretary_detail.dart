import 'dart:typed_data';

class SecretaryDetail{
  String surName;
  String foreName;
  String formerSurname;
  String formerForename;
  DateTime dateOfBirth;
  String numberOfBodyCorporate;
  String bodyCorporateName;
  String nameOfRegister;
  String residentialAddress;
  String postCode;
  String signatureImage;
  DateTime registeredDate;
  SecretaryDetail({this.signatureImage,this.postCode,this.residentialAddress,this.dateOfBirth,this.formerForename,this.formerSurname,this.foreName,this.surName,this.bodyCorporateName,this.nameOfRegister,this.numberOfBodyCorporate,this.registeredDate});

  SecretaryDetail.fromJson(Map data):
        surName=data['sur_name'],
        foreName=data['fore_name'],
        formerSurname=data['former_surname'],
        formerForename=data['former_forename'],
        dateOfBirth=data['date_of_birth']!=null?DateTime.parse(data['date_of_birth']):null,
        numberOfBodyCorporate=data['number_of_body_corporate'],
        bodyCorporateName=data['body_corporate_name'],
        nameOfRegister=data['name_of_register'],
        residentialAddress=data['residential_address'],
        postCode=data['post_code'],
        signatureImage=data['signature_image'],
        registeredDate=data['date_registered']!=null?DateTime.parse(data['date_registered']):null;

  toMap(){
   return {
     'sur_name':surName,
     'fore_name':foreName,
     'former_surname':formerSurname,
     'former_forename':formerForename,
     'date_of_birth':dateOfBirth?.toString(),
     'number_of_body_corporate':numberOfBodyCorporate,
     'body_corporate_name':bodyCorporateName,
     'name_of_register':nameOfRegister,
     'residential_address':residentialAddress,
     'post_code':postCode,
     'signature_image':signatureImage,
     'date_registered':registeredDate?.toString()
   };
  }

}