class PresenterDetail{
  String name;
  String address;
  String dxNumber;
  String contactPerson;
  String telephone;
  String faxNumber;
  String email;
  String referenceNumber;
  PresenterDetail({this.name,this.email,this.address,this.contactPerson,this.dxNumber,this.faxNumber,this.referenceNumber,this.telephone});
  PresenterDetail.fromJson(Map data):
      name=data["name"],
      address=data['address'],
      dxNumber=data['dx_number'],
      contactPerson=data['contact_person'],
      telephone=data['telephone'],
      faxNumber=data['fax_number'],
      email=data['email'],
      referenceNumber=data['reference_number'];

  toMap(){
    return {
      "name":name,
      "address":address,
      "dx_number":dxNumber,
      "contact_person":contactPerson,
      "telephone":telephone,
      "fax_number":faxNumber,
      "email":email,
      "reference_number":referenceNumber
    };
  }
}