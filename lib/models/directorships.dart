class DirectorShips{
  String companyName;
  String corporationPlace;
  String companyNumber;
  DirectorShips({this.companyName,this.companyNumber,this.corporationPlace});
  DirectorShips.fromJson(Map data):
      companyName=data['company_name'],
      corporationPlace=data['corporation_place'],
      companyNumber=data['company_number'];
  toMap(){
    return {
      "company_name":companyName,
      "corporation_place":corporationPlace,
      "company_number":companyNumber
    };
  }
}