class OrderModel {
  String id;
  Map orderData;
  int dataType;
  String paymentId;
  bool paid;
  bool completed;
  String userId;
  int status;
  DateTime date;
  String agentId;
  OrderModel({this.date,this.id,this.agentId,this.userId,this.completed,this.paid,this.paymentId,this.dataType,this.orderData});
  OrderModel.fromJson(Map data,String id):
      id=id,
      orderData=data['order_data'],
      dataType=data['data_type'],
      status=data['status'],
      paymentId=data['payment_id'],
      paid=data['paid'],
      date=data['date']!=null?DateTime.parse(data['date']):null,
      completed=data['completed'],
      userId=data['user_id'],
      agentId=data['agent_id'];

  toMap(){
    return {
      'order_data':orderData,
      'data_type':dataType,
      'status':status,
      'payment_id':paymentId,
      'paid':paid,
      'completed':completed,
      'user_id':userId,
      'agent_id':agentId,
      'date':date.toString()
    };
  }
}