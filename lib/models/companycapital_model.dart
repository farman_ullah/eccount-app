import 'package:eccount/models/authorisedshare_model.dart';

class CompanyCapitalModel{
  String totalValueOfAuthorisedShares;
  int totalNumberOfAuthorisedShares;
  List<ShareModel> authorisedShareList;
  String totalIssuedSharesValue;
  int totalNumberOfIssuedShares;
  List<ShareModel> issuedShareList;
  CompanyCapitalModel({this.issuedShareList,this.totalNumberOfIssuedShares,this.totalIssuedSharesValue,this.authorisedShareList,this.totalNumberOfAuthorisedShares,this.totalValueOfAuthorisedShares});
  CompanyCapitalModel.fromJson(Map data):
      totalValueOfAuthorisedShares=data['total_value_of_authorised_shares'],
      totalNumberOfAuthorisedShares=data['total_number_of_authorised_shares'],
      authorisedShareList=getShareList(data['authorised_share_list']),
      totalIssuedSharesValue=data['total_value_of_issued_shares'],
      totalNumberOfIssuedShares=data['total_number_of_issued_shares'],
      issuedShareList=getShareList(data['issued_share_list']);


  static getShareList(List data){
    return data.map((e) => ShareModel.fromJson(e)).toList();
  }

  toMap(){
    return {
      'total_value_of_authorised_shares':totalValueOfAuthorisedShares,
      'total_number_of_authorised_shares':totalNumberOfAuthorisedShares,
      'authorised_share_list':authorisedShareList.map((e) => e.toMap()).toList(),
      'total_value_of_issued_shares':totalIssuedSharesValue,
      'total_number_of_issued_shares':totalNumberOfIssuedShares,
      'issued_share_list':issuedShareList.map((e) => e.toMap()).toList()
    };
  }
}