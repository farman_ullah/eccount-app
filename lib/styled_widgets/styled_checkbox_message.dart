import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:flutter/material.dart';

class StyledCheckBoxMessage extends StatelessWidget {
  final bool value;
  final Function onPressed;
  final String message;
  StyledCheckBoxMessage({this.onPressed,this.value,this.message});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(value: value, onChanged: onPressed,activeColor: primaryColor,),
        SizedBox(width: 8,),
        Flexible(child: AutoSizeText(message??""))
      ],
    );
  }
}
