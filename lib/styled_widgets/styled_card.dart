
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class StyledCard extends StatelessWidget {
  final String title;
  final Color titleColor;
  final double titleFontSize;
  final double borderRadius;
  final double elevation;
  final EdgeInsets padding;
  final Color shadowColor;
  final Color backgroundColor;
  StyledCard({this.shadowColor,this.backgroundColor,this.padding,this.elevation,this.titleFontSize,this.titleColor,@required this.title,this.borderRadius});
  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: shadowColor,
      color: backgroundColor,
      elevation: elevation??0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderRadius??0)),
      child: Padding(
        padding: padding??EdgeInsets.zero,
        child: Center(
          child: AutoSizeText(title,style: TextStyle(color: titleColor,fontSize: titleFontSize),),
        ),
      ),
    );
  }
}
