import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/core/services/firestorage_service.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/show.dart';
import 'package:eccount/shared/styles.dart';
import 'package:eccount/ui/registrations/comapny_formation/companyformation_viewmodel.dart';
import 'package:eccount/ui/registrations/registration_viewmodel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:signature/signature.dart';


class SignatureView extends StatefulWidget {
  final bool isSubview;
  SignatureView(this.isSubview);
  @override
  _SignatureViewState createState() => _SignatureViewState();
}

class _SignatureViewState extends State<SignatureView> {
  final SignatureController _controller = SignatureController(
    penStrokeWidth: 5,
    penColor: Colors.purple,
    exportBackgroundColor: Colors.white,
  );
  var _signatureCanvas;
  @override
  void initState() {
    _signatureCanvas = Signature(
      controller: _controller,
      width: Get.width,
      height: Get.height,
      backgroundColor: Colors.white,
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: kIsWeb?false:true,
        title: AutoSizeText("Signature View",style: TextStyle(color: Colors.black),),
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        actions: [
          TextButton(onPressed: ()async{
           if(_controller.isEmpty){
             Show.showErrorSnackBar(title: "Error", error: "Please provide your signature to add");
           }
           else{
             Show.showLoader();
             Uint8List imageData=await _controller.toPngBytes();
             String image=await Get.find<FireStorageService>().uploadData(data: imageData);
             if(widget.isSubview){
               if(Get.isOverlaysOpen){
                 Get.back();
               }
               final registrationViewModel=Get.find<RegistrationViewModel>();
               Get.find<CompanyFormationViewModel>().companyFormation.declarationSignatureImage=image;
                 registrationViewModel.changeIndex(registrationViewModel.selectedIndex+1);
             }
             else{
               Get.back();
               Get.back(result: image);
             }
           }
          }, child: AutoSizeText('ADD',style: TextStyle(color: primaryColor,fontSize: FontSizes.s16,fontWeight: FontWeight.w600),))
        ],
      ),
      body: _signatureCanvas,
      floatingActionButton: FloatingActionButton(
        backgroundColor: primaryColor,
        child: Icon(Icons.refresh),
        onPressed: (){
          _controller.clear();
        },
      ),
    );
  }
}

