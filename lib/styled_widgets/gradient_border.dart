
import 'package:flutter/material.dart';


class GradientBorder extends StatelessWidget {
  final EdgeInsets borderWidth;
  final BorderRadiusGeometry borderRadius;
  final Color backGroundColor;
  final List<Color> colors;
  final Widget child;
  final EdgeInsets innerPadding;
  GradientBorder({this.borderWidth=EdgeInsets.zero,@required this.child,this.borderRadius,this.innerPadding=EdgeInsets.zero,this.backGroundColor=Colors.white,this.colors});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: borderWidth,
      decoration: BoxDecoration(
           borderRadius: borderRadius,
          gradient: LinearGradient(colors: colors,stops: [0.6,1],begin: Alignment.topCenter,end: Alignment.bottomCenter)
      ),
      child: child,
    );
  }
}
