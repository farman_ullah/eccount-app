
import 'package:flutter/material.dart';
class TxtField extends StatelessWidget {

  final double radius2=15;
  final String lblTxt;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final Color fillColor;
  final String hintTxt;
  final bool ispassword;
  final bool isunique;
  final Color hintTextColor;
  Function onSaved;
  Function validate;
  String initialValue;
  bool isdropDown;
  TxtField({Key key,this.hintTextColor,this.isdropDown=false,this.onSaved,this.validate,this.initialValue,this.lblTxt, this.prefixIcon, this.suffixIcon, this.fillColor,this.hintTxt,this.ispassword=false,this.isunique=false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return this.isdropDown==false?  TextFormField(
      initialValue: this.initialValue,
      validator: this.validate,
      onSaved: onSaved,

      style: TextStyle(color: Colors.black.withOpacity(0.6)),

      //  initialValue: 'Input text',
      decoration: InputDecoration(

        fillColor: this.fillColor,

        filled: true,

        //   labelText: lblTxt,

        hintStyle: TextStyle(color:this.hintTextColor?? Colors.grey.shade400),
        //  labelStyle: TextStyle(color: Colors.grey.shade500),
        //   errorText: 'Error message',
        focusedBorder: OutlineInputBorder(
          borderRadius:BorderRadius.circular(radius2),
          borderSide: BorderSide(width: 1,color: Colors.white),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius:BorderRadius.circular(radius2),
          borderSide: BorderSide(width: 1,color: Colors.orange),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius:BorderRadius.circular(radius2),
          borderSide: BorderSide(width: 1,color: Colors.white),
        ),
        border: OutlineInputBorder(
            borderRadius:BorderRadius.circular(radius2),
            borderSide: BorderSide(width: 1,)
        ),
        errorBorder: OutlineInputBorder(
            borderRadius:BorderRadius.circular(radius2),
            borderSide: BorderSide(width: 1,color: Colors.white)
        ),
        suffixIcon: this.suffixIcon,
        prefixIcon: this.prefixIcon,
        hintText: this.hintTxt,


        //    fillColor: AppConstants.cardBgColor,

      ),


    ):DropdownButtonHideUnderline(

      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          new InputDecorator(
            decoration: InputDecoration(

              focusedBorder: OutlineInputBorder(
                borderRadius:BorderRadius.circular(radius2),
                borderSide: BorderSide(width: 1,color: Colors.white),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius:BorderRadius.circular(radius2),
                borderSide: BorderSide(width: 1,color: Colors.orange),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius:BorderRadius.circular(radius2),
                borderSide: BorderSide(width: 1,color: Colors.white),
              ),
              border: OutlineInputBorder(
                  borderRadius:BorderRadius.circular(radius2),
                  borderSide: BorderSide(width: 1,)
              ),
              errorBorder: OutlineInputBorder(
                  borderRadius:BorderRadius.circular(radius2),
                  borderSide: BorderSide(width: 1,color: Colors.white)
              ),

              filled: true,
              fillColor: this.fillColor,
              hintText: 'Choose Country',

            ),

            child: new DropdownButton<String>(

              isDense: true,
              onChanged: (va) {
                print('value change');



              },
              items: [

                DropdownMenuItem<String>(
                  value: "Private Limited Company,",
                  child: Text("Private Limited Company"),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }



}