
import 'package:eccount/shared/app_colors.dart';
import 'package:flutter/material.dart';


class StyledButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final EdgeInsetsGeometry padding;
  StyledButton(this.text,this.onPressed,this.padding);
  @override
  Widget build(BuildContext context) {
    return TextButton(
      clipBehavior: Clip.none,
      onPressed: onPressed,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(primaryColor),
        overlayColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered))
              return primaryColorDark;
            if (states.contains(MaterialState.focused) ||
                states.contains(MaterialState.pressed))
              return primaryColorDarkPressed;
            return primaryColor;
          },
        ),
        // Shape sets the border radius from default 3 to 0.
        shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.focused) ||
                states.contains(MaterialState.pressed))
              return RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.all(Radius.circular(5)));
            return RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)));
          },
        ),
        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(padding),
        // Side adds pressed highlight outline.
//                            side: MaterialStateProperty.resolveWith<BorderSide>(
//                                    (Set<MaterialState> states) {
//                                  if (states.contains(MaterialState.focused) ||
//                                      states.contains(MaterialState.pressed))
//                                    return BorderSide(
//                                        width: 3, color: buttonPrimaryPressedOutline);
//                                  // Transparent border placeholder as Flutter does not allow
//                                  // negative margins.
//                                  return BorderSide(width: 3, color: Colors.white);
//                                })
      ),
      child: Text(
        text,
        style: TextStyle(color: Colors.white, height: 1).copyWith(
            fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }
}
