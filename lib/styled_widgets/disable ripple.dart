import 'package:flutter/material.dart';

class DisableRipple extends StatelessWidget {
  final dynamic child;

  DisableRipple({@required this.child});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      ),
      child: child,
    );;
  }
}
