import 'package:eccount/shared/app_colors.dart';
import 'package:flutter/material.dart';

class CircledIcon extends StatelessWidget {
  final double size;
  final IconData imagePath;
  CircledIcon(this.imagePath,this.size);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: primaryLightest,
      ),
      child: Align(
        alignment: Alignment.center,
        child: Icon(
          imagePath,
          size: size*0.5,
          color: primaryColor,
        ),
      ),
    );
  }
}
