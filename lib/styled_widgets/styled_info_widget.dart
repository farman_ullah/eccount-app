import 'package:eccount/shared/show.dart';
import 'package:flutter/material.dart';


class StyledInfoWidget extends StatelessWidget {
  final Widget child;
  final String text;
  StyledInfoWidget({this.text,this.child});
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Expanded(child: child),
      GestureDetector(
        onTap: (){
          Show.showBottomSheet(text);
        },
          child: Icon(Icons.info,size: 34,color: Colors.black,))
    ],);
  }
}
