import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class StyledTextFormField extends StatelessWidget {
  final String hintText;
  final String labelText;
  final TextEditingController controller;
  final Color backGroundColor;
  final bool obscure;
  final List<TextInputFormatter> inputFormatter;
  final Function onChanged;
  final TextStyle hintStyle;
  final BorderRadiusGeometry borderRadius;
  final TextStyle labelStyle;
  final TextInputType inputType;
  final Function validate;
  final EdgeInsets contentPadding;
  final Color borderColor;
  final String initialValue;
  final Function onSaved;
  final bool readOnly;
  final int maxLines;
  final Function onTapped;
  final Function onEditingComplete;
  final Widget suffix;
  StyledTextFormField({this.onEditingComplete,this.maxLines,this.onTapped,this.readOnly,this.suffix,this.obscure,this.contentPadding,this.borderColor,this.borderRadius,this.hintStyle,this.labelStyle,this.onChanged,this.controller,this.labelText,this.hintText,this.backGroundColor,this.validate,this.onSaved,this.inputType,this.initialValue,this.inputFormatter});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: maxLines??1,
      readOnly: readOnly??false,
      controller: controller,
      onTap: onTapped,
      keyboardType: inputType,
      onSaved: onSaved,
      validator: validate,
      initialValue: initialValue,
      cursorColor: primaryColor,
      obscureText: obscure??false,
      inputFormatters: inputFormatter,
      onEditingComplete:onEditingComplete,
      onChanged: onChanged,
      style: TextStyle(textBaseline: TextBaseline.alphabetic,height: 1.2),
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
        contentPadding: contentPadding??EdgeInsets.symmetric(horizontal: 15,vertical: 20),
        filled: backGroundColor!=null?true:false,
        isDense: true,
        fillColor: backGroundColor,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        hintText: hintText,
        hintStyle: hintStyle,
        labelText: labelText,
        suffixIcon: suffix,

//        suffix: suffix,
        labelStyle: labelStyle,
        focusedErrorBorder: OutlineInputBorder(borderRadius: borderRadius??BorderRadius.all(Radius.zero),borderSide: BorderSide(color:Colors.grey)),
        enabledBorder: OutlineInputBorder(borderRadius: borderRadius??BorderRadius.all(Radius.zero,),borderSide: BorderSide(color: Colors.grey[500])),
        focusedBorder: OutlineInputBorder(borderRadius: borderRadius??BorderRadius.all(Radius.zero),borderSide: BorderSide(color: borderColor??Colors.grey)),
        errorBorder: OutlineInputBorder(borderRadius: borderRadius??BorderRadius.all(Radius.zero),borderSide: BorderSide(color:Colors.red)),
      ),
    );
  }
}
