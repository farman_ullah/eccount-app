

import 'package:flutter/material.dart';

class GradientButton extends StatelessWidget {


  
  final  Color backgroundColor;
  final Color textColor;
  final String buttonText;
  final VoidCallback onPress;
  final bool isTransparent;
  final double borderRadius;
  Widget child;
  GradientButton({Key key, this.child,this.backgroundColor,this.textColor=Colors.white, this.buttonText, this.onPress, this.isTransparent=false,this.borderRadius=10}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ElevatedButton(
      onPressed: onPress,
      style: ElevatedButton.styleFrom(primary: Colors.transparent
      ,
       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderRadius))
      ),
      child: Center(child:this.child?? Text("${this.buttonText}", style: TextStyle(color: this.textColor,fontSize: 15))),
    );



    // return   TextButton(
    //   style: TextButton.styleFrom(
    //
    //
    //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    //     backgroundColor: this.backgroundColor??Colors.blue.shade700,
    //     //padding: const EdgeInsets.all(16.0),
    //     primary: Colors.white,
    //     //    textStyle: const TextStyle(fontSize: 20),
    //   ),
    //   onPressed:onPress,
    //   child: Center(child: Text("${this.buttonText}", style: TextStyle(color: this.textColor,fontSize: 15))),
    // );


  }
}
