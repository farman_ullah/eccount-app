import 'package:animations/animations.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/styles.dart';
import 'package:flutter/material.dart';

class StyledDashboardCard extends StatelessWidget {
  final int index;
  final String title;
  final String subTitle;
  final String icon;
  final Widget openedWidget;
  StyledDashboardCard({this.openedWidget,this.title,this.index,this.icon,this.subTitle});
//  final String title;
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
        clipBehavior: Clip.none,
        closedColor: Colors.white,
        closedElevation: 0,
        closedShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        closedBuilder: (_,__){
      return Card(
        margin: EdgeInsets.zero,
        elevation: 12,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: [
                      ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: SizedBox(
                              child: Image.asset("assets/$index.png",scale: 2,))),
//                    Positioned(
//                        top: 0,
//                        bottom: 0,
//                        right: -25,
//                        child: Center(
//                          child: Container(
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.circular(50) ,
//                                  gradient: LinearGradient(colors: [Colors.purpleAccent,Colors.deepPurple],begin: Alignment.topCenter)
//                              ),
//                              width: 50,
//                              height: 50,
//                              child: ClipRRect(
//                                  borderRadius: BorderRadius.circular(50),
//                                  child: Image.asset(icon,fit: BoxFit.fill,))),
//                        ))
                    ],)),
              Expanded(
                flex: 3,
                child: Padding(
                  padding: EdgeInsets.only(left: 15,right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(title,style: TextStyle(color: Colors.black,fontSize: FontSizes.s24,fontWeight: FontWeight.w900),),
                      Container(width: 50,height: 1,color: Colors.deepPurple,margin: EdgeInsets.symmetric(vertical: 15),),
                      Row(children: [
                        Flexible(child: AutoSizeText(subTitle,style: TextStyle(color: Colors.grey,fontSize: FontSizes.s16),)),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            margin: EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(color: Colors.deepPurpleAccent,borderRadius: BorderRadius.circular(50)),
                            width: 40,height: 40,child: Icon(Icons.arrow_forward_ios,color: Colors.white,),),
                        )
                      ],)

                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }, openBuilder: (_,__){
      return openedWidget;
    });
  }
}
