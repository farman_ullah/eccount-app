
import 'package:eccount/shared/styles.dart';
import 'package:flutter/material.dart';

import '../gradient_button.dart';
class AlignButtons extends StatelessWidget {

  Function btn1Press1;
  Function btnPress2;
  String btn1Text;
  String btn2Text;
   AlignButtons({Key key,this.btn1Text,this.btn2Text,this.btn1Press1,this.btnPress2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Align(
      alignment: Alignment.center,
      child: Container(width: 200,
       
        child: Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [


              Container(

                  width:150,

                  height: 30,


                  decoration: BoxDecoration(

                      borderRadius: BorderRadius.circular(10),

                      gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                  child: GradientButton(buttonText:this.btn1Text,onPress: btn1Press1,)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,

                children: [

                  Flexible(
                    child: TextButton(onPressed: (){}, child: Row(children: [
                      Icon(Icons.arrow_back_ios_outlined,),


                      Flexible(child: Text("BACK",style: TextStyles.body2.copyWith(color: Colors.grey),)),

                    ],)),
                  ),

                  Container(

                      width:150,

                      height: 30,


                      decoration: BoxDecoration(

                          borderRadius: BorderRadius.circular(10),

                          gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple])),

                      child: GradientButton(



                        buttonText: btn2Text,onPress: btnPress2,)),
                ],
              ),


            ],),
        ),
      ),
    );
  }
}
