import 'package:eccount/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class StepCircle extends StatelessWidget {
  final String value;
  Widget child;
   StepCircle({Key key,this.value,this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
 return   Stack(
      alignment: Alignment.center,
      children: [
        Container(

          width: Get.width*0.1,
          height: Get.height*0.1,
          decoration: BoxDecoration(shape: BoxShape.circle,  gradient: LinearGradient(colors: [Colors.deepPurpleAccent, Colors.purple]),),




        ),
    this.child??    Text(this.value,style: TextStyles.h2.copyWith(color: Colors.white),),
      ],
    );
  }
}
