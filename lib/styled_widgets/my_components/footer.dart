
import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
class Footer extends StatelessWidget {
  final TextStyle textStyle;
  const Footer({Key key,this.textStyle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
 return  Container(
      decoration: BoxDecoration(

          gradient: LinearGradient(colors: [
            Colors.deepPurpleAccent,
            Colors.purple
          ])),

      width:   ResponsiveWrapper.of(context).screenWidth,height: 50,

      child: Padding(
        padding:  EdgeInsets.only(left:ResponsiveWrapper.of(context).screenWidth/4*0.1),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [


            Text("Eccount",style: textStyle,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [

                Text("SUPPORT@ECCOUNT.IE",style: textStyle),
                SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),
                Text("TERMS OF SERVICES",style: textStyle),
                SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),
                Text("PRIVACY",style: textStyle),
                SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),
                Text("COOKIES",style: textStyle),
                SizedBox(width:ResponsiveWrapper.of(context).screenWidth/6*0.1 ,),

              ],)
          ],),
      ),

    );
  }
}
