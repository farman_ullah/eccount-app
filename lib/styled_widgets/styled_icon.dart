import 'package:auto_size_text/auto_size_text.dart';
import 'package:eccount/shared/app_colors.dart';
import 'package:eccount/shared/styles.dart';
import 'package:flutter/material.dart';

class StyledIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Image.asset("assets/logo.png"),
        AutoSizeText("ccount",style: TextStyle(foreground: Paint()..shader = iconTextGradient,fontSize: FontSizes.s18),)
      ],
    );
  }
}
