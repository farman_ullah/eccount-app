
import 'package:eccount/ui/auth/auth_view.dart';
import 'package:eccount/ui/auth/auth_viewmodel.dart';
import 'package:eccount/ui/dashboard/dashboard_view.dart';
import 'package:eccount/ui/dashboard/dashboard_viewmodel.dart';
import 'package:eccount/ui/onboarding/onboarding_view.dart';
import 'package:eccount/ui/order/orderlist/orderlist_controller.dart';
import 'package:eccount/ui/order/orderlist/orderlist_view.dart';
import 'package:eccount/ui/profileview/profile_controller.dart';
import 'package:eccount/ui/profileview/profile_view.dart';
import 'package:eccount/ui/web_specific/auth/login_view.dart';
import 'package:eccount/ui/web_specific/auth/signup_view.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_controller.dart';
import 'package:eccount/ui/web_specific/dashboard/dashboard_view.dart';
import 'package:eccount/ui/web_specific/homepage/homepage.dart';
import 'package:eccount/ui/web_specific/homepage/homepage_controller.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'ui/splash_screen/splashscreen_view.dart';

List<GetPage> pages=[
  GetPage(name: '/splash', page: ()=>SplashScreen()),
  GetPage(name: '/onboarding', page: ()=>OnboardingView()),
  GetPage(name: "/auth", page: ()=>AuthView(),binding: BindingsBuilder((){
    Get.put(AuthViewModel());
  })),
  GetPage(name: '/dashboard', page: ()=>kIsWeb?WebDashboardView():DashBoardView(),binding: BindingsBuilder((){
    if(kIsWeb){
      Get.put(WebDashboardController());
    }
    Get.put(DashboardViewModel());
  })),
  GetPage(name: '/profile', page: ()=>ProfileView(),binding: BindingsBuilder((){
    Get.put(ProfileController());
  })),
  GetPage(name: '/orders', page: ()=>OrderListView()),
  GetPage(name: '/home', page: ()=>HomePage(),binding: BindingsBuilder((){
    Get.put(HomePageController());
  })),
  GetPage(name: '/login', page: ()=>WebLoginView(),binding: BindingsBuilder((){
    Get.put(AuthViewModel());
  })),
  GetPage(name: '/signup', page: ()=>WebSignUpView()),
];