

const String country="country";
const String error='error';
const String unknown_error="unknown_error";

//onboarding strings
//TODO

//auth screen strings
const String app_login_text="login_textc";
const String login="login";
const String login_message='login_message';
const String mobile_number="mobilenumber";
const String app_register_text="register_text";
const String register_message='register_message';
const String register="register";
const String signup="signup";
const String enter_name="enter_name";
const String name='name';
const String email="email";
const String enter_email="enter_email";
const String govt_id="govt_id";
const String id_card_entry='id_card_entry';
const String password='password';
const String create_password='create_password';
const String confirm_password='confirm_password';
const String confirm_password_msg="confirm_message_msg";
const String signup_error="signup_error";
const String enter_pass="enter_pass";
const String forget_pass='forget_pass';
const String login_with="login_with";
//textfield errors

const String name_empty_error="name_empty_error";
const String email_empty_error="email_empty_error";
const String email_invalid_error="email_invalid_error";
const String govtid_empty_error='govtid_empty_error';
const String pass_empty_error="pass_empty_error";
const String pass_length_error="pass_length_error";
const String pass_confirm_error="pass_confirm_error";
const String pass_nomatch_error="pass_nomatch_error";