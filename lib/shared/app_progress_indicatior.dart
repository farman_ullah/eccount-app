import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class AppProgressIndication extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height*0.3,
      width: Get.height*0.3,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
//          color: Colors.black,
      borderRadius: BorderRadius.circular(10)),
      child: Lottie.asset('assets/lottie/loading.json'),
    );
//    Platform.isAndroid?CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Get.theme.primaryColor),):CupertinoActivityIndicator()
  }
  
}
