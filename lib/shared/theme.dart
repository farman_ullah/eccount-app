

import 'package:eccount/shared/app_colors.dart';
import 'package:flutter/material.dart';

class AppThemes {
  static ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    backgroundColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    primaryColorLight: Colors.grey[400],
    primaryColorDark: Colors.black,
    canvasColor: Colors.white,
    appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(color: Colors.black)
    ),
    primaryColor: primaryColor,
    accentColor: Colors.grey[400],
    hintColor: Colors.grey,
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: primaryColor,
      selectionHandleColor: primaryColor,
    ),
    inputDecorationTheme: InputDecorationTheme(
      border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: primaryColor)),
    ),
    primarySwatch: Colors.purple,
    iconTheme: IconThemeData(color: Colors.white),
    buttonColor: Colors.black,
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.focused) ||
                states.contains(MaterialState.pressed) || states.contains(MaterialState.hovered))
              return Colors.black;
            return Colors.black54;
          },
        ),
      )
    ),
    textTheme: TextTheme(
      headline6: TextStyle(
        color: Colors.white,
      ),
    ),
  );

  static ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.black12,
    accentColor: Colors.deepOrangeAccent,
    scaffoldBackgroundColor: Colors.black,
    canvasColor: Colors.black,
    backgroundColor: Colors.black,
    hintColor: Colors.deepOrangeAccent,
    bottomAppBarColor: Colors.grey,
    textTheme: TextTheme(
      headline6: TextStyle(
        color: Colors.white,
      ),
    ),
  );
}