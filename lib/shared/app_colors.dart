import 'package:flutter/material.dart';

const Color primaryLightest=Color(0xffe6daf1);
const Color primaryLight= Color(0xffa77dcf);
const Color primaryColor=Color(0xff8B53C0);
const Color primaryColorDark=Color(0xff753ea8);
const Color primaryColorDarkPressed = Color(0xff5b3082);

const Color subTitleColor=Color(0xff8892a9);

final Shader iconTextGradient = LinearGradient(
  colors: <Color>[Color.fromRGBO(0,202,234,1), Color.fromRGBO(105,57,178,1)],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));