enum RegistrationType{
  CompanyFormation,
  BusinessNameRegistration,
  VatRegistration
}


enum OrderStatus{
  UnPaid,
  PaymentFailed,
  UnderProcess,
  IncorrectInformation,
  Completed
}

getOrderStatusToInt(OrderStatus status){
  switch(status){
    case OrderStatus.UnPaid:
      return 0;
    case OrderStatus.PaymentFailed:
      return 1;
    case OrderStatus.UnderProcess:
      return 2;
    case OrderStatus.IncorrectInformation:
      return 3;
    case OrderStatus.Completed:
      return 4;
  }
}

getOrderStatusStringFromInt(int status){
  switch(status){
    case 0:
      return "PAYMENT DUE";
    case 1:
      return "Payment Failed.Please contact admin.";
    case 2:
      return "Order is Underprocess";
    case 3:
      return "Please enter the correct info as required by agent";
    case 4:
      return "Order Completed";
  }
}

getRegistrationTypeToInt(RegistrationType type){
  switch(type){
    case RegistrationType.CompanyFormation:
      return 0;
    case RegistrationType.BusinessNameRegistration:
      return 1;
    case RegistrationType.VatRegistration:
      return 2;
  }
}

getRegistrationTypeStringFromInt(int number){
  switch(number){
    case 0:
      return "Company Formation";
    case 1:
      return "Business Name Registration";
    case 2:
      return "Vat Registration";
  }
}


