import 'package:auto_size_text/auto_size_text.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:eccount/shared/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'app_progress_indicatior.dart';

class Show{
  static void showSnackBar({@required String title,@required String message,int duration}){
    Get.snackbar(title, message,snackPosition: SnackPosition.BOTTOM,duration: Duration(seconds: duration??2));
  }
  static void showErrorSnackBar({@required String title,@required String error,int duration}){
    if(kIsWeb){
      BotToast.showCustomText(toastBuilder: (cancelFunc)=>Container(
          padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(25),color: Colors.red),
          child: AutoSizeText(error,style: TextStyles.h3.copyWith(color: Colors.white),)),duration: Duration(seconds: 3));
    }
    else {
      Get.showSnackbar(GetBar(
        title: title,
        message: error,
        backgroundColor: Colors.red,
        isDismissible: true,
        duration: Duration(seconds: duration ?? 2),
      ));
    }
  }
  static void showBottomSheet(String text){
    Get.bottomSheet(Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(15) ,topRight: Radius.circular(15))
      ),
      padding: EdgeInsets.all(15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: IconButton(icon: Icon(Icons.close,color: Colors.black,), onPressed:(){
              Get.back();
            }),
          ),
          Flexible(child: AutoSizeText(text,style: TextStyles.body1,)),
          SizedBox(height:Get.bottomBarHeight/2,)
        ],
      ),
    ));
  }
  static void showLoader(){
    Get.dialog(
        Center(child: AppProgressIndication(),),barrierDismissible: false,barrierColor: Colors.black87);
  }
}