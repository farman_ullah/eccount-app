
import 'package:eccount/shared/app_strings.dart';
import 'package:get/get.dart';

class AppTranslations extends Translations{
  @override
  // TODO: implement keys
  Map<String, Map<String, String>> get keys =>{
    'en_US':{
      app_login_text:'Log in to Eccounts',
      login_message:'Enter your mobile number to Log in.',
      app_register_text:'Register',
      register_message:'Enter your mobile number to Register.',
      mobile_number:'Mobile Number',
      country:'Country',
      login:'Log In',
      register:'Register',
      error:"Error",
      signup:"Sign Up",
      enter_name:"Enter your name",
      name:'Name',
      email:'E-mail',
      enter_email:'Enter your e-mail',
      govt_id:'Govt id',
      id_card_entry:'Enter your identity card number',
      create_password:'Create a password',
      password:"Password",
      confirm_password:'Confirm Password',
      confirm_password_msg:'Confirm your password',
      name_empty_error:'Name is required',
      email_empty_error:'Email is required',
      email_invalid_error:"Enter a valid email",
      govtid_empty_error:"Govt id is required",
      pass_empty_error:'Password is required',
      pass_length_error:'Password cannot be less than 8 characters',
      pass_confirm_error:"Confirm your password",
      pass_nomatch_error:"Password doesn't match",
      signup_error:"Sign Up Error",
      enter_pass:"Enter password",
      forget_pass:"Forget Password?",
      login_with:"Login with"
    }
  };
}